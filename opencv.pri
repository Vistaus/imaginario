OPENCV_LIBS_ADDED=0
defined(OPENCV_LIBS, var) {
    message("Using OpenCV libs from OPENCV_LIBS variable")
    CONFIG += opencv
    OPENCV_LIBS_ADDED=1
    !isEmpty(OPENCV_LIBDIR): LIBS += -L$${OPENCV_LIBDIR}
    LIBS += $${OPENCV_LIBS}
    INCLUDEPATH += $${OPENCV_INCLUDEPATH}
} else:packagesExist(opencv) {
    message("Using OpenCV from pkg-config")
    CONFIG += opencv
    OPENCV_LIBS_ADDED=1
    PKGCONFIG += opencv
} else:exists("/usr/include/opencv2/objdetect/objdetect.hpp") {
    message("Using OpenCV2 from system include file")
    CONFIG += opencv
    OPENCV_LIBDIR="$$[QT_HOST_LIBS]"
}


