# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-02-04 19:01+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: ../src/database.cpp:396
msgid "Uncategorized"
msgstr ""

#: ../src/database.cpp:397
msgid "People"
msgstr ""

#: ../src/database.cpp:398
msgid "Places"
msgstr ""

#: ../src/database.cpp:399
msgid "Events"
msgstr ""

#: ../src/database.cpp:400
msgid "Imported tags"
msgstr ""

#: ../src/folder_model.cpp:92
#, qt-format
msgid "%1 MB Volume"
msgstr ""

#: ../src/folder_model.cpp:97
#, qt-format
msgid "%1 GB Volume"
msgstr ""

#: ../src/folder_model.cpp:100
#, qt-format
msgid "%1 TB Volume"
msgstr ""

#: ../src/desktop/qml/Actions.qml:17
msgid "Copy"
msgstr ""

#: ../src/desktop/qml/Actions.qml:24
msgid "Create new tag…"
msgstr ""

#: ../src/desktop/qml/Actions.qml:31
msgid "Delete from catalog"
msgstr ""

#: ../src/desktop/qml/Actions.qml:40
msgid "Delete from drive"
msgstr ""

#: ../src/desktop/qml/Actions.qml:51
msgid "Edit tag…"
msgstr ""

#: ../src/desktop/qml/Actions.qml:59
#: ../src/desktop/qml/TagDeleteConfirmation.qml:34
#: ../src/ubuntu-touch/qml/TagActions.qml:22
#: ../src/ubuntu-touch/qml/TagDeleteConfirmation.qml:29
msgid "Delete tag"
msgid_plural "Delete tags"
msgstr[0] ""
msgstr[1] ""

#: ../src/desktop/qml/Actions.qml:66
msgid "Import..."
msgstr ""

#: ../src/desktop/qml/Actions.qml:72
msgid "Refresh thumbnail"
msgstr ""

#: ../src/desktop/qml/Actions.qml:78
msgid "Zoom in"
msgstr ""

#: ../src/desktop/qml/Actions.qml:84
msgid "Zoom out"
msgstr ""

#: ../src/desktop/qml/AreaFilterRow.qml:12
msgid "With area tags"
msgstr ""

#: ../src/desktop/qml/DeleteConfirmation.qml:13
#: ../src/desktop/qml/TagDeleteConfirmation.qml:16
msgid "Delete confirmation"
msgstr ""

#: ../src/desktop/qml/DeleteConfirmation.qml:18
#, qt-format
msgid "Delete the %1 selected images from the disk?"
msgstr ""

#: ../src/desktop/qml/DeleteConfirmation.qml:18
#, qt-format
msgid "Remove the %1 selected images from the catalogue?"
msgstr ""

#: ../src/desktop/qml/DeleteConfirmation.qml:24
#: ../src/desktop/qml/FindByDateDialog.qml:65
#: ../src/desktop/qml/FindByRatingDialog.qml:49
#: ../src/desktop/qml/FindByRollDialog.qml:70
#: ../src/desktop/qml/MimeTypesDialog.qml:87
#: ../src/desktop/qml/TagDeleteConfirmation.qml:27
#: ../src/desktop/qml/TagEditDialog.qml:140
#: ../src/desktop/qml/TagIconPicker.qml:64
#: ../src/desktop/qml/VersionConfirmation.qml:39
#: ../src/desktop/qml/WizardDialog.qml:88
#: ../src/ubuntu-touch/qml/DeleteConfirmation.qml:26
#: ../src/ubuntu-touch/qml/TagDeleteConfirmation.qml:24
msgid "Cancel"
msgstr ""

#: ../src/desktop/qml/DeleteConfirmation.qml:31
msgid "Delete photos"
msgstr ""

#: ../src/desktop/qml/DeleteConfirmation.qml:31
msgid "Remove photos"
msgstr ""

#: ../src/desktop/qml/ExternalEditors.qml:79
msgid "MIME types..."
msgstr ""

#: ../src/desktop/qml/ExternalEditors.qml:178
msgid "Add command..."
msgstr ""

#: ../src/desktop/qml/FilterBar.qml:19
msgid "Find:"
msgstr ""

#: ../src/desktop/qml/FindByDateDialog.qml:14
msgid "Find by date"
msgstr ""

#: ../src/desktop/qml/FindByDateDialog.qml:22
msgid "Select period:"
msgstr ""

#: ../src/desktop/qml/FindByDateDialog.qml:38
msgid "Start date:"
msgstr ""

#: ../src/desktop/qml/FindByDateDialog.qml:43
msgid "End date:"
msgstr ""

#: ../src/desktop/qml/FindByDateDialog.qml:72
#: ../src/desktop/qml/FindByRatingDialog.qml:56
#: ../src/desktop/qml/FindByRollDialog.qml:77
#: ../src/desktop/qml/TagTreeView.qml:75
msgid "Find"
msgstr ""

#: ../src/desktop/qml/FindByDateDialog.qml:86
msgid "Custom range"
msgstr ""

#: ../src/desktop/qml/FindByDateDialog.qml:87
msgid "Today"
msgstr ""

#: ../src/desktop/qml/FindByDateDialog.qml:88
msgid "Yesterday"
msgstr ""

#: ../src/desktop/qml/FindByDateDialog.qml:89
msgid "Last 7 days"
msgstr ""

#: ../src/desktop/qml/FindByDateDialog.qml:90
msgid "Last 30 days"
msgstr ""

#: ../src/desktop/qml/FindByDateDialog.qml:91
msgid "Last 90 days"
msgstr ""

#: ../src/desktop/qml/FindByDateDialog.qml:92
msgid "Last 360 days"
msgstr ""

#: ../src/desktop/qml/FindByRatingDialog.qml:14
#: ../src/desktop/qml/FindByRollDialog.qml:15
msgid "Find by rating"
msgstr ""

#: ../src/desktop/qml/FindByRatingDialog.qml:23
msgid "Minimum rating:"
msgstr ""

#: ../src/desktop/qml/FindByRatingDialog.qml:28
msgid "Maximum rating:"
msgstr ""

#: ../src/desktop/qml/FindByRollDialog.qml:25
msgid "View all pictures imported"
msgstr ""

#: ../src/desktop/qml/FindByRollDialog.qml:34
msgid "At"
msgstr ""

#: ../src/desktop/qml/FindByRollDialog.qml:35
msgid "Between"
msgstr ""

#: ../src/desktop/qml/FindByRollDialog.qml:47
msgid "and"
msgstr ""

#: ../src/desktop/qml/FindByRollDialog.qml:62
#, qt-format
msgid "Estimated photo count: %1"
msgstr ""

#: ../src/desktop/qml/FindByRollDialog.qml:92
#, qt-format
msgid "%1 (%2)"
msgstr ""

#: ../src/desktop/qml/ImageInfoTab.qml:35
msgid "Name"
msgstr ""

#: ../src/desktop/qml/ImageInfoTab.qml:51
msgid "Version"
msgstr ""

#: ../src/desktop/qml/ImageInfoTab.qml:64
#: ../src/ubuntu-touch/qml/TitleEditSheet.qml:69
msgid "Title"
msgstr ""

#: ../src/desktop/qml/ImageInfoTab.qml:78
msgid "Date"
msgstr ""

#: ../src/desktop/qml/ImageInfoTab.qml:88
msgid "Size"
msgstr ""

#: ../src/desktop/qml/ImageInfoTab.qml:102 ../src/desktop/qml/imaginario.qml:66
msgid "Rating"
msgstr ""

#: ../src/desktop/qml/ImageInfoTab.qml:137
#, qt-format
msgid "%1x%2"
msgstr ""

#: ../src/desktop/qml/ImageInfoTab.qml:161
#, qt-format
msgid "%1 - %2"
msgstr ""

#: ../src/desktop/qml/ImageInfoTab.qml:169
msgid "No image selected"
msgstr ""

#: ../src/desktop/qml/ImagePanel.qml:71
msgid "Detach version"
msgstr ""

#: ../src/desktop/qml/ImagePanel.qml:110
msgid "Set rating"
msgstr ""

#: ../src/desktop/qml/ImagePanel.qml:151 ../src/ubuntu-touch/qml/TagList.qml:43
msgid "Tags:"
msgstr ""

#: ../src/desktop/qml/imaginario.qml:14
msgid "&Photo"
msgstr ""

#: ../src/desktop/qml/imaginario.qml:17
msgid "Quit"
msgstr ""

#: ../src/desktop/qml/imaginario.qml:24
msgid "&Edit"
msgstr ""

#: ../src/desktop/qml/imaginario.qml:27
#: ../src/desktop/qml/ImportWizardDups.qml:198
msgid "Select all"
msgstr ""

#: ../src/desktop/qml/imaginario.qml:32
msgid "Select none"
msgstr ""

#: ../src/desktop/qml/imaginario.qml:43
msgid "Preferences..."
msgstr ""

#: ../src/desktop/qml/imaginario.qml:50
msgid "&View"
msgstr ""

#: ../src/desktop/qml/imaginario.qml:52
msgid "Fullscreen"
msgstr ""

#: ../src/desktop/qml/imaginario.qml:58
msgid "Thumbnail elements"
msgstr ""

#: ../src/desktop/qml/imaginario.qml:60
msgid "Tags"
msgstr ""

#: ../src/desktop/qml/imaginario.qml:72
msgid "Face/area tags"
msgstr ""

#: ../src/desktop/qml/imaginario.qml:84
msgid "&Find"
msgstr ""

#: ../src/desktop/qml/imaginario.qml:86
msgid "By import roll"
msgstr ""

#: ../src/desktop/qml/imaginario.qml:88
#: ../src/ubuntu-touch/qml/ImporterPage.qml:15
msgid "Last import roll"
msgstr ""

#: ../src/desktop/qml/imaginario.qml:92
msgid "Choose import rolls..."
msgstr ""

#: ../src/desktop/qml/imaginario.qml:99 ../src/desktop/qml/imaginario.qml:113
#: ../src/desktop/qml/imaginario.qml:127
msgid "Clear filter"
msgstr ""

#: ../src/desktop/qml/imaginario.qml:105
msgid "By date"
msgstr ""

#: ../src/desktop/qml/imaginario.qml:107
msgid "Set date filter..."
msgstr ""

#: ../src/desktop/qml/imaginario.qml:119
msgid "By rating"
msgstr ""

#: ../src/desktop/qml/imaginario.qml:121
msgid "Set rating filter..."
msgstr ""

#: ../src/desktop/qml/imaginario.qml:134
msgid "Non geotagged photos"
msgstr ""

#: ../src/desktop/qml/imaginario.qml:138
msgid "Photos with tag areas"
msgstr ""

#: ../src/desktop/qml/imaginario.qml:143
msgid "&Tags"
msgstr ""

#: ../src/desktop/qml/imaginario.qml:201
#: ../src/desktop/qml/LocationFilterRow.qml:12
msgid "Location filter"
msgstr ""

#: ../src/desktop/qml/imaginario.qml:216
msgid "Image information"
msgstr ""

#: ../src/desktop/qml/ImportDelegate.qml:44
msgid "RAW+edit"
msgstr ""

#: ../src/desktop/qml/ImportDialog.qml:14
msgid "Import photos"
msgstr ""

#: ../src/desktop/qml/ImportWizardDups.qml:64
#: ../src/desktop/qml/ImportWizardProgress.qml:24
#, qt-format
msgid "Duplicate items: <b>%1</b>"
msgstr ""

#: ../src/desktop/qml/ImportWizardDups.qml:203
msgid "Unselect all"
msgstr ""

#: ../src/desktop/qml/ImportWizardDups.qml:208
msgid "Select undecided"
msgstr ""

#: ../src/desktop/qml/ImportWizardDups.qml:209
msgid "Select all items which don't have an action set"
msgstr ""

#: ../src/desktop/qml/ImportWizardDups.qml:218
#, qt-format
msgid "Action for the %1 selected items:"
msgstr ""

#: ../src/desktop/qml/ImportWizardDups.qml:225
msgid "&Ignore (don't import)"
msgstr ""

#: ../src/desktop/qml/ImportWizardDups.qml:228
msgid "Items will not be imported"
msgstr ""

#: ../src/desktop/qml/ImportWizardDups.qml:236
msgid "Import as &new"
msgstr ""

#: ../src/desktop/qml/ImportWizardDups.qml:239
msgid "Items will be imported with no relationship"
msgstr ""

#: ../src/desktop/qml/ImportWizardDups.qml:247
msgid "Import &replacing"
msgstr ""

#: ../src/desktop/qml/ImportWizardDups.qml:250
msgid "Items will be imported replacing the existing ones"
msgstr ""

#: ../src/desktop/qml/ImportWizardDups.qml:259
msgid "Import as new &version"
msgstr ""

#: ../src/desktop/qml/ImportWizardDups.qml:262
msgid "Items will be imported as a version"
msgstr ""

#: ../src/desktop/qml/ImportWizardIntro.qml:19
msgid "Import from:"
msgstr ""

#: ../src/desktop/qml/ImportWizardIntro.qml:27
#: ../src/desktop/qml/Preferences.qml:27
msgid "Choose folder..."
msgstr ""

#: ../src/desktop/qml/ImportWizardIntro.qml:51
msgid "Copy files to the Photos folder"
msgstr ""

#: ../src/desktop/qml/ImportWizardIntro.qml:59
msgid "Include subfolders"
msgstr ""

#: ../src/desktop/qml/ImportWizardIntro.qml:67
msgid "Select import source"
msgstr ""

#: ../src/desktop/qml/ImportWizardProgress.qml:17
#, qt-format
msgid "Imported items: <b>%1</b>"
msgstr ""

#: ../src/desktop/qml/ImportWizardProgress.qml:31
#, qt-format
msgid "Failed items: <b>%1</b>"
msgstr ""

#: ../src/desktop/qml/ImportWizardProgress.qml:38
#, qt-format
msgid "Ignored items: <b>%1</b>"
msgstr ""

#: ../src/desktop/qml/ImportWizardProgress.qml:55
#: ../src/desktop/qml/WizardImporterDigikam.qml:32
#: ../src/desktop/qml/WizardImporterFspot.qml:32
#: ../src/desktop/qml/WizardImporterShotwell.qml:32
#, qt-format
msgid "Current status: <b>%1</b>"
msgstr ""

#: ../src/desktop/qml/ImportWizardProgress.qml:66
msgid ""
"Some items appear to be already present in your photo database.\n"
"\n"
"In the next page you'll be able to see them and decide how to proceed."
msgstr ""

#: ../src/desktop/qml/ImportWizardProgress.qml:76
msgid "Importing..."
msgstr ""

#: ../src/desktop/qml/ImportWizardProgress.qml:88
#: ../src/desktop/qml/WizardImporterDigikam.qml:80
#: ../src/desktop/qml/WizardImporterFspot.qml:79
#: ../src/desktop/qml/WizardImporterShotwell.qml:73
msgid "Import completed"
msgstr ""

#: ../src/desktop/qml/ImportWizardView.qml:17
msgid "Found files:"
msgstr ""

#: ../src/desktop/qml/ImportWizardView.qml:55
msgid "Import tags"
msgstr ""

#: ../src/desktop/qml/LocationFilterRow.qml:12
msgid "Non geotagged"
msgstr ""

#: ../src/desktop/qml/LocationsMap.qml:33
#, qt-format
msgid "%1 non geotagged items"
msgstr ""

#: ../src/desktop/qml/LocationsMap.qml:39
msgid "Show"
msgstr ""

#: ../src/desktop/qml/LocationsMap.qml:89
#: ../src/ubuntu-touch/qml/FilterSheet.qml:110
msgid "Apply filter"
msgstr ""

#: ../src/desktop/qml/MimeTypesDialog.qml:11
msgid "Select MIME types"
msgstr ""

#: ../src/desktop/qml/MimeTypesDialog.qml:21
msgid ""
"Enter the MIME types supported by this operation, one at a time, and press "
"the \"Add\" button"
msgstr ""

#: ../src/desktop/qml/MimeTypesDialog.qml:36
msgid "Add"
msgstr ""

#: ../src/desktop/qml/MimeTypesDialog.qml:51
msgid "Selected MIME types:"
msgstr ""

#: ../src/desktop/qml/MimeTypesDialog.qml:77
msgid "No MIME types selected: the action will be available for all file types"
msgstr ""

#: ../src/desktop/qml/MimeTypesDialog.qml:94
#: ../src/desktop/qml/TagEditDialog.qml:147
#: ../src/desktop/qml/TagIconPicker.qml:71
msgid "Ok"
msgstr ""

#: ../src/desktop/qml/Preferences.qml:18
msgid "When importing photos, copy them to:"
msgstr ""

#: ../src/desktop/qml/Preferences.qml:45
msgid "Store tags and descriptions"
msgstr ""

#: ../src/desktop/qml/Preferences.qml:51
msgid "Inside the image files when possible"
msgstr ""

#: ../src/desktop/qml/Preferences.qml:57
msgid "In XMP files next to the images"
msgstr ""

#: ../src/desktop/qml/Preferences.qml:63
msgid "Only in the Imaginario database"
msgstr ""

#: ../src/desktop/qml/Preferences.qml:83
msgid "Select image directory"
msgstr ""

#: ../src/desktop/qml/PreferencesDialog.qml:9
msgid "Preferences"
msgstr ""

#: ../src/desktop/qml/PreferencesDialog.qml:18
msgid "General"
msgstr ""

#: ../src/desktop/qml/PreferencesDialog.qml:27
msgid "External editors"
msgstr ""

#: ../src/desktop/qml/RatingFilterRow.qml:12
msgid "Rated photos"
msgstr ""

#: ../src/desktop/qml/RollFilterRow.qml:12
msgid "Import roll"
msgstr ""

#: ../src/desktop/qml/TagDeleteConfirmation.qml:21
#, qt-format
msgid "Delete tag <b>%1</b>?"
msgstr ""

#: ../src/desktop/qml/TagEditDialog.qml:17
#, qt-format
msgid "Edit tag %1"
msgstr ""

#: ../src/desktop/qml/TagEditDialog.qml:17
msgid "Create new tag"
msgstr ""

#: ../src/desktop/qml/TagEditDialog.qml:31
msgid "Icon:"
msgstr ""

#: ../src/desktop/qml/TagEditDialog.qml:66
msgid "Name:"
msgstr ""

#: ../src/desktop/qml/TagEditDialog.qml:99
#: ../src/ubuntu-touch/qml/TagEditPage.qml:101
#: ../src/ubuntu-touch/qml/TagsEditPage.qml:41
msgid "Parent tag:"
msgstr ""

#: ../src/desktop/qml/TagEditDialog.qml:112
msgid "No parent"
msgstr ""

#: ../src/desktop/qml/TagFilterRow.qml:43
msgid "or"
msgstr ""

#: ../src/desktop/qml/TagIconPicker.qml:14
#, qt-format
msgid "Choose an icon for tag %1"
msgstr ""

#: ../src/desktop/qml/TagIconPicker.qml:18
msgid "From a tagged photo:"
msgstr ""

#: ../src/desktop/qml/TagTreeView.qml:87
msgid "Find with"
msgstr ""

#: ../src/desktop/qml/VersionConfirmation.qml:14
#: ../src/desktop/qml/VersionConfirmation.qml:46
#: ../src/ubuntu-touch/qml/ImageDisplay.qml:182
msgid "Make version"
msgstr ""

#: ../src/desktop/qml/VersionConfirmation.qml:33
msgid "This makes the photos appear as a single item in the library."
msgstr ""

#: ../src/desktop/qml/VersionSelector.qml:24
msgid "Original"
msgstr ""

#: ../src/desktop/qml/WelcomeDialog.qml:9
#: ../src/desktop/qml/WelcomeWizardIntro.qml:17
msgid "Welcome to Imaginario"
msgstr ""

#: ../src/desktop/qml/WelcomeWizardFinished.qml:17
msgid "Setup complete"
msgstr ""

#: ../src/desktop/qml/WelcomeWizardFinished.qml:24
msgid ""
"Imaginario has been configured. You can close this window and start using "
"the application."
msgstr ""

#: ../src/desktop/qml/WelcomeWizardIntro.qml:24
msgid ""
"Looks like this is the first time you use Imaginario.<br><br>Please take a "
"few seconds to configure the application according to your needs and (if "
"available) importing your existing photo database into Imaginario."
msgstr ""

#: ../src/desktop/qml/WizardDialog.qml:24
msgid "Next"
msgstr ""

#: ../src/desktop/qml/WizardDialog.qml:96
msgid "Previous"
msgstr ""

#: ../src/desktop/qml/WizardDialog.qml:114
msgid "Close"
msgstr ""

#: ../src/desktop/qml/WizardImporterDigikam.qml:15
#, qt-format
msgid "Digikam version: <b>%1</b>"
msgstr ""

#: ../src/desktop/qml/WizardImporterDigikam.qml:22
#, qt-format
msgid "Digikam database version: <b>%1</b>"
msgstr ""

#: ../src/desktop/qml/WizardImporterDigikam.qml:39
#: ../src/desktop/qml/WizardImporterFspot.qml:39
#: ../src/desktop/qml/WizardImporterShotwell.qml:39
msgid "Start import"
msgstr ""

#: ../src/desktop/qml/WizardImporterDigikam.qml:55
#: ../src/desktop/qml/WizardImporterFspot.qml:55
#: ../src/desktop/qml/WizardImporterShotwell.qml:55
msgid "Ready"
msgstr ""

#: ../src/desktop/qml/WizardImporterDigikam.qml:61
#: ../src/desktop/qml/WizardImporterFspot.qml:61
#: ../src/desktop/qml/WizardImporterShotwell.qml:61
msgid "Importing tags"
msgstr ""

#: ../src/desktop/qml/WizardImporterDigikam.qml:68
msgid "Importing albums as photo rolls"
msgstr ""

#: ../src/desktop/qml/WizardImporterDigikam.qml:74
#: ../src/desktop/qml/WizardImporterFspot.qml:73
#: ../src/desktop/qml/WizardImporterShotwell.qml:67
msgid "Importing photos"
msgstr ""

#: ../src/desktop/qml/WizardImporterDigikam.qml:86
#: ../src/desktop/qml/WizardImporterFspot.qml:85
#: ../src/desktop/qml/WizardImporterShotwell.qml:79
msgid "Import failed"
msgstr ""

#: ../src/desktop/qml/WizardImporterFspot.qml:15
#, qt-format
msgid "F-Spot version: <b>%1</b>"
msgstr ""

#: ../src/desktop/qml/WizardImporterFspot.qml:22
#, qt-format
msgid "F-Spot database version: <b>%1</b>"
msgstr ""

#: ../src/desktop/qml/WizardImporterFspot.qml:68
msgid "Importing rolls"
msgstr ""

#: ../src/desktop/qml/WizardImporterSelect.qml:14
msgid ""
"Imaginario can import your photo database from a program you used previously."
msgstr ""

#: ../src/desktop/qml/WizardImporterSelect.qml:36
msgid "Import photos from Digikam"
msgstr ""

#: ../src/desktop/qml/WizardImporterSelect.qml:41
#, qt-format
msgid ""
"It appears you have been using Digikam before. There are <b>%1 photos</b> in "
"your Digikam database."
msgstr ""

#: ../src/desktop/qml/WizardImporterSelect.qml:54
msgid "Import photos from F-Spot"
msgstr ""

#: ../src/desktop/qml/WizardImporterSelect.qml:59
#, qt-format
msgid ""
"It appears you have been using F-Spot before. There are <b>%1 photos</b> in "
"your F-Spot database."
msgstr ""

#: ../src/desktop/qml/WizardImporterSelect.qml:72
msgid "Import photos from Shotwell"
msgstr ""

#: ../src/desktop/qml/WizardImporterSelect.qml:77
#, qt-format
msgid ""
"It appears you have been using Shotwell before. There are <b>%1 photos</b> "
"in your Shotwell database."
msgstr ""

#: ../src/desktop/qml/WizardImporterSelect.qml:85
msgid "Don't import photos from another program."
msgstr ""

#: ../src/desktop/qml/WizardImporterShotwell.qml:15
#, qt-format
msgid "Shotwell version: <b>%1</b>"
msgstr ""

#: ../src/desktop/qml/WizardImporterShotwell.qml:22
#, qt-format
msgid "Shotwell database version: <b>%1</b>"
msgstr ""

#: ../src/ubuntu-touch/qml/AreaChooser.qml:17
msgid "Open map"
msgstr ""

#: ../src/ubuntu-touch/qml/AreaChooserPage.qml:16
msgid "Define area"
msgstr ""

#: ../src/ubuntu-touch/qml/AreaChooserPage.qml:20
msgid "Close map"
msgstr ""

#: ../src/ubuntu-touch/qml/AreaChooserPage.qml:25
msgid "Confirm"
msgstr ""

#: ../src/ubuntu-touch/qml/AutoTagPage.qml:19
msgid "Auto-tag based on source"
msgstr ""

#: ../src/ubuntu-touch/qml/DateChooser.qml:20
msgid "Pick a date"
msgstr ""

#: ../src/ubuntu-touch/qml/DateChooser.qml:32
msgid "Enter a date"
msgstr ""

#: ../src/ubuntu-touch/qml/DateChooser.qml:43
msgid "Clear date"
msgstr ""

#: ../src/ubuntu-touch/qml/DateChooser.qml:49
#: ../src/ubuntu-touch/qml/DuplicateSheet.qml:89
#: ../src/ubuntu-touch/qml/TagEditPage.qml:139
#: ../src/ubuntu-touch/qml/TagEditSheet.qml:60
#: ../src/ubuntu-touch/qml/TagsEditPage.qml:71
msgid "Done"
msgstr ""

#: ../src/ubuntu-touch/qml/DeleteAction.qml:9
msgid "Delete"
msgstr ""

#: ../src/ubuntu-touch/qml/DeleteConfirmation.qml:15
msgid "File deletion"
msgstr ""

#: ../src/ubuntu-touch/qml/DeleteConfirmation.qml:16
msgid "Confirm deletion of the selected file?"
msgid_plural "Confirm deletion of the selected files?"
msgstr[0] ""
msgstr[1] ""

#: ../src/ubuntu-touch/qml/DeleteConfirmation.qml:31
msgid "Delete file"
msgid_plural "Delete files"
msgstr[0] ""
msgstr[1] ""

#: ../src/ubuntu-touch/qml/DuplicateSheet.qml:18
msgid "Duplicate item"
msgstr ""

#: ../src/ubuntu-touch/qml/DuplicateSheet.qml:33
msgid "This image might be a duplicate of one of the images below:"
msgstr ""

#: ../src/ubuntu-touch/qml/DuplicateSheet.qml:73
msgid "Don't import item"
msgstr ""

#: ../src/ubuntu-touch/qml/DuplicateSheet.qml:74
msgid "Import as a new item"
msgstr ""

#: ../src/ubuntu-touch/qml/DuplicateSheet.qml:75
msgid "Import as a version"
msgstr ""

#: ../src/ubuntu-touch/qml/DuplicateSheet.qml:76
msgid "Import replacing"
msgstr ""

#: ../src/ubuntu-touch/qml/DuplicateSheet.qml:120
msgid "This photo won't be imported"
msgstr ""

#: ../src/ubuntu-touch/qml/DuplicateSheet.qml:128
msgid "The photo is not a duplicate; it will be imported as a new item"
msgstr ""

#: ../src/ubuntu-touch/qml/DuplicateSheet.qml:136
msgid "The photo will be imported as a version of the above selected photo"
msgstr ""

#: ../src/ubuntu-touch/qml/DuplicateSheet.qml:144
msgid ""
"The photo will be imported and will replace the photo selected above, which "
"will be removed from the archive"
msgstr ""

#: ../src/ubuntu-touch/qml/EditTagsAction.qml:8
#: ../src/ubuntu-touch/qml/TagEditSheet.qml:21
#: ../src/ubuntu-touch/qml/TagsEditPage.qml:15
msgid "Edit tags"
msgstr ""

#: ../src/ubuntu-touch/qml/ExporterPage.qml:15
msgid "Export images"
msgstr ""

#: ../src/ubuntu-touch/qml/ExportPage.qml:16
msgid "Share items with"
msgstr ""

#: ../src/ubuntu-touch/qml/FaceDetect.qml:19
msgid ""
"Press and hold to tag someone.<br/><br/>Open the menu again for the option "
"of automatic detection of faces."
msgstr ""

#: ../src/ubuntu-touch/qml/FaceDetect.qml:51
msgid "No faces were found"
msgstr ""

#: ../src/ubuntu-touch/qml/FaceTagPopup.qml:14
msgid "Enable face tagging"
msgstr ""

#: ../src/ubuntu-touch/qml/FaceTagPopup.qml:20
msgid "Detect faces"
msgstr ""

#: ../src/ubuntu-touch/qml/FaceTagPopup.qml:26
msgid "Disable face tagging"
msgstr ""

#: ../src/ubuntu-touch/qml/FilterSheet.qml:13
#, qt-format
msgid "Filter images (%1)"
msgstr ""

#: ../src/ubuntu-touch/qml/FilterSheet.qml:43
msgid "Filter by tags"
msgstr ""

#: ../src/ubuntu-touch/qml/FilterSheet.qml:63
msgid "Filter by date"
msgstr ""

#: ../src/ubuntu-touch/qml/FilterSheet.qml:66
msgid "Taken after"
msgstr ""

#: ../src/ubuntu-touch/qml/FilterSheet.qml:75
msgid "Taken before"
msgstr ""

#: ../src/ubuntu-touch/qml/FilterSheet.qml:85
msgid "Filter by location"
msgstr ""

#: ../src/ubuntu-touch/qml/FilterSheet.qml:88
msgid "Area selected"
msgstr ""

#: ../src/ubuntu-touch/qml/FilterSheet.qml:88
msgid "Select area"
msgstr ""

#: ../src/ubuntu-touch/qml/FilterSheet.qml:98
msgid "Show hidden tags"
msgstr ""

#: ../src/ubuntu-touch/qml/FilterSheet.qml:122
msgid "Search results"
msgstr ""

#: ../src/ubuntu-touch/qml/GeoLocationSheet.qml:15
msgid "Edit location"
msgstr ""

#: ../src/ubuntu-touch/qml/GeoLocationSheet.qml:28
msgid "Set location"
msgstr ""

#: ../src/ubuntu-touch/qml/GeoLocationSheet.qml:34
msgid "Clear location"
msgstr ""

#: ../src/ubuntu-touch/qml/GeoLocationSheet.qml:40
#: ../src/ubuntu-touch/qml/TitleEditSheet.qml:30
msgid "Undo"
msgstr ""

#: ../src/ubuntu-touch/qml/HiddenTagsPage.qml:14
#: ../src/ubuntu-touch/qml/SettingsPage.qml:38
msgid "Hide photos by tag"
msgstr ""

#: ../src/ubuntu-touch/qml/HiddenTagsPage.qml:43
msgid "Hide photos having these tags:"
msgstr ""

#: ../src/ubuntu-touch/qml/HubImporter.qml:25
#, qt-format
msgid "Import %1 items"
msgstr ""

#: ../src/ubuntu-touch/qml/HubImporter.qml:70
#, qt-format
msgid "Importing item %1 of %2"
msgstr ""

#: ../src/ubuntu-touch/qml/ImageDisplay.qml:164
msgid "View/edit location"
msgstr ""

#: ../src/ubuntu-touch/qml/ImageDisplay.qml:173
msgid "View image versions"
msgstr ""

#: ../src/ubuntu-touch/qml/ImageDisplay.qml:191
msgid "Set as master version"
msgstr ""

#: ../src/ubuntu-touch/qml/ImageDisplay.qml:198
msgid "Not a version"
msgstr ""

#: ../src/ubuntu-touch/qml/ImageDisplay.qml:207
msgid "Tag people"
msgstr ""

#: ../src/ubuntu-touch/qml/ImageDisplay.qml:224
#: ../src/ubuntu-touch/qml/TitleEditSheet.qml:16
msgid "Edit title and description"
msgstr ""

#: ../src/ubuntu-touch/qml/imaginario.qml:42
msgid "Select source"
msgstr ""

#: ../src/ubuntu-touch/qml/ImporterPage.qml:9
msgid "Import images"
msgstr ""

#: ../src/ubuntu-touch/qml/ImportItemDelegate.qml:111
msgid "Imported"
msgstr ""

#: ../src/ubuntu-touch/qml/ImportItemDelegate.qml:123
msgid "Duplicate check"
msgstr ""

#: ../src/ubuntu-touch/qml/ImportItemDelegate.qml:124
msgid "This item is likely a duplicate; tap to decide what to do with it."
msgstr ""

#: ../src/ubuntu-touch/qml/ImportItemDelegate.qml:138
msgid "Failed"
msgstr ""

#: ../src/ubuntu-touch/qml/ImportItemDelegate.qml:139
msgid "This item cannot be imported. Tap to ignore it."
msgstr ""

#: ../src/ubuntu-touch/qml/ImportItemDelegate.qml:153
msgid "Ignored"
msgstr ""

#: ../src/ubuntu-touch/qml/ImportItemDelegate.qml:154
msgid "This item won't be imported"
msgstr ""

#: ../src/ubuntu-touch/qml/ImportPrompt.qml:17
msgid ""
"No pictures. You can import pictures from other applications by pressing the "
"button below."
msgstr ""

#: ../src/ubuntu-touch/qml/ImportPrompt.qml:24
msgid "Import pictures"
msgstr ""

#: ../src/ubuntu-touch/qml/MainPage.qml:15
msgid "Imaginario"
msgstr ""

#: ../src/ubuntu-touch/qml/MainPage.qml:22
#: ../src/ubuntu-touch/qml/MainPage.qml:31
msgid "Filter"
msgstr ""

#: ../src/ubuntu-touch/qml/MainPage.qml:37
msgid "Select"
msgstr ""

#: ../src/ubuntu-touch/qml/MainPage.qml:42
msgid "Import items"
msgstr ""

#: ../src/ubuntu-touch/qml/MainPage.qml:48
#: ../src/ubuntu-touch/qml/SettingsPage.qml:12
msgid "Settings"
msgstr ""

#: ../src/ubuntu-touch/qml/MainPage.qml:55
#: ../src/ubuntu-touch/qml/MainPage.qml:74
msgid "Back"
msgstr ""

#: ../src/ubuntu-touch/qml/MainPage.qml:114
#, qt-format
msgid "Select items (%1)"
msgstr ""

#: ../src/ubuntu-touch/qml/MakeVersionPage.qml:13
msgid "Mark as version"
msgstr ""

#: ../src/ubuntu-touch/qml/MakeVersionPage.qml:34
msgid "<a href=\"http://localhost/help\">What are versions?</a>"
msgstr ""

#: ../src/ubuntu-touch/qml/MakeVersionPage.qml:43
#, qt-format
msgid ""
"<p>You can mark two or more photos as different versions (or edits) of the "
"same shot.<p>Only the final version will be shown in the photos overview. "
"The other versions of the photo will still be visibles in search results, "
"and when tapping on the \"Versions\" icon <img src=\"qrc:/icons/versions\" "
"height=\"%1\"> in the photo viewer page."
msgstr ""

#: ../src/ubuntu-touch/qml/MakeVersionPage.qml:50
msgid ""
"You are about to mark this photo as an edited version of an original photo."
msgstr ""

#: ../src/ubuntu-touch/qml/MakeVersionPage.qml:57
msgid "Choose original photo"
msgstr ""

#: ../src/ubuntu-touch/qml/MakeVersionPage.qml:64
msgid "Pick original"
msgstr ""

#: ../src/ubuntu-touch/qml/OrganizeTagsPage.qml:9
#: ../src/ubuntu-touch/qml/SettingsPage.qml:48
msgid "Organize tags"
msgstr ""

#: ../src/ubuntu-touch/qml/SettingsPage.qml:28
msgid "Auto-tag based on source application"
msgstr ""

#: ../src/ubuntu-touch/qml/ShareAction.qml:8
msgid "Share"
msgstr ""

#: ../src/ubuntu-touch/qml/SourceOptionsPage.qml:43
msgid "Import EXIF/XMP tags"
msgstr ""

#: ../src/ubuntu-touch/qml/SourceOptionsPage.qml:51
msgid "Auto-tag imported images:"
msgstr ""

#: ../src/ubuntu-touch/qml/TagActions.qml:14
#: ../src/ubuntu-touch/qml/TagEditPage.qml:15
msgid "Edit tag"
msgstr ""

#: ../src/ubuntu-touch/qml/TagArea.qml:85
msgid "?"
msgstr ""

#: ../src/ubuntu-touch/qml/TagArea.qml:93
msgid "Pick a name"
msgstr ""

#: ../src/ubuntu-touch/qml/TagDeleteConfirmation.qml:13
msgid "Tag deletion"
msgstr ""

#: ../src/ubuntu-touch/qml/TagDeleteConfirmation.qml:14
#, qt-format
msgid ""
"Confirm deletion of the <b>%1</b> tag?<br><br>The tag will also be removed "
"from any photos using it."
msgid_plural ""
"Confirm deletion of the selected tags?<br><br>The tags will also be removed "
"from any photos using them."
msgstr[0] ""
msgstr[1] ""

#: ../src/ubuntu-touch/qml/TagEditPage.qml:73
msgid "Cannot set an empty name"
msgstr ""

#: ../src/ubuntu-touch/qml/TagEditPage.qml:80
msgid "A tag with this name already exists"
msgstr ""

#: ../src/ubuntu-touch/qml/TagEditPage.qml:91
msgid "The tag will be renamed in all images"
msgstr ""

#: ../src/ubuntu-touch/qml/TagEditPage.qml:92
msgid "Tap on the tag name above to edit it"
msgstr ""

#: ../src/ubuntu-touch/qml/TagEditPage.qml:165
msgid "Invalid tag name"
msgstr ""

#: ../src/ubuntu-touch/qml/TagEditPage.qml:175
#: ../src/ubuntu-touch/qml/TagSections.qml:78
msgid "OK"
msgstr ""

#: ../src/ubuntu-touch/qml/TagList.qml:43
#: ../src/ubuntu-touch/qml/TagViewEditable.qml:36
msgid "No tags"
msgstr ""

#: ../src/ubuntu-touch/qml/TagList.qml:128
msgid "..."
msgstr ""

#: ../src/ubuntu-touch/qml/TagSections.qml:23
msgid "All"
msgstr ""

#: ../src/ubuntu-touch/qml/TagSections.qml:26
msgid "…"
msgstr ""

#: ../src/ubuntu-touch/qml/TagSections.qml:70
msgid "More categories"
msgstr ""

#: ../src/ubuntu-touch/qml/TagSections.qml:74
msgid "Choose category:"
msgstr ""

#: ../src/ubuntu-touch/qml/TagsEditPage.qml:36
msgid "Set a new parent for the selected tags"
msgstr ""

#: ../src/ubuntu-touch/qml/TagsEditPage.qml:45
msgid "don't change"
msgstr ""

#: ../src/ubuntu-touch/qml/TagViewEditable.qml:43
msgid "Create a new tag"
msgstr ""

#: ../src/ubuntu-touch/qml/TagViewEditable.qml:46
msgid "Add tag"
msgstr ""

#: ../src/ubuntu-touch/qml/TitleEditSheet.qml:75
msgid "Enter a title"
msgstr ""

#: ../src/ubuntu-touch/qml/TitleEditSheet.qml:85
msgid "Description"
msgstr ""

#: ../src/ubuntu-touch/qml/TitleEditSheet.qml:92
msgid "Enter a description"
msgstr ""

#: ../src/ubuntu-touch/qml/VersionsPage.qml:11
msgid "Image versions"
msgstr ""

#: ../src/ubuntu-touch/qml/ZoomInfo.qml:38
#, c-format, qt-format
msgid "%1x"
msgstr ""
