# Copied from the ubuntu-click-tools module, with INSTALL_PREFIX added.

defineTest(ubuntuAddPreTargetDep) {
    equals(TEMPLATE,subdirs) {
        #this hack adds a extra "first" target dependency
        #because subdirs can not handle build dependencies
        isEmpty(target_first.target){
            target_first.target = first
            QMAKE_EXTRA_TARGETS += target_first
            export(QMAKE_EXTRA_TARGETS)
        }
        target_first.depends += $$1

        export(target_first)
        export(target_first.target)
        export(target_first.depends)
    } else {
        PRE_TARGETDEPS+=$$1
        export(PRE_TARGETDEPS)
    }
}

!isEmpty(UBUNTU_TRANSLATION_SOURCES){
    # iterate over all QML/JS files and create a basic translation template
    template_pot.target=$$_PRO_FILE_PWD_/po/template.pot
    for(filelist, UBUNTU_TRANSLATION_SOURCES) {
        resolved_filelist = $$absolute_path($$filelist,$$_PRO_FILE_PWD_)
        resolved_filelist = $$files($$resolved_filelist)
        for(file,resolved_filelist) {
            template_pot.depends+=$$file
        }

    }

    !isEmpty(template_pot.depends) {
        # mardy: relative filelist, not to have absolute paths in the pot file
        relative_filelist =
        for(filelist, template_pot.depends) {
            relative_filelist += $$relative_path($$filelist,$$TOP_BUILD_DIR)
        }
        template_pot.commands=mkdir -p $$_PRO_FILE_PWD_/po && xgettext -o $$template_pot.target --qt --c++ --add-comments=TRANSLATORS --keyword=qsTr --keyword=QT_TR_NOOP --keyword=tr --keyword=tr:1,2 --keyword=ctr:1c,2 $$relative_filelist --from-code=UTF-8

        QMAKE_EXTRA_TARGETS+=template_pot
    }

    !isEmpty(UBUNTU_PO_FILES){
        isEmpty(UBUNTU_TRANSLATION_DOMAIN):error("UBUNTU_TRANSLATION_DOMAIN not defined")
        # compile the mo files into po files
        for(filelist, UBUNTU_PO_FILES) {
            resolved_filelist = $$absolute_path($$filelist,$$_PRO_FILE_PWD_)
            resolved_filelist = $$files($$resolved_filelist)
            for(file,resolved_filelist) {
                lang=$$basename(file)
                lang=$$split(lang, .)
                lang=$$first(lang)

                target_name=mo_target_$$lang
                target_file=$$shadowed($$_PRO_FILE_PWD_)/po/$${lang}/$${UBUNTU_TRANSLATION_DOMAIN}.mo

                $${target_name}.target=$$target_file
                $${target_name}.commands=mkdir -p po/$${lang} && msgfmt -o $${target_file} $$absolute_path($$file,$$_PRO_FILE_PWD_)
                $${target_name}.depends+=$$absolute_path($$file,$$_PRO_FILE_PWD_)

                $${target_name}_inst.path = $${INSTALL_PREFIX}/share/locale/$${lang}/LC_MESSAGES
                $${target_name}_inst.CONFIG +=no_check_exist
                $${target_name}_inst.files=$$target_file

                QMAKE_EXTRA_TARGETS+=$$target_name
                ubuntuAddPreTargetDep($${target_file})

                INSTALLS += $${target_name}_inst
            }
        }
    }
}
