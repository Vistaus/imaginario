/*
 * Copyright (C) 2018-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "database.h"
#include "digikam_importer.h"
#include "metadata.h"

#include <QDebug>
#include <QDir>
#include <QFileInfo>
#include <QFutureWatcher>
#include <QMetaObject>
#include <QSettings>
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlQuery>
#include <QStandardPaths>
#include <QtConcurrent>

using namespace Imaginario;

namespace Imaginario {

class DigikamImporterPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(DigikamImporter)

public:
    DigikamImporterPrivate(DigikamImporter *q);
    ~DigikamImporterPrivate();

    void setStatus(DigikamImporter::Status status);
    inline void setProgress(double progress);
    QString findConfigurationFile() const;
    bool checkDatabase();

    QString writeTagIcon(const QString &icon);
    bool importTags();
    bool importRolls();
    bool importPhotos();
    QList<Tag> importPhotoTags(int id, bool *pOk);
    void exec();

private:
    QSqlDatabase m_db;
    QString m_baseDir;
    DigikamImporter::Status m_status;
    int m_count;
    double m_progress;
    QString m_dbVersion;
    QString m_version;
    QDir m_iconDir;
    QHash<int,TagId> m_tagMap;
    QHash<int,RollId> m_rollMap;
    QHash<int,QString> m_albumDir;
    QFuture<void> m_execFuture;
    mutable DigikamImporter *q_ptr;
};

} // namespace

DigikamImporterPrivate::DigikamImporterPrivate(DigikamImporter *q):
    m_db(QSqlDatabase::addDatabase("QSQLITE", "digikam")),
    m_status(DigikamImporter::Initializing),
    m_count(-1),
    m_progress(0.0),
    q_ptr(q)
{
    qRegisterMetaType<DigikamImporter::Status>("DigikamImporter::Status");
    if (checkDatabase()) {
        setStatus(DigikamImporter::Ready);
    } else {
        setStatus(DigikamImporter::Failed);
    }
}

DigikamImporterPrivate::~DigikamImporterPrivate()
{
    QString name = m_db.connectionName();
    m_db = QSqlDatabase();
    QSqlDatabase::removeDatabase(name);
}

void DigikamImporterPrivate::setStatus(DigikamImporter::Status status)
{
    Q_Q(DigikamImporter);

    if (m_status == status) return;
    m_status = status;
    Q_EMIT q->statusChanged(status);
    m_progress = 0.0;
    Q_EMIT q->progressChanged();
}

void DigikamImporterPrivate::setProgress(double progress)
{
    Q_Q(DigikamImporter);
    m_progress = progress;
    Q_EMIT q->progressChanged();
}

QString DigikamImporterPrivate::findConfigurationFile() const
{
    const QStringList paths {
        QStandardPaths::writableLocation(QStandardPaths::GenericConfigLocation),
        QDir::homePath() + "/.kde/share/config",
        QDir::homePath() + "/.kde4/share/config",
    };

    for (const QString &path: paths) {
        QString configurationFile(path + "/digikamrc");
        if (QFileInfo::exists(configurationFile)) return configurationFile;
    }
    return QString();
}

bool DigikamImporterPrivate::checkDatabase()
{
    QString configPath = findConfigurationFile();
    if (configPath.isEmpty()) return false;

    QSettings config(configPath, QSettings::IniFormat);

    config.beginGroup("Database Settings");
    m_baseDir = config.value("Database Name").toString();
    QString dbPath = m_baseDir + "digikam4.db";
    QString faceDbPath = config.value("Database Name Face").toString() +
        "recognition.db";
    QString thumbnailsDbPath =
        config.value("Database Name Thumbnails").toString() +
        "thumbnails-digikam.db";
    config.endGroup();

    config.beginGroup("General Settings");
    m_version = config.value("Version").toString();
    config.endGroup();

    m_db.setDatabaseName(dbPath);
    if (!m_db.open()) return false;

    /* maybe TODO: open faces and thumbnails */

    QSqlQuery q(m_db);
    q.exec("SELECT value FROM Settings WHERE keyword = 'DBVersion'");
    if (Q_UNLIKELY(!q.next())) return false;
    m_dbVersion = q.value(0).toString();

    if (m_dbVersion < "7") return false;

    q.exec("SELECT COUNT(*) FROM Images");
    if (Q_UNLIKELY(!q.next())) return false;
    m_count = q.value(0).toInt();

    m_db.close();
    return true;
}

QString DigikamImporterPrivate::writeTagIcon(const QString &icon)
{
    QString ret;

    if (!icon.isEmpty()) {
        QFileInfo fileInfo(icon);
        if (fileInfo.exists()) {
            ret = QStringLiteral("tag_icon:") + fileInfo.absoluteFilePath();
        } else {
            ret = QStringLiteral("stock_icon:") + icon;
        }
    }
    return ret;
}

bool DigikamImporterPrivate::importTags()
{
    setStatus(DigikamImporter::ImportingTags);

    QSqlQuery q(m_db);
    q.setForwardOnly(true);
    bool ok = q.exec("SELECT id, pid, name, iconkde "
                     "FROM Tags");
    if (Q_UNLIKELY(!ok)) return false;

    Database *db = Database::instance();
    QSet<int> incompleteTags;

    while (q.next()) {
        int id = q.value(0).toInt();
        int parentId = q.value(1).toInt();
        QString name = q.value(2).toString();
        QString icon = writeTagIcon(q.value(3).toString());

        Tag existingTag = db->tag(name);
        if (Q_UNLIKELY(existingTag.isValid())) {
            m_tagMap.insert(id, existingTag.id());
            continue;
        }

        Tag parentTag = db->uncategorizedTag();
        if (parentId != 0) {
            /* We have a parent; check if we already created it */
            const auto i = m_tagMap.find(parentId);
            if (i != m_tagMap.constEnd()) {
                parentTag = Tag(i.value());
            } else {
                parentTag = db->createTag(parentTag,
                                          QString("digikam #%1").arg(parentId),
                                          QString());
                if (Q_UNLIKELY(!parentTag.isValid())) return false;
                incompleteTags.insert(parentId);
                m_tagMap.insert(parentId, parentTag.id());
            }
        }

        auto it = incompleteTags.find(id);
        if (it != incompleteTags.end()) {
            /* The tag has already been created, with bogus data.
             * Let's update it with the correct data. */
            Database::TagUpdate tagData {
                name,
                icon,
                QVariant::fromValue(parentTag)
            };
            ok = db->updateTag(Tag(m_tagMap[id]), tagData);
            if (Q_UNLIKELY(!ok)) return false;
            incompleteTags.erase(it);
        } else {
            Tag tag = db->createTag(parentTag, name, icon);
            if (Q_UNLIKELY(!tag.isValid())) return false;
            m_tagMap.insert(id, tag.id());
        }
    }
    return true;
}

bool DigikamImporterPrivate::importRolls()
{
    /* It's a bit improper, but let's import albums as rolls */

    setStatus(DigikamImporter::ImportingRolls);

    QSqlQuery q(m_db);
    q.setForwardOnly(true);
    bool ok = q.exec("SELECT a.id, a.date, a.caption, a.relativePath,"
                     " r.specificPath "
                     "FROM Albums AS a JOIN AlbumRoots AS r "
                     "ON a.albumRoot = r.id");
    if (Q_UNLIKELY(!ok)) return false;

    Database *db = Database::instance();

    while (q.next()) {
        int id = q.value(0).toInt();
        QDate date = QDate::fromString(q.value(1).toString(), Qt::ISODate);
        /* TODO: import description, once the Database class supports it:
         * https://gitlab.com/mardy/imaginario/issues/4
        QString description = q.value(2).toString();
         */

        RollId rollId = db->createRoll(QDateTime(date));
        if (Q_UNLIKELY(rollId < 0)) return false;

        m_rollMap.insert(id, rollId);

        QString relativePath = q.value(3).toString();
        QString specificPath = q.value(4).toString();
        m_albumDir.insert(id, specificPath + relativePath);
    }

    return true;
}

bool DigikamImporterPrivate::importPhotos()
{
    setStatus(DigikamImporter::ImportingPhotos);

    QSqlQuery q(m_db);
    q.setForwardOnly(true);
    bool ok = q.exec("SELECT p.id, p.album, p.name,"
                     " c.comment,"
                     " i.rating, i.creationDate,"
                     " l.latitudeNumber, l.longitudeNumber "
                     "FROM Images AS p "
                     "LEFT JOIN ImageInformation AS i ON p.id = i.imageid "
                     "LEFT JOIN ImageComments AS c"
                     " ON p.id = c.imageid AND c.type = 1 "
                     "LEFT JOIN ImagePositions AS l ON p.id = l.imageid ");
    if (Q_UNLIKELY(!ok)) return false;

    Database *db = Database::instance();
    Metadata metadata;

    int importedPhotos = 0;
    while (q.next()) {
        importedPhotos++;
        if (importedPhotos % 10 == 0) {
            setProgress(double(importedPhotos) / m_count);
        }

        int i = 0;
        Database::Photo p;
        int id = q.value(i++).toInt();
        int albumId = q.value(i++).toInt();
        p.setFileName(q.value(i++).toString());
        p.setDescription(q.value(i++).toString());
        p.setRating(q.value(i++).toInt());
        QDateTime time =
            QDateTime::fromString(q.value(i++).toString(), Qt::ISODate);
        if (time.isValid()) {
            p.setTime(time);
        }
        Geo latitude = q.value(i++).toReal();
        Geo longitude = q.value(i++).toReal();

        const auto albumDir = m_albumDir.find(albumId);
        if (albumDir == m_albumDir.constEnd()) {
            p.setBaseUri(m_baseDir);
        } else {
            p.setBaseUri(albumDir.value());
        }

        if (latitude != 0 || longitude != 0) {
            p.setLocation(GeoPoint(latitude, longitude));
        } else {
            Metadata::ImportData data;
            if (metadata.readImportData(p.filePath(), data)) {
                p.setLocation(data.location);
            }
        }

        const auto ri = m_rollMap.find(albumId);
        RollId rollId;
        if (Q_UNLIKELY(ri == m_rollMap.constEnd())) {
            rollId = 0;
        } else {
            rollId = ri.value();
        }

        PhotoId photoId = db->addPhoto(p, rollId);
        if (Q_UNLIKELY(photoId < 0)) return false;

        QList<Tag> tags = importPhotoTags(id, &ok);
        if (Q_UNLIKELY(!ok)) return false;

        ok = db->addTags(photoId, tags);
        if (Q_UNLIKELY(!ok)) return false;
    }

    return true;
}

QList<Tag> DigikamImporterPrivate::importPhotoTags(int id, bool *pOk)
{
    QList<Tag> tags;

    QSqlQuery q(m_db);
    q.setForwardOnly(true);
    bool ok = q.exec(QString("SELECT tagid FROM ImageTags "
                             "WHERE imageid = %1").arg(id));
    if (Q_UNLIKELY(!ok)) {
        *pOk = false;
        return tags;
    }

    while (q.next()) {
        int tagId = q.value(0).toInt();
        Tag tag(m_tagMap.value(tagId, -1));
        if (Q_UNLIKELY(!tag.isValid())) {
            qWarning() << "tag Id not found" << tagId;
        } else {
            tags.append(tag);
        }
    }

    *pOk = true;
    return tags;
}

void DigikamImporterPrivate::exec()
{
    if (!m_db.open()) return;

    m_tagMap.clear();
    m_rollMap.clear();

    Database *db = Database::instance();
    db->transaction();

    bool ok = importTags();
    if (Q_UNLIKELY(!ok)) {
        db->rollback();
        setStatus(DigikamImporter::Failed);
        return;
    }

    ok = importRolls();
    if (Q_UNLIKELY(!ok)) {
        db->rollback();
        setStatus(DigikamImporter::Failed);
        return;
    }

    ok = importPhotos();
    if (Q_UNLIKELY(!ok)) {
        db->rollback();
        setStatus(DigikamImporter::Failed);
        return;
    }

    m_db.close();

    db->commit();
    setStatus(DigikamImporter::Done);
}

DigikamImporter::DigikamImporter(QObject *parent):
    QObject(parent),
    d_ptr(new DigikamImporterPrivate(this))
{
}

DigikamImporter::~DigikamImporter()
{
    delete d_ptr;
}

QString DigikamImporter::dbVersion() const
{
    Q_D(const DigikamImporter);
    return d->m_dbVersion;
}

QString DigikamImporter::version() const
{
    Q_D(const DigikamImporter);
    return d->m_version;
}

DigikamImporter::Status DigikamImporter::status() const
{
    Q_D(const DigikamImporter);
    return d->m_status;
}

double DigikamImporter::progress() const
{
    Q_D(const DigikamImporter);
    return d->m_progress;
}

void DigikamImporter::exec()
{
    Q_D(DigikamImporter);
    d->m_execFuture = QtConcurrent::run(d, &DigikamImporterPrivate::exec);
}

int DigikamImporter::count() const
{
    Q_D(const DigikamImporter);
    return d->m_count;
}

#include "digikam_importer.moc"
