/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGINARIO_SETTINGS_H
#define IMAGINARIO_SETTINGS_H

#include "types.h"

#include <QJsonArray>
#include <QObject>
#include <QString>
#include <QStringList>

namespace Imaginario {

class SettingsPrivate;
class Settings: public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool embed READ embed WRITE setEmbed NOTIFY embedChanged)
    Q_PROPERTY(bool writeXmp READ writeXmp WRITE setWriteXmp \
               NOTIFY writeXmpChanged)
    Q_PROPERTY(bool copyFiles READ copyFiles WRITE setCopyFiles \
               NOTIFY copyFilesChanged)
    Q_PROPERTY(QString photoDir READ photoDir WRITE setPhotoDir \
               NOTIFY photoDirChanged)
    Q_PROPERTY(QStringList hiddenTags READ hiddenTags WRITE setHiddentags \
               NOTIFY hiddenTagsChanged)
    Q_PROPERTY(QString autoTagData READ autoTagData WRITE setAutoTagData \
               NOTIFY autoTagDataChanged)
    Q_PROPERTY(bool showTags READ showTags WRITE setShowTags \
               NOTIFY showTagsChanged)
    Q_PROPERTY(bool showAreaTags READ showAreaTags WRITE setShowAreaTags \
               NOTIFY showAreaTagsChanged)
    Q_PROPERTY(bool showRating READ showRating WRITE setShowRating \
               NOTIFY showRatingChanged)
    Q_PROPERTY(QJsonArray helpers READ helpers WRITE setHelpers \
               NOTIFY helpersChanged)

public:
    Settings(QObject *parent = 0);
    ~Settings();

    void setEmbed(bool embed);
    bool embed() const;

    void setWriteXmp(bool writeXmp);
    bool writeXmp() const;

    void setCopyFiles(bool copyFiles);
    bool copyFiles() const;

    void setPhotoDir(const QString &photoDir);
    QString photoDir() const;

    void setHiddentags(const QStringList &tags);
    QStringList hiddenTags() const;

    void setAutoTagData(const QString &data);
    QString autoTagData() const;

    void setShowTags(bool show);
    bool showTags() const;

    void setShowAreaTags(bool show);
    bool showAreaTags() const;

    void setShowRating(bool show);
    bool showRating() const;

    void setHelpers(const QJsonArray &helpers);
    QJsonArray helpers() const;

Q_SIGNALS:
    void embedChanged();
    void writeXmpChanged();
    void copyFilesChanged();
    void photoDirChanged();
    void hiddenTagsChanged();
    void autoTagDataChanged();
    void showTagsChanged();
    void showAreaTagsChanged();
    void showRatingChanged();
    void helpersChanged();

private:
    SettingsPrivate *d_ptr;
    Q_DECLARE_PRIVATE(Settings)
};

} // namespace

#endif // IMAGINARIO_SETTINGS_H
