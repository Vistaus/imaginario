/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGINARIO_METADATA_H
#define IMAGINARIO_METADATA_H

#include <QDateTime>
#include <QGeoCoordinate>
#include <QImage>
#include <QMetaType>
#include <QObject>
#include <QRectF>
#include <QStringList>
#include <QUrl>
#include <QVariantMap>
#include <QVector>

class JobExecutorTest;

class QSize;

namespace Imaginario {

class MetadataPrivate;
class Metadata: public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool embed READ embed WRITE setEmbed NOTIFY embedChanged)

public:
    struct RegionInfo {
        RegionInfo() {}
        RegionInfo(const QRectF &a, const QString &n,
                   const QString &d = QString()):
            area(a), name(n), description(d) {}
        QRectF area;
        QString name;
        QString description;
    };
    typedef QVector<RegionInfo> Regions;

    /* This contains the bits of data which we always read when importing
     * items */
    struct ImportData {
        QString title;
        QString description;
        QDateTime time;
        QStringList tags;
        Regions regions;
        int rating;
        QGeoCoordinate location;

        ImportData(): rating(-1) {}
    };

    struct Changes {
        Changes(): fields(None), rating(0) {}
        enum Fields {
            None = 0,
            WriteTags = 1 << 0,
            WriteRating = 1 << 1,
            WriteLocation = 1 << 2,
            WriteTitle = 1 << 3,
            WriteDescription = 1 << 4,
            WriteRegions = 1 << 5,
            WriteFreeFields = 1 << 6,
        };
        void clear() { *this = Changes(); }
        void setTags(const QStringList &v) { tags = v; fields |= WriteTags; }
        void setRegions(const Regions &v) { regions = v; fields |= WriteRegions; }
        void setLocation(const QGeoCoordinate &v) {
            location = v; fields |= WriteLocation; }
        void setRating(int v) { rating = v; fields |= WriteRating; }
        void setTitle(const QString &v) { title = v; fields |= WriteTitle; }
        void setDescription(const QString &v) { description = v; fields |= WriteDescription; }
        void setFreeField(const QString &key, const QVariant &value) {
            freeFields.insert(key, value);
            fields |= WriteFreeFields;
        }

    private:
        friend class MetadataPrivate;
        friend class ::JobExecutorTest;
        int fields;
        QStringList tags;
        Regions regions;
        int rating;
        QGeoCoordinate location;
        QString title;
        QString description;
        QVariantMap freeFields;
    };

    Metadata(QObject *parent = 0);
    ~Metadata();

    void setEmbed(bool embed);
    bool embed() const;

    void writeRegions(const QString &file, const Regions &regions) {
        Changes changes; changes.setRegions(regions);
        writeChanges(file, changes);
    }
    void writeTags(const QString &file, const QStringList &tags) {
        Changes changes; changes.setTags(tags);
        writeChanges(file, changes);
    }

    void writeRating(const QString &file, int rating) {
        Changes changes; changes.setRating(rating);
        writeChanges(file, changes);
    }
    void writeLocation(const QString &file, const QGeoCoordinate &p) {
        Changes changes; changes.setLocation(p);
        writeChanges(file, changes);
    }
    void writeTitle(const QString &file, const QString &title) {
        Changes changes; changes.setTitle(title);
        writeChanges(file, changes);
    }
    void writeDescription(const QString &file, const QString &description) {
        Changes changes; changes.setDescription(description);
        writeChanges(file, changes);
    }

    bool writeChanges(const QString &file, const Changes &changes);
    bool readImportData(const QString &file, ImportData &data) const;
    QVariant readFreeField(const QString &file, const QString &key,
                           QMetaType::Type type = QMetaType::QString) const;
    QImage loadPreview(const QString &file, QSize *size,
                       const QSize &requestedSize) const;

Q_SIGNALS:
    void embedChanged();

private:
    MetadataPrivate *d_ptr;
    Q_DECLARE_PRIVATE(Metadata)
};

inline uint qHash(const Metadata::RegionInfo &r, uint seed = 0)
{
    return ::qHash(r.name, seed);
}

/* Fuzzy comparison, use it only in unit tests! */
inline bool operator==(const Metadata::RegionInfo &r1,
                       const Metadata::RegionInfo &r2)
{
    return r1.name == r2.name &&
        r1.description == r2.description &&
        (r1.area.topLeft() * 10).toPoint() == (r2.area.topLeft() * 10).toPoint() &&
        (r1.area.size() * 10).toSize() == (r2.area.size() * 10).toSize();
}

} // namespace

Q_DECLARE_METATYPE(Imaginario::Metadata::RegionInfo)

#endif // IMAGINARIO_METADATA_H
