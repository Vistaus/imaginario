/*
 * Copyright (C) 2017-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGINARIO_MIME_DATABASE_H
#define IMAGINARIO_MIME_DATABASE_H

#include <QMimeDatabase>
#include <QObject>

namespace Imaginario {

/* This is just a tiny wrapper to expose QMimeDatabase to QML.
 */
class MimeDatabase: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QStringList allMimeNames READ allMimeNames CONSTANT)

public:
    MimeDatabase(QObject *parent = 0): QObject(parent) {}
    ~MimeDatabase() {}

    QStringList allMimeNames() const {
        QList<QMimeType> mimeTypes = m_mimeDb.allMimeTypes();
        QStringList ret;
        for (const QMimeType &mimeType: mimeTypes) {
            ret.append(mimeType.name());
        }
        std::sort(ret.begin(), ret.end());
        return ret;
    }

private:
    QMimeDatabase m_mimeDb;
};

} // namespace

#endif // IMAGINARIO_MIME_DATABASE_H
