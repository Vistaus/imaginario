/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "database.h"
#include "job.h"
#include "job_executor.h"
#include "metadata.h"
#include "utils.h"
#include "writer.h"

#include <QDebug>
#include <QDir>
#include <QJSValue>

using namespace Imaginario;

namespace Imaginario {

class WriterPrivate
{
    Q_DECLARE_PUBLIC(Writer)

public:
    struct TagChanges {
        TagChanges() {}
        QString name;
        QString icon;
        QVariant parent;
    };

    enum Operation {
        Replace = 0,
        Add,
    };

    WriterPrivate(Writer *q);
    ~WriterPrivate();

    void updateTagInImages(const QList<Database::Photo> &photos);

    Tag createTag(const QString &name, Tag parentTag, const QString &icon);
    void updateTag(Tag tag, const Database::TagUpdate &changes);
    void setPhotoTags(QList<PhotoId> ids, const QList<Tag> &tags,
                      const QList<Tag> &addTags, const QList<Tag> &removeTags);
    void setRegions(PhotoId photoId, const QJSValue &regions, Operation op);
    void setRating(QList<PhotoId> ids, int rating);
    void setLocation(QList<PhotoId> ids, const GeoPoint &p);
    void setTitle(QList<PhotoId> ids, const QString &title);
    void setDescription(QList<PhotoId> ids, const QString &description);
    PhotoId createItem(const QUrl &url);
    void deletePhotos(QList<PhotoId> ids);
    void rebuildThumbnails(QList<PhotoId> ids);

private:
    bool m_writeXmp;
    QHash<QUrl,int> m_photoIds;
    mutable Writer *q_ptr;
};

} // namespace

WriterPrivate::WriterPrivate(Writer *q):
    m_writeXmp(true),
    q_ptr(q)
{
}

WriterPrivate::~WriterPrivate()
{
}

void WriterPrivate::updateTagInImages(const QList<Database::Photo> &photos)
{
    QVector<Job> jobs;
    jobs.reserve(photos.count());
    Q_FOREACH(const Database::Photo &photo, photos) {
        jobs.append(PhotoJob(photo.id(), PhotoJob::WriteTags));
    }

    JobExecutor::instance()->addJobs(jobs);
}

Tag WriterPrivate::createTag(const QString &name, Tag parentTag,
                             const QString &icon)
{
    Database *db = Database::instance();
    Tag tag = db->tag(name);
    if (tag.isValid()) return tag;

    db->transaction();
    tag = db->createTag(parentTag, name, icon);
    db->commit();
    return tag;
}

void WriterPrivate::updateTag(Tag tag, const Database::TagUpdate &changes)
{
    Database *db = Database::instance();
    db->transaction();
    bool ok = db->updateTag(tag, changes);

    if (Q_LIKELY(ok)) {
        db->commit();
        if (m_writeXmp && !changes.name.isEmpty()) {
            SearchFilters filters;
            filters.requiredTags.append(QList<Tag>{tag});
            QList<Database::Photo> photos = db->findPhotos(filters);
            updateTagInImages(photos);
        }
    } else {
        db->rollback();
    }
}

void WriterPrivate::setPhotoTags(QList<PhotoId> ids,
                                 const QList<Tag> &tags,
                                 const QList<Tag> &addTags,
                                 const QList<Tag> &removeTags)
{
    Database *db = Database::instance();

    bool settingTags = addTags.isEmpty() && removeTags.isEmpty();

    db->transaction();

    QVector<Job> jobs;
    jobs.reserve(ids.count());

    Q_FOREACH(PhotoId id, ids) {
        Database::Photo photo = db->photo(id);
        if (Q_UNLIKELY(!photo.isValid())) {
            qWarning() << "No such photo" << id;
            continue;
        }

        if (settingTags) {
            db->setTags(id, tags);
        } else {
            db->addTags(id, addTags);
            db->removeTags(id, removeTags);
        }

        jobs.append(PhotoJob(id, PhotoJob::WriteTags));
    }

    db->commit();
    if (m_writeXmp) {
        JobExecutor::instance()->addJobs(jobs);
    }
}

void WriterPrivate::setRegions(PhotoId photoId, const QJSValue &regions,
                               Operation op)
{
    if (Q_UNLIKELY(!regions.isArray())) {
        qWarning() << "setRegions expects an array of regions";
        return;
    }

    Database *db = Database::instance();
    Database::Photo photo = db->photo(photoId);
    if (Q_UNLIKELY(!photo.isValid())) {
        qWarning() << "No such photo" << photoId;
        return;
    }

    PhotoJob job(photoId, PhotoJob::WriteRegions);

    QList<Tag> oldTags;
    Q_FOREACH(const TagArea &a, db->tagAreas(photoId)) {
        oldTags.append(a.tag());
    }

    bool ok = true;
    db->transaction();
    for (int i = 0; ; i++) {
        const QJSValue &r = regions.property(i);
        if (!r.isObject()) break;
        Tag tag(r.property("tag").toVariant().value<Tag>());
        if (!tag.isValid()) continue;
        QRectF rect(r.property("rect").toVariant().toRectF());
        if (!rect.isValid()) continue;

        QJSValue descProp = r.property("description");
        QString description =
            descProp.isString() ? descProp.toString() : QString();
        job.setRegionDescription(rect, description);
        ok = db->addTagWithArea(photoId, tag, rect);
        if (Q_UNLIKELY(!ok)) break;

        oldTags.removeOne(tag);
    }

    /* All the tag areas have been set. Now we need to remove the previous tag
     * areas that have been deleted. */
    if (ok && op == Replace && !oldTags.isEmpty()) {
        ok = db->removeTags(photoId, oldTags);
    }

    if (ok) {
        db->commit();
        if (m_writeXmp) {
            JobExecutor *executor = JobExecutor::instance();
            executor->addJob(job);
        }
    } else {
        db->rollback();
    }
}

void WriterPrivate::setRating(QList<PhotoId> ids, int rating)
{
    Database *db = Database::instance();

    QVector<Job> jobs;
    jobs.reserve(ids.count());

    db->transaction();

    Q_FOREACH(PhotoId id, ids) {
        Database::Photo photo = db->photo(id);
        if (Q_UNLIKELY(!photo.isValid())) {
            qWarning() << "No such photo" << id;
            continue;
        }

        db->setRating(id, rating);
        jobs.append(PhotoJob(id, PhotoJob::WriteRating));
    }
    db->commit();
    if (m_writeXmp) {
        JobExecutor::instance()->addJobs(jobs);
    }
}

void WriterPrivate::setLocation(QList<PhotoId> ids, const GeoPoint &p)
{
    Database *db = Database::instance();

    QVector<Job> jobs;
    jobs.reserve(ids.count());

    db->transaction();

    Q_FOREACH(PhotoId id, ids) {
        Database::Photo photo = db->photo(id);
        if (Q_UNLIKELY(!photo.isValid())) {
            qWarning() << "No such photo" << id;
            continue;
        }

        db->setLocation(id, p);
        jobs.append(PhotoJob(id, PhotoJob::WriteLocation));
    }
    db->commit();
    if (m_writeXmp) {
        JobExecutor::instance()->addJobs(jobs);
    }
}

void WriterPrivate::setTitle(QList<PhotoId> ids, const QString &title)
{
    Database *db = Database::instance();

    QVector<Job> jobs;
    jobs.reserve(ids.count());

    db->transaction();

    Q_FOREACH(PhotoId id, ids) {
        Database::Photo photo = db->photo(id);
        if (Q_UNLIKELY(!photo.isValid())) {
            qWarning() << "No such photo" << id;
            continue;
        }

        db->setDescription(id, title);
        jobs.append(PhotoJob(id, PhotoJob::WriteTitle));
    }
    db->commit();
    if (m_writeXmp) {
        JobExecutor::instance()->addJobs(jobs);
    }
}

void WriterPrivate::setDescription(QList<PhotoId> ids, const QString &desc)
{
    if (!m_writeXmp) return; // FIXME information gets totally lost...

    Database *db = Database::instance();

    Q_FOREACH(PhotoId id, ids) {
        Database::Photo photo = db->photo(id);
        if (Q_UNLIKELY(!photo.isValid())) {
            qWarning() << "No such photo" << id;
            continue;
        }

        PhotoJob job(id);
        job.setDescription(desc);
        JobExecutor::instance()->addJob(job);
    }
}

PhotoId WriterPrivate::createItem(const QUrl &url)
{
    if (!url.isLocalFile()) return -1;

    Database *db = Database::instance();

    QFileInfo fileInfo(url.toLocalFile());
    if (Q_UNLIKELY(!fileInfo.exists())) return -1;

    Metadata metadata;
    Metadata::ImportData data;
    metadata.readImportData(fileInfo.filePath(), data);

    Database::Photo photo;
    photo.setBaseUri(QStringLiteral("file://") +
                     fileInfo.absolutePath());
    photo.setFileName(fileInfo.fileName());
    photo.setDescription(data.title);
    photo.setTime(data.time);
    photo.setRating(data.rating);
    photo.setLocation(data.location);

    /* We might want to add a parameter to decide whether to import tags;
     * but given that this method exists to import photos modified by external
     * programs, one might want to use the tags from the original photo
     * instead.
     * Maybe the best option would be to return the list of embedded tags as
     * well, in order to let the called decide what to do.
     */
    return db->addPhoto(photo, 0);
}

void WriterPrivate::deletePhotos(QList<PhotoId> ids)
{
    Database *db = Database::instance();

    Q_FOREACH(PhotoId id, ids) {
        Database::Photo photo = db->photo(id);
        if (Q_UNLIKELY(!photo.isValid())) {
            qWarning() << "No such photo" << id;
            continue;
        }

        QString filePath = photo.url().toLocalFile();
        QFile::remove(filePath);
        QFile::remove(filePath + ".xmp");
    }
}

void WriterPrivate::rebuildThumbnails(QList<PhotoId> ids)
{
    Database *db = Database::instance();

    Q_FOREACH(PhotoId id, ids) {
        Database::Photo photo = db->photo(id);
        if (Q_UNLIKELY(!photo.isValid())) {
            qWarning() << "No such photo" << id;
            continue;
        }

        m_photoIds.insert(photo.url(), id);

        ThumbnailJob job(photo.url());
        JobExecutor::instance()->addJob(job);
    }
}

Writer::Writer(QObject *parent):
    QObject(parent),
    d_ptr(new WriterPrivate(this))
{
    QObject::connect(JobExecutor::instance(),
                     &JobExecutor::thumbnailUpdated,
                     [this](QUrl url) {
        auto i = d_ptr->m_photoIds.find(url);
        Q_EMIT thumbnailUpdated(i.value());

        d_ptr->m_photoIds.erase(i);
    });
}

Writer::~Writer()
{
    delete d_ptr;
}

void Writer::setEmbed(bool embed)
{
    JobExecutor::instance()->setEmbedMetadata(embed);
    Q_EMIT embedChanged();
}

bool Writer::embed() const
{
    return JobExecutor::instance()->embedMetadata();
}

void Writer::setWriteXmp(bool writeXmp)
{
    Q_D(Writer);

    if (d->m_writeXmp == writeXmp) return;
    d->m_writeXmp = writeXmp;
    Q_EMIT writeXmpChanged();
}

bool Writer::writeXmp() const
{
    Q_D(const Writer);
    return d->m_writeXmp;
}

Tag Writer::createTag(const QString &name, Tag parentTag,
                      const QString &icon)
{
    Q_D(Writer);
    return d->createTag(name, parentTag, icon);
}

void Writer::updateTag(Tag tag, const QVariantMap &changes)
{
    Q_D(Writer);

    Database::TagUpdate tagChanges;
    if (changes.contains("name")) {
        tagChanges.name = changes["name"].toString();
    }
    if (changes.contains("icon")) {
        tagChanges.icon = changes["icon"].toString();
    }
    if (changes.contains("parent")) {
        tagChanges.parent = changes["parent"];
    }
    d->updateTag(tag, tagChanges);
}

void Writer::deleteTag(Tag tag)
{
    Q_D(Writer);

    Database *db = Database::instance();

    SearchFilters filters;
    filters.requiredTags.append(QList<Tag>{tag});
    QList<Database::Photo> photos = db->findPhotos(filters);

    db->transaction();
    bool ok = db->deleteTag(tag);
    if (Q_LIKELY(ok)) {
        db->commit();
        if (d->m_writeXmp) {
            d->updateTagInImages(photos);
        }
    } else {
        db->rollback();
    }
}

void Writer::setPhotoTags(const QVariant &photoIds, const QVariant &tags)
{
    Q_D(Writer);
    QList<Tag> empty;
    d->setPhotoTags(parseList<PhotoId>(photoIds), parseList<Tag>(tags),
                    empty, empty);
}

void Writer::changePhotoTags(const QVariant &photoIds,
                             const QVariant &addTags,
                             const QVariant &removeTags)
{
    Q_D(Writer);
    QList<Tag> empty;
    QList<Tag> addTagsList = parseList<Tag>(addTags);
    QList<Tag> removeTagsList = parseList<Tag>(removeTags);
    if (Q_UNLIKELY(addTagsList.isEmpty() && removeTagsList.isEmpty())) {
        return;
    }
    d->setPhotoTags(parseList<PhotoId>(photoIds), empty,
                    addTagsList, removeTagsList);
}

void Writer::setRegions(PhotoId photoId, const QJSValue &regions)
{
    Q_D(Writer);
    d->setRegions(photoId, regions, WriterPrivate::Replace);
}

void Writer::addRegions(PhotoId photoId, const QJSValue &regions)
{
    Q_D(Writer);
    d->setRegions(photoId, regions, WriterPrivate::Add);
}

void Writer::setRating(const QVariant &photoIds, int rating)
{
    Q_D(Writer);
    d->setRating(parseList<PhotoId>(photoIds), rating);
}

void Writer::setLocation(const QVariant &ids, const GeoPoint &p)
{
    Q_D(Writer);
    d->setLocation(parseList<PhotoId>(ids), p);
}

void Writer::setTitle(const QVariant &ids, const QString &title)
{
    Q_D(Writer);
    d->setTitle(parseList<PhotoId>(ids), title);
}

void Writer::setDescription(const QVariant &ids, const QString &desc)
{
    Q_D(Writer);
    d->setDescription(parseList<PhotoId>(ids), desc);
}

void Writer::makeVersion(PhotoId photoId, PhotoId master)
{
    Database *db = Database::instance();
    db->makeVersion(photoId, master);
}

void Writer::makeDefaultVersion(PhotoId photoId)
{
    Database *db = Database::instance();
    QList<Database::Photo> photos = db->photoVersions(photoId);
    if (Q_UNLIKELY(photos.count() < 2)) {
        qWarning() << "makeDefaultVersion: needed at least 2 photos";
        return;
    }

    db->transaction();
    bool ok = true;
    Q_FOREACH(const Database::Photo &p, photos) {
        if (p.id() == photoId) continue;
        if (Q_UNLIKELY(!db->makeVersion(p.id(), photoId))) {
            ok = false;
            break;
        }
    }

    if (ok) {
        db->commit();
    } else {
        db->rollback();
    }
}

void Writer::removeVersion(PhotoId photoId)
{
    Database *db = Database::instance();
    QList<Database::Photo> versions = db->photoVersions(photoId);
    if (Q_UNLIKELY(versions.isEmpty())) {
        qWarning() << "removeVersion: no versions found";
        return;
    }

    Database::Photo photo = db->photo(photoId);

    db->transaction();
    bool mustCommit = true;

    bool ok = db->makeVersion(photoId, -1);
    if (Q_UNLIKELY(!ok)) mustCommit = false;

    if (versions.count() > 2) {
        if (photo.isDefaultVersion()) {
            /* We are removing the master version; we must update the other
             * versions to point to a different master */
            PhotoId first = -1;
            Q_FOREACH(const Database::Photo &p, versions) {
                if (p.id() == photoId) continue;

                ok = db->makeVersion(p.id(), first);
                if (Q_UNLIKELY(!ok)) { mustCommit = false; break; }

                if (first < 0) {
                    first = p.id();
                }
            }
        }
    } else {
        /* remove the orphaned version, too */
        PhotoId orphan = (versions[0].id() != photoId) ?
            versions[0].id() : versions[1].id();
        ok = db->makeVersion(orphan, -1);
        if (Q_UNLIKELY(!ok)) { mustCommit = false; }
    }

    if (mustCommit) {
        db->commit();
    } else {
        db->rollback();
    }
}

int Writer::createItem(const QUrl &url)
{
    Q_D(Writer);
    return d->createItem(url);
}

void Writer::deletePhotos(const QVariant &photoIds)
{
    Q_D(Writer);
    d->deletePhotos(parseList<PhotoId>(photoIds));
}

void Writer::rebuildThumbnails(const QVariant &photoIds)
{
    Q_D(Writer);
    d->rebuildThumbnails(parseList<PhotoId>(photoIds));
}
