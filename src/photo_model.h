/*
 * Copyright (C) 2014-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGINARIO_PHOTO_MODEL_H
#define IMAGINARIO_PHOTO_MODEL_H

#include "database.h"
#include "types.h"

#include <QAbstractListModel>
#include <QDateTime>
#include <QMimeType>
#include <QQmlParserStatus>
#include <QVariant>

namespace Imaginario {

class PhotoModelPrivate;
class PhotoModel: public QAbstractListModel, public QQmlParserStatus
{
    Q_OBJECT
    Q_INTERFACES(QQmlParserStatus)
    Q_ENUMS(Roles TimeOrder)
    Q_PROPERTY(int count READ rowCount NOTIFY countChanged)
    Q_PROPERTY(QDateTime time0 READ time0 WRITE setTime0 NOTIFY time0Changed)
    Q_PROPERTY(QDateTime time1 READ time1 WRITE setTime1 NOTIFY time1Changed)
    Q_PROPERTY(int roll0 READ roll0 WRITE setRoll0 NOTIFY roll0Changed)
    Q_PROPERTY(int roll1 READ roll1 WRITE setRoll1 NOTIFY roll1Changed)
    Q_PROPERTY(int rating0 READ rating0 WRITE setRating0 NOTIFY rating0Changed)
    Q_PROPERTY(int rating1 READ rating1 WRITE setRating1 NOTIFY rating1Changed)
    Q_PROPERTY(GeoPoint location0 READ location0 WRITE setLocation0 \
               NOTIFY location0Changed)
    Q_PROPERTY(GeoPoint location1 READ location1 WRITE setLocation1 \
               NOTIFY location1Changed)
    Q_PROPERTY(bool nonGeoTagged READ nonGeoTagged WRITE setNonGeoTagged \
               NOTIFY nonGeoTaggedChanged)
    Q_PROPERTY(QVariant requiredTags READ requiredTags \
               WRITE setRequiredTags NOTIFY requiredTagsChanged)
    Q_PROPERTY(QVariant forbiddenTags READ forbiddenTags \
               WRITE setForbiddenTags NOTIFY forbiddenTagsChanged)
    Q_PROPERTY(bool areaTagged READ areaTagged WRITE setAreaTagged \
               NOTIFY areaTaggedChanged)
    Q_PROPERTY(QVariant excludedPhotos READ excludedPhotos WRITE setExcludedPhotos \
               NOTIFY excludedPhotosChanged)
    Q_PROPERTY(bool skipNonDefaultVersions READ skipNonDefaultVersions \
               WRITE setSkipNonDefaultVersions \
               NOTIFY skipNonDefaultVersionsChanged)
    Q_PROPERTY(QList<PhotoId> photoIds READ photoIds WRITE setPhotoIds \
               NOTIFY photoIdsChanged)
    Q_PROPERTY(PhotoId photoVersions READ photoVersions \
               WRITE setPhotoVersions NOTIFY photoVersionsChanged)
    Q_PROPERTY(Qt::SortOrder sortOrder READ sortOrder WRITE setSortOrder \
               NOTIFY sortOrderChanged)

public:
    enum Roles {
        PhotoIdRole = Qt::UserRole + 1,
        UrlRole,
        FileNameRole,
        TitleRole,
        DescriptionRole,
        TimeRole,
        RollIdRole,
        RatingRole,
        LocationRole,
        HasVersionsRole,
        IsDefaultVersionRole,
        MasterVersionRole,
        SizeRole,
        MimeTypeRole,
    };

    enum TimeOrder {
        OlderThan,
        NewerThan,
    };

    PhotoModel(QObject *parent = 0);
    ~PhotoModel();

    void setTime0(const QDateTime &time);
    QDateTime time0() const;

    void setTime1(const QDateTime &time);
    QDateTime time1() const;

    void setRoll0(int roll);
    int roll0() const;

    void setRoll1(int roll);
    int roll1() const;

    void setRating0(int rating);
    int rating0() const;

    void setRating1(int rating);
    int rating1() const;

    void setLocation0(const GeoPoint &location);
    GeoPoint location0() const;

    void setLocation1(const GeoPoint &location);
    GeoPoint location1() const;

    void setNonGeoTagged(bool nonGeoTagged);
    bool nonGeoTagged() const;

    void setRequiredTags(const QVariant &tags);
    QVariant requiredTags() const;

    void setForbiddenTags(const QVariant &tags);
    QVariant forbiddenTags() const;

    void setAreaTagged(bool areaTagged);
    bool areaTagged() const;

    void setExcludedPhotos(const QVariant &photos);
    QVariant excludedPhotos() const;

    void setSkipNonDefaultVersions(bool skip);
    bool skipNonDefaultVersions() const;

    void setPhotoIds(const QList<PhotoId> &photoIds);
    QList<PhotoId> photoIds() const;

    void setPhotoVersions(PhotoId photoId);
    PhotoId photoVersions() const;

    void setSortOrder(Qt::SortOrder order);
    Qt::SortOrder sortOrder() const;

    // For TimeModel only
    QList<Database::Photo> photos() const;

    Q_INVOKABLE int firstIndex(TimeOrder order, const QDateTime &time) const;
    Q_INVOKABLE int indexOfItem(PhotoId photoId) const;
    Q_INVOKABLE void deletePhotos(const QVariant &indexes);

    Q_INVOKABLE void forceItemRefresh(int row);

    Q_INVOKABLE QVariant get(int row, const QString &role) const;

    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index,
                  int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

    void classBegin() Q_DECL_OVERRIDE;
    void componentComplete() Q_DECL_OVERRIDE;

Q_SIGNALS:
    void countChanged();
    void time0Changed();
    void time1Changed();
    void roll0Changed();
    void roll1Changed();
    void rating0Changed();
    void rating1Changed();
    void location0Changed();
    void location1Changed();
    void nonGeoTaggedChanged();
    void requiredTagsChanged();
    void forbiddenTagsChanged();
    void areaTaggedChanged();
    void excludedPhotosChanged();
    void skipNonDefaultVersionsChanged();
    void photoIdsChanged();
    void photoVersionsChanged();
    void sortOrderChanged();

private:
    PhotoModelPrivate *d_ptr;
    Q_DECLARE_PRIVATE(PhotoModel)
};

} // namespace

Q_DECLARE_METATYPE(Imaginario::PhotoModel::TimeOrder)
Q_DECLARE_METATYPE(QMimeType)

#endif // IMAGINARIO_PHOTO_MODEL_H
