/*
 * Copyright (C) 2016-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "database.h"
#include "fspot_importer.h"
#include "metadata.h"

#include <QDebug>
#include <QDir>
#include <QFileInfo>
#include <QFutureWatcher>
#include <QMetaObject>
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlQuery>
#include <QStandardPaths>
#include <QtConcurrent>

using namespace Imaginario;

namespace Imaginario {

static int iconCount = 0;

class FspotImporterPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(FspotImporter)

public:
    FspotImporterPrivate(FspotImporter *q);
    ~FspotImporterPrivate();

    void setStatus(FspotImporter::Status status);
    inline void setProgress(double progress);
    bool checkDatabase();

    void setupIconDir();
    QString nextIconName();
    QString writeTagIcon(const QString &icon);
    bool importTags();
    bool importRolls();
    bool importPhotos();
    QList<Tag> importPhotoTags(int id, bool *pOk);
    void exec();

private:
    QSqlDatabase m_db;
    FspotImporter::Status m_status;
    int m_count;
    double m_progress;
    QString m_dbVersion;
    QString m_version;
    QDir m_iconDir;
    QHash<int,TagId> m_tagMap;
    QHash<int,RollId> m_rollMap;
    QFuture<void> m_execFuture;
    mutable FspotImporter *q_ptr;
};

} // namespace

FspotImporterPrivate::FspotImporterPrivate(FspotImporter *q):
    m_db(QSqlDatabase::addDatabase("QSQLITE", "fspot")),
    m_status(FspotImporter::Initializing),
    m_count(-1),
    m_progress(0.0),
    q_ptr(q)
{
    qRegisterMetaType<FspotImporter::Status>("FspotImporter::Status");
    if (checkDatabase()) {
        setStatus(FspotImporter::Ready);
    } else {
        setStatus(FspotImporter::Failed);
    }
}

FspotImporterPrivate::~FspotImporterPrivate()
{
    QString name = m_db.connectionName();
    m_db = QSqlDatabase();
    QSqlDatabase::removeDatabase(name);
}

void FspotImporterPrivate::setStatus(FspotImporter::Status status)
{
    Q_Q(FspotImporter);

    if (m_status == status) return;
    m_status = status;
    Q_EMIT q->statusChanged(status);
    m_progress = 0.0;
    Q_EMIT q->progressChanged();
}

void FspotImporterPrivate::setProgress(double progress)
{
    Q_Q(FspotImporter);
    m_progress = progress;
    Q_EMIT q->progressChanged();
}

bool FspotImporterPrivate::checkDatabase()
{
    QString dbName =
        QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) +
        "/f-spot/photos.db";
    m_db.setDatabaseName(dbName);
    if (!m_db.open()) return false;

    QSqlQuery q(m_db);
    q.exec("SELECT data FROM meta WHERE name = 'F-Spot Version'");
    if (Q_UNLIKELY(!q.next())) return false;
    m_version = q.value(0).toString();

    q.exec("SELECT data FROM meta WHERE name = 'F-Spot Database Version'");
    if (Q_UNLIKELY(!q.next())) return false;

    m_dbVersion = q.value(0).toString();
    if (m_dbVersion < "17.1") return false;

    q.exec("SELECT COUNT(*) FROM photos");
    if (Q_UNLIKELY(!q.next())) return false;
    m_count = q.value(0).toInt();

    m_db.close();
    return true;
}

void FspotImporterPrivate::setupIconDir()
{
    QString base =
        QStandardPaths::writableLocation(QStandardPaths::DataLocation);
    m_iconDir = base + QStringLiteral("/tag_icons");
    if (!m_iconDir.exists()) {
        m_iconDir.mkpath(".");
    }

    iconCount = 0;
}

QString FspotImporterPrivate::nextIconName()
{
    return QString("Imported-%1.png").arg(iconCount++);
}

QString FspotImporterPrivate::writeTagIcon(const QString &icon)
{
    QString ret;

    if (icon.startsWith("stock_icon:")) {
        return icon;
    } else {
        // From gdk-pixbuf/gdk-pixdata.h:
        enum GdkPixdataType {
            /* colorspace + alpha */
            GDK_PIXDATA_COLOR_TYPE_RGB    = 0x01,
            GDK_PIXDATA_COLOR_TYPE_RGBA   = 0x02,
            GDK_PIXDATA_COLOR_TYPE_MASK   = 0xff,
            /* width, support 8bits only currently */
            GDK_PIXDATA_SAMPLE_WIDTH_8    = 0x01 << 16,
            GDK_PIXDATA_SAMPLE_WIDTH_MASK = 0x0f << 16,
            /* encoding */
            GDK_PIXDATA_ENCODING_RAW      = 0x01 << 24,
            GDK_PIXDATA_ENCODING_RLE      = 0x02 << 24,
            GDK_PIXDATA_ENCODING_MASK     = 0x0f << 24
        };

        QByteArray iconData = QByteArray::fromBase64(icon.toLatin1());
        QDataStream in(&iconData, QIODevice::ReadOnly);
        int32_t magic, length, type, rowStride, width, height;
        in >> magic;
        if (magic != 0x47646b50) { // GdkP
            return ret;
        }
        in >> length >> type >> rowStride >> width >> height;
        int offset = in.device()->pos();
        int colorType = type & GDK_PIXDATA_COLOR_TYPE_MASK;
        int sampleWidth = type & GDK_PIXDATA_SAMPLE_WIDTH_MASK;
        int encoding = type & GDK_PIXDATA_ENCODING_MASK;
        if (sampleWidth != GDK_PIXDATA_SAMPLE_WIDTH_8 ||
            (encoding != GDK_PIXDATA_ENCODING_RAW &&
             encoding != GDK_PIXDATA_ENCODING_RLE) ||
            (colorType != GDK_PIXDATA_COLOR_TYPE_RGB &&
             colorType != GDK_PIXDATA_COLOR_TYPE_RGBA)) {
            qWarning() << "Wrong format for tag icon";
            return ret;
        }
        auto format = colorType == GDK_PIXDATA_COLOR_TYPE_RGB ?
            QImage::Format_RGB888 : QImage::Format_ARGB32;
        const uchar *bitmapData;
        uchar *data = 0;
        if (encoding == GDK_PIXDATA_ENCODING_RLE) {
            data = new uchar[height * rowStride];
            int bpp = (colorType == GDK_PIXDATA_COLOR_TYPE_RGB) ? 3 : 4;
            const uchar *rle_buffer = (const uchar*)iconData.constData() + offset;
            uchar *image_buffer = data;
            uchar *image_limit = data + rowStride * height;
            bool check_overrun = false;

            while (image_buffer < image_limit) {
                uint length = *(rle_buffer++);

                if (length & 128) {
                    length = length - 128;
                    check_overrun = (image_buffer + length * bpp) > image_limit;
                    if (check_overrun) {
                        length = (image_limit - image_buffer) / bpp;
                    }
                    do {
                        memcpy (image_buffer, rle_buffer, bpp);
                        image_buffer += bpp;
                    } while (--length);
                    rle_buffer += bpp;
                } else {
                    length *= bpp;
                    check_overrun = image_buffer + length > image_limit;
                    if (check_overrun)
                        length = image_limit - image_buffer;
                    memcpy (image_buffer, rle_buffer, length);
                    image_buffer += length;
                    rle_buffer += length;
                }
            }
            if (check_overrun) {
                qWarning() << "Image data probably corrupt";
            }

            bitmapData = data;
        } else {
            bitmapData = (const uchar*)iconData.constData() + offset;
        }
        QImage image(bitmapData, width, height, rowStride, format);
        QString fileName = nextIconName();
        image.save(m_iconDir.filePath(fileName));
        ret = "tag_icon:" + fileName;
        delete[] data;
    }
    return ret;
}

bool FspotImporterPrivate::importTags()
{
    setStatus(FspotImporter::ImportingTags);

    setupIconDir();

    QSqlQuery q(m_db);
    q.setForwardOnly(true);
    bool ok = q.exec("SELECT id, name, category_id, sort_priority, icon "
                     "FROM tags");
    if (Q_UNLIKELY(!ok)) return false;

    Database *db = Database::instance();
    QSet<int> incompleteTags;

    while (q.next()) {
        int id = q.value(0).toInt();
        QString name = q.value(1).toString();
        int parentId = q.value(2).toInt();
        //int priority = q.value(3).toInt();
        QString icon = writeTagIcon(q.value(4).toString());

        Tag existingTag = db->tag(name);
        if (Q_UNLIKELY(existingTag.isValid())) {
            m_tagMap.insert(id, existingTag.id());
            continue;
        }

        Tag parentTag = db->uncategorizedTag();
        if (parentId != 0) {
            /* We have a parent; check if we already created it */
            const auto i = m_tagMap.find(parentId);
            if (i != m_tagMap.constEnd()) {
                parentTag = Tag(i.value());
            } else {
                parentTag = db->createTag(parentTag,
                                          QString("f-spot #%1").arg(parentId),
                                          QString());
                if (Q_UNLIKELY(!parentTag.isValid())) return false;
                incompleteTags.insert(parentId);
                m_tagMap.insert(parentId, parentTag.id());
            }
        }

        auto it = incompleteTags.find(id);
        if (it != incompleteTags.end()) {
            /* The tag has already been created, with bogus data.
             * Let's update it with the correct data. */
            Database::TagUpdate tagData {
                name,
                icon,
                QVariant::fromValue(parentTag)
            };
            ok = db->updateTag(Tag(m_tagMap[id]), tagData);
            if (Q_UNLIKELY(!ok)) return false;
            incompleteTags.erase(it);
        } else {
            Tag tag = db->createTag(parentTag, name, icon);
            if (Q_UNLIKELY(!tag.isValid())) return false;
            m_tagMap.insert(id, tag.id());
        }
    }
    return true;
}

bool FspotImporterPrivate::importRolls()
{
    setStatus(FspotImporter::ImportingRolls);

    QSqlQuery q(m_db);
    q.setForwardOnly(true);
    bool ok = q.exec("SELECT id, time FROM rolls");
    if (Q_UNLIKELY(!ok)) return false;

    Database *db = Database::instance();

    while (q.next()) {
        int id = q.value(0).toInt();
        QVariant timeV = q.value(1);
        QDateTime time = (timeV.toInt() != -1) ?
            QDateTime::fromTime_t(timeV.toUInt()) : QDateTime();

        RollId rollId = db->createRoll(time);
        if (Q_UNLIKELY(rollId < 0)) return false;

        m_rollMap.insert(id, rollId);
    }

    return true;
}

bool FspotImporterPrivate::importPhotos()
{
    setStatus(FspotImporter::ImportingPhotos);

    QSqlQuery q(m_db);
    q.setForwardOnly(true);
    bool ok = q.exec("SELECT p.id, p.time, v.base_uri, v.filename,"
                     " p.description, v.name, p.roll_id, p.rating,"
                     " v.version_id, p.default_version_id "
                     "FROM photo_versions AS v JOIN photos AS p "
                     "ON p.id = v.photo_id "
                     "ORDER BY p.id");
    if (Q_UNLIKELY(!ok)) return false;

    Database *db = Database::instance();
    Metadata metadata;

    QHash<int,PhotoId> versions;
    int lastId = -1;

    int importedPhotos = 0;
    while (q.next()) {
        importedPhotos++;
        if (importedPhotos % 10 == 0) {
            setProgress(double(importedPhotos) / m_count);
        }

        int i = 0;
        Database::Photo p;
        int id = q.value(i++).toInt();
        QVariant timeV = q.value(i++);
        if (timeV.toInt() != -1) {
            p.setTime(QDateTime::fromTime_t(timeV.toUInt()));
        }
        p.setBaseUri(q.value(i++).toString());
        p.setFileName(q.value(i++).toString());
        QString description = q.value(i++).toString();
        QString name = q.value(i++).toString();
        int impRollId = q.value(i++).toInt();
        p.setRating(q.value(i++).toInt());
        int versionId = q.value(i++).toInt();
        int defaultVersionId = q.value(i++).toInt();

        Metadata::ImportData data;
        if (metadata.readImportData(p.filePath(), data)) {
            p.setLocation(data.location);
        }

        const auto ri = m_rollMap.find(impRollId);
        RollId rollId;
        if (Q_UNLIKELY(ri == m_rollMap.constEnd())) {
            qWarning() << "roll Id not found" << impRollId;
            rollId = 0;
        } else {
            rollId = ri.value();
        }

        bool isDefault = versionId == defaultVersionId;
        if (isDefault) {
            p.setDescription(description);
        } else {
            p.setDescription(name);
        }

        PhotoId photoId = db->addPhoto(p, rollId);
        if (Q_UNLIKELY(photoId < 0)) return false;

        if (id != lastId) {
            versions.clear();
            lastId = id;
        }

        if (!isDefault) {
            const auto vi = versions.find(defaultVersionId);
            if (vi != versions.constEnd()) {
                PhotoId masterId = vi.value();
                ok = db->makeVersion(photoId, masterId);
                if (Q_UNLIKELY(!ok)) return false;
            } else {
                /* We'll do it when we find the master */
                versions.insert(versionId, photoId);
            }
        } else {
            /* Master: make any version which was found before */
            for (PhotoId v: versions) {
                ok = db->makeVersion(v, photoId);
                if (Q_UNLIKELY(!ok)) return false;
            }
            versions.insert(versionId, photoId);
        }

        QList<Tag> tags = importPhotoTags(id, &ok);
        if (Q_UNLIKELY(!ok)) return false;

        ok = db->addTags(photoId, tags);
        if (Q_UNLIKELY(!ok)) return false;
    }

    return true;
}

QList<Tag> FspotImporterPrivate::importPhotoTags(int id, bool *pOk)
{
    QList<Tag> tags;

    QSqlQuery q(m_db);
    q.setForwardOnly(true);
    bool ok = q.exec(QString("SELECT tag_id FROM photo_tags "
                             "WHERE photo_id = %1").arg(id));
    if (Q_UNLIKELY(!ok)) {
        *pOk = false;
        return tags;
    }

    while (q.next()) {
        int tagId = q.value(0).toInt();
        Tag tag(m_tagMap.value(tagId, -1));
        if (Q_UNLIKELY(!tag.isValid())) {
            qWarning() << "tag Id not found" << tagId;
        } else {
            tags.append(tag);
        }
    }

    *pOk = true;
    return tags;
}

void FspotImporterPrivate::exec()
{
    if (!m_db.open()) return;

    m_tagMap.clear();
    m_rollMap.clear();

    Database *db = Database::instance();
    db->transaction();

    bool ok = importTags();
    if (Q_UNLIKELY(!ok)) {
        db->rollback();
        setStatus(FspotImporter::Failed);
        return;
    }

    ok = importRolls();
    if (Q_UNLIKELY(!ok)) {
        db->rollback();
        setStatus(FspotImporter::Failed);
        return;
    }

    ok = importPhotos();
    if (Q_UNLIKELY(!ok)) {
        db->rollback();
        setStatus(FspotImporter::Failed);
        return;
    }

    m_db.close();

    db->commit();
    setStatus(FspotImporter::Done);
}

FspotImporter::FspotImporter(QObject *parent):
    QObject(parent),
    d_ptr(new FspotImporterPrivate(this))
{
}

FspotImporter::~FspotImporter()
{
    delete d_ptr;
}

QString FspotImporter::dbVersion() const
{
    Q_D(const FspotImporter);
    return d->m_dbVersion;
}

QString FspotImporter::version() const
{
    Q_D(const FspotImporter);
    return d->m_version;
}

FspotImporter::Status FspotImporter::status() const
{
    Q_D(const FspotImporter);
    return d->m_status;
}

double FspotImporter::progress() const
{
    Q_D(const FspotImporter);
    return d->m_progress;
}

void FspotImporter::exec()
{
    Q_D(FspotImporter);
    d->m_execFuture = QtConcurrent::run(d, &FspotImporterPrivate::exec);
}

int FspotImporter::count() const
{
    Q_D(const FspotImporter);
    return d->m_count;
}

#include "fspot_importer.moc"
