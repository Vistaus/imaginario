/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "abstract_database_p.h"
#include "job_database.h"

#include <QDebug>
#include <QHash>
#include <QJsonDocument>
#include <QJsonObject>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlField>

#define LATEST_DB_VERSION 3

using namespace Imaginario;

namespace Imaginario {

const QString queryUpdateJob =
    QStringLiteral("UPDATE jobs SET data=:data WHERE id =:jobId");
const QString queryWriteJob =
    QStringLiteral("INSERT INTO jobs"
                   " (type, in_progress, uniqueKey, data) "
                   "VALUES (:type, 0, :key, :data)");
const QString queryStartJob("UPDATE jobs SET in_progress = 1 WHERE id = ?");
const QString queryDeleteJob("DELETE FROM jobs WHERE id = ?");
const QString queryFirst("SELECT id, type, in_progress, uniqueKey, data "
                         "FROM jobs ORDER BY id LIMIT 1");
const QString queryNext("SELECT id, type, in_progress, uniqueKey, data "
                        "FROM jobs WHERE id > ? ORDER BY id LIMIT 1");

class JobDatabasePrivate: public AbstractDatabasePrivate
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(JobDatabase)

public:
    JobDatabasePrivate(JobDatabase *q);
    ~JobDatabasePrivate();
    bool init();

    bool createDb() Q_DECL_OVERRIDE;
    bool updateFrom(int version) Q_DECL_OVERRIDE;

    Job findJob(const QString &uniqueKey);
    bool addJob(const Job &job);

private:
};

} // namespace

JobDatabasePrivate::JobDatabasePrivate(JobDatabase *q):
    AbstractDatabasePrivate(q, "jobs.db", "jobconn")
{
    if (Q_UNLIKELY(!init())) {
        qWarning() << "DB init failed";
    }
}

JobDatabasePrivate::~JobDatabasePrivate()
{
}

bool JobDatabasePrivate::createDb()
{
    exec("CREATE TABLE jobs ("
         " id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
         " type INTEGER NOT NULL,"
         " uniqueKey TEXT NOT NULL,"
         " in_progress INTEGER NOT NULL,"
         " data TEXT,"
         "UNIQUE (uniqueKey, in_progress)"
         ")");
    if (Q_UNLIKELY(db().lastError().isValid())) return false;

    return true;
}

bool JobDatabasePrivate::updateFrom(int version)
{
    if (version == 1 || version == 2) {
        /* Ideally, we would migrate the data here. But given the small size of
         * our userbase and the likelyhood of this table being empty, laziness
         * wins.
         */
        exec("DROP TABLE IF EXISTS photo_jobs");
        if (Q_UNLIKELY(db().lastError().isValid())) return false;
        exec("DROP TABLE IF EXISTS jobs");
        if (Q_UNLIKELY(db().lastError().isValid())) return false;

        if (Q_UNLIKELY(!createDb())) return false;
    }

    if (!setDbVersion(LATEST_DB_VERSION)) return false;

    return true;
}

bool JobDatabasePrivate::init()
{
    bool ok = AbstractDatabasePrivate::init(LATEST_DB_VERSION);
    if (Q_UNLIKELY(!ok)) return false;

    return ok;
}

Job JobDatabasePrivate::findJob(const QString &uniqueKey)
{
    QString query =
        QString("SELECT id, type, in_progress, uniqueKey, data FROM jobs "
                "WHERE uniqueKey = '%1' AND in_progress = 0").
        arg(uniqueKey);
    QSqlQuery q(db());
    q.setForwardOnly(true);
    q.exec(query);
    if (q.next()) {
        QJsonDocument json = QJsonDocument::fromJson(q.value(4).toByteArray());
        return Job(q.value(0).toInt(),
                   q.value(1).toInt(),
                   q.value(2).toInt(),
                   q.value(3).toString(),
                   json.object().toVariantMap());
    } else {
        return Job();
    }
}

bool JobDatabasePrivate::addJob(const Job &job)
{
    QString uniqueKey = job.uniqueKey();
    Job old = findJob(uniqueKey);


    const QString &queryText = old.isValid() ? queryUpdateJob : queryWriteJob;
    QSqlQuery q(db());
    q.prepare(queryText);
    q.bindValue(":type", int(job.type()));
    q.bindValue(":jobId", old.id());
    q.bindValue(":key", uniqueKey);
    QVariantMap data(old.data());
    data.unite(job.data());
    if (data.isEmpty()) {
        q.bindValue(":data", QString());
    } else {
        QJsonDocument doc(QJsonObject::fromVariantMap(data));
        q.bindValue(":data", doc.toJson(QJsonDocument::Compact));
    }

    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "DB error on photo:" << q.executedQuery() << q.lastError();
        return false;
    }

    return true;
}

JobDatabase::JobDatabase(QObject *parent):
    AbstractDatabase(new JobDatabasePrivate(this), parent)
{
}

JobDatabase::~JobDatabase()
{
}

bool JobDatabase::addJobs(const QVector<Job> &jobs)
{
    Q_D(JobDatabase);

    Q_FOREACH(const Job &job, jobs) {
        bool ok = d->addJob(job);
        if (Q_UNLIKELY(!ok)) return false;
    }
    return true;
}

bool JobDatabase::markJobInProgress(int jobId)
{
    Q_D(JobDatabase);
    QSqlQuery q(d->db());
    q.prepare(queryStartJob);

    q.addBindValue(jobId);
    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "DB error starting job" << q.lastError();
        return false;
    }

    return true;
}

bool JobDatabase::removeJob(int jobId)
{
    Q_D(JobDatabase);
    QSqlQuery q(d->db());
    q.prepare(queryDeleteJob);

    q.addBindValue(jobId);
    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "DB error removing job" << q.lastError();
        return false;
    }

    return true;
}

bool JobDatabase::next(Job &job)
{
    Q_D(JobDatabase);

    QSqlQuery q(d->db());
    bool ok = false;
    if (job.isValid()) {
        q.prepare(queryNext);
        q.addBindValue(job.id());
        ok = q.exec();
    } else {
        ok = q.exec(queryFirst);
    }
    if (Q_UNLIKELY(!ok)) {
        qWarning() << "DB error fetching next job" << q.lastError();
        return false;
    }

    if (q.next()) {
        QJsonDocument json = QJsonDocument::fromJson(q.value(4).toByteArray());
        job = Job(q.value(0).toInt(),
                  q.value(1).toInt(),
                  q.value(2).toInt(),
                  q.value(3).toString(),
                  json.object().toVariantMap());
    } else {
        job = Job();
    }
    return job.isValid();
}

#include "job_database.moc"
