/*
 * Copyright (C) 2016-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGINARIO_ROLL_H
#define IMAGINARIO_ROLL_H

#include "types.h"

#include <QDateTime>
#include <QObject>

namespace Imaginario {

class Roll {
#if (QT_VERSION >= QT_VERSION_CHECK(5, 5, 0))
    Q_GADGET
    Q_PROPERTY(int rollId READ id CONSTANT)
    Q_PROPERTY(QDateTime time READ time CONSTANT)
#endif
public:
    Roll(): m_id(-1) {}
    explicit Roll(RollId id): m_id(id) {}
    RollId id() const { return m_id; }
    bool isValid() const { return m_id >= 0; }
    QDateTime time() const;

    bool operator==(const Roll other) const { return m_id == other.m_id; }
    bool operator<(const Roll other) const { return m_id < other.m_id; }

private:
    RollId m_id;
};

} // namespace

Q_DECLARE_METATYPE(Imaginario::Roll)

#endif // IMAGINARIO_ROLL_H
