/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGINARIO_IMAGE_PROVIDER_H
#define IMAGINARIO_IMAGE_PROVIDER_H

#include <QQuickImageProvider>

namespace Imaginario {

class ImageProviderPrivate;
class ImageProvider: public QQuickImageProvider
{
public:
    ImageProvider();
    ~ImageProvider();

    QImage requestImage(const QString &id,
                        QSize *size,
                        const QSize &requestedSize) Q_DECL_OVERRIDE;

private:
    ImageProviderPrivate *d_ptr;
    Q_DECLARE_PRIVATE(ImageProvider)
};

} // namespace

#endif // IMAGINARIO_IMAGE_PROVIDER_H
