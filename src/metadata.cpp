/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "metadata.h"

#include <QDebug>
#include <QFileInfo>
#include <QPointF>
#include <QSize>
#include <exiv2/exiv2.hpp>
#include <math.h>
#include <sys/stat.h>
#include <type_traits>
#ifdef Q_CC_MSVC
#define _CRT_DECLARE_NONSTDC_NAMES 1
#include <sys/utime.h>
#else
#include <utime.h>
#endif

using namespace Imaginario;

namespace Imaginario {

static inline QString cleanText(const std::string &s)
{
    return QString::fromStdString(s).remove(QChar::Null);
}

/* The ExifData, IptcData and XmpData lack a common ancestor with virtual
 * methods; yet, their API is very consistent, so we can access them with
 * template functions. */
struct KeyTypeMapping {
    Exiv2::IptcKey operator()(const Exiv2::IptcData &);
    Exiv2::ExifKey operator()(const Exiv2::ExifData &);
    Exiv2::XmpKey operator()(const Exiv2::XmpData &);
};

struct DatumTypeMapping {
    Exiv2::Iptcdatum operator()(const Exiv2::IptcData &);
    Exiv2::Exifdatum operator()(const Exiv2::ExifData &);
    Exiv2::Xmpdatum operator()(const Exiv2::XmpData &);
};

template <typename DataType>
const Exiv2::Value *readValueT(DataType &data, const std::string &key)
{
    typedef typename std::result_of<KeyTypeMapping(DataType)>::type KeyType;
    const auto i = data.findKey(KeyType(key));
    return (i != data.end()) ? &i->value() : nullptr;
}

template <typename DataType>
void clearKeyT(DataType &data, const std::string &key)
{
    typedef typename std::result_of<KeyTypeMapping(DataType)>::type KeyType;
    auto i = data.findKey(KeyType(key));
    while (i != data.end() && !i->key().compare(0, key.size(), key)) {
        i = data.erase(i);
    }
}

template <typename DataType>
QStringList stringValuesT(DataType &data, const std::string &key)
{
    typedef typename std::result_of<KeyTypeMapping(DataType)>::type KeyType;
    QStringList values;
    for (auto i = data.findKey(KeyType(key));
         i != data.end() && i->key() == key;
         i++) {
        values.append(cleanText(i->value().toString(0)));
    }
    return values;
}

static Exiv2::Metadatum &getWritableDatum(Exiv2::Image &image,
                                          const std::string &key)
{
    if (key[0] == 'E') {
        return image.exifData()[key];
    } else if (key[0] == 'I') {
        return image.iptcData()[key];
    } else { // assume XMP
        return image.xmpData()[key];
    }
}

static inline const Exiv2::Value *readValue(const Exiv2::Image &image,
                                            const std::string &key)
{
    if (key[0] == 'E') {
        return readValueT(image.exifData(), key);
    } else if (key[0] == 'I') {
        return readValueT(image.iptcData(), key);
    } else { // assume XMP
        return readValueT(image.xmpData(), key);
    }
}

static const Exiv2::Value *lookUp(const Exiv2::Image &image,
                                  const char * const *keys)
{
    for (; *keys != NULL; keys++) {
        const char *key = *keys;
        const Exiv2::Value *value = readValue(image, key);
        if (value) return value;
    }

    return 0;
}

static void clearKey(Exiv2::Image &image, const std::string &key)
{
    if (key[0] == 'X') {
        clearKeyT(image.xmpData(), key);
    } else if (key[0] == 'E') {
        clearKeyT(image.exifData(), key);
    } else if (key[0] == 'I') {
        clearKeyT(image.iptcData(), key);
    }
}

static void clearKeys(Exiv2::Image &image, const char * const *keys)
{
    for (; *keys != NULL; keys++) {
        clearKey(image, *keys);
    }
}

static inline QString title(const Exiv2::Image &image)
{
    static const char *const keys[] = {
        "Xmp.dc.title",
        "Iptc.Application2.ObjectName",
        "Iptc.Application2.Subject",
        NULL
    };

    const Exiv2::Value *v = lookUp(image, keys);
    return v ? cleanText(v->toString(0)) : QString();
}

static inline QString description(const Exiv2::Image &image)
{
    static const char *const keys[] = {
        "Xmp.dc.description",
        "Exif.Photo.UserComment",
        "Exif.Image.ImageDescription",
        NULL
    };

    const Exiv2::Value *v = lookUp(image, keys);
    const auto *vComment = dynamic_cast<const Exiv2::CommentValue*>(v);
    if (vComment && vComment->charsetId() == Exiv2::CommentValue::invalidCharsetId) {
        return QString();
    }
    return v ? cleanText(vComment ? vComment->comment() : v->toString(0)) : QString();
}

static inline QDateTime creationData(const Exiv2::Image &image)
{
    static const char *const keys[] = {
        "Exif.Photo.DateTimeOriginal",
        "Xmp.exif.DateTimeOriginal",
        "Xmp.xmp.CreateDate",
        "Exif.Photo.DateTimeDigitized",
        "Xmp.exif.DateTimeDigitized",
        "Exif.Image.DateTime",
        "Xmp.tiff.DateTime",
        NULL
    };

    const Exiv2::Value *v = lookUp(image, keys);
    if (v) {
        QString timeString = QString::fromStdString(v->toString());
        return timeString.contains('-') ?
            QDateTime::fromString(timeString, Qt::ISODate) :
            QDateTime::fromString(timeString, "yyyy:MM:dd HH:mm:ss");
    } else {
        QFileInfo fileInfo(QString::fromStdString(image.io().path()));
        return fileInfo.created();
    }
}

static inline QStringList keywords(const Exiv2::Image &image)
{
    QStringList keywords;

    static const char *const keys[] = {
        "Xmp.dc.subject",
        NULL
    };

    const Exiv2::Value *v = lookUp(image, keys);
    if (!v) return keywords;

    int n = v->count();
    for (int j = 0; j < n; j++) {
        keywords.append(QString::fromStdString(v->toString(j)));
    }

    return keywords;
}

static inline int rating(const Exiv2::Image &image)
{
    static const char *const keys[] = {
        "Xmp.xmp.Rating",
        NULL
    };
    const Exiv2::Value *v = lookUp(image, keys);
    return v ? int(v->toLong()) : -1;
}

static inline Metadata::Regions readRegions(const Exiv2::Image &image)
{
    Metadata::Regions regions;

    const Exiv2::XmpData &xmpData = image.xmpData();

    int width = -1;
    int height = -1;
    Exiv2::XmpData::const_iterator i =
        xmpData.findKey(Exiv2::XmpKey("Xmp.mwg-rs.Regions/mwg-rs:AppliedToDimensions/stDim:w"));
    if (i != xmpData.end()) {
        width = i->value().toLong();
        i = xmpData.findKey(Exiv2::XmpKey("Xmp.mwg-rs.Regions/mwg-rs:AppliedToDimensions/stDim:h"));
        if (i != xmpData.end()) height = i->value().toLong();
    }

    if (width > 0 && height > 0 &&
        (width != image.pixelWidth() || height != image.pixelHeight())) {
        return regions;
    }

    /* Set C locale, or libexiv2 might fail to parse the floats */
    char oldLocale[256];
    strncpy(oldLocale, setlocale(LC_NUMERIC, NULL), sizeof(oldLocale));
    setlocale(LC_NUMERIC, "C");

    for (int j = 1; ; j++) {
        std::string prefix = "Xmp.mwg-rs.Regions/mwg-rs:RegionList[" +
            std::to_string(j) + "]/mwg-rs:";
        i = xmpData.findKey(Exiv2::XmpKey(prefix + "Name"));
        if (i == xmpData.end()) break;

        Metadata::RegionInfo r;

        r.name = cleanText(i->value().toString());
        i = xmpData.findKey(Exiv2::XmpKey(prefix + "Description"));
        if (i != xmpData.end()) {
            r.description = cleanText(i->value().toString());
        }

        i = xmpData.findKey(Exiv2::XmpKey(prefix + "Area/stArea:x"));
        if (i == xmpData.end()) break;
        qreal x = i->value().toFloat();
        i = xmpData.findKey(Exiv2::XmpKey(prefix + "Area/stArea:y"));
        if (i == xmpData.end()) break;
        qreal y = i->value().toFloat();
        i = xmpData.findKey(Exiv2::XmpKey(prefix + "Area/stArea:w"));
        if (i == xmpData.end()) break;
        qreal w = i->value().toFloat();
        i = xmpData.findKey(Exiv2::XmpKey(prefix + "Area/stArea:h"));
        if (i == xmpData.end()) break;
        qreal h = i->value().toFloat();

        r.area = QRectF(x - w / 2, y - h / 2, w, h);
        regions.append(r);
    }
    setlocale(LC_NUMERIC, oldLocale);

    return regions;
}

static void writeRegions(Exiv2::Image &image,
                         const Metadata::Regions &regions)
{
    static const char *const keys[] = {
        "Xmp.mwg-rs.Regions/mwg-rs:RegionList",
        NULL
    };
    clearKeys(image, keys);

    Exiv2::XmpData &xmpData = image.xmpData();
    xmpData["Xmp.mwg-rs.Regions/mwg-rs:AppliedToDimensions/stDim:w"] =
        std::to_string(image.pixelWidth());
    xmpData["Xmp.mwg-rs.Regions/mwg-rs:AppliedToDimensions/stDim:h"] =
        std::to_string(image.pixelHeight());
    xmpData["Xmp.mwg-rs.Regions/mwg-rs:AppliedToDimensions/stDim:unit"] =
        "pixel";
    // Open the region list
    xmpData["Xmp.mwg-rs.Regions/mwg-rs:RegionList"] = "";

    /* Set C locale, or libexiv2 might fail to parse the floats */
    char oldLocale[256];
    strncpy(oldLocale, setlocale(LC_NUMERIC, NULL), sizeof(oldLocale));
    setlocale(LC_NUMERIC, "C");

    int i = 1;
    Q_FOREACH(const Metadata::RegionInfo &ri, regions) {
        std::string prefix = "Xmp.mwg-rs.Regions/mwg-rs:RegionList[" +
            std::to_string(i++) + "]/mwg-rs:";
        xmpData[prefix + "Name"] = ri.name.toStdString();
        xmpData[prefix + "Description"] = ri.description.toStdString();
        xmpData[prefix + "Type"] = "Face";
        QPointF c = ri.area.center();
        xmpData[prefix + "Area/stArea:x"] = std::to_string(c.x());
        xmpData[prefix + "Area/stArea:y"] = std::to_string(c.y());
        xmpData[prefix + "Area/stArea:w"] = std::to_string(ri.area.width());
        xmpData[prefix + "Area/stArea:h"] = std::to_string(ri.area.height());
        xmpData[prefix + "Area/stArea:unit"] = "normalized";
    }
    setlocale(LC_NUMERIC, oldLocale);
}

struct GeoCoord {
    GeoCoord(const Exiv2::Value &v): positive(true) {
        int count = v.count();
        coord[0] = count > 0 ? v.toFloat(0) : NAN;
        coord[1] = count > 1 ? v.toFloat(1) : 0;
        coord[2] = count > 2 ? v.toFloat(2) : 0;
    }
    GeoCoord(const QStringList &parts): positive(true) {
        int count = parts.count();
        coord[0] = count > 0 ? parts[0].toFloat() : NAN;
        coord[1] = count > 1 ? parts[1].toFloat() : 0;
        coord[2] = count > 2 ? parts[2].toFloat() : 0;
    }
    void setPositive(bool p) { positive = p; }
    qreal toGeo() {
        qreal sum = coord[0] + coord[1] / 60.0 + coord[2] / 3600.0;
        return positive ? sum : -sum;
    }
private:
    qreal coord[3];
    bool positive;
};

static qreal parseXmpGeoCoord(const QString &s)
{
    if (s.isEmpty()) return 200.0;
    QChar reference = s.at(s.length() - 1).toUpper();
    QStringList coords = s.left(s.length() -1).split(',');
    GeoCoord c(coords);
    if (reference == 'W' || reference == 'S') {
        c.setPositive(false);
    }
    return c.toGeo();
}

static std::string geoToXmp(qreal geo, Qt::Orientation orientation)
{
    char direction;

    /* Vertical orientation means latitude, horizonal means longitude */
    if (geo < 0) {
        geo = -geo;
        direction = (orientation == Qt::Vertical) ? 'S' : 'W';
    } else {
        direction = (orientation == Qt::Vertical) ? 'N' : 'E';
    }

    int degrees = floor(geo);
    double minutes = (geo - degrees) * 60.0;

    QString coord = "%1,%2%3";
    coord = coord.arg(degrees).arg(minutes, 0, 'f', 8).arg(direction);
    return coord.toStdString();
}

static Exiv2::URationalValue geoToExif(qreal geo)
{
    qreal coord = qAbs(geo);

    int degrees = floor(coord);
    coord -= degrees;
    coord *= 60;
    int minutes = floor(coord);
    coord -= minutes;
    int seconds = coord * 3600 * 100;

    Exiv2::URationalValue value;
    value.value_.push_back(std::make_pair(degrees, 1));
    value.value_.push_back(std::make_pair(minutes, 1));
    value.value_.push_back(std::make_pair(seconds, 6000));
    return value;
}

static inline QGeoCoordinate readExifGeoLocation(const Exiv2::ExifData &exifData)
{
    Exiv2::ExifData::const_iterator i =
        exifData.findKey(Exiv2::ExifKey("Exif.GPSInfo.GPSLatitude"));
    if (i == exifData.end()) return QGeoCoordinate();
    GeoCoord lat(i->value());

    i = exifData.findKey(Exiv2::ExifKey("Exif.GPSInfo.GPSLatitudeRef"));
    if (i != exifData.end() &&
        i->value().toString().compare(0, 1, "S") == 0) {
        lat.setPositive(false);
    }

    i = exifData.findKey(Exiv2::ExifKey("Exif.GPSInfo.GPSLongitude"));
    if (i == exifData.end()) return QGeoCoordinate();
    GeoCoord lon(i->value());

    i = exifData.findKey(Exiv2::ExifKey("Exif.GPSInfo.GPSLongitudeRef"));
    if (i != exifData.end() &&
        i->value().toString().compare(0, 1, "W") == 0) {
        lon.setPositive(false);
    }

    return QGeoCoordinate(lat.toGeo(), lon.toGeo());
}

static inline QGeoCoordinate readGeoLocation(const Exiv2::Image &image)
{
    const Exiv2::XmpData &xmpData = image.xmpData();
    Exiv2::XmpData::const_iterator i =
        xmpData.findKey(Exiv2::XmpKey("Xmp.exif.GPSLatitude"));
    if (i != xmpData.end()) {
        QString lat = QString::fromStdString(i->value().toString());
        i = xmpData.findKey(Exiv2::XmpKey("Xmp.exif.GPSLongitude"));
        if (i != xmpData.end()) {
            QString lon = QString::fromStdString(i->value().toString());
            return QGeoCoordinate(parseXmpGeoCoord(lat), parseXmpGeoCoord(lon));
        }
    }

    /* Else, read the location from the EXIF data */
    return readExifGeoLocation(image.exifData());
}

static inline void writeGeoLocation(Exiv2::Image &image, const QGeoCoordinate &p)
{
    Exiv2::XmpData &xmpData = image.xmpData();
    xmpData["Xmp.exif.GPSLatitude"] = geoToXmp(p.latitude(), Qt::Vertical);
    xmpData["Xmp.exif.GPSLongitude"] = geoToXmp(p.longitude(), Qt::Horizontal);

    /* For added compatibility, write also the EXIF tags */
    Exiv2::ExifData &exifData = image.exifData();
    exifData["Exif.GPSInfo.GPSLatitudeRef"] = p.latitude() >= 0 ? "N" : "S";
    exifData["Exif.GPSInfo.GPSLatitude"] = geoToExif(p.latitude());
    exifData["Exif.GPSInfo.GPSLongitudeRef"] = p.longitude() >= 0 ? "E" : "W";
    exifData["Exif.GPSInfo.GPSLongitude"] = geoToExif(p.longitude());
}

static void clearGeoLocation(Exiv2::Image &image)
{
    static const char *const keys[] = {
        "Xmp.exif.GPSLatitude",
        "Xmp.exif.GPSLongitude",
        "Exif.GPSInfo.GPSLatitude",
        "Exif.GPSInfo.GPSLatitudeRef",
        "Exif.GPSInfo.GPSLongitude",
        "Exif.GPSInfo.GPSLongitudeRef",
        NULL
    };
    clearKeys(image, keys);
}

static void writeField(Exiv2::Image &image,
                       const QString &key,
                       const QVariant &value)
{
    std::string keyStd = key.toStdString();
    clearKey(image, keyStd);

    switch (QMetaType::Type(value.type())) {

    case QMetaType::QString:
        {
            Exiv2::Metadatum &datum = getWritableDatum(image, keyStd);
            datum.setValue(value.toString().toStdString());
        }
        break;

    case QMetaType::QStringList:
        {
            QStringList list = value.toStringList();
            if (keyStd[0] == 'E') {
                qWarning() << "Exif specs does not allow string lists";
            } else if (keyStd[0] == 'I') {
                for (const QString &s: list) {
                    auto datum = Exiv2::Iptcdatum(Exiv2::IptcKey(keyStd));
                    datum.setValue(s.toStdString());
                    image.iptcData().add(datum);
                }
            } else {
                Q_FOREACH(const QString &s, list) {
                    image.xmpData()[keyStd] = s.toStdString();
                }
            }
        }
        break;

    default:
        qWarning() << Q_FUNC_INFO << "Unsupported type" << value.typeName();
    }
}

static Exiv2::Image::AutoPtr loadImage(const QString &file)
{
    Exiv2::Image::AutoPtr image;

    /* If an XMP sidecar file exists, read it */
    QString sidecar = file + ".xmp";
    try {
        image = Exiv2::ImageFactory::open(sidecar.toUtf8().constData());
        image->readMetadata();
    } catch (Exiv2::AnyError &e) {
        try {
            image = Exiv2::ImageFactory::open(file.toUtf8().constData());
            image->readMetadata();
        } catch (Exiv2::AnyError &e) {
            qDebug() << "Exiv2 exception, code" << e.code();
        }
    }

    return image;
}

class MetadataPrivate
{
public:
    MetadataPrivate(Metadata *q);
    ~MetadataPrivate();

    bool writeChanges(const QString &fileName,
                      const Metadata::Changes &changes);
private:
    friend class Metadata;
    bool m_embed;
};

} // namespace

MetadataPrivate::MetadataPrivate(Metadata *):
    m_embed(false)
{
}

MetadataPrivate::~MetadataPrivate()
{
}

static bool imageIsWritable(const QString &file)
{
    try {
        int type = Exiv2::ImageFactory::getType(file.toUtf8().constData());
        Exiv2::AccessMode mode =
            Exiv2::ImageFactory::checkMode(type, Exiv2::mdXmp);
        return mode == Exiv2::amWrite || mode == Exiv2::amReadWrite;
    } catch (Exiv2::AnyError &e) {
        qDebug() << "Exiv2 exception, code" << e.code();
        return false;
    }
}

static bool saveMetadata(Exiv2::Image *image)
{
    std::string fileName = image->io().path();
    struct stat statBefore;
    stat(fileName.c_str(), &statBefore);

    try {
        image->writeMetadata();
    } catch (Exiv2::AnyError &e) {
        qCritical() << "Exiv2 exception, code" << e.code();
        return false;
    }

    /* preserve modification time */
    struct stat statAfter;
    stat(fileName.c_str(), &statAfter);
    struct utimbuf times;
    times.actime = statAfter.st_atime;
    times.modtime = statBefore.st_mtime;
    utime(fileName.c_str(), &times);

    return true;
}

bool MetadataPrivate::writeChanges(const QString &fileName,
                                   const Metadata::Changes &changes)
{
    QString destFile;
    bool useSidecar;

    if (m_embed && imageIsWritable(fileName)) {
        destFile = fileName;
        useSidecar = false;
    } else {
        destFile = fileName + ".xmp";
        useSidecar = true;
    }

    Exiv2::Image::AutoPtr image;

    try {
        image = Exiv2::ImageFactory::open(destFile.toUtf8().constData());
        image->readMetadata();
    } catch (Exiv2::AnyError &e) {
        if (useSidecar) {
            // The sidecar could not be opened, create it from the image
            try {
                Exiv2::Image::AutoPtr source =
                    Exiv2::ImageFactory::open(fileName.toUtf8().constData());
                source->readMetadata();
                image = Exiv2::ImageFactory::create(Exiv2::ImageType::xmp,
                                                    destFile.toUtf8().constData());
                image->setMetadata(*source);
            } catch (Exiv2::AnyError &e) {
                qDebug() << "Sidecar creation failed" << e.what();
                return false;
            }
        } else {
            qDebug() << "Exiv2 exception, code" << e.what();
            return false;
        }
    }

    Exiv2::XmpData &xmpData = image->xmpData();

    if (changes.fields & Metadata::Changes::WriteTags) {
        // Clear the current tags
        Exiv2::XmpData::iterator i =
            xmpData.findKey(Exiv2::XmpKey("Xmp.dc.subject"));
        if (i != xmpData.end()) { xmpData.erase(i); }
        // Write the new tags
        Q_FOREACH(const QString &tag, changes.tags) {
            xmpData["Xmp.dc.subject"] = tag.toStdString();
        }
    }

    if (changes.fields & Metadata::Changes::WriteRegions) {
        writeRegions(*image, changes.regions);
    }

    if (changes.fields & Metadata::Changes::WriteRating) {
        if (changes.rating >= 0) {
            xmpData["Xmp.xmp.Rating"] = changes.rating;
        } else {
            Exiv2::XmpData::iterator i =
                xmpData.findKey(Exiv2::XmpKey("Xmp.xmp.Rating"));
            if (i != xmpData.end()) { xmpData.erase(i); }
        }
    }

    if (changes.fields & Metadata::Changes::WriteLocation) {
        clearGeoLocation(*image);
        if (changes.location.isValid()) {
            writeGeoLocation(*image, changes.location);
        }
    }

    if (changes.fields & Metadata::Changes::WriteTitle) {
        xmpData["Xmp.dc.title"] = changes.title.toStdString();
    }

    if (changes.fields & Metadata::Changes::WriteDescription) {
        xmpData["Xmp.dc.description"] = changes.description.toStdString();
        Exiv2::ExifData &exifData = image->exifData();
        exifData["Exif.Image.ImageDescription"] =
            changes.description.toStdString();
    }

    if (changes.fields & Metadata::Changes::WriteFreeFields) {
        for (auto i = changes.freeFields.constBegin();
             i != changes.freeFields.constEnd();
             i++) {
            writeField(*image, i.key(), i.value());
        }
    }

    return saveMetadata(image.get());
}

Metadata::Metadata(QObject *parent):
    QObject(parent),
    d_ptr(new MetadataPrivate(this))
{
}

Metadata::~Metadata()
{
    delete d_ptr;
}

void Metadata::setEmbed(bool embed)
{
    Q_D(Metadata);
    if (embed == d->m_embed) return;
    d->m_embed = embed;
    Q_EMIT embedChanged();
}

bool Metadata::embed() const
{
    Q_D(const Metadata);
    return d->m_embed;
}

bool Metadata::writeChanges(const QString &file, const Changes &changes)
{
    Q_D(Metadata);
    return d->writeChanges(file, changes);
}

bool Metadata::readImportData(const QString &file, ImportData &data) const
{
    Exiv2::Image::AutoPtr image = loadImage(file);
    if (Q_UNLIKELY(!image.get())) return false;

    data.title = title(*image);
    data.description = description(*image);
    data.time = creationData(*image);
    data.tags = keywords(*image);
    data.regions = readRegions(*image);
    data.rating = rating(*image);
    data.location = readGeoLocation(*image);

    return true;
}

QVariant Metadata::readFreeField(const QString &file, const QString &key,
                                 QMetaType::Type type) const
{
    QVariant variant;

    Exiv2::Image::AutoPtr image = loadImage(file);
    if (Q_UNLIKELY(!image.get())) return variant;

    std::string keyStd = key.toStdString();
    if (type == QMetaType::QStringList) {
        if (keyStd[0] == 'E') {
            qWarning() << "Exif specs does not allow string lists";
        } else if (keyStd[0] == 'I') {
            variant = stringValuesT(image->iptcData(), keyStd);
        } else {
            QStringList list;
            const Exiv2::Value *v = readValue(*image, keyStd);
            if (v) {
                int n = v->count();
                for (int i = 0; i < n; i++) {
                    list.append(QString::fromStdString(v->toString(i)));
                }
            }
            variant = list;
        }
    } else {
        // Assuming string; if needed, add support for more types
        const Exiv2::Value *v = readValue(*image, keyStd);
        if (v) {
            variant = cleanText(v->toString(0));
        }
    }
    return variant;
}

QImage Metadata::loadPreview(const QString &file, QSize *size,
                             const QSize &requestedSize) const
{
    Exiv2::Image::AutoPtr image;
    try {
        image = Exiv2::ImageFactory::open(file.toUtf8().constData());
        image->readMetadata();
    } catch (Exiv2::AnyError &e) {
        qDebug() << "Exiv2 exception, code" << e.code();
        return QImage();
    }

    Exiv2::PreviewManager loader(*image);
    Exiv2::PreviewPropertiesList list = loader.getPreviewProperties();

    if (list.empty()) return QImage();

    bool found = false;
    Exiv2::PreviewPropertiesList::const_iterator i, best;
    for (i = list.begin(); i != list.end(); i++) {
        best = i;
        if (i->width_ >= uint(requestedSize.width()) &&
            i->height_ >= uint(requestedSize.height())) {
            found = true;
            break;
        }
    }

    if (!found) return QImage();

    QSize previewSize = QSize(best->width_, best->height_);
    if (size != 0) *size = previewSize;

    Exiv2::PreviewImage exivPreview = loader.getPreviewImage(*best);
    QImage preview;
    preview.loadFromData(exivPreview.pData(), exivPreview.size());

    /* Handle image orientation */
    Exiv2::ExifData &exifData = image->exifData();
    QString orientationString =
        exifData["Exif.Thumbnail.Orientation"].toString().c_str();
    if (orientationString.isEmpty()) {
        orientationString =
            exifData["Exif.Image.Orientation"].toString().c_str();
    }
    int orientation = orientationString.toInt();
    if (orientation < 1 || orientation > 8) orientation = 1;
    int rotation, mirror;

    switch (orientation) {
    case 1: rotation = 0; mirror = 0; break;
    case 2: rotation = 0; mirror = 1; break;
    case 3: rotation = 2; mirror = 0; break;
    case 4: rotation = 2; mirror = 1; break;
    case 5: rotation = 1; mirror = 1; break;
    case 6: rotation = 1; mirror = 0; break;
    case 7: rotation = 3; mirror = 1; break;
    case 8: rotation = 3; mirror = 0; break;
    }

    QTransform transformation;
    if (!requestedSize.isEmpty()) {
        QSize newSize = preview.size();
        newSize.scale(requestedSize, Qt::KeepAspectRatioByExpanding);
        newSize.rwidth() = qMax(newSize.width(), 1);
        newSize.rheight() = qMax(newSize.height(), 1);
        transformation.scale((qreal)newSize.width() / preview.width(),
                             (qreal)newSize.height() / preview.height());
    }

    if (rotation != 0) {
        transformation.rotate(rotation * 90);
    }

    if (mirror != 0) {
        transformation.scale(-1, 1);
    }

    return preview.transformed(transformation, Qt::SmoothTransformation);
}
