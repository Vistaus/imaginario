/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "clipboard.h"
#include "digikam_importer.h"
#include "folder_model.h"
#include "fspot_importer.h"
#include "helper_model.h"
#include "importer.h"
#include "location_model.h"
#include "mime_database.h"
#include "photo_model.h"
#include "roll_model.h"
#include "settings.h"
#include "shotwell_importer.h"
#include "tag_area_model.h"
#include "tag_model.h"
#include "time_model.h"
#include "time_widget.h"
#include "types.h"
#include "updater.h"
#include "utils.h"
#include "writer.h"

#include <QtQml>

using namespace Imaginario;

static QObject *mimeDatabaseProvider(QQmlEngine *, QJSEngine *)
{
    return new MimeDatabase;
}

static QObject *settingsProvider(QQmlEngine *, QJSEngine *)
{
    return new Settings;
}

static QObject *utilsProvider(QQmlEngine *, QJSEngine *)
{
    return new Utils;
}

void Imaginario::registerTypes()
{
    qmlRegisterType<Mardy::Updater>("Mardy", 1, 0, "Updater");

    qRegisterMetaType<GeoPoint>("GeoPoint");
    qRegisterMetaType<PhotoId>("PhotoId");
    qRegisterMetaType<QMimeType>("QMimeType");
    qRegisterMetaType<Tag>("Tag");

#ifdef DESKTOP_BUILD
    qmlRegisterSingletonType<Clipboard>("Imaginario", 1, 0, "Clipboard",
                                        [](QQmlEngine *, QJSEngine *) -> QObject * {
                                            return new Clipboard;
                                        });
    qmlRegisterType<DigikamImporter>("Imaginario", 1, 0, "DigikamImporter");
    qmlRegisterType<FolderModel>("Imaginario", 1, 0, "FolderModel");
    qmlRegisterType<FspotImporter>("Imaginario", 1, 0, "FspotImporter");
    qmlRegisterType<HelperModel>("Imaginario", 1, 0, "HelperModel");
    qmlRegisterType<ShotwellImporter>("Imaginario", 1, 0, "ShotwellImporter");
#endif
    qmlRegisterType<Importer>("Imaginario", 1, 0, "Importer");
    qmlRegisterType<LocationModel>("Imaginario", 1, 0, "LocationModel");
    qmlRegisterSingletonType<MimeDatabase>("Imaginario", 1, 0, "MimeDatabase",
                                           mimeDatabaseProvider);
    qmlRegisterType<PhotoModel>("Imaginario", 1, 0, "PhotoModel");
    qmlRegisterType<RollModel>("Imaginario", 1, 0, "RollModel");
    qmlRegisterSingletonType<Settings>("Imaginario", 1, 0, "Settings",
                                       settingsProvider);
    qmlRegisterType<TagAreaModel>("Imaginario", 1, 0, "TagAreaModel");
    qmlRegisterType<TagModel>("Imaginario", 1, 0, "TagModel");
    qmlRegisterType<TimeModel>("Imaginario", 1, 0, "TimeModel");
    qmlRegisterType<TimeWidget>("Imaginario", 1, 0, "TimeWidget");
    qmlRegisterSingletonType<Utils>("Imaginario", 1, 0, "Utils",
                                    utilsProvider);
    qmlRegisterType<Writer>("Imaginario", 1, 0, "Writer");
}
