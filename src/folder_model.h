/*
 * Copyright (C) 2016-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGINARIO_FOLDER_MODEL_H
#define IMAGINARIO_FOLDER_MODEL_H

#include <QAbstractListModel>
#include <QScopedPointer>

namespace Imaginario {

class FolderModelPrivate;
class FolderModel: public QAbstractListModel
{
    Q_OBJECT
    Q_ENUMS(Roles)
    Q_PROPERTY(int count READ rowCount NOTIFY countChanged)

public:
    enum Roles {
        PathRole = Qt::UserRole + 1,
    };

    FolderModel(QObject *parent = 0);
    ~FolderModel();

    Q_INVOKABLE QVariant get(int row, const QString &role) const;

    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index,
                  int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

Q_SIGNALS:
    void countChanged();

private:
    QScopedPointer<FolderModelPrivate> d_ptr;
    Q_DECLARE_PRIVATE(FolderModel)
};

} // namespace

#endif // IMAGINARIO_FOLDER_MODEL_H
