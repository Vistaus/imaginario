/*
 * Copyright (C) 2014-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "abstract_database_p.h"
#include "database.h"

#include <QByteArray>
#include <QDebug>
#include <QHash>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlField>
#include <QStringBuilder>
#include <locale.h>
#include <stdio.h>
#include <time.h>

#define LATEST_DB_VERSION 4

using namespace Imaginario;

static Database *m_instance = 0;

struct TagData {
    TagData(): parent(-1), isCategory(false), usageCount(0) {}
    TagData(TagId parent, const QString &name,
            const QString &icon, bool isCategory):
        parent(parent), name(name), icon(icon), isCategory(isCategory),
        usageCount(0)
    {}
    TagId parent;
    QString name;
    QString icon;
    bool isCategory;
    int usageCount;
};

struct RollData {
    RollData() {}
    RollData(const QDateTime &time): time(time) {}

    QDateTime time;
};

namespace Imaginario {

class DatabasePrivate: public AbstractDatabasePrivate
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(Database)

public:
    DatabasePrivate(Database *q);
    ~DatabasePrivate();
    bool init();

    bool createDb() Q_DECL_OVERRIDE;
    bool createIndexesAndTriggersForPhotoVersions();
    bool createTriggersForCategories();
    bool updateFrom(int version) Q_DECL_OVERRIDE;

    int ensureDefaultTag(const char *rawName);
    bool loadDefaultTags();
    bool loadTags();
    void queueUpdateTagsPopularity(bool forced = false);

    void afterTransaction(bool succeeded) Q_DECL_OVERRIDE;

    Database::Photo photoFromValue(const QSqlQuery &q, PhotoId id);

    RollId createRoll(const QDateTime &time);
    bool loadRolls();
    RollData &findRoll(RollId);

    PhotoId addPhoto(const Database::Photo &photo, RollId roll);
    Database::Photo photo(PhotoId photoId);
    bool deletePhotos(const QList<PhotoId> &photoIds);
    bool makeVersion(PhotoId photoId, PhotoId master);
    bool addTags(PhotoId photo, const QList<Tag> &tags);
    bool clearTags(PhotoId photo);
    QList<Tag> tags(PhotoId photoId);

    QList<Database::Photo> photos(const QList<PhotoId> &photoIds);
    QList<Database::Photo> findPhotos(SearchFilters filters,
                                      Qt::SortOrder sort);
    PhotoId masterVersion(PhotoId photoId);
    QList<Database::Photo> photoVersions(PhotoId photoId);

    Tag createTag(Tag parent, const QString &name, const QString &icon);
    bool deleteTag(Tag);
    QList<Tag> allTags();
    QList<Tag> tags(Tag parent);
    bool updateIsCategory(const TagId &tagId);
    bool checkParentLoop(Tag item, Tag newParent) const;

    TagData &findTag(TagId id) {
        QHash<TagId,TagData>::iterator i = m_tags.find(id);
        return i.value();
    }

    int computeUsageCount(Tag tag, const QList<PhotoId> &photos);

private Q_SLOTS:
    bool updateTagsPopularity();

private:
    friend class Tag;
    QHash<RollId,RollData> m_rolls;
    QHash<TagId,TagData> m_tags;
    QHash<PhotoId,Database::Photo> m_cachedPhotos;
    TagId m_defaultTagUncategorized;
    TagId m_defaultTagPeople;
    TagId m_defaultTagPlaces;
    TagId m_defaultTagEvents;
    TagId m_defaultTagImported;
    double m_log2maxTagUsageCount;
    int m_tagChangesCount;
    bool m_tagUpdateQueued;
};

} // namespace

DatabasePrivate::DatabasePrivate(Database *q):
    AbstractDatabasePrivate(q, "photos.db"),
    m_log2maxTagUsageCount(1),
    m_tagChangesCount(0),
    m_tagUpdateQueued(false)
{
    m_cachedPhotos.reserve(1000);

    qRegisterMetaType<Roll>("Roll");
}

DatabasePrivate::~DatabasePrivate()
{
}

bool DatabasePrivate::createDb()
{
    QSqlDatabase &db = this->db();
    exec("CREATE TABLE photos ("
         " id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
         " time INTEGER NOT NULL,"
         " base_uri STRING NOT NULL,"
         " filename STRING NOT NULL,"
         " title TEXT NOT NULL,"
         " roll_id INTEGER NOT NULL,"
         " version_flag INTEGER NOT NULL DEFAULT 0,"
         " rating INTEGER NULL,"
         " latitude REAL,"
         " longitude REAL"
         ")");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    exec("CREATE TABLE photo_tags ("
         " photo_id INTEGER,"
         " tag_id INTEGER,"
         " area TEXT, "
         " UNIQUE (photo_id, tag_id)"
         ")");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    exec("CREATE TABLE photo_versions ("
         " photo_id INTEGER,"
         " version_id INTEGER,"
         "UNIQUE (photo_id, version_id)"
         ")");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;
    if (Q_UNLIKELY(!createIndexesAndTriggersForPhotoVersions())) return false;

    exec("CREATE INDEX idx_photos_roll_id ON photos(roll_id)");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;
    exec("CREATE TRIGGER tg_delete_photo "
         "BEFORE DELETE ON photos "
         "FOR EACH ROW BEGIN "
         "  DELETE FROM photo_tags WHERE photo_id = OLD.id;"
         "  DELETE FROM photo_versions WHERE photo_id = OLD.id;"
         "END");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    exec("CREATE TABLE tags ("
         " id INTEGER PRIMARY KEY NOT NULL,"
         " name TEXT UNIQUE,"
         " category_id INTEGER,"
         " is_category BOOLEAN,"
         " sort_priority INTEGER,"
         " icon TEXT"
         ")");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;
    if (Q_UNLIKELY(!createTriggersForCategories())) return false;

    exec("CREATE TABLE rolls ("
         " id INTEGER PRIMARY KEY NOT NULL,"
         " time INTEGER NOT NULL,"
         " source TEXT NOT NULL DEFAULT '',"
         " description TEXT NOT NULL DEFAULT ''"
         ")");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;

    return true;
}

bool DatabasePrivate::createIndexesAndTriggersForPhotoVersions()
{
    QSqlDatabase &db = this->db();

    exec("CREATE INDEX idx_photo_versions_id ON photo_versions(photo_id)");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;
    exec("CREATE TRIGGER tg_add_version "
         "AFTER INSERT ON photo_versions "
         "FOR EACH ROW BEGIN "
         "  UPDATE photos SET version_flag = 2 WHERE id = NEW.version_id;"
         "  UPDATE photos SET version_flag = 1 WHERE id = NEW.photo_id;"
         "END");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;
    exec("CREATE TRIGGER tg_delete_version "
         "BEFORE DELETE ON photo_versions "
         "FOR EACH ROW BEGIN "
         "  UPDATE photos SET version_flag = 0 WHERE id = OLD.version_id;"
         "  UPDATE photos SET version_flag = ("
         "    CASE WHEN ("
         "      SELECT COUNT(*) FROM photo_versions"
         "      WHERE photo_id = OLD.photo_id"
         "    ) > 1 THEN 1 ELSE 0 END"
         "  ) WHERE id = OLD.photo_id;"
         "END");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;
    return true;
}

bool DatabasePrivate::createTriggersForCategories()
{
    QSqlDatabase &db = this->db();

    exec("CREATE TRIGGER tg_add_tag "
         "AFTER INSERT ON tags "
         "FOR EACH ROW BEGIN "
         "  UPDATE tags SET is_category = 1 WHERE id = NEW.category_id;"
         "END");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;
    exec("CREATE TRIGGER tg_edit_tag "
         "BEFORE UPDATE OF category_id ON tags "
         "FOR EACH ROW BEGIN "
         "  UPDATE tags SET is_category = 1 WHERE id = NEW.category_id;"
         "END");
    if (Q_UNLIKELY(db.lastError().isValid())) return false;
    return true;
}

bool DatabasePrivate::updateFrom(int version)
{
    QSqlDatabase &db = this->db();

    if (version == 1) {
        /* Table photos:
         * - rename description to title
         * - drop default_version_id
         * - add version_flag
         */
        exec("CREATE TABLE photosNew ("
             " id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
             " time INTEGER NOT NULL,"
             " base_uri STRING NOT NULL,"
             " filename STRING NOT NULL,"
             " title TEXT NOT NULL,"
             " roll_id INTEGER NOT NULL,"
             " version_flag INTEGER NOT NULL DEFAULT 0,"
             " rating INTEGER NULL,"
             " latitude REAL,"
             " longitude REAL"
             ")");
        if (Q_UNLIKELY(db.lastError().isValid())) return false;

        exec("INSERT INTO photosNew (id, time, base_uri, filename,"
             " title, roll_id, rating, latitude, longitude) "
             "SELECT id, time, base_uri, filename,"
             " description, roll_id, rating, latitude, longitude "
             "FROM photos");
        if (Q_UNLIKELY(db.lastError().isValid())) return false;

        exec("DROP TABLE photos");
        if (Q_UNLIKELY(db.lastError().isValid())) return false;
        exec("ALTER TABLE photosNew RENAME TO photos");
        if (Q_UNLIKELY(db.lastError().isValid())) return false;

        exec("CREATE INDEX idx_photos_roll_id ON photos(roll_id)");
        if (Q_UNLIKELY(db.lastError().isValid())) return false;
        exec("CREATE TRIGGER tg_delete_photo "
             "BEFORE DELETE ON photos "
             "FOR EACH ROW BEGIN "
             "  DELETE FROM photo_tags WHERE photo_id = OLD.id;"
             "  DELETE FROM photo_versions WHERE photo_id = OLD.id;"
             "END");
        if (Q_UNLIKELY(db.lastError().isValid())) return false;

        /* Table photo_version:
         * - drop all fields but photo_id and version_id
         */
        exec("DROP TABLE photo_versions");
        if (Q_UNLIKELY(db.lastError().isValid())) return false;
        exec("CREATE TABLE photo_versions ("
             " photo_id INTEGER,"
             " version_id INTEGER,"
             "UNIQUE (photo_id, version_id)"
             ")");
        if (Q_UNLIKELY(db.lastError().isValid())) return false;
        if (Q_UNLIKELY(!createIndexesAndTriggersForPhotoVersions())) return false;

        /* Table rolls:
         * - add source field
         * - add description field
         */
        exec("ALTER TABLE rolls ADD COLUMN source TEXT NOT NULL DEFAULT ''");
        if (Q_UNLIKELY(db.lastError().isValid())) return false;
        exec("ALTER TABLE rolls ADD COLUMN description TEXT "
             "NOT NULL DEFAULT ''");
        if (Q_UNLIKELY(db.lastError().isValid())) return false;
        version++;
    }

    if (version == 2) {
        /* Add area field to photo_tags table */
        exec("ALTER TABLE photo_tags ADD COLUMN area TEXT");
        if (Q_UNLIKELY(db.lastError().isValid())) return false;
        version++;
    }

    if (version == 3) {
        if (Q_UNLIKELY(!createTriggersForCategories())) return false;
        version++;
    }

    if (!setDbVersion(LATEST_DB_VERSION)) return false;

    return true;
}

bool DatabasePrivate::init()
{
    bool ok = AbstractDatabasePrivate::init(LATEST_DB_VERSION);
    if (Q_UNLIKELY(!ok)) return false;

    ok = loadTags();
    return ok;
}

int DatabasePrivate::ensureDefaultTag(const char *rawName)
{
    QString keyName = QString("default_tag_%1").arg(rawName);
    QString tagName;
    QSqlQuery q(db());
    q.setForwardOnly(true);
    q.exec("SELECT data FROM meta WHERE name='" + keyName + "'");
    if (Q_LIKELY(q.next())) {
        tagName = q.value(0).toString();
    } else {
        tagName = tr(rawName);
        q.prepare("INSERT INTO meta (name, data) VALUES (:keyName, :tagName)");
        q.bindValue(":keyName", keyName);
        q.bindValue(":tagName", tagName);
        q.exec();
        if (Q_UNLIKELY(q.lastError().isValid())) return -1;
    }

    q.prepare("SELECT id FROM tags WHERE name=:tagName");
    q.bindValue(":tagName", tagName);
    q.exec();
    if (Q_LIKELY(q.next())) {
        return q.value(0).toInt();
    } else {
        q.prepare("INSERT INTO tags (name, category_id, is_category, "
                  "sort_priority, icon) "
                  "VALUES (:tagName, -1, 0, 0, :tagIcon)");
        q.bindValue(":tagName", tagName);
        QString iconName = QString("qrc:/icons/tag_%1").arg(rawName);
        q.bindValue(":tagIcon", iconName);
        q.exec();
        if (Q_UNLIKELY(q.lastError().isValid())) return -1;
        return q.lastInsertId().toInt();
    }
}

bool DatabasePrivate::loadDefaultTags()
{
    const struct {
        const char *name;
        TagId *ptrToId;
    } defaultTags[] = {
        { QT_TR_NOOP("Uncategorized"), &m_defaultTagUncategorized },
        { QT_TR_NOOP("People"), &m_defaultTagPeople },
        { QT_TR_NOOP("Places"), &m_defaultTagPlaces },
        { QT_TR_NOOP("Events"), &m_defaultTagEvents },
        { QT_TR_NOOP("Imported tags"), &m_defaultTagImported },
        { 0, 0}
    };

    QSqlDatabase &db = this->db();
    db.transaction();
    for (int i = 0; defaultTags[i].name != 0; i++) {
        int tagId = ensureDefaultTag(defaultTags[i].name);
        if (Q_UNLIKELY(tagId < 0)) {
            db.rollback();
            return false;
        }
        *(defaultTags[i].ptrToId) = tagId;
    }
    db.commit();

    return true;
}

bool DatabasePrivate::loadTags()
{
    if (Q_UNLIKELY(!loadDefaultTags())) {
        qWarning() << "Failed to load default tags";
        return false;
    }

    QSqlQuery q(db());
    q.setForwardOnly(true);
    q.exec("SELECT id, name, category_id, is_category, icon "
           "FROM tags");
    while (q.next()) {
        TagId id = q.value(0).toInt();
        m_tags.insert(id, TagData(q.value(2).toInt(),
                                  q.value(1).toString(),
                                  q.value(4).toString(),
                                  q.value(3).toBool()));
    }

    if (Q_UNLIKELY(q.lastError().isValid())) return false;

    return updateTagsPopularity();
}

bool DatabasePrivate::updateTagsPopularity()
{
    int maxTagUsageCount = 1;
    m_tagChangesCount = 0;
    m_tagUpdateQueued = false;

    QSqlQuery q(db());
    q.setForwardOnly(true);
    q.exec("SELECT tag_id, COUNT (*) AS popularity "
           "FROM photo_tags GROUP BY tag_id");
    while (q.next()) {
        TagId id = q.value(0).toInt();
        int usageCount = q.value(1).toInt();
        QHash<TagId,TagData>::iterator i = m_tags.find(id);
        if (Q_UNLIKELY(i == m_tags.end())) continue;
        i.value().usageCount = usageCount;
        if (usageCount > maxTagUsageCount) {
            maxTagUsageCount = usageCount;
        }
    }

    m_log2maxTagUsageCount = log2(maxTagUsageCount + 1);
    return !q.lastError().isValid();
}

void DatabasePrivate::queueUpdateTagsPopularity(bool forced)
{
    /* If we are in a transaction, we'll update the popularity
     * before committing */
    if (m_inTransaction) {
        m_tagUpdateQueued = true;
        return;
    }

    /* We don't really want to update the tags popularity at every change,
     * since its precision is not essential.  We update it only once in a
     * while.
     */
    m_tagChangesCount++;
    if (!m_tagUpdateQueued && (forced || m_tagChangesCount == 16)) {
        m_tagUpdateQueued = true;
        QMetaObject::invokeMethod(this, "updateTagsPopularity",
                                  Qt::QueuedConnection);
    }
}

void DatabasePrivate::afterTransaction(bool succeeded)
{
    if (succeeded && m_tagUpdateQueued) {
        updateTagsPopularity();
    }
    m_tagUpdateQueued = false;
}

Database::Photo DatabasePrivate::photoFromValue(const QSqlQuery &q,
                                                PhotoId id)
{
    Database::PhotoData *d = new Database::PhotoData;
    int i = 0;
    d->id = (id >= 0) ? id : q.value(i++).toInt();
    QVariant time = q.value(i++);
    if (Q_LIKELY(time.toInt() != -1)) {
        d->time = QDateTime::fromTime_t(time.toUInt());
    }
    d->baseUri = q.value(i++).toString();
    d->fileName = q.value(i++).toString();
    d->description = q.value(i++).toString();
    d->rollId = q.value(i++).toInt();
    d->versionFlags = Database::PhotoData::VersionFlags(q.value(i++).toInt());
    d->rating = q.value(i++).toInt();
    QVariant lat = q.value(i++);
    QVariant lon = q.value(i++);
    if (!lat.isNull() && !lon.isNull()) {
        d->location = GeoPoint(lat.toDouble(), lon.toDouble());
    }
    Database::Photo photo(d);
    m_cachedPhotos[d->id] = photo;
    return photo;
}

RollId DatabasePrivate::createRoll(const QDateTime &t)
{
    QSqlQuery q(db());
    q.prepare("INSERT INTO rolls (time) VALUES (?)");
    q.addBindValue(t.isValid() ? t.toTime_t() : qint64(time(NULL)));
    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "Error creating roll:" << q.lastError();
        return RollId(-1);
    }

    RollId rollId = (q.lastInsertId().toInt());
    if (!m_rolls.isEmpty()) {
        m_rolls.insert(rollId, RollData(t));
    }

    Roll roll(rollId);
    emit("rollAdded", Q_ARG(Roll, roll));

    return rollId;
}

bool DatabasePrivate::loadRolls()
{
    QSqlQuery q(db());
    q.setForwardOnly(true);
    q.exec("SELECT id, time "
           "FROM rolls");
    while (q.next()) {
        RollId id = q.value(0).toInt();
        QVariant timeV = q.value(1);
        QDateTime time = (timeV.toInt() != -1) ?
            QDateTime::fromTime_t(timeV.toUInt()) : QDateTime();
        m_rolls.insert(id, RollData(time));
    }

    // Add an extra one for invalid rolls
    m_rolls.insert(-1, RollData(QDateTime()));
    return !q.lastError().isValid();
}

RollData &DatabasePrivate::findRoll(RollId id)
{
    if (m_rolls.isEmpty()) loadRolls();

    const auto i = m_rolls.find(id);
    return i != m_rolls.constEnd() ? i.value() : m_rolls[-1];
}

PhotoId DatabasePrivate::addPhoto(const Database::Photo &photo, RollId roll)
{
    QSqlQuery q(db());
    q.prepare("INSERT INTO photos (time, base_uri, filename, title,"
              " roll_id, version_flag, rating, latitude, longitude) "
              "VALUES (?, ?, ?, ?, ?, 0, ?, ?, ?)");
    q.addBindValue(photo.time().isValid() ? photo.time().toTime_t() : -1);
    q.addBindValue(photo.baseUri());
    q.addBindValue(photo.fileName());
    q.addBindValue(photo.description().isNull() ? "" : photo.description());
    q.addBindValue(roll);
    q.addBindValue(photo.rating());
    GeoPoint location = photo.location();
    if (location.isValid()) {
        q.addBindValue(location.lat);
        q.addBindValue(location.lon);
    } else {
        q.addBindValue(QVariant(QVariant::Double));
        q.addBindValue(QVariant(QVariant::Double));
    }

    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "DB error:" << q.lastError();
        return -1;
    }

    PhotoId id = q.lastInsertId().toInt();
    Database::Photo cachedPhoto(photo);
    cachedPhoto.d->id = id;
    cachedPhoto.d->rollId = roll;
    m_cachedPhotos[id] = cachedPhoto;

    emit("photoAdded", Q_ARG(int, id));

    return id;
}

Database::Photo DatabasePrivate::photo(PhotoId photoId)
{
    QHash<PhotoId,Database::Photo>::const_iterator i =
        m_cachedPhotos.constFind(photoId);
    if (i != m_cachedPhotos.constEnd()) {
        return *i;
    }

    QString query =
        QString("SELECT time, base_uri, filename, title,"
                " roll_id, version_flag, rating, latitude, longitude "
                "FROM photos WHERE id = %1").arg(photoId);
    QSqlQuery q(db());
    q.setForwardOnly(true);
    q.exec(query);
    if (Q_UNLIKELY(!q.next())) {
        qDebug() << "Photo not found" << photoId;
        return Database::Photo();
    }
    return photoFromValue(q, photoId);
}

bool DatabasePrivate::deletePhotos(const QList<PhotoId> &photoIds)
{
    QStringList photoIdsText;
    for (PhotoId photoId: photoIds) {
        photoIdsText.append(QString::number(photoId));
    }
    QString query = QString("DELETE FROM photos WHERE id IN (" +
                            photoIdsText.join(',') + ")");
    QSqlQuery q(db());
    q.exec(query);
    if (Q_UNLIKELY(q.lastError().isValid())) {
        qDebug() << "Error deleting photos" << q.lastError();
        return false;
    }

    // Note: triggers will take care of clearing the other linked tables

    for (PhotoId photoId: photoIds) {
        m_cachedPhotos.remove(photoId);

        emit("photoDeleted", Q_ARG(PhotoId, photoId));
    }

    queueUpdateTagsPopularity();
    return true;
}

bool DatabasePrivate::makeVersion(PhotoId photoId, PhotoId master)
{
    QSqlQuery qq(db());
    if (master > 0) {
        /* first, update all versions which had photo_id as master version to
         * point at the new master photo. This is because we don't want to
         * have a tree of versions, but rather just a flat list. */
        qq.prepare("UPDATE photo_versions SET photo_id = ? "
                   "WHERE photo_id = ?");
        qq.addBindValue(master);
        qq.addBindValue(photoId);
        qq.exec();
        qq.prepare("INSERT OR REPLACE INTO photo_versions "
                   "(photo_id, version_id) VALUES (?, ?)");
        qq.addBindValue(master);
        qq.addBindValue(photoId);
        qq.exec();
    } else {
        qq.prepare("DELETE FROM photo_versions WHERE version_id = ?");
        qq.addBindValue(photoId);
        qq.exec();
    }
    if (Q_UNLIKELY(qq.lastError().isValid())) return false;

    if (master > 0) {
        /* Update cache */
        QHash<PhotoId,Database::Photo>::iterator i =
            m_cachedPhotos.find(photoId);
        if (i != m_cachedPhotos.end()) {
            i->d->versionFlags = Database::PhotoData::IsNonDefaultVersion;
        }

        i = m_cachedPhotos.find(master);
        if (i != m_cachedPhotos.end()) {
            i->d->versionFlags = Database::PhotoData::IsDefaultVersion;
        }
        emit("photoChanged", Q_ARG(int, master));
    } else {
        /* We don't know who was the default version, and whether it
         * still has other versions attached or not.
         * Let's just clear the cache. */
        m_cachedPhotos.clear();
    }

    emit("photoChanged", Q_ARG(int, photoId));
    return true;
}

bool DatabasePrivate::addTags(PhotoId photo, const QList<Tag> &tags)
{
    bool ok = true;

    Q_FOREACH(const Tag tag, tags) {
        QSqlQuery q(db());
        q.prepare("INSERT OR IGNORE INTO photo_tags (photo_id, tag_id) "
                  "VALUES (?, ?)");
        q.addBindValue(photo);
        q.addBindValue(tag.id());
        if (Q_UNLIKELY(!q.exec())) {
            qWarning() << "DB error adding tag:" << q.lastError();
            ok = false;
        }
    }

    queueUpdateTagsPopularity(true);
    emit("photoTagsChanged", Q_ARG(int, photo));
    return ok;
}

bool DatabasePrivate::clearTags(PhotoId photo)
{
    QSqlQuery q(db());
    q.prepare("DELETE FROM photo_tags WHERE photo_id = ?");
    q.addBindValue(photo);
    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "DB error clearing tags:" << q.lastError();
        return false;
    }
    return true;
}

QList<Tag> DatabasePrivate::tags(PhotoId photoId)
{
    QSqlQuery q(db());
    q.setForwardOnly(true);
    q.prepare("SELECT tag_id FROM photo_tags WHERE photo_id = ?");
    q.addBindValue(photoId);
    q.exec();
    QList<Tag> result;
    while (q.next()) {
        TagId id = q.value(0).toInt();
        result.append(Tag(id));
    }
    return result;
}

bool DatabasePrivate::updateIsCategory(const TagId &tagId)
{
    TagData &data = m_tags[tagId];
    if (!data.isCategory) return true;

    QSqlQuery q(db());
    q.setForwardOnly(true);
    q.prepare("SELECT COUNT(*) FROM tags WHERE category_id = ?");
    q.addBindValue(tagId);
    q.exec();
    q.next();
    int count = q.value(0).toInt();
    if (count == 0) {
        QString updateStmt = QString("UPDATE tags SET is_category = 0 "
                                     "WHERE id=%1").arg(tagId);
        if (Q_UNLIKELY(!execOk(updateStmt))) return false;
        data.isCategory = false;
        emit("tagChanged", Q_ARG(Tag, Tag(tagId)));
    }
    return true;
}

bool DatabasePrivate::checkParentLoop(Tag item, Tag newParent) const
{
    for (Tag parent = newParent; parent.isValid(); parent = parent.parent()) {
        if (parent == item) return false;
    }
    return true;
}

QList<Database::Photo> DatabasePrivate::photos(const QList<PhotoId> &photoIds)
{
    QList<Database::Photo> result;

    QSqlQuery q(db());
    q.setForwardOnly(true);

    QStringList photoIdsText;
    Q_FOREACH(PhotoId photoId, photoIds) {
        photoIdsText.append(QString::number(photoId));
    }
    q.exec("SELECT id, time, base_uri, filename, title, roll_id,"
              " version_flag, rating, latitude, longitude "
              "FROM photos WHERE id IN (" + photoIdsText.join(',') +
              ") ORDER BY TIME ASC");
    while (q.next()) {
        result.append(photoFromValue(q, -1));
    }

    return result;
}

QList<Database::Photo> DatabasePrivate::findPhotos(SearchFilters filters,
                                                   Qt::SortOrder sort)
{
    QList<Database::Photo> result;

    QString query =
        QStringLiteral("SELECT id, time, base_uri, filename, title,"
                       " roll_id, version_flag, rating,"
                       " latitude, longitude "
                       "FROM photos");

    QStringList wheres;

    QSqlDatabase &db = this->db();

    QSqlField date("time", QVariant::LongLong);
    if (filters.time0.isValid()) {
        date.setValue(filters.time0.toTime_t());
        wheres.append("time >= " + db.driver()->formatValue(date));
    }
    if (filters.time1.isValid()) {
        date.setValue(filters.time1.toTime_t());
        wheres.append("time <= " + db.driver()->formatValue(date));
    }

    QSqlField roll("roll", QVariant::Int);
    if (filters.roll0 >= 0) {
        roll.setValue(filters.roll0);
        wheres.append("roll_id >= " + db.driver()->formatValue(roll));
    }
    if (filters.roll1 >= 0) {
        roll.setValue(filters.roll1);
        wheres.append("roll_id <= " + db.driver()->formatValue(roll));
    }

    QSqlField rating("rating", QVariant::Int);
    if (filters.rating0 >= 0) {
        rating.setValue(filters.rating0);
        wheres.append("rating >= " + db.driver()->formatValue(rating));
    }
    if (filters.rating1 >= 0) {
        rating.setValue(filters.rating1);
        wheres.append("rating <= " + db.driver()->formatValue(rating));
    }

    if (filters.areaTagged) {
        QString areaTagQuery("id IN (SELECT DISTINCT photo_id FROM photo_tags "
                             "WHERE area NOT NULL)");
        wheres.append(areaTagQuery);
    }

    if (!filters.requiredTags.isEmpty()) {
        QStringList requiredTagsQueries;
        Q_FOREACH(const QList<Tag> &requiredTags, filters.requiredTags) {
            if (Q_UNLIKELY(requiredTags.isEmpty())) continue;
            QString requiredTagsQuery("id IN (SELECT photo_id FROM photo_tags "
                                      "WHERE tag_id IN (");
            requiredTagsQuery.reserve(requiredTagsQuery.size() +
                                      requiredTags.count() * 5 + 2);
            bool first = true;
            Q_FOREACH(const Tag &t, requiredTags) {
                if (first) {
                    first = false;
                } else {
                    requiredTagsQuery += ',';
                }
                requiredTagsQuery.append(QString::number(t.id()));
            }
            requiredTagsQuery += ") GROUP BY (photo_id) HAVING count(*) = " %
                QString::number(requiredTags.count()) % ")";
            requiredTagsQueries.append(requiredTagsQuery);
        }
        wheres.append(QString("(%1)").arg(requiredTagsQueries.join(" OR ")));
    }

    QStringList forbiddenTags;
    Q_FOREACH(const Tag &t, filters.forbiddenTags) {
        bool isRequired = false;
        Q_FOREACH(const QList<Tag> &requiredTags, filters.requiredTags) {
            if (requiredTags.contains(t)) { isRequired = true; break; }
        }
        if (!isRequired) {
            forbiddenTags.append(QString::number(t.id()));
        }
    }
    if (!forbiddenTags.isEmpty()) {
        wheres.append("id NOT IN (SELECT photo_id FROM photo_tags "
                      "WHERE tag_id IN (" + forbiddenTags.join(',') + "))");
    }

    if (filters.location0.isValid() && filters.location1.isValid()) {
        wheres.append(QString("latitude >= %1 AND latitude <= %2 AND "
                              "longitude >= %3 AND longitude <= %4").
                      arg(filters.location1.lat).arg(filters.location0.lat).
                      arg(filters.location0.lon).arg(filters.location1.lon));
    } else if (filters.nonGeoTagged) {
        wheres.append("latitude IS NULL");
    }

    if (!filters.excludedPhotos.isEmpty()) {
        QStringList excludedPhotos;
        Q_FOREACH(PhotoId id, filters.excludedPhotos) {
            excludedPhotos.append(QString::number(id));
        }
        wheres.append("id NOT IN (" + excludedPhotos.join(',') + ")");
    }

    if (filters.skipNonDefaultVersions) {
        wheres.append("version_flag <= 1");
    }

    if (!wheres.isEmpty()) {
        query += " WHERE " + wheres.join(" AND ");
    }

    query += QStringLiteral(" ORDER BY time ") +
        ((sort == Qt::DescendingOrder) ? "DESC" : "ASC");
    QSqlQuery q(db);
    q.setForwardOnly(true);
    q.exec(query);
    m_cachedPhotos.clear();
    while (q.next()) {
        result.append(photoFromValue(q, -1));
    }

    return result;
}

/* Note how this internal method behaves differently from the public one: that
 * one always returns a valid photoId, while this one returns 0 if the item
 * has no versions. */
PhotoId DatabasePrivate::masterVersion(PhotoId photoId)
{
    QSqlQuery q(db());
    q.setForwardOnly(true);

    PhotoId masterId = 0;
    Database::Photo p = photo(photoId);
    if (p.isDefaultVersion()) {
        masterId = photoId;
    } else if (p.isNonDefaultVersion()) {
        q.prepare("SELECT photo_id FROM photo_versions WHERE version_id = ?");
        q.addBindValue(photoId);
        q.exec();
        if (Q_UNLIKELY(!q.next())) {
            qWarning() << "Master version not found for" << photoId;
            return masterId;
        }
        masterId = q.value(0).toInt();
    } else {
        // No versions, return 0
    }

    return masterId;
}

QList<Database::Photo> DatabasePrivate::photoVersions(PhotoId photoId)
{
    QList<Database::Photo> result;

    QSqlQuery q(db());
    q.setForwardOnly(true);

    PhotoId masterId = masterVersion(photoId);
    if (!masterId) {
        // No versions
        return result;
    }
    q.prepare("SELECT id, time, base_uri, filename, title, roll_id,"
              " version_flag, rating, latitude, longitude "
              "FROM photos WHERE id = ? OR id IN ("
              " SELECT version_id FROM photo_versions"
              " WHERE photo_id = ?"
              ") ORDER BY TIME ASC");
    q.addBindValue(masterId);
    q.addBindValue(masterId);
    q.exec();
    while (q.next()) {
        result.append(photoFromValue(q, -1));
    }

    return result;
}

Tag DatabasePrivate::createTag(Tag parent, const QString &name,
                               const QString &icon)
{
    QSqlQuery q(db());
    q.prepare("INSERT INTO tags (name, category_id, is_category,"
              " sort_priority, icon) "
              "VALUES (?, ?, 0, 0, ?)");
    q.addBindValue(name);
    q.addBindValue(parent.id());
    q.addBindValue(icon);
    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "DB error:" << q.lastError();
        return Tag(-1);
    }

    TagId id = q.lastInsertId().toInt();
    m_tags.insert(id, TagData(parent.id(), name, icon, false));

    Tag tag(id);
    emit("tagCreated", Q_ARG(Tag, tag));

    if (parent.isValid()) {
        TagData &p = m_tags[parent.id()];
        if (!p.isCategory) {
            /* The trigger takes care of updating the DB; we just need
             * to update the cached info and signal the change */
            p.isCategory = true;
            emit("tagChanged", Q_ARG(Tag, parent));
        }
    }
    return tag;
}

bool DatabasePrivate::deleteTag(Tag tag)
{
    TagId id = tag.id();

    QString query = QString("DELETE FROM tags WHERE id = %1").arg(id);
    QSqlQuery q(db());
    q.exec(query);
    if (Q_UNLIKELY(q.lastError().isValid())) {
        qDebug() << "Error deleting tag" << tag.name();
        return false;
    }

    query = QString("DELETE FROM photo_tags WHERE tag_id = %1").arg(id);
    q.exec(query);
    if (Q_UNLIKELY(q.lastError().isValid())) {
        qDebug() << "Error deleting tag" << tag.name();
        return false;
    }

    /* Instead of walking through the cached photos and updating them, we just
     * clear the cache. Maybe profiling will tell us wrong, but from the time
     * being let's opt for this simpler option. */
    m_cachedPhotos.clear();

    Tag parent = tag.parent();
    m_tags.remove(id);

    emit("tagDeleted", Q_ARG(Tag, tag));
    if (parent.isValid()) {
        if (Q_UNLIKELY(!updateIsCategory(parent.id()))) return false;
    }

    queueUpdateTagsPopularity();
    return true;
}

QList<Tag> DatabasePrivate::allTags()
{
    QList<Tag> result;
    Q_FOREACH(TagId id, m_tags.keys()) {
        result.append(Tag(id));
    }
    return result;
}

QList<Tag> DatabasePrivate::tags(Tag parent)
{
    QList<Tag> result;
    QHashIterator<TagId,TagData> i(m_tags);
    while (i.hasNext()) {
        i.next();
        if (i.value().parent == parent.id()) {
            result.append(Tag(i.key()));
        }
    }
    return result;
}

int DatabasePrivate::computeUsageCount(Tag tag, const QList<PhotoId> &photos)
{
    QString query =
        QStringLiteral("SELECT COUNT(*) FROM photo_tags "
                       "WHERE tag_id = %1 AND photo_id IN (%2)");
    QStringList photoIds;
    Q_FOREACH(PhotoId id, photos) {
        photoIds.append(QString::number(id));
    }
    query = query.arg(tag.id()).arg(photoIds.join(','));

    QSqlQuery q(db());
    q.exec(query);
    q.next();
    return q.value(0).toInt();
}

Database::Database(QObject *parent):
    AbstractDatabase(new DatabasePrivate(this), parent)
{
}

Database::~Database()
{
    m_instance = 0;
}

Database *Database::instance()
{
    if (!m_instance) {
        m_instance = new Database;
        if (Q_UNLIKELY(!m_instance->d_func()->init())) {
            delete m_instance;
            m_instance = 0;
        }
    }
    return m_instance;
}

void Database::clearCache()
{
    Q_D(Database);
    d->m_cachedPhotos.clear();
}

RollId Database::createRoll(const QDateTime &time)
{
    Q_D(Database);
    return d->createRoll(time);
}

QList<Roll> Database::rolls(int limit)
{
    Q_D(Database);

    QString query =
        QStringLiteral("SELECT id FROM rolls "
                       "ORDER BY time DESC LIMIT ") +
        QString::number(limit);

    QSqlQuery q(d->db());
    q.exec(query);
    QList<Roll> ret;
    while (q.next()) {
        ret.append(Roll(q.value(0).toInt()));
    }
    return ret;
}

/* This won't add tags */
PhotoId Database::addPhoto(const Photo &photo, RollId roll)
{
    Q_D(Database);
    return d->addPhoto(photo, roll);
}

Database::Photo Database::photo(PhotoId photoId)
{
    Q_D(Database);
    return d->photo(photoId);
}

bool Database::deletePhoto(PhotoId photoId)
{
    Q_D(Database);
    return d->deletePhotos({photoId});
}

bool Database::deletePhotos(const QList<PhotoId> &ids)
{
    Q_D(Database);
    return d->deletePhotos(ids);
}

bool Database::makeVersion(PhotoId photoId, PhotoId master)
{
    Q_D(Database);
    return d->makeVersion(photoId, master);
}

bool Database::addTags(PhotoId photo, const QList<Tag> &tags)
{
    Q_D(Database);
    return d->addTags(photo, tags);
}

bool Database::addTagWithArea(PhotoId photo, Tag tag, const QRectF &area)
{
    Q_D(Database);
    bool ok = true;

    QSqlQuery q(d->db());
    q.prepare("INSERT OR REPLACE INTO photo_tags (photo_id, tag_id, area) "
              "VALUES (?, ?, ?)");
    q.addBindValue(photo);
    q.addBindValue(tag.id());
    if (area.isValid()) {
        q.addBindValue(QString("%1 %2 %3 %4").
                       arg(area.x(), 0, 'f', 4).
                       arg(area.y(), 0, 'f', 4).
                       arg(area.width(), 0, 'f', 4).
                       arg(area.height(), 0, 'f', 4));
    } else {
        q.addBindValue(QString());
    }
    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "DB error adding tag with area:" << q.lastError();
        ok = false;
    }

    d->queueUpdateTagsPopularity(false);
    d->emit("photoTagAreasChanged", Q_ARG(int, photo));
    return ok;
}

bool Database::setTags(PhotoId photo, const QList<Tag> &tags)
{
    Q_D(Database);
    d->clearTags(photo);
    return d->addTags(photo, tags);
}

bool Database::removeTags(PhotoId photo, const QList<Tag> &tags)
{
    Q_D(Database);

    QStringList tagIdsText;
    Q_FOREACH(Tag tag, tags) {
        tagIdsText.append(QString::number(tag.id()));
    }
    QSqlQuery q(d->db());
    q.prepare("DELETE FROM photo_tags WHERE photo_id = ? AND tag_id IN (" +
              tagIdsText.join(',') + ")");
    q.addBindValue(photo);
    if (Q_UNLIKELY(!q.exec())) {
        qWarning() << "DB error removing tags from photo" << q.lastError();
        return false;
    }

    d->queueUpdateTagsPopularity(true);
    d->emit("photoTagsChanged", Q_ARG(int, photo));
    return true;
}

QList<Tag> Database::tags(PhotoId photoId)
{
    Q_D(Database);
    return d->tags(photoId);
}

QVector<TagArea> Database::tagAreas(PhotoId photoId)
{
    Q_D(Database);
    QSqlQuery q(d->db());
    q.setForwardOnly(true);
    q.prepare("SELECT tag_id, area FROM photo_tags WHERE photo_id = ?"
              " AND area NOT NULL");
    q.addBindValue(photoId);
    q.exec();
    QVector<TagArea> result;
    const char *oldLocale = setlocale(LC_NUMERIC, NULL);
    setlocale(LC_NUMERIC, "C");
    while (q.next()) {
        TagId id = q.value(0).toInt();
        QByteArray coords = q.value(1).toString().toLatin1();
        double x, y, w, h;
        int parsed = sscanf(coords.constData(), "%lf %lf %lf %lf", &x, &y, &w, &h);
        if (parsed != 4) continue;
        result.append(TagArea(QRectF(x, y, w, h), id));
    }
    setlocale(LC_NUMERIC, oldLocale);
    return result;
}

bool Database::setDescription(PhotoId photo, const QString &description)
{
    Q_D(Database);
    QSqlQuery q(d->db());
    q.prepare("UPDATE photos SET title = ? WHERE id = ?");
    q.addBindValue(description);
    q.addBindValue(photo);
    q.exec();
    if (Q_UNLIKELY(q.lastError().isValid())) return false;

    // update the cache
    QHash<PhotoId,Database::Photo>::iterator i =
        d->m_cachedPhotos.find(photo);
    if (i != d->m_cachedPhotos.end()) {
        i->setDescription(description);
    }

    d->emit("photoChanged", Q_ARG(int, photo));
    return true;
}

bool Database::setRating(PhotoId photo, int rating)
{
    Q_D(Database);
    QSqlQuery q(d->db());
    q.prepare("UPDATE photos SET rating = ? WHERE id = ?");
    q.addBindValue(rating);
    q.addBindValue(photo);
    q.exec();
    if (Q_UNLIKELY(q.lastError().isValid())) return false;

    // update the cache
    QHash<PhotoId,Database::Photo>::iterator i =
        d->m_cachedPhotos.find(photo);
    if (i != d->m_cachedPhotos.end()) {
        i->setRating(rating);
    }

    d->emit("photoChanged", Q_ARG(int, photo));
    return true;
}

bool Database::setLocation(PhotoId photo, const GeoPoint &p)
{
    Q_D(Database);
    QSqlQuery q(d->db());
    q.prepare("UPDATE photos SET latitude = ?, longitude = ? WHERE id = ?");
    if (p.isValid()) {
        q.addBindValue(p.lat);
        q.addBindValue(p.lon);
    } else {
        q.addBindValue(QVariant(QVariant::Double));
        q.addBindValue(QVariant(QVariant::Double));
    }
    q.addBindValue(photo);
    q.exec();
    if (Q_UNLIKELY(q.lastError().isValid())) return false;

    // update the cache
    QHash<PhotoId,Database::Photo>::iterator i =
        d->m_cachedPhotos.find(photo);
    if (i != d->m_cachedPhotos.end()) {
        i->setLocation(p);
    }

    d->emit("photoChanged", Q_ARG(int, photo));
    return true;
}

QList<Database::Photo> Database::photos(const QList<PhotoId> &photoIds)
{
    Q_D(Database);
    return d->photos(photoIds);
}

QList<Database::Photo> Database::findPhotos(SearchFilters filters,
                                            Qt::SortOrder sort)
{
    Q_D(Database);
    return d->findPhotos(filters, sort);
}

QList<Database::Photo> Database::photoVersions(PhotoId photoId)
{
    Q_D(Database);
    return d->photoVersions(photoId);
}

PhotoId Database::masterVersion(PhotoId photoId)
{
    Q_D(Database);
    PhotoId masterId = d->masterVersion(photoId);
    return masterId ? masterId : photoId;
}

Tag Database::createTag(Tag parent, const QString &name, const QString &icon)
{
    Q_D(Database);
    return d->createTag(parent, name, icon);
}

bool Database::updateTag(Tag tag, const TagUpdate &data)
{
    Q_D(Database);
    QStringList assignments;
    Tag oldParent;
    Tag newParent;
    if (!data.name.isEmpty()) { assignments.append("name = ?"); }
    if (!data.icon.isEmpty()) { assignments.append("icon = ?"); }
    if (data.parent.isValid()) {
        newParent = data.parent.value<Tag>();
        if (!d->checkParentLoop(tag, newParent)) return false;
        assignments.append("category_id = ?");
        oldParent = tag.parent();
    } else {
        oldParent = Tag();
    }

    if (Q_UNLIKELY(assignments.isEmpty())) {
        return true;
    }

    QSqlQuery q(d->db());
    q.prepare("UPDATE tags SET " + assignments.join(",") + " WHERE id = ?");
    if (!data.name.isEmpty()) { q.addBindValue(data.name); }
    if (!data.icon.isEmpty()) { q.addBindValue(data.icon); }
    if (data.parent.isValid()) { q.addBindValue(newParent.id()); }
    q.addBindValue(tag.id());

    q.exec();
    if (Q_UNLIKELY(q.lastError().isValid())) return false;

    // update the cache
    TagData &tagData = d->m_tags[tag.id()];
    if (!data.name.isEmpty()) { tagData.name = data.name; }
    if (!data.icon.isEmpty()) { tagData.icon = data.icon; }
    if (data.parent.isValid()) { tagData.parent = newParent.id(); }

    d->emit("tagChanged", Q_ARG(Tag, tag));

    if (oldParent.isValid()) {
        if (Q_UNLIKELY(!d->updateIsCategory(oldParent.id()))) return false;
    }
    if (newParent.isValid()) {
        TagData &p = d->m_tags[newParent.id()];
        if (!p.isCategory) {
            p.isCategory = true;
            d->emit("tagChanged", Q_ARG(Tag, newParent));
        }
    }
    return true;
}

bool Database::deleteTag(Tag tag)
{
    Q_D(Database);
    return d->deleteTag(tag);
}

Tag Database::tag(const QString &name)
{
    Q_D(const Database);
    QHashIterator<TagId,TagData> i(d->m_tags);
    while (i.hasNext()) {
        i.next();
        if (i.value().name == name) {
            return Tag(i.key());
        }
    }
    return Tag();
}

QList<Tag> Database::allTags()
{
    Q_D(Database);
    return d->allTags();
}

QList<Tag> Database::tags(Tag parent)
{
    Q_D(Database);
    return d->tags(parent);
}

QList<Tag> Database::tags(const QStringList &tagNames)
{
    QList<Tag> tags;
    Q_FOREACH(const QString &tagName, tagNames) {
        Tag t = tag(tagName);
        if (Q_LIKELY(t.isValid())) {
            tags.append(t);
        }
    }
    return tags;
}

int Database::computeUsageCount(Tag tag, const QList<PhotoId> &photos)
{
    Q_D(Database);
    return d->computeUsageCount(tag, photos);
}

Tag Database::uncategorizedTag() const
{
    Q_D(const Database);
    return Tag(d->m_defaultTagUncategorized);
}

Tag Database::peopleTag() const
{
    Q_D(const Database);
    return Tag(d->m_defaultTagPeople);
}

Tag Database::placesTag() const
{
    Q_D(const Database);
    return Tag(d->m_defaultTagPlaces);
}

Tag Database::eventsTag() const
{
    Q_D(const Database);
    return Tag(d->m_defaultTagEvents);
}

Tag Database::importedTagsTag() const
{
    Q_D(const Database);
    return Tag(d->m_defaultTagImported);
}

QString Tag::name() const {
    return m_instance->d_func()->findTag(m_id).name;
}

QString Tag::icon() const {
    return m_instance->d_func()->findTag(m_id).icon;
}

bool Tag::isCategory() const {
    return m_instance->d_func()->findTag(m_id).isCategory;
}

Tag Tag::parent() const {
    return Tag(m_instance->d_func()->findTag(m_id).parent);
}

float Tag::popularity() const {
    DatabasePrivate *d = m_instance->d_func();
    return log2(d->findTag(m_id).usageCount + 1) / d->m_log2maxTagUsageCount;
}

int Tag::usageCount() const {
    return m_instance->d_func()->findTag(m_id).usageCount;
}

QDateTime Roll::time() const {
    return m_instance->d_func()->findRoll(m_id).time;
}

#include "database.moc"
