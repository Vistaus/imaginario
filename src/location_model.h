/*
 * Copyright (C) 2016-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGINARIO_LOCATION_MODEL_H
#define IMAGINARIO_LOCATION_MODEL_H

#include "types.h"

#include <QAbstractListModel>
#include <QQmlParserStatus>
#include <QStringList>
#include <QVariant>

namespace Imaginario {

class LocationModelPrivate;
class LocationModel: public QAbstractListModel, public QQmlParserStatus
{
    Q_OBJECT
    Q_INTERFACES(QQmlParserStatus)
    Q_ENUMS(Roles)
    Q_PROPERTY(int count READ rowCount NOTIFY countChanged)
    Q_PROPERTY(QVariant model READ model WRITE setModel NOTIFY modelChanged)
    Q_PROPERTY(QStringList usedRoles READ usedRoles WRITE setUsedRoles \
               NOTIFY usedRolesChanged)
    Q_PROPERTY(qreal zoomLevel READ zoomLevel WRITE setZoomLevel \
               NOTIFY zoomLevelChanged)
    Q_PROPERTY(int nonGeoTaggedCount READ nonGeoTaggedCount \
               NOTIFY countChanged)

public:
    enum Roles {
        LocationRole = Qt::UserRole + 1,
        CountRole,
        ItemRole, /* Not exposed to QML, otherwise MapItemView would try to
                     compute it */
    };

    LocationModel(QObject *parent = 0);
    ~LocationModel();

    void setModel(const QVariant &model);
    QVariant model() const;

    void setUsedRoles(const QStringList &roles);
    QStringList usedRoles() const;

    void setZoomLevel(qreal zoomLevel);
    qreal zoomLevel() const;

    int nonGeoTaggedCount() const;

    Q_INVOKABLE QVariantList getItems(int row) const;
    Q_INVOKABLE QVariant get(int row, const QString &role) const;

    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index,
                  int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

    void classBegin() Q_DECL_OVERRIDE;
    void componentComplete() Q_DECL_OVERRIDE;

Q_SIGNALS:
    void countChanged();
    void modelChanged();
    void usedRolesChanged();
    void zoomLevelChanged();

private:
    LocationModelPrivate *d_ptr;
    Q_DECLARE_PRIVATE(LocationModel)
};

} // namespace

#endif // IMAGINARIO_LOCATION_MODEL_H
