include(../common-config.pri)

TARGET = imaginario

CONFIG += \
    c++11 \
    link_pkgconfig \
    no_keywords

QT += \
    concurrent \
    positioning \
    quick \
    sql

!defined(EXIV2_LIBS, var):packagesExist(exiv2) {
    PKGCONFIG += exiv2
} else {
    LIBS += $${EXIV2_LIBS} -lexiv2
    INCLUDEPATH += $${EXIV2_INCLUDEPATH}
}

SOURCES += \
    abstract_database.cpp \
    application.cpp \
    database.cpp \
    digikam_importer.cpp \
    duplicate_detector.cpp \
    fspot_importer.cpp \
    helper_model.cpp \
    i18n.cpp \
    icon_thumbnailer.cpp \
    image_provider.cpp \
    importer.cpp \
    job_database.cpp \
    job_executor.cpp \
    location_model.cpp \
    main.cpp \
    metadata.cpp \
    photo_model.cpp \
    program_finder.cpp \
    roll_model.cpp \
    settings.cpp \
    shotwell_importer.cpp \
    tag_area_model.cpp \
    tag_model.cpp \
    thumbnailer.cpp \
    time_model.cpp \
    time_widget.cpp \
    types.cpp \
    updater.cpp \
    utils.cpp \
    writer.cpp

HEADERS += \
    abstract_database.h abstract_database_p.h \
    application.h \
    database.h \
    digikam_importer.h \
    duplicate_detector.h \
    face_detector.h \
    fspot_importer.h \
    helper_model.h \
    i18n.h \
    image_provider.h \
    importer.h \
    job.h \
    job_database.h \
    job_executor.h \
    location_model.h \
    metadata.h \
    mime_database.h \
    photo_model.h \
    program_finder.h \
    roll.h \
    roll_model.h \
    settings.h \
    shotwell_importer.h \
    tag_area_model.h \
    tag_model.h \
    thumbnailer.h \
    time_model.h \
    time_widget.h \
    types.h \
    updater.h \
    utils.h \
    writer.h

include(../opencv.pri)
CONFIG(opencv) {
    STATIC_LIBS = \
        opencv_objdetect \
        opencv_highgui \
        opencv_imgproc \
        opencv_core \
        IlmImf \
        IlmThread \
        Iex \
        Half
    equals(OPENCV_LIBS_ADDED, 0) {
        for (lib, STATIC_LIBS) {
            LIBS += "-l$${lib}"
        }
    }
    CONFIG(embedlibs) {
        for (lib, STATIC_LIBS) {
            so_file = lib$${lib}.so
            extra_libs.files += $$files($${OPENCV_LIBDIR}/$${so_file}.*)
        }
        extra_libs.path = $${INSTALL_LIB_DIR}
        INSTALLS += extra_libs
    } else {
    }
    SOURCES += face_detector.cpp

    face_detect.files = \
        $${TOP_SRC_DIR}/data/haarcascade_frontalface_default.xml
    face_detect.path = $${INSTALL_DATA_DIR}/$${DATA_DIR}
    INSTALLS += face_detect
} else {
    SOURCES += face_detector_stub.cpp
}

DEFINES += \
    APPLICATION_NAME=\\\"$${APPLICATION_NAME}\\\" \
    APPLICATION_VERSION=\\\"$${PROJECT_VERSION}\\\" \
    RELATIVE_DATA_DIR=\\\"$${RELATIVE_DATA_DIR}\\\"

target.path = $${INSTALL_BIN_DIR}
INSTALLS += target

CONFIG(desktop) {
    DEFINES += DESKTOP_BUILD

    QT += widgets # for styling

    SOURCES += \
        clipboard.cpp \
        folder_model.cpp
    HEADERS += \
        clipboard.h \
        folder_model.h

    include(third-party/SortFilterProxyModel/SortFilterProxyModel.pri)

    RESOURCES += \
        $${TOP_SRC_DIR}/data/desktop/icons/icons-desktop.qrc \
        desktop/qml/ui.qrc

    linux {
        SOURCES += \
            theme_image_provider.cpp
        HEADERS += \
            theme_image_provider.h
        DEFINES += HAS_THEME
    } else {
        RESOURCES += \
            $${TOP_SRC_DIR}/data/desktop/icons/icons-theme.qrc
    }
} else {
    theme.files = ubuntu-touch/theme
    theme.path = $${INSTALL_PREFIX}/
    INSTALLS += theme

    RESOURCES += \
        $${TOP_SRC_DIR}/data/ubuntu-touch/icons/icons-ubuntu-touch.qrc \
        ubuntu-touch/qml/ui.qrc
}

RESOURCES += \
    $${TOP_SRC_DIR}/data/icons/icons.qrc

macx {
    TARGET = Imaginario
    QMAKE_SUBSTITUTES += $${TOP_SRC_DIR}/data/desktop/ImaginarioInfo.plist.in
    QMAKE_INFO_PLIST = $${TOP_BUILD_DIR}/data/desktop/ImaginarioInfo.plist
    QT += svg # so that macdeployqt copies the svg plugin

    SOURCES += macos.mm
    HEADERS += macos.h
    LIBS += -framework CoreServices
}

win32 {
    RC_FILE = $${TOP_SRC_DIR}/data/desktop/windows/imaginario.rc
}
