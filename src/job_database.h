/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGINARIO_JOB_DATABASE_H
#define IMAGINARIO_JOB_DATABASE_H

#include "abstract_database.h"
#include "job.h"

#include <QObject>
#include <QVector>

namespace Imaginario {

class JobDatabasePrivate;
class JobDatabase: public AbstractDatabase
{
    Q_OBJECT

public:
    JobDatabase(QObject *parent = 0);
    ~JobDatabase();

    bool addJob(const Job &job) { return addJobs(QVector<Job>() << job); }
    bool addJobs(const QVector<Job> &jobs);
    bool markJobInProgress(int jobId);
    bool removeJob(int jobId);

    bool next(Job &job);

private:
    Q_DECLARE_PRIVATE(JobDatabase)
};

} // namespace

#endif // IMAGINARIO_JOB_DATABASE_H
