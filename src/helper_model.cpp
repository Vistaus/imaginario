/*
 * Copyright (C) 2017-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "helper_model.h"

#include "program_finder.h"
#include "utils.h"
#ifdef Q_OS_MACOS
#include "macos.h"
#endif

#include <QDebug>
#include <QDir>
#include <QJSEngine>
#include <QJSValue>
#include <QJsonDocument>
#include <QJsonObject>
#include <QProcess>
#include <QStandardPaths>
#include <QUrl>

using namespace Imaginario;

namespace Imaginario {

struct Helper {
    QString actionName;
    QString command;
    bool canBatch;
    bool readOnly;
    bool makeVersion;
    QStringList mimeTypes;
};

class HelperModelPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(HelperModel)

public:
    enum UrlPolicy {
        PreferLocal = 0, // Use local URLs when possible, but supports remote too
        OnlyLocal,
        AlwaysUrl,
    };
    HelperModelPrivate(HelperModel *q);
    ~HelperModelPrivate();

    void queueUpdate();
    void buildHelpersList();
    QString prepareFile(const QUrl &url, bool makeVersion,
                        UrlPolicy urlPolicy) const;
    static QStringList splitCommand(const QString &command);

private Q_SLOTS:
    void update();

private:
    QHash<int, QByteArray> m_roles;
    bool m_updateQueued;
    QDir m_photoDir;
    QJsonArray m_helpersData;
    QMimeType m_mimeType;
    QVector<Helper> m_allHelpers;
    QVector<Helper> m_helpers;
    HelperModel *q_ptr;
};

} // namespace

HelperModelPrivate::HelperModelPrivate(HelperModel *q):
    QObject(q),
    m_updateQueued(false),
    m_photoDir(QStandardPaths::writableLocation(
        QStandardPaths::PicturesLocation)),
    q_ptr(q)
{
    m_roles[HelperModel::ActionNameRole] = "actionName";
    m_roles[HelperModel::CommandRole] = "command";
    m_roles[HelperModel::CanBatchRole] = "canBatch";
    m_roles[HelperModel::ReadOnlyRole] = "readOnly";
    m_roles[HelperModel::MakeVersionRole] = "makeVersion";
}

HelperModelPrivate::~HelperModelPrivate()
{
}

void HelperModelPrivate::queueUpdate()
{
    if (!m_updateQueued) {
        QMetaObject::invokeMethod(this, "update", Qt::QueuedConnection);
        m_updateQueued = true;
    }
}

void HelperModelPrivate::update()
{
    Q_Q(HelperModel);

    m_updateQueued = false;

    q->beginResetModel();

    if (m_mimeType.isValid()) {
        m_helpers.clear();
        for (const auto &helper: m_allHelpers) {
            if (helper.mimeTypes.isEmpty()) {
                m_helpers.append(helper);
            } else {
                for (const QString &type: helper.mimeTypes) {
                    if (type.endsWith("/*")) {
                        QString category = type.left(type.size() - 1);
                        if (m_mimeType.name().startsWith(category)) {
                            m_helpers.append(helper);
                            break;
                        }
                    }

                    if (m_mimeType.inherits(type)) {
                        m_helpers.append(helper);
                        break;
                    }
                }
            }
        }
    } else {
        m_helpers = m_allHelpers;
    }

    q->endResetModel();
}

void HelperModelPrivate::buildHelpersList()
{
    ProgramFinder finder;

    m_allHelpers.clear();
    for (const QJsonValue &v: m_helpersData) {
        QJsonObject o = v.toObject();

        QString command = o.value("command").toString();

        if (command.isEmpty()) {
            const QStringList possibleNames =
                o.value("possibleNames").toVariant().toStringList();
            for (const QString &executable: possibleNames) {
                const QJsonObject program = finder.find(executable);
                if (!program.isEmpty()) {
                    command = program.value("command").toString();
                    break;
                }
            }
            // The program was not found; let's skip this entry
            if (command.isEmpty()) continue;
        }

        m_allHelpers.append({
             o["actionName"].toString(),
             command,
             o["canBatch"].toBool(),
             o["readOnly"].toBool(),
             o["makeVersion"].toBool(),
#if (QT_VERSION >= QT_VERSION_CHECK(5, 6, 0))
             o["mimeTypes"].toVariant().toStringList(),
#else
             QJsonValue(o["mimeTypes"]).toVariant().toStringList(),
#endif
        });
    }
}

QString HelperModelPrivate::prepareFile(const QUrl &url,
                                        bool makeVersion,
                                        UrlPolicy urlPolicy) const
{
    if (!url.isLocalFile()) {
        if (makeVersion) {
            qWarning() << "Versioning of non local files is currently unsupported";
            return QString();
        }
        if (urlPolicy == OnlyLocal) {
            qWarning() << "Passing of non local files is currently unsupported";
            return QString();
        }
    }

    QString filePath = (url.isLocalFile() && urlPolicy != AlwaysUrl) ?
        url.toLocalFile() : url.toString();
    if (makeVersion) {
        QString tmpFilePath;
        QFileInfo fileInfo(filePath);

        QList<QDir> dirs { fileInfo.dir(), m_photoDir };
        for (const QDir &dir: dirs) {
            if (!dir.exists()) continue;
            tmpFilePath = makeFileVersion(dir, fileInfo);
            if (QFile::copy(filePath, tmpFilePath)) {
                filePath = tmpFilePath;
                break;
            }
        }

        if (Q_UNLIKELY(filePath != tmpFilePath)) {
            qWarning() << "Could not copy file into new version:" << filePath;
            filePath = QString();
        }
    }

    return filePath;
}

QStringList HelperModelPrivate::splitCommand(const QString &command)
{
    bool inQuotes = false;
    QString part;
    QStringList parts;

    for (const QChar &ch: command) {
        if (ch == '"') {
            inQuotes = !inQuotes;
        } else if (ch == ' ' && !inQuotes) {
            parts.append(part);
            part.clear();
        } else {
            part.append(ch);
        }
    }
    if (!part.isEmpty()) {
        parts.append(part);
    }
    return parts;
}

HelperModel::HelperModel(QObject *parent):
    QAbstractListModel(parent),
    d_ptr(new HelperModelPrivate(this))
{
    QObject::connect(this, SIGNAL(modelReset()),
                     this, SIGNAL(countChanged()));
}

HelperModel::~HelperModel()
{
}

void HelperModel::setHelpersData(const QJsonArray &newData)
{
    Q_D(HelperModel);
    QJsonArray data(newData);
    if (data.isEmpty()) {
        data = defaultHelpersData();
    }
    if (data == d->m_helpersData) return;
    d->m_helpersData = data;
    Q_EMIT helpersDataChanged();

    d->buildHelpersList();
    d->queueUpdate();
}

QJsonArray HelperModel::helpersData() const
{
    Q_D(const HelperModel);
    return d->m_helpersData;
}

QJsonArray HelperModel::defaultHelpersData() const
{
    return QJsonArray {
        QJsonObject {
            { "possibleNames", QJsonArray { "PhotoTeleport", "Photokinesis", "phototeleport" } },
            { "actionName", tr("Share online with PhotoTeleport") },
            { "readOnly", true },
            { "makeVersion", false },
            { "canBatch", true },
            { "mimeTypes", QJsonArray { "image/*" } },
        },
        QJsonObject {
            { "possibleNames", QJsonArray { "MapperoGeotagger", "mappero-geotagger", "mappero" } },
            { "actionName", tr("Change location with Mappero Geotagger") },
            { "readOnly", false },
            { "makeVersion", false },
            { "canBatch", true },
            { "mimeTypes", QJsonArray { "image/*" } },
        },
        QJsonObject {
            { "actionName", "Edit in Gimp" },
            { "command", "gimp" },
            { "canBatch", false },
            { "readOnly", false },
            { "makeVersion", true },
        },
        QJsonObject {
            { "actionName", "Rotate EXIF 90°" },
            { "command", "exiftran -9 -i -g -p" },
            { "canBatch", true },
            { "readOnly", false },
            { "makeVersion", false },
            { "mimeTypes", QJsonArray { "image/jpeg" } },
        },
        QJsonObject {
            { "actionName", "Rotate EXIF 270°" },
            { "command", "exiftran -2 -i -g -p" },
            { "canBatch", true },
            { "readOnly", false },
            { "makeVersion", false },
            { "mimeTypes", QJsonArray { "image/jpeg" } },
        },
    };
}

static inline void swap(QJsonValueRef v1, QJsonValueRef v2)
{
    QJsonValue temp(v1);
    v1 = QJsonValue(v2);
    v2 = temp;
}

QJsonArray HelperModel::callableApplications() const
{
    /* If this method gets called more than once, we should try to optimize
     * this and load all apps only once */
    ProgramFinder finder;
    QJsonArray applications = finder.callableApplications();
    std::sort(applications.begin(), applications.end(),
              [](const QJsonValue &a, const QJsonValue &b) {
        QString nameA = a.toObject().value("actionName").toString();
        QString nameB = b.toObject().value("actionName").toString();
        return nameA.localeAwareCompare(nameB) < 0;
    });
    return applications;
}

void HelperModel::setPhotoDir(const QString &dir)
{
    Q_D(HelperModel);
    if (d->m_photoDir.path() == dir) return;
    d->m_photoDir = dir;
    Q_EMIT photoDirChanged();
}

QString HelperModel::photoDir() const
{
    Q_D(const HelperModel);
    return d->m_photoDir.path();
}

void HelperModel::setMimeType(const QMimeType &mimeType)
{
    Q_D(HelperModel);
    if (d->m_mimeType == mimeType) return;
    d->m_mimeType = mimeType;
    Q_EMIT mimeTypeChanged();

    d->queueUpdate();
}

QMimeType HelperModel::mimeType() const
{
    Q_D(const HelperModel);
    return d->m_mimeType;
}

void HelperModel::classBegin()
{
    Q_D(HelperModel);
    d->m_updateQueued = true;
}

void HelperModel::componentComplete()
{
    Q_D(HelperModel);
    d->update();
}

bool HelperModel::runHelper(int row, const QUrl &url,
                            const QJSValue &callback)
{
    return runHelper(row, QList<QUrl>{ url }, callback);
}

bool HelperModel::runHelper(int row, const QList<QUrl> &urls,
                            const QJSValue &callback)
{
    Q_D(HelperModel);

    if (row < 0 || row >= d->m_helpers.count()) return false;

    const Helper &e = d->m_helpers[row];

    QStringList arguments = d->splitCommand(e.command);
    QString command = arguments.takeFirst();

    HelperModelPrivate::UrlPolicy urlPolicy = HelperModelPrivate::PreferLocal;
    int fileIndex = -1;
    for (int i = 0; i < arguments.count(); i++) {
        const QString &arg = arguments[i];
        if (arg[0] != '%') continue;
        switch (arg[1].unicode()) {
        case 'u':
        case 'U':
            urlPolicy = HelperModelPrivate::AlwaysUrl;
            fileIndex = i;
            break;
        case 'f':
        case 'F':
            urlPolicy = HelperModelPrivate::OnlyLocal;
            fileIndex = i;
            break;
        }
    }

    QStringList fileNames;
    for (const QUrl &url: urls) {
        QString fileName = d->prepareFile(url, e.makeVersion, urlPolicy);
        if (fileName.isEmpty()) return false;
        fileNames.append(fileName);
    }

#ifdef Q_OS_MACOS
    if (MacOS::runApp(command, fileNames)) {
        QJSValue copy(callback);
        QJSValue names = fileNames.count() == 1 ?
            fileNames.first() : qjsEngine(this)->toScriptValue(fileNames);
        copy.call(QJSValueList{ names, e.makeVersion });
    }
#else
    if (fileIndex >= 0) {
        arguments.removeAt(fileIndex);
        int i = fileIndex;
        Q_FOREACH(const QString &fileName, fileNames) {
            arguments.insert(i++, fileName);
        }
    } else {
        arguments.append(fileNames);
    }

    QProcess *process = new QProcess;
    QObject::connect(process,
                     static_cast<void (QProcess::*)(int, QProcess::ExitStatus)>
                         (&QProcess::finished),
                     [this,process,fileNames,callback,e]() {
        QJSValue copy(callback);
        QJSValue names = fileNames.count() == 1 ?
            fileNames.first() : qjsEngine(this)->toScriptValue(fileNames);
        copy.call(QJSValueList{ names, e.makeVersion });
        process->deleteLater();
    });
    process->start(command, arguments);
#endif

    return true;
}

QVariant HelperModel::get(int row, const QString &roleName) const
{
    int role = roleNames().key(roleName.toLatin1(), -1);
    return data(index(row, 0), role);
}

int HelperModel::rowCount(const QModelIndex &parent) const
{
    Q_D(const HelperModel);
    Q_UNUSED(parent);
    return d->m_helpers.count();
}

QVariant HelperModel::data(const QModelIndex &index, int role) const
{
    Q_D(const HelperModel);

    int row = index.row();
    if (row < 0 || row >= d->m_helpers.count()) return QVariant();

    const Helper &e = d->m_helpers[row];
    switch (role) {
    case ActionNameRole:
        return e.actionName;
    case CommandRole:
        return e.command;
    case CanBatchRole:
        return e.canBatch;
    case ReadOnlyRole:
        return e.readOnly;
    case MakeVersionRole:
        return e.makeVersion;
    default:
        qWarning() << "Unknown role ID:" << role;
        return QVariant();
    }
}

QHash<int, QByteArray> HelperModel::roleNames() const
{
    Q_D(const HelperModel);
    return d->m_roles;
}

#include "helper_model.moc"
