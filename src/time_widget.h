/*
 * Copyright (C) 2016-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGINARIO_TIME_WIDGET_H
#define IMAGINARIO_TIME_WIDGET_H

#include <QColor>
#include <QDate>
#include <QFont>
#include <QQuickPaintedItem>
#include <QVariant>

namespace Imaginario {

class TimeWidgetPrivate;
class TimeWidget: public QQuickPaintedItem
{
    Q_OBJECT
    Q_ENUMS(TimeApproximation)
    Q_PROPERTY(QVariant model READ model WRITE setModel NOTIFY modelChanged)
    Q_PROPERTY(bool olderFirst READ olderFirst WRITE setOlderFirst \
               NOTIFY olderFirstChanged)
    Q_PROPERTY(QDate date0 READ date0 WRITE setDate0 NOTIFY date0Changed)
    Q_PROPERTY(QDate date1 READ date1 WRITE setDate1 NOTIFY date1Changed)
    Q_PROPERTY(QColor activeColor READ activeColor WRITE setActiveColor \
               NOTIFY activeColorChanged)
    Q_PROPERTY(QColor inactiveColor READ inactiveColor WRITE setInactiveColor \
               NOTIFY inactiveColorChanged)
    Q_PROPERTY(QColor textColor READ textColor WRITE setTextColor \
               NOTIFY textColorChanged)
    Q_PROPERTY(QFont font READ font WRITE setFont NOTIFY fontChanged)
    Q_PROPERTY(int monthWidth READ monthWidth WRITE setMonthWidth \
               NOTIFY monthWidthChanged)
    Q_PROPERTY(double contentOffset READ contentOffset WRITE setContentOffset \
               NOTIFY contentOffsetChanged)
    Q_PROPERTY(double requestedContentOffset READ requestedContentOffset \
               WRITE setRequestedContentOffset \
               NOTIFY requestedContentOffsetChanged)
    Q_PROPERTY(int updateCounter READ updateCounter NOTIFY updated)

public:
    enum TimeApproximation {
        ApproxToCloser,
        ApproxToOlder,
        ApproxToNewer,
    };

    TimeWidget(QQuickItem *parent = 0);
    ~TimeWidget();

    void setModel(const QVariant &model);
    QVariant model() const;

    void setOlderFirst(bool olderFirst);
    bool olderFirst() const;

    void setDate0(const QDate &date);
    QDate date0() const;

    void setDate1(const QDate &date);
    QDate date1() const;

    void setActiveColor(const QColor &color);
    QColor activeColor() const;

    void setInactiveColor(const QColor &color);
    QColor inactiveColor() const;

    void setTextColor(const QColor &color);
    QColor textColor() const;

    void setFont(const QFont &font);
    QFont font() const;

    void setMonthWidth(int width);
    int monthWidth() const;

    void setContentOffset(double offset);
    double contentOffset() const;

    void setRequestedContentOffset(double offset);
    double requestedContentOffset() const;

    int updateCounter() const;

    Q_INVOKABLE double offsetForDate(const QDate &date,
                                     TimeApproximation approx) const;
    Q_INVOKABLE QDate dateForOffset(double offset,
                                    TimeApproximation approx) const;

    void paint(QPainter *painter) Q_DECL_OVERRIDE;

protected:
    void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void wheelEvent(QWheelEvent *event) Q_DECL_OVERRIDE;

Q_SIGNALS:
    void dateClicked(QDate date);
    void updated();

    void modelChanged();
    void olderFirstChanged();
    void date0Changed();
    void date1Changed();
    void activeColorChanged();
    void inactiveColorChanged();
    void textColorChanged();
    void fontChanged();
    void monthWidthChanged();
    void contentOffsetChanged();
    void requestedContentOffsetChanged();

private:
    TimeWidgetPrivate *d_ptr;
    Q_DECLARE_PRIVATE(TimeWidget)
};

} // namespace

Q_DECLARE_METATYPE(Imaginario::TimeWidget::TimeApproximation)

#endif // IMAGINARIO_TIME_WIDGET_H
