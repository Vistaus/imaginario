/*
 * Copyright (C) 2019-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGINARIO_MACOS_H
#define IMAGINARIO_MACOS_H

#include <QJsonArray>
#include <QString>
#include <QStringList>

namespace Imaginario {

class MacOS
{
public:
    static bool runApp(const QString &app, const QStringList &files);
    static QJsonArray scanApps();
};

} // namespace

#endif // IMAGINARIO_MACOS_H
