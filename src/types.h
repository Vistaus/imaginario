/*
 * Copyright (C) 2014-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGINARIO_TYPES_H
#define IMAGINARIO_TYPES_H

#include <QDateTime>
#include <QDebug>
#include <QGeoCoordinate>
#include <QList>
#include <QMetaType>
#include <QPointF>
#include <QRectF>
#include <QString>
#include <QStringList>
#include <math.h>

namespace Imaginario {

typedef int RollId;
typedef int PhotoId;
typedef int TagId;

#ifdef USE_DOUBLES_FOR_LATLON
typedef double Geo;
#else
typedef float Geo;
#endif

#ifdef USE_DOUBLES_FOR_LATLON
#define GSIN(x) sin(x)
#define GCOS(x) cos(x)
#define GASIN(x) asin(x)
#define GTAN(x) tan(x)
#define GATAN(x) atan(x)
#define GATAN2(x, y) atan2(x, y)
#define GEXP(x) exp(x)
#define GLOG(x) log(x)
#define GPOW(x, y) pow(x, y)
#define GSQTR(x) sqrt(x)
#define GFLOOR(x) floor(x)
#define GROUND(x) round(x)
#define GCEIL(x) ceil(x)
#define GTRUNC(x) trunc(x)
#define GMOD(x, y) fmod(x, y)
#else
#define GSIN(x) sinf(x)
#define GCOS(x) cosf(x)
#define GASIN(x) asinf(x)
#define GTAN(x) tanf(x)
#define GATAN(x) atanf(x)
#define GATAN2(x, y) atan2f(x, y)
#define GEXP(x) expf(x)
#define GLOG(x) logf(x)
#define GPOW(x, y) powf(x, y)
#define GSQTR(x) sqrtf(x)
#define GFLOOR(x) floorf(x)
#define GROUND(x) roundf(x)
#define GCEIL(x) ceilf(x)
#define GTRUNC(x) truncf(x)
#define GMOD(x, y) fmodf(x, y)
#endif

struct GeoPoint {
#if (QT_VERSION >= QT_VERSION_CHECK(5, 5, 0))
    Q_GADGET
    Q_PROPERTY(bool valid READ isValid)
    Q_PROPERTY(qreal lat MEMBER lat)
    Q_PROPERTY(qreal lon MEMBER lon)
#endif
public:
    GeoPoint(): lat(NAN), lon(0) {}
    GeoPoint(const QPointF &p): lat(p.x()), lon(p.y()) {}
    GeoPoint(const QGeoCoordinate &p): lat(p.latitude()), lon(p.longitude()) {}
    GeoPoint(Geo lat, Geo lon): lat(lat), lon(lon) {}
    Geo lat;
    Geo lon;

    inline GeoPoint normalized() const;
    QPointF toPointF() const { return QPointF(lat, lon); }
    QGeoCoordinate toGeoCoordinate() const { return QGeoCoordinate(lat, lon); }
    Geo distanceTo(const GeoPoint &other) const;
    bool isValid() const { return lat == lat; /* NaN != NaN */ }
    friend inline bool operator==(const GeoPoint &, const GeoPoint &);
    friend inline bool operator!=(const GeoPoint &, const GeoPoint &);
};

GeoPoint GeoPoint::normalized() const
{
    GeoPoint ret(GMOD(lat, 180), GMOD(lon, 360));
    /* TODO: normalize latitude */
    if (ret.lon > 180) {
        ret.lon -= 360;
    } else if (ret.lon < -180) {
        ret.lon += 360;
    }
    return ret;
}

inline bool operator==(const GeoPoint &p1, const GeoPoint &p2) {
    return p1.lon == p2.lon && (p1.isValid() == p2.isValid()) &&
        (!p1.isValid() || p1.lat == p2.lat);
}

inline bool operator!=(const GeoPoint &p1, const GeoPoint &p2) {
    return p1.lon != p2.lon || (p1.isValid() != p2.isValid()) ||
        (p1.isValid() && p1.lat != p2.lat);
}

class Tag {
#if (QT_VERSION >= QT_VERSION_CHECK(5, 5, 0))
    Q_GADGET
    Q_PROPERTY(bool valid READ isValid CONSTANT)
    Q_PROPERTY(TagId id READ id CONSTANT)
    Q_PROPERTY(QString name READ name CONSTANT)
    Q_PROPERTY(QString icon READ icon CONSTANT)
    Q_PROPERTY(QString iconSource READ iconSource CONSTANT)
    Q_PROPERTY(Tag parent READ parent CONSTANT)
#endif
public:
    Tag(): m_id(-1) {}
    explicit Tag(TagId id): m_id(id) {}
    TagId id() const { return m_id; }
    bool isValid() const { return m_id >= 0; }
    QString name() const;
    QString icon() const;
    bool isCategory() const;
    Tag parent() const;
    float popularity() const;
    int usageCount() const;

    bool operator==(const Tag other) const { return m_id == other.m_id; }
    bool operator<(const Tag other) const { return m_id < other.m_id; }
    static QStringList tagNames(const QList<Tag> &tags) {
        QStringList names;
        Q_FOREACH(const Tag t, tags) {
            names.append(t.name());
        }
        return names;
    }

    QString iconSource() const {
        QString i = icon();
        if (i.isEmpty()) {
            return QStringLiteral("qrc:/icons/tag_NoIcon");
        } else if (i.startsWith("file://") || i.startsWith("tag_icon:")) {
            return QStringLiteral("image://item/") + i;
        } else {
            return i;
        }
    }

private:
    TagId m_id;
};

inline uint qHash(const Tag &tag, uint seed = 0) {
    return ::qHash(tag.id(), seed);
}

class TagArea
{
public:
    TagArea() {}
    TagArea(const QRectF &rect, Tag tag):
        m_rect(rect), m_tag(tag) {}
    TagArea(const QRectF &rect, TagId tagId):
        m_rect(rect), m_tag(Tag(tagId)) {}

    const QRectF &rect() const { return m_rect; }
    Tag tag() const { return m_tag; }

    bool operator==(const TagArea &other) const {
        return m_rect == other.m_rect && m_tag == other.m_tag;
    }

private:
    QRectF m_rect;
    Tag m_tag;
};

inline uint qHash(const TagArea &ta, uint seed = 0) {
    return ::qHash(ta.tag().id(), seed);
}

struct SearchFilters
{
    SearchFilters():
        roll0(-1),
        roll1(-1),
        rating0(-1),
        rating1(-1),
        areaTagged(false),
        nonGeoTagged(false),
        skipNonDefaultVersions(false)
    {}

    QDateTime time0;
    QDateTime time1;
    RollId roll0;
    RollId roll1;
    int rating0;
    int rating1;
    /* This is a list of OR-ed lists of required tags:
     * (QList<Tag>) OR (QList<Tag>) OR ... */
    QList<QList<Tag>> requiredTags;
    QList<Tag> forbiddenTags;
    bool areaTagged;
    GeoPoint location0; // Top left
    GeoPoint location1; // Bottom right
    bool nonGeoTagged;
    QList<PhotoId> excludedPhotos;
    bool skipNonDefaultVersions;

    bool operator==(const SearchFilters &other) const {
        return this->time0 == other.time0 && this->time1 == other.time1 &&
            this->roll0 == other.roll0 && this->roll1 == other.roll1 &&
            this->rating0 == other.rating0 && this->rating1 == other.rating1 &&
            this->requiredTags == other.requiredTags &&
            this->forbiddenTags == other.forbiddenTags &&
            this->areaTagged == other.areaTagged &&
            this->location0 == other.location0 && this->location1 == other.location1 &&
            this->nonGeoTagged == other.nonGeoTagged &&
            this->excludedPhotos == other.excludedPhotos &&
            this->skipNonDefaultVersions == other.skipNonDefaultVersions;
    }
};

void registerTypes();

} // namespace

Q_DECLARE_METATYPE(Imaginario::GeoPoint)
Q_DECLARE_METATYPE(Imaginario::PhotoId)
Q_DECLARE_METATYPE(Imaginario::Tag)

inline QDebug operator<<(QDebug dbg, const Imaginario::Tag &t) {
    dbg.nospace() << "(" << t.name() << ")";
    return dbg.space();
}

#endif // IMAGINARIO_TYPES_H
