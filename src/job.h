/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGINARIO_JOB_H
#define IMAGINARIO_JOB_H

#include "types.h"

#include <QFlags>
#include <QSharedDataPointer>
#include <QString>
#include <QUrl>
#include <QVariantMap>

namespace Imaginario {

class JobDatabase;
class JobDatabasePrivate;

class Job {
protected:
    class JobData: public QSharedData {
    public:
        JobData(const JobData &other):
            QSharedData(other), m_type(other.m_type),
            m_id(other.m_id), m_inProgress(other.m_inProgress),
            m_uniqueKey(other.m_uniqueKey),
            m_data(other.m_data) {}
        JobData(int type):
            m_type(type), m_id(-1), m_inProgress(false) {}
        JobData(int type, int id, bool inProgress,
                const QString &uniqueKey, const QVariantMap &data):
            m_type(type), m_id(id), m_inProgress(inProgress),
            m_uniqueKey(uniqueKey), m_data(data) {}
        friend class Job;
        int m_type;
        int m_id;
        bool m_inProgress;
        QString m_uniqueKey;
        QVariantMap m_data;
    };

public:
    enum Type {
        Undefined = 0,
        Photo,
        Thumbnail,
    };
    Job(Type type = Undefined): d(new JobData(type)) {}

    Job(const Job &other): d(other.d) {}

    Type type() const { return Type(d->m_type); }
    bool isValid() const { return d->m_id >= 0; }
    int id() const { return d->m_id; }
    void setInProgress(bool value) { d->m_inProgress = value; }
    bool inProgress() const { return d->m_inProgress; }

protected:
    const QVariantMap &data() const { return d->m_data; }
    void setUniqueKey(const QString &key) { d->m_uniqueKey = key; }

private:
    friend class JobDatabase;
    friend class JobDatabasePrivate;
    Job(int id, int type, bool inProgress,
        const QString &uniqueKey, const QVariantMap &data):
        d(new JobData(type, id, inProgress, uniqueKey, data)) {}
    void setId(int id) { d->m_id = id; }
    QString uniqueKey() const { return d->m_uniqueKey; }

protected:
    Job(JobData *d): d(d) {}
    QSharedDataPointer<JobData> d;
};

class PhotoJob: public Job {
private:
    struct Keys {
        static constexpr const char *photoId = "photoId";
        static constexpr const char *opWriteTags = "opWriteTags";
        static constexpr const char *opWriteRating = "opWriteRating";
        static constexpr const char *opWriteLocation = "opWriteLocation";
        static constexpr const char *opWriteTitle = "opWriteTitle";
        static constexpr const char *opWriteDescription = "opWriteDescription";
        static constexpr const char *opWriteRegions = "opWriteRegions";
    };
public:
    enum Operation {
        None = 0,
        WriteTags = 1 << 0,
        WriteRating = 1 << 1,
        WriteLocation = 1 << 2,
        WriteTitle = 1 << 3,
        WriteDescription = 1 << 4,
        WriteRegions = 1 << 5,
    };
    Q_DECLARE_FLAGS(Operations, Operation)
    PhotoJob(PhotoId photoId, Operations ops = None):
        Job(new JobData(Photo)) {
        d->m_data[Keys::photoId] = photoId;
        addOperations(ops);
        setUniqueKey(QString("photo-%1").arg(photoId));
    }

    PhotoId photoId() const { return d->m_data[Keys::photoId].toInt(); }

    void addOperations(Operations ops) {
        if (ops & WriteTags) { d->m_data[Keys::opWriteTags] = true; }
        if (ops & WriteRating) { d->m_data[Keys::opWriteRating] = true; }
        if (ops & WriteLocation) { d->m_data[Keys::opWriteLocation] = true; }
        if (ops & WriteTitle) { d->m_data[Keys::opWriteTitle] = true; }
        if (ops & WriteDescription) {
            qWarning() << "Must call PhotoJob::setDescription";
        }
        if (ops & WriteRegions) { d->m_data[Keys::opWriteRegions] = true; }
    }
    Operations operations() const {
        Operations ops = None;
        QStringList keys = d->m_data.keys();
        for (const QString &key: keys) {
            if (key == Keys::opWriteTags) ops |= WriteTags;
            if (key == Keys::opWriteRating) ops |= WriteRating;
            if (key == Keys::opWriteLocation) ops |= WriteLocation;
            if (key == Keys::opWriteTitle) ops |= WriteTitle;
            if (key == Keys::opWriteDescription) ops |= WriteDescription;
            if (key == Keys::opWriteRegions) ops |= WriteRegions;
        }
        return ops;
    }

    void setDescription(const QString &text) {
        d->m_data[Keys::opWriteDescription] = text;
    }
    QString description() const {
        return data()[Keys::opWriteDescription].toString();
    }

    void setRegionDescription(const QRectF &area, const QString &text) {
        QVariantMap extra = data()[Keys::opWriteRegions].toMap();
        extra[rectToString(area)] = text;
        d->m_data[Keys::opWriteRegions] = extra;
    }
    QString regionDescription(const QRectF &area) const {
        QVariantMap extra = data()[Keys::opWriteRegions].toMap();
        return extra[rectToString(area)].toString();
    }

private:
    static QString rectToString(const QRectF &rect) {
        QRect r((rect.topLeft() * 100).toPoint(),
                (rect.size() * 100).toSize());
        return QString("rect-%1,%2-%3x%4").
            arg(r.x()).arg(r.y()).arg(r.width()).arg(r.height());
    }
};

class ThumbnailJob: public Job {
private:
    struct Keys {
        static constexpr const char *uri = "uri";
    };
public:
    ThumbnailJob(const QUrl &uri):
        Job(new JobData(Thumbnail)) {
        d->m_data[Keys::uri] = uri;
        setUniqueKey(QString("thumb-%1").arg(uri.toString()));
    }

    QUrl uri() const { return d->m_data[Keys::uri].toUrl(); }
};

} // namespace

Q_DECLARE_OPERATORS_FOR_FLAGS(Imaginario::PhotoJob::Operations)

#endif // IMAGINARIO_JOB_H
