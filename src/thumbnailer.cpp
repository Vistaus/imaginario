/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "thumbnailer.h"

#include <QCoreApplication>
#include <QCryptographicHash>
#include <QDateTime>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QImageReader>
#include <QStandardPaths>

using namespace Imaginario;

enum ThumnailSize {
    Size256 = 0,
    Size512,
    SizeLast
};

static bool sizeIsUsed[] = { false, false };

static struct SizeData {
    const char *dirName;
    int size;
} sizeData[] = {
    { "/thumbnails/large", 256 },
    { "/thumbnails/xlarge", 512 },
};

namespace Imaginario {

class ThumbnailerPrivate
{
    ThumbnailerPrivate();

    bool setupCacheDir(const QString &base);
    QString thumbnailFileName(const QUrl &uri) const {
        QByteArray ba = QCryptographicHash::hash(uri.toEncoded(),
                                                 QCryptographicHash::Md5);
        return QString::fromLatin1(ba.toHex()) + ".png";
    }

    QString thumbnailPath(const QUrl &image, ThumnailSize size) const;
    QString thumbnailPath(const QUrl &uri, const QSize &requestedSize) const;
    bool createThumbnail(QImage &thumbNail,
                         const QString &thumbnailFile, int size) const;

private:
    friend class Thumbnailer;
    QDir m_dir[SizeLast];
    QDir m_failDir;
    QString m_software;
};

} // namespace

ThumbnailerPrivate::ThumbnailerPrivate()
{
    QString cache =
        QStandardPaths::writableLocation(QStandardPaths::GenericCacheLocation);
    if (!setupCacheDir(cache)) {
        cache = QStandardPaths::writableLocation(QStandardPaths::CacheLocation);
        setupCacheDir(cache);
    }

    m_software = QCoreApplication::applicationName() + " " +
        QCoreApplication::applicationVersion();
}

bool ThumbnailerPrivate::setupCacheDir(const QString &base)
{
    bool ok;

    for (int i = 0; i < SizeLast; i++) {
        QDir dir(base + sizeData[i].dirName);
        if (!dir.exists()) {
            ok = dir.mkpath(".");
            if (ok) {
                QFile::setPermissions(dir.path(),
                                      QFileDevice::ReadOwner |
                                      QFileDevice::WriteOwner |
                                      QFileDevice::ExeOwner);
            }
        } else {
            /* The directory already exists; check if we can actually use it */
            QFile file(dir.filePath("imaginario_test"));
            ok = file.open(QIODevice::WriteOnly);
            if (ok) {
                file.remove();
            }
        }

        if (ok) {
            m_dir[i] = dir;
        }
    }

    m_failDir = QDir(base + "/thumbnails/fail/" +
                     QCoreApplication::applicationName());
    if (!m_failDir.exists()) {
        if (m_failDir.mkpath(".")) {
            QFile::setPermissions(m_failDir.path(),
                                  QFileDevice::ReadOwner |
                                  QFileDevice::WriteOwner |
                                  QFileDevice::ExeOwner);
        }
    }

    return ok;
}

QString ThumbnailerPrivate::thumbnailPath(const QUrl &image,
                                          ThumnailSize size) const
{
    return m_dir[size].filePath(thumbnailFileName(image));
}

QString ThumbnailerPrivate::thumbnailPath(const QUrl &uri,
                                          const QSize &requestedSize) const
{
    if (Q_UNLIKELY(requestedSize.isNull())) return QString();

    bool firstSize = true;
    for (int i = 0; i < SizeLast; i++) {
        const SizeData &data = sizeData[i];
        if (requestedSize.width() <= data.size &&
            requestedSize.height() <= data.size) {
            if (firstSize) {
                sizeIsUsed[i] = true;
                firstSize = false;
            }
            QFileInfo fileInfo(thumbnailPath(uri, ThumnailSize(i)));
            if (fileInfo.exists()) return fileInfo.filePath();
        }
    }

    return QString();
}

bool ThumbnailerPrivate::createThumbnail(QImage &thumbnail,
                                         const QString &thumbnailFile,
                                         int size) const
{
    if (size > 0) {
        QSize imageSize = thumbnail.size();
        if (imageSize.width() > size || imageSize.height() > size) {
            thumbnail = thumbnail.scaled(size, size, Qt::KeepAspectRatio,
                                         Qt::SmoothTransformation);
        }
    }

    return thumbnail.save(thumbnailFile, "PNG", -1);
}

Thumbnailer::Thumbnailer():
    d_ptr(new ThumbnailerPrivate)
{
}

Thumbnailer::~Thumbnailer()
{
    delete d_ptr;
}

QImage Thumbnailer::load(const QUrl &uri, const QSize &requestedSize) const
{
    Q_D(const Thumbnailer);

    QImageReader reader(d->thumbnailPath(uri, requestedSize));
    QSize imageSize = reader.size();
    /* scale when loading, if possible */
    if (imageSize.isValid()) {
        QSize scaledSize = imageSize.scaled(requestedSize,
                                            Qt::KeepAspectRatio);
        reader.setScaledSize(scaledSize);
    }
    QImage thumbnail = reader.read();
    if (!thumbnail.isNull()) {
        QFileInfo fileInfo(uri.toLocalFile());
        if (thumbnail.text(QStringLiteral("Thumb::URI")) != uri.toEncoded() ||
            thumbnail.text(QStringLiteral("Thumb::MTime")) !=
            QString::number(fileInfo.lastModified().toTime_t())) {
            /* This thumbnail refers to another file, or another version of
             * this file */
            return QImage();
        }

        if (!imageSize.isValid()) {
            thumbnail = thumbnail.scaled(requestedSize, Qt::KeepAspectRatio,
                                         Qt::SmoothTransformation);
        }
    }

    return thumbnail;
}

bool Thumbnailer::create(const QUrl &uri) const
{
    Q_D(const Thumbnailer);

    QFileInfo fileInfo(uri.toLocalFile());
    if (Q_UNLIKELY(!fileInfo.isReadable())) return false;

    QString fileName = d->thumbnailFileName(uri);

    /* If a failed thumbnailing attempt exists, don't try again. */
    if (d->m_failDir.exists(fileName)) {
        /* TODO: if the file was created by an older version of imaginario, we
         * could try again. */
        return false;
    }

    QImageReader reader(fileInfo.filePath());
#if (QT_VERSION >= QT_VERSION_CHECK(5, 5, 0))
    reader.setAutoTransform(true);
#endif

    /* Find what's the biggest size we are interested in */
    int maxSize = 256;
    for (int i = SizeLast - 1; i >= 0; i--) {
        if (sizeIsUsed[i]) maxSize = sizeData[i].size;
    }

    /* scale when loading, if possible */
    QSize imageSize = reader.size();
    if (imageSize.isValid() &&
        (imageSize.width() > maxSize || imageSize.height() > maxSize)) {
        QSize scaledSize = imageSize.scaled(maxSize, maxSize, Qt::KeepAspectRatio);
        reader.setScaledSize(scaledSize);
    }

    QImage thumbnail = reader.read();
    bool thumbnailIsValid = !thumbnail.isNull();
    if (!thumbnailIsValid) {
        thumbnail = QImage(QSize(1, 1), QImage::Format_ARGB32);
    }
    thumbnail.setText(QStringLiteral("Software"), d->m_software);
    thumbnail.setText(QStringLiteral("Thumb::URI"), uri.toEncoded());
    thumbnail.setText(QStringLiteral("Thumb::MTime"),
                      QString::number(fileInfo.lastModified().toTime_t()));
    if (thumbnailIsValid) {
        if (thumbnail.format() != QImage::Format_ARGB32) {
            thumbnail = thumbnail.convertToFormat(QImage::Format_ARGB32);
        }
        bool saved = false;
        for (int i = SizeLast - 1; i >= 0; i--) {
            if (sizeIsUsed[i] || (i == 0 && !saved)) {
                saved = d->createThumbnail(thumbnail,
                                           d->m_dir[i].filePath(fileName),
                                           sizeData[i].size);
            }
        }
        return saved;
    } else {
        d->createThumbnail(thumbnail,
                           d->m_failDir.filePath(fileName),
                           0);
        return false;
    }
}

bool &Thumbnailer::size256Used()
{
    return sizeIsUsed[Size256];
}

bool &Thumbnailer::size512Used()
{
    return sizeIsUsed[Size512];
}
