/*
 * Copyright (C) 2016-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "time_widget.h"

#include "time_model.h"

#include <QDebug>
#include <QFontMetrics>
#include <QGuiApplication>
#include <QPainter>
#include <QPalette>

using namespace Imaginario;

namespace Imaginario {

class TimeWidgetPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(TimeWidget)

public:
    TimeWidgetPrivate(TimeWidget *q);
    ~TimeWidgetPrivate();

    void paint(QPainter *painter);
    void updateFontHeight();
    QDate roundToMonth(const QDate &date) const;
    double offsetForDate(const QDate &date,
                         TimeWidget::TimeApproximation approx) const;
    QDate monthAt(double offset, TimeWidget::TimeApproximation approx) const;
    static inline int monthDiff(const QDate &date0, const QDate &date);

private Q_SLOTS:
    void updateWidth();
    void updateCounts();

private:
    TimeModel *m_model;
    QDate m_firstMonth;
    QDate m_date0;
    QDate m_date1;
    QColor m_activeColor;
    QColor m_inactiveColor;
    QColor m_textColor;
    QFont m_font;
    int m_fontHeight;
    int m_monthWidth;
    double m_contentOffset;
    double m_requestedContentOffset;
    bool m_olderFirst;
    QVector<float> m_monthlyCount;
    int m_dragStartX;
    int m_dragStartOffset;
    int m_updateCounter;
    TimeWidget *q_ptr;
};

} // namespace

TimeWidgetPrivate::TimeWidgetPrivate(TimeWidget *q):
    QObject(q),
    m_model(0),
    m_monthWidth(20),
    m_contentOffset(0),
    m_requestedContentOffset(0),
    m_olderFirst(false),
    m_dragStartX(0),
    m_dragStartOffset(0),
    m_updateCounter(0),
    q_ptr(q)
{
    QPalette palette = QGuiApplication::palette();
    q->setFillColor(palette.color(QPalette::Base));
    m_activeColor = palette.color(QPalette::Highlight);
    m_inactiveColor = palette.color(QPalette::Disabled,
                                    QPalette::Highlight);
    m_textColor = palette.color(QPalette::Text);
    updateFontHeight();
}

TimeWidgetPrivate::~TimeWidgetPrivate()
{
}

void TimeWidgetPrivate::updateWidth()
{
    Q_Q(TimeWidget);
    int months = m_monthlyCount.count();
    q->setImplicitWidth(months * m_monthWidth);
    q->update();

    m_updateCounter++;
    Q_EMIT q->updated();
}

void TimeWidgetPrivate::updateCounts()
{
    m_monthlyCount.clear();

    int numMonths = m_model ? m_model->rowCount() : 0;
    if (numMonths > 0) {
        QModelIndex index = m_model->index(0);
        m_firstMonth = QDate(index.data(TimeModel::TimeRole).toDate().year(), 1, 1);

        index = m_model->index(numMonths - 1);
        QDate lastMonth(index.data(TimeModel::TimeRole).toDate().year(), 12, 1);

        m_monthlyCount = QVector<float>(monthDiff(m_firstMonth, lastMonth) + 1);
        for (int i = 0; i < numMonths; i++) {
            QModelIndex index = m_model->index(i, 0);
            QDate month = index.data(TimeModel::TimeRole).toDate();
            qreal count = index.data(TimeModel::NormalizedCountRole).toReal();
            m_monthlyCount[monthDiff(m_firstMonth, month)] = count;
        }
    } else {
        // Just show an empty year
        m_firstMonth = QDate(QDate::currentDate().year(), 1, 1);
        m_monthlyCount = QVector<float>(12);
    }

    updateWidth();
}

void TimeWidgetPrivate::updateFontHeight()
{
    QFontMetrics metrics(m_font);
    m_fontHeight = metrics.ascent();
}

void TimeWidgetPrivate::paint(QPainter *painter)
{
    Q_Q(TimeWidget);

    QTransform transformation = painter->transform();
    transformation.translate(-m_contentOffset, 0);
    painter->setTransform(transformation);

    painter->setFont(m_font);
    QPen pen(m_textColor);
    painter->setPen(pen);
    painter->setRenderHints(QPainter::Antialiasing, true);

    int firstMonthDisplayed = m_olderFirst ? 1 : 12;
    double h = q->height();
    for (int i = m_monthlyCount.count() - 1; i >= 0; i--) {
        double x = i * m_monthWidth;
        int monthIndex = m_olderFirst ? i : (m_monthlyCount.count() - 1 - i);
        QDate currentMonth = m_firstMonth.addMonths(monthIndex);

        QColor barColor = (currentMonth < m_date0 ||
                           (m_date1.isValid() && currentMonth > m_date1)) ?
            m_inactiveColor : m_activeColor;
        int barHeight = ceil(h * m_monthlyCount[monthIndex]);
        painter->fillRect(x + 2, h, m_monthWidth - 4, -barHeight,
                          barColor);

        int tickLength = h / 8;
        if (currentMonth.month() == firstMonthDisplayed) {
            painter->drawText(x + 2, tickLength + m_fontHeight,
                              QString::number(currentMonth.year()));
            tickLength = h / 4;
        }

        painter->drawLine(x, 0, x, tickLength);
    }
}

int TimeWidgetPrivate::monthDiff(const QDate &date0, const QDate &date)
{
    return (date.year() - date0.year()) * 12 + (date.month() - date0.month());
}

QDate TimeWidgetPrivate::roundToMonth(const QDate &date) const
{
    // Get the first of that month
    QDate rounded = date.addDays(1 - date.day());
    if (rounded.daysTo(date) > 15) {
        rounded = rounded.addMonths(1);
    }
    return rounded;
}

double TimeWidgetPrivate::offsetForDate(const QDate &date,
                                        TimeWidget::TimeApproximation approx) const
{
    QDate d = (approx == TimeWidget::ApproxToCloser) ? roundToMonth(date) : date;
    int monthsFromOldest = monthDiff(m_firstMonth, d);
    int monthOffset = m_olderFirst ? monthsFromOldest : (m_monthlyCount.count() - 1 - monthsFromOldest);
    if (approx == TimeWidget::ApproxToOlder) {
        monthOffset++;
    }
    return m_monthWidth * monthOffset;
}

QDate TimeWidgetPrivate::monthAt(double offset,
                                 TimeWidget::TimeApproximation approx) const
{
    int offs = offset + m_contentOffset;
    int column = offs / m_monthWidth;
    if (column < 0 || column > m_monthlyCount.count()) {
        return QDate();
    }

    int monthIndex =
        m_olderFirst ? column : (m_monthlyCount.count() - column);
    if (approx == TimeWidget::ApproxToOlder ||
        (approx == TimeWidget::ApproxToCloser &&
         offs % m_monthWidth > m_monthWidth / 2)) {
        monthIndex--;
    }
    return m_firstMonth.addMonths(monthIndex);
}

TimeWidget::TimeWidget(QQuickItem *parent):
    QQuickPaintedItem(parent),
    d_ptr(new TimeWidgetPrivate(this))
{
    setAcceptedMouseButtons(Qt::LeftButton | Qt::MidButton);
}

TimeWidget::~TimeWidget()
{
    delete d_ptr;
}

void TimeWidget::paint(QPainter *painter)
{
    Q_D(TimeWidget);
    d->paint(painter);
}

void TimeWidget::mousePressEvent(QMouseEvent *event)
{
    Q_D(TimeWidget);
    if (event->button() == Qt::MidButton) {
        d->m_dragStartX = event->x();
        d->m_dragStartOffset = d->m_contentOffset;
    } else if (event->button() == Qt::LeftButton) {
        QDate month = d->monthAt(event->localPos().x(), ApproxToOlder);
        if (month.isValid()) {
            Q_EMIT dateClicked(month);
        }
    }
    event->accept();
}

void TimeWidget::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::MidButton) {
        setRequestedContentOffset(contentOffset());
        event->accept();
        return;
    }
    QQuickPaintedItem::mouseReleaseEvent(event);
}

void TimeWidget::mouseMoveEvent(QMouseEvent *event)
{
    Q_D(TimeWidget);
    if (event->buttons() == Qt::MidButton) {
        setContentOffset(d->m_dragStartOffset - event->x() + d->m_dragStartX);
        event->accept();
        return;
    }
    QQuickPaintedItem::mouseMoveEvent(event);
}

void TimeWidget::wheelEvent(QWheelEvent *event)
{
     QPoint numPixels = event->pixelDelta();
     QPoint numDegrees = event->angleDelta() / 4;

     if (!numPixels.isNull()) {
         setContentOffset(contentOffset() + numPixels.x() + numPixels.y());
     } else if (!numDegrees.isNull()) {
         setRequestedContentOffset(requestedContentOffset() + (numDegrees.x() + numDegrees.y()) * 6);
     }

     event->accept();
}

void TimeWidget::setModel(const QVariant &model)
{
    Q_D(TimeWidget);

    TimeModel *m = model.value<TimeModel*>();
    if (m == d->m_model) return;

    if (d->m_model) {
        d->m_model->disconnect(d);
    }
    d->m_model = m;
    if (d->m_model) {
        QObject::connect(d->m_model, SIGNAL(modelReset()),
                         d, SLOT(updateCounts()));
    }

    d->updateCounts();
    Q_EMIT modelChanged();
}

QVariant TimeWidget::model() const
{
    Q_D(const TimeWidget);
    return QVariant::fromValue(d->m_model);
}

void TimeWidget::setOlderFirst(bool olderFirst)
{
    Q_D(TimeWidget);
    if (d->m_olderFirst == olderFirst) return;
    d->m_olderFirst = olderFirst;
    update();
    Q_EMIT olderFirstChanged();
}

bool TimeWidget::olderFirst() const
{
    Q_D(const TimeWidget);
    return d->m_olderFirst;
}

void TimeWidget::setDate0(const QDate &date)
{
    Q_D(TimeWidget);
    d->m_date0 = date;
    update();
    Q_EMIT date0Changed();
}

QDate TimeWidget::date0() const
{
    Q_D(const TimeWidget);
    return d->m_date0;
}

void TimeWidget::setDate1(const QDate &date)
{
    Q_D(TimeWidget);
    d->m_date1 = date;
    update();
    Q_EMIT date1Changed();
}

QDate TimeWidget::date1() const
{
    Q_D(const TimeWidget);
    return d->m_date1;
}

void TimeWidget::setActiveColor(const QColor &color)
{
    Q_D(TimeWidget);
    d->m_activeColor = color;
    update();
    Q_EMIT activeColorChanged();
}

QColor TimeWidget::activeColor() const
{
    Q_D(const TimeWidget);
    return d->m_activeColor;
}

void TimeWidget::setInactiveColor(const QColor &color)
{
    Q_D(TimeWidget);
    d->m_inactiveColor = color;
    update();
    Q_EMIT inactiveColorChanged();
}

QColor TimeWidget::inactiveColor() const
{
    Q_D(const TimeWidget);
    return d->m_inactiveColor;
}

void TimeWidget::setTextColor(const QColor &color)
{
    Q_D(TimeWidget);
    d->m_textColor = color;
    update();
    Q_EMIT textColorChanged();
}

QColor TimeWidget::textColor() const
{
    Q_D(const TimeWidget);
    return d->m_textColor;
}

void TimeWidget::setFont(const QFont &font)
{
    Q_D(TimeWidget);
    d->m_font = font;
    d->updateFontHeight();
    update();
    Q_EMIT fontChanged();
}

QFont TimeWidget::font() const
{
    Q_D(const TimeWidget);
    return d->m_font;
}

void TimeWidget::setMonthWidth(int width)
{
    Q_D(TimeWidget);
    d->m_monthWidth = width;
    d->updateWidth();
    Q_EMIT monthWidthChanged();
}

int TimeWidget::monthWidth() const
{
    Q_D(const TimeWidget);
    return d->m_monthWidth;
}

void TimeWidget::setContentOffset(double offset)
{
    Q_D(TimeWidget);
    d->m_contentOffset = offset;
    update();
    Q_EMIT contentOffsetChanged();
}

double TimeWidget::contentOffset() const
{
    Q_D(const TimeWidget);
    return d->m_contentOffset;
}

void TimeWidget::setRequestedContentOffset(double offset)
{
    Q_D(TimeWidget);
    d->m_requestedContentOffset =
        qMax(0.0, qMin(offset, implicitWidth() - width()));
    Q_EMIT requestedContentOffsetChanged();
}

double TimeWidget::requestedContentOffset() const
{
    Q_D(const TimeWidget);
    return d->m_requestedContentOffset;
}

int TimeWidget::updateCounter() const
{
    Q_D(const TimeWidget);
    return d->m_updateCounter;
}

double TimeWidget::offsetForDate(const QDate &date,
                                 TimeApproximation approx) const
{
    Q_D(const TimeWidget);
    return d->offsetForDate(date, approx);
}

QDate TimeWidget::dateForOffset(double offset,
                                TimeApproximation approx) const
{
    Q_D(const TimeWidget);
    return d->monthAt(offset, approx);
}

#include "time_widget.moc"
