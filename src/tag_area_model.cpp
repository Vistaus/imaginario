/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tag_area_model.h"

#include "application.h"
#include "database.h"
#include "face_detector.h"

#include <QDebug>
#include <QFlags>
#include <QHash>
#include <QRectF>
#include <QVariant>
#include <QVector>
#include <QtConcurrent>

using namespace Imaginario;

namespace Imaginario {

typedef QList<QRectF> Areas;

class NameArea
{
public:
    enum Source {
        None = 0,
        Local = 1 << 0,
        Database = 1 << 1,
        Detected = 1 << 2,
    };
    Q_DECLARE_FLAGS(UpdateSource, Source)

    NameArea(): source(Local) {}
    NameArea(const TagArea &a):
        rect(a.rect()),
        tag(a.tag()),
        tagName(a.tag().name()),
        source(Database)
    {}
    NameArea(const QRectF &rect, const QString &tagName = QString()):
        rect(rect),
        tagName(tagName),
        source(Detected)
    {}
    NameArea(const QJsonObject &o);

    QRectF rect;
    Tag tag;
    QString tagName;
    Source source;
};

NameArea::NameArea(const QJsonObject &o):
    rect(o["x"].toDouble(), o["y"].toDouble(),
         o["width"].toDouble(), o["height"].toDouble()),
    tagName(o["name"].toString()),
    source(Local)
{
}

class TagAreaModelPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(TagAreaModel)

public:
    TagAreaModelPrivate(TagAreaModel *q);
    ~TagAreaModelPrivate();

    void queueUpdate(NameArea::UpdateSource source);
    Q_INVOKABLE void update();

    Areas detectFaces(QUrl fileUrl);

    bool areaIsSame(const QRectF &a, const QRectF &b);
    bool areaIsNew(const QRectF &area);

private Q_SLOTS:
    void onDetectionFinished();
    void onDatabaseChanged(PhotoId id);
    void onTagChanged(Tag tag);

private:
    QHash<int, QByteArray> m_roles;
    Database *m_db;
    NameArea::UpdateSource m_updateQueued;
    PhotoId m_photoId;
    QVector<NameArea> m_areas;
    QJsonArray m_editingAreas;
    FaceDetector m_detector;
    QFutureWatcher<Areas> m_detectOperation;
    mutable TagAreaModel *q_ptr;
};

} // namespace

static inline QString dataPath()
{
    Application *app =
        static_cast<Application*>(QCoreApplication::instance());
    return app->dataPath() + "data/";
}

TagAreaModelPrivate::TagAreaModelPrivate(TagAreaModel *q):
    m_db(Database::instance()),
    m_updateQueued(NameArea::None),
    m_photoId(-1),
    m_detector(dataPath()),
    q_ptr(q)
{
    m_roles[TagAreaModel::TagNameRole] = "tagName";
    m_roles[TagAreaModel::RectRole] = "rect";
    m_roles[TagAreaModel::EditingRole] = "editing";
    m_roles[TagAreaModel::TagRole] = "tag";

    QObject::connect(&m_detectOperation, SIGNAL(finished()),
                     this, SLOT(onDetectionFinished()));
    QObject::connect(m_db, SIGNAL(photoTagAreasChanged(PhotoId)),
                     this, SLOT(onDatabaseChanged(PhotoId)));
    /* FIXME This should be fixed in the Database class, which should emit the
     * photoTagAreasChanged() signal when a tag is deleted which has an area
     * assigned to it.
     */
    QObject::connect(m_db, SIGNAL(photoTagsChanged(PhotoId)),
                     this, SLOT(onDatabaseChanged(PhotoId)));

    QObject::connect(m_db, &Database::tagChanged,
                     this, &TagAreaModelPrivate::onTagChanged);
    QObject::connect(m_db, &Database::tagDeleted,
                     this, &TagAreaModelPrivate::onTagChanged);
}

TagAreaModelPrivate::~TagAreaModelPrivate()
{
}

void TagAreaModelPrivate::queueUpdate(NameArea::UpdateSource source)
{
    if ((m_updateQueued | source) != m_updateQueued) {
        QMetaObject::invokeMethod(this, "update", Qt::QueuedConnection);
        m_updateQueued |= source;
    }
}

void TagAreaModelPrivate::update()
{
    Q_Q(TagAreaModel);

    QVector<NameArea> newAreas;
    if (m_updateQueued & NameArea::Local) {
        for (const QJsonValue &v: m_editingAreas) {
            newAreas.append(NameArea(v.toObject()));
        }
    }

    if (m_updateQueued & NameArea::Database) {
        QVector<TagArea> storedAreas = m_db->tagAreas(m_photoId);
        for (const TagArea &a: storedAreas) {
            newAreas.append(NameArea(a));
        }
    }

    for (const NameArea &a: m_areas) {
        if (!(m_updateQueued & a.source)) {
            newAreas.append(a);
        }
    }
    m_updateQueued &= ~(NameArea::Local | NameArea::Database);
    q->beginResetModel();
    m_areas = newAreas;
    q->endResetModel();
}

Areas TagAreaModelPrivate::detectFaces(QUrl fileUrl)
{
    return m_detector.checkFile(fileUrl);
}

bool TagAreaModelPrivate::areaIsSame(const QRectF &a, const QRectF &b)
{
    QRectF intersection = a & b;
    if (intersection.isEmpty()) return false;

    qreal areaInt = intersection.width() * intersection.height();
    qreal areaA = a.width() * a.height();
    return areaInt > areaA * 0.7;
}

bool TagAreaModelPrivate::areaIsNew(const QRectF &area)
{
    Q_FOREACH(const NameArea &na, m_areas) {
        if (areaIsSame(na.rect, area)) return false;
    }
    return true;
}

void TagAreaModelPrivate::onDetectionFinished()
{
    Q_Q(TagAreaModel);

    if (!m_detectOperation.isCanceled()) {
        Areas areas = m_detectOperation.result();

        int index = m_areas.count();
        Areas filteredAreas;
        Q_FOREACH(const QRectF &area, areas) {
            if (areaIsNew(area)) {
                filteredAreas.append(area);
            }
        }
        q->beginInsertRows(QModelIndex(),
                           index, index + filteredAreas.count() - 1);
        Q_FOREACH(const QRectF &area, filteredAreas) {
            m_areas.append(NameArea(area));
        }
        q->endInsertRows();
    }

    Q_EMIT q->detectingChanged();
}

void TagAreaModelPrivate::onDatabaseChanged(PhotoId photoId)
{
    if (photoId != m_photoId) return;

    m_updateQueued |= NameArea::Database;
    update();
}

void TagAreaModelPrivate::onTagChanged(Tag tag)
{
    bool mustUpdate = false;

    for (const NameArea &a: m_areas) {
        if (a.tag == tag) {
            mustUpdate = true;
            break;
        }
    }

    if (mustUpdate) {
        m_updateQueued |= NameArea::Database;
        update();
    }
}

TagAreaModel::TagAreaModel(QObject *parent):
    QAbstractListModel(parent),
    d_ptr(new TagAreaModelPrivate(this))
{
    QObject::connect(this, SIGNAL(modelReset()),
                     this, SIGNAL(countChanged()));
    QObject::connect(this, SIGNAL(rowsRemoved(const QModelIndex&,int,int)),
                     this, SIGNAL(countChanged()));
    QObject::connect(this, SIGNAL(rowsInserted(const QModelIndex&,int,int)),
                     this, SIGNAL(countChanged()));
}

TagAreaModel::~TagAreaModel()
{
    delete d_ptr;
}

void TagAreaModel::setPhotoId(PhotoId photoId)
{
    Q_D(TagAreaModel);
    if (d->m_photoId == photoId) return;
    d->m_photoId = photoId;

    d->queueUpdate(NameArea::Database);

    Q_EMIT photoIdChanged();
}

PhotoId TagAreaModel::photoId() const
{
    Q_D(const TagAreaModel);
    return d->m_photoId;
}

bool TagAreaModel::canDetect() const
{
    Q_D(const TagAreaModel);
    return d->m_detector.isValid();
}

bool TagAreaModel::isDetecting() const
{
    Q_D(const TagAreaModel);
    return d->m_detectOperation.isRunning();
}

void TagAreaModel::setEditingAreas(const QJsonArray &areas)
{
    Q_D(TagAreaModel);

    if (areas == d->m_editingAreas) return;
    d->m_editingAreas = areas;
    d->queueUpdate(NameArea::Local);
    Q_EMIT editingAreasChanged();
}

QJsonArray TagAreaModel::editingAreas() const
{
    Q_D(const TagAreaModel);
    return d->m_editingAreas;
}

bool TagAreaModel::detectFaces()
{
    Q_D(TagAreaModel);
    if (!d->m_detector.isValid() || isDetecting()) {
        return false;
    }
    QUrl fileUrl = d->m_db->photo(d->m_photoId).url();
    QFuture<Areas> future =
        QtConcurrent::run(d, &TagAreaModelPrivate::detectFaces,
                          fileUrl);
    d->m_detectOperation.setFuture(future);

    Q_EMIT detectingChanged();

    return true;
}

void TagAreaModel::cancelDetection()
{
    Q_D(TagAreaModel);
    /* This will cause the finished() signal to be emitted and the
     * future to be canceled, if it's running. */
    d->m_detectOperation.setFuture(QFuture<Areas>());
}

QVariant TagAreaModel::get(int row, const QString &roleName) const
{
    int role = roleNames().key(roleName.toLatin1(), -1);
    return data(index(row, 0), role);
}

int TagAreaModel::rowCount(const QModelIndex &parent) const
{
    Q_D(const TagAreaModel);
    Q_UNUSED(parent);
    return d->m_areas.count();
}

QVariant TagAreaModel::data(const QModelIndex &index, int role) const
{
    Q_D(const TagAreaModel);

    int row = index.row();
    if (row < 0 || row >= d->m_areas.count()) return QVariant();

    const NameArea &a = d->m_areas[row];
    switch (role) {
    case TagNameRole:
        return a.tagName;
    case RectRole:
        return a.rect;
    case EditingRole:
        return a.source == NameArea::Local;
    case TagRole:
        return QVariant::fromValue(a.tag);
    default:
        qWarning() << "Unknown role ID:" << role;
        return QVariant();
    }
}

QHash<int, QByteArray> TagAreaModel::roleNames() const
{
    Q_D(const TagAreaModel);
    return d->m_roles;
}

void TagAreaModel::classBegin()
{
    Q_D(TagAreaModel);
    d->m_updateQueued =
        NameArea::UpdateSource(NameArea::Database | NameArea::Local);
}

void TagAreaModel::componentComplete()
{
    Q_D(TagAreaModel);
    d->update();
}

#include "tag_area_model.moc"
