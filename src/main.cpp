/*
 * Copyright (C) 2014-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "application.h"
#include "image_provider.h"
#ifdef HAS_THEME
#include "theme_image_provider.h"
#endif
#include "types.h"

#include <QDir>
#include <QProcessEnvironment>
#include <QQmlApplicationEngine>
#include <QQmlContext>

int main(int argc, char *argv[])
{
    // Workaround for https://bugs.launchpad.net/bugs/1323853
    qputenv("UBUNTU_MENUPROXY", "");

    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();

    // If running in debug mode, use a different DB file
    if (env.contains(QStringLiteral("IMAGINARIO_DEBUG"))) {
        qputenv("XDG_CONFIG_HOME",
                QDir::homePath().toUtf8() + "/.config/imaginario-dbg");
    }

    Imaginario::Application app(argc, argv);

    // Make sure that builtin helpers are found
    qputenv("PATH", QCoreApplication::applicationDirPath().toUtf8() + ":" +
            qgetenv("PATH"));

    Imaginario::registerTypes();

    QQmlApplicationEngine engine;
    engine.addImageProvider(QStringLiteral("item"),
                            new Imaginario::ImageProvider);
#ifdef HAS_THEME
    engine.addImageProvider(QStringLiteral("theme"),
                            new Imaginario::ThemeImageProvider);
#endif
    engine.addImportPath("qrc:/");
    engine.setBaseUrl(QUrl::fromLocalFile(app.dataPath()));
    engine.load(QUrl("qrc:/imaginario.qml"));

    return app.exec();
}
