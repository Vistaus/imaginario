import Imaginario 1.0
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

ActionSelectionPopover {
    id: root

    property var model: null

    signal detectionRequested()

    actions: ActionList {
        Action {
            text: i18n.tr("Enable face tagging")
            enabled: !Settings.showAreaTags
            onTriggered: Settings.showAreaTags = true
        }

        Action {
            text: i18n.tr("Detect faces")
            enabled: Settings.showAreaTags && model.canDetect
            onTriggered: root.detectionRequested()
        }

        Action {
            text: i18n.tr("Disable face tagging")
            enabled: Settings.showAreaTags
            onTriggered: Settings.showAreaTags = false
        }
    }
}
