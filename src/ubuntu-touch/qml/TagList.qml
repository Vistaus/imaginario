import QtQuick 2.0
import Ubuntu.Components 1.3

Item {
    id: root

    property alias tags: tagRepeater.model
    property var selectedIndexes: []
    property var selectedTags: []

    signal ellipseClicked()

    property bool __updating: false

    implicitHeight: flow.implicitHeight

    onSelectedIndexesChanged: if (!__updating) {
        var newTags = []
        for (var i = 0; i < selectedIndexes.length; i++) {
            newTags.push(tags[selectedIndexes[i]])
        }
        __updating = true
        selectedTags = newTags
        __updating = false
    }

    onSelectedTagsChanged: if (!__updating) {
        var newIndexes = []
        for (var i = 0; i < selectedTags.length; i++) {
            newIndexes.push(tags.indexOf(selectedTags[i]))
        }
        __updating = true
        selectedIndexes = newIndexes
        __updating = false
    }

    Flow {
        id: flow
        anchors.fill: parent
        spacing: units.gu(0.5)
        Label {
            height: contentHeight + units.gu(1)
            text: tags.length > 0 ? i18n.tr("Tags:") : i18n.tr("No tags")
            elide: Text.ElideRight
            verticalAlignment: Text.AlignVCenter
        }
        Repeater {
            id: tagRepeater
            model: tags
            Rectangle {
                width: contents.width + units.gu(1)
                height: contents.height + units.gu(1)
                color: flow.isSelected(index) ? "white" : "#888"
                opacity: (y + height <= flow.height) ? 1.0 : 0.0
                radius: units.gu(1)

                Item {
                    id: contents
                    anchors.centerIn: parent
                    width: childrenRect.width
                    height: childrenRect.height
                    Label {
                        id: tagLabel
                        width: Math.min(flow.width, implicitWidth)
                        verticalAlignment: Text.AlignVCenter
                        text: modelData
                        color: flow.isSelected(index) ? "black": "#666"
                        fontSize: "small"
                        elide: Text.ElideRight
                    }
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: flow.toggleSelection(index)
                }
            }
        }

        function lastVisibleItem() {
            var item = null
            for (var i = 0; i < tagRepeater.count; i++) {
                if (tagRepeater.itemAt(i).opacity > 0) {
                    item = tagRepeater.itemAt(i)
                }
            }
            return item
        }

        function isSelected(index) {
            return selectedIndexes.indexOf(index) >= 0
        }

        function toggleSelection(index) {
            var i = selectedIndexes.indexOf(index)
            var newSelectedList = selectedIndexes
            if (i >= 0) {
                newSelectedList.splice(i, 1)
            } else {
                newSelectedList.push(index)
            }
            selectedIndexes = newSelectedList
        }
    }

    Item {
        anchors.fill: flow
        Rectangle {
            property Item lastVisibleItem: flow.lastVisibleItem()
            y: lastVisibleItem ? lastVisibleItem.y : 0
            x: lastVisibleItem ? Math.min(lastVisibleItem.x + lastVisibleItem.width + flow.spacing, parent.width - width) : 0
            width: contents.width + units.gu(1)
            height: contents.height + units.gu(1)
            visible: false
            color: "white"
            onLastVisibleItemChanged: computeVisibility()
            onHeightChanged: computeVisibility()
            radius: units.gu(1)

            Item {
                id: contents
                anchors.centerIn: parent
                width: childrenRect.width
                height: childrenRect.height
                Label {
                    id: tagLabel
                    verticalAlignment: Text.AlignVCenter
                    text: i18n.tr("...")
                    fontSize: "small"
                    color: "black"
                }
            }

            MouseArea {
                anchors.fill: parent
                onClicked: root.ellipseClicked()
            }

            function computeVisibility() {
                if (lastVisibleItem == null ||
                    lastVisibleItem == tagRepeater.itemAt(tagRepeater.count - 1)) {
                    visible = false
                } else {
                    visible = true
                }
            }
        }
    }
}
