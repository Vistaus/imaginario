import Imaginario 1.0
import QtQuick 2.3
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem

Page {
    id: root

    property var duplicates

    signal ignoreItem()
    signal importAsNotDuplicate()
    signal importAsVersion(int photoId)
    signal importReplacing(int photoId)

    property int __highlightBorderSize: units.dp(4)

    title: i18n.tr("Duplicate item")
    flickable: flick

    Flickable {
        id: flick
        anchors.fill: parent
        contentHeight: contentItem.childrenRect.height

        Column {
            anchors { left: parent.left; right: parent.right; margins: units.gu(1) }
            spacing: units.gu(1)

            Label {
                anchors { left: parent.left; right: parent.right }
                wrapMode: Text.Wrap
                text: i18n.tr("This image might be a duplicate of one of the images below:")
            }

            ListView {
                id: listview
                anchors { left: parent.left; right: parent.right }
                height: units.gu(13) + __highlightBorderSize
                orientation: ListView.Horizontal
                spacing: 1
                delegate: Item {
                    height: ListView.view.height
                    width: image.width + __highlightBorderSize
                    Rectangle {
                        anchors.fill: parent
                        visible: parent.ListView.isCurrentItem
                        color: UbuntuColors.orange
                    }
                    Image {
                        id: image
                        anchors.centerIn: parent
                        height: parent.height - __highlightBorderSize
                        smooth: true
                        source: model.url
                        asynchronous: true
                        sourceSize.height: height
                        MouseArea {
                            anchors.fill: parent
                            onClicked: listview.currentIndex = index
                        }
                    }
                }
                model: PhotoModel {
                    photoIds: duplicates
                }
            }

            ListItem.ItemSelector {
                id: picker
                expanded: true
                model: [
                    i18n.tr("Don't import item"),
                    i18n.tr("Import as a new item"),
                    i18n.tr("Import as a version"),
                    i18n.tr("Import replacing")
                ]
            }

            Label {
                id: actionInfo
                anchors { left: parent.left; right: parent.right }
                wrapMode: Text.Wrap
            }

            ListItem.SingleControl {
                height: units.gu(6)
                control: Button {
                    text: i18n.tr("Done")
                    anchors {
                        fill: parent
                        margins: units.gu(1)
                    }
                    onClicked: {
                        if (root.state == "ignoreItem") {
                            root.ignoreItem()
                        } else if (root.state == "importAsNotDuplicate") {
                            root.importAsNotDuplicate()
                        } else {
                            var photoId = listview.model.get(listview.currentIndex, "photoId")
                            if (root.state == "importAsVersion") {
                                root.importAsVersion(photoId)
                            } else if (root.state == "importReplacing") {
                                root.importReplacing(photoId)
                            }
                        }
                        pageStack.pop()
                    }
                }
            }
        }
    }

    states: [
        State {
            name: "ignoreItem"
            when: picker.selectedIndex == 0
            PropertyChanges {
                target: actionInfo
                text: i18n.tr("This photo won't be imported")
            }
        },
        State {
            name: "importAsNotDuplicate"
            when: picker.selectedIndex == 1
            PropertyChanges {
                target: actionInfo
                text: i18n.tr("The photo is not a duplicate; it will be imported as a new item")
            }
        },
        State {
            name: "importAsVersion"
            when: picker.selectedIndex == 2
            PropertyChanges {
                target: actionInfo
                text: i18n.tr("The photo will be imported as a version of the above selected photo")
            }
        },
        State {
            name: "importReplacing"
            when: picker.selectedIndex == 3
            PropertyChanges {
                target: actionInfo
                text: i18n.tr("The photo will be imported and will replace the photo selected above, which will be removed from the archive")
            }
        }
    ]
}
