import QtQuick 2.0
import Ubuntu.Components 1.3

PinchArea {
    id: root

    property int initialSourceSize
    property url source
    property Item unscaledDelegate
    property size imageSize
    property real contentScale: 1.0

    signal clicked
    signal pressAndHold(var mouse)

    property real __initialScale: 1.0
    property real __maxScale: 5.0
    property bool __pinching: false
    property real __centerX: 0.0
    property real __centerY: 0.0
    property int __scaledSourceSize: initialSourceSize
    property Item __pinchImage: image1
    property Item __hiResImage: image2
    property Item __visibleImage: __pinchImage.paintedWidth > 0 ? __pinchImage : unscaledDelegate
    property point __contentOrigin: Qt.point(0, 0)

    anchors.fill: parent

    onInitialSourceSizeChanged: if (__pinchImage.sourceSize.width == 0) {
        __pinchImage.sourceSize.width = initialSourceSize
        __pinchImage.sourceSize.height = initialSourceSize
    }

    onImageSizeChanged: computeMaxScale()
    onWidthChanged: computeMaxScale()

    onSourceChanged: {
        contentScale = 1.0
        __initialScale = 1.0
        __centerX = 0.0; __centerY = 0.0
        __scaledSourceSize = initialSourceSize
        __pinchImage = image1
        __hiResImage = image2
        __hiResImage.source = ""
        __pinchImage.source = ""
        __pinchImage.sourceSize.width = initialSourceSize
        __pinchImage.sourceSize.height = initialSourceSize
    }

    onPinchStarted: {
        __pinchImage.source = root.source
        __pinching = true
        __initialScale = contentScale
        var center = root.mapToItem(container, pinch.startCenter.x, pinch.startCenter.y)
        console.log("pinch started at " + Qt.point(center.x, center.y))
        __centerX = center.x / container.width
        __centerY = center.y / container.height
    }
    onPinchFinished: {
        __pinching = false
        __scaledSourceSize = Math.max(width, height) * contentScale
        __hiResImage.sourceSize.width = __scaledSourceSize
        __hiResImage.sourceSize.height = __scaledSourceSize
        __hiResImage.source = source
        __hiResImage.z = 2
        flickable.returnToBounds()
    }
    onPinchUpdated: {
        if (pinch.pointCount >= 2) {
            contentScale = Math.min(Math.max(__initialScale * pinch.scale, 1.0), __maxScale)
            __contentOrigin = Qt.point(__centerX * container.width - pinch.center.x, __centerY * container.height - pinch.center.y)
            if (imageSize.width > 0) {
                zoomInfo.zoomLevel = __visibleImage.paintedWidth / imageSize.width
            }
        }
    }

    function swapImages(sender) {
        if (sender == __pinchImage) return
        if (__pinchImage == image1) {
            __pinchImage = image2
            __hiResImage = image1
        } else {
            __pinchImage = image1
            __hiResImage = image2
        }
        __hiResImage.source = ""
        __hiResImage.z = 0
        __pinchImage.z = 1
    }

    Behavior on contentScale {
        enabled: !__pinching
        NumberAnimation { duration: 100 }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: { console.log("outer clicked"); root.clicked() }
        onPressAndHold: root.pressAndHold(mouse)
    }

    Rectangle {
        anchors.fill: parent
        color: "black"
        visible: __pinchImage.visible && __pinchImage.opacity == 1.0
    }

    Flickable {
        id: flickable

        anchors.fill: parent
        enabled: contentScale > 1.0
        visible: enabled
        interactive: !__pinching
        contentWidth: __visibleImage.paintedWidth
        contentHeight: __visibleImage.paintedHeight
        contentX: __contentOrigin.x
        contentY: __contentOrigin.y
        topMargin: Math.max((height - contentHeight) / 2, 0)
        leftMargin: Math.max((width - contentWidth) / 2, 0)

        Item {
            id: container
            width: root.width * contentScale
            height: root.height * contentScale

            ScalingImage {
                id: image1
                onLoaded: root.swapImages(this)
            }

            ScalingImage {
                id: image2
                onLoaded: root.swapImages(this)
            }
        }

        MouseArea {
            anchors.fill: parent
            onClicked: { console.log("inner clicked"); root.clicked() }
        }
    }

    ZoomInfo {
        id: zoomInfo
        visible: flickable.visible
        anchors { right: parent.right; top: parent.top; margins: units.gu(2) }
    }

    function computeMaxScale() {
        if (imageSize.width > 0 && width > 0) {
            __maxScale = Math.max(imageSize.width / width, imageSize.height / height) * 5
        } else {
            __maxScale = 5.0
        }
    }
}
