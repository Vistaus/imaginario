import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Content 1.3

Page {
    id: root

    property alias model: imageView.model
    property var transfer: null
    property bool quitWhenDone: false

    visible: false

    header: PageHeader {
        title: i18n.tr("Export images")
        leadingActionBar.actions: [
            Action {
                iconName: "close"
                onTriggered: {
                    transfer.state = ContentTransfer.Aborted
                    done()
                }
            }
        ]
        trailingActionBar.actions: [
            Action {
                iconName: "ok"
                enabled: imageView.selectedIndexes.length > 0
                onTriggered: __exportItems(transfer, imageView.model, imageView.selectedIndexes)
            }
        ]
    }

    ImageView {
        id: imageView
        anchors.fill: parent
        selectMode: true
    }

    Component {
        id: itemComponent
        ContentItem {}
    }

    function __exportItems(transfer, model, selectedIndexes) {
        var items = []
        for (var i = 0; i < selectedIndexes.length; i++) {
            var url = model.get(selectedIndexes[i], "url")
            console.log("Exporting image: " + url)
            items.push(itemComponent.createObject(root, { "url": url }))
        }
        transfer.items = items
        transfer.state = ContentTransfer.Charged
        done()
    }

    function done() {
        if (quitWhenDone) {
            Qt.quit()
        } else {
            pageStack.pop()
        }
    }
}
