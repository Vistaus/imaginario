.pragma library

function shortAppId(appId) {
    var parts = appId.split("_", 3);
    if (parts.length === 3) {
        return parts.slice(0, 2).join("_");
    } else {
        return appId;
    }
}
