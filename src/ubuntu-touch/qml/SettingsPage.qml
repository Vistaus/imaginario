import Imaginario 1.0
import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem

Page {
    id: root

    property var model: null

    header: PageHeader {
        title: i18n.tr("Settings")
        flickable: flick
    }

    property Item __autoTagPage: null
    property Item __hiddenTagsPage: null

    Flickable {
        id: flick
        anchors.fill: parent
        contentHeight: contentItem.childrenRect.height

        Column {
            anchors { left: parent.left; right: parent.right; margins: units.gu(1) }

            ListItem.Standard {
                text: i18n.tr("Auto-tag based on source application")
                progression: true
                onClicked: {
                    __autoTagPage = pageStack.push(Qt.resolvedUrl("AutoTagPage.qml"), {
                        "tagData": Settings.autoTagData,
                    })
                }
            }

            ListItem.Standard {
                text: i18n.tr("Hide photos by tag")
                progression: true
                onClicked: {
                    __hiddenTagsPage = pageStack.push(Qt.resolvedUrl("HiddenTagsPage.qml"), {
                        "selectedTags": Settings.hiddenTags,
                    })
                }
            }

            ListItem.Standard {
                text: i18n.tr("Organize tags")
                progression: true
                onClicked: pageStack.push(Qt.resolvedUrl("OrganizeTagsPage.qml"), {})
            }
        }
    }

    Connections {
        target: __autoTagPage
        onDone: Settings.autoTagData = __autoTagPage.tagData
    }

    Connections {
        target: __hiddenTagsPage
        onDone: Settings.hiddenTags = __hiddenTagsPage.selectedTags
    }
}
