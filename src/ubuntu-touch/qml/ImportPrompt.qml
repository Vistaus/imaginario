import QtQuick 2.0
import Ubuntu.Components 1.3

Column {
    id: root

    signal importRequested

    anchors.centerIn: parent
    width: Math.min(units.gu(60), parent.width)
    spacing: units.gu(2)

    Label {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: units.gu(1)
        text: i18n.tr("No pictures. You can import pictures from other applications by pressing the button below.")
        wrapMode: Text.Wrap
    }

    Button {
        anchors.horizontalCenter: parent.horizontalCenter
        width: units.gu(20)
        text: i18n.tr("Import pictures")
        onClicked: root.importRequested()
    }
}
