import Imaginario 1.0
import QtLocation 5.0
import QtPositioning 5.3
import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem

Page {
    id: root

    property var location
    property int photoId: -1

    header: PageHeader {
        title: i18n.tr("Edit location")
        leadingActionBar.actions: [
            Action {
                iconName: "back"
                onTriggered: {
                    if (_changed) saveLocation()
                    pageStack.pop()
                }
            }
        ]
        trailingActionBar.actions: [
            Action {
                iconSource: "qrc:/icons/location-add"
                text: i18n.tr("Set location")
                visible: _marker == null
                onTriggered: mapView.setLocation()
            },
            Action {
                iconSource: "qrc:/icons/location-remove"
                text: i18n.tr("Clear location")
                visible: _marker != null
                onTriggered: mapView.clearLocation()
            },
            Action {
                iconName: "undo"
                text: i18n.tr("Undo")
                visible: _changed
                onTriggered: mapView.update()
            }
        ]
    }

    property var _loc: Utils.geoFields(location)
    property bool _changed: false
    property var _marker: null

    Component.onCompleted: mapView.update()

    Writer {
        id: writer
        embed: Settings.embed
    }

    Plugin {
        id: locationPlugin
        preferred: [ "nokia", "osm" ]
        PluginParameter { name: "app_id"; value: "4gpOPuEpKNJAaxknU0B1" }
        PluginParameter { name: "token"; value: "fUe83z2Tl9OgWrkhS_VDUw" }
    }

    Map {
        id: mapView
        anchors.fill: parent
        gesture.enabled: true
        center: QtPositioning.coordinate(_loc.latitude, _loc.longitude, 0)

        Component.onCompleted: {
            // workaround for nokia plugin
            plugin = locationPlugin
        }

        function update() {
            root._changed = false
            if (QtPositioning.coordinate(_loc.latitude, _loc.longitude).isValid) {
                console.log("Appending point")
                _marker = markerComponent.createObject(mapView, {
                    "coordinate": QtPositioning.coordinate(_loc.latitude, _loc.longitude),
                    "dropAnimation": false,
                })
                addMapItem(_marker)
            }
        }

        function setLocation() {
            root._changed = true
            _marker = markerComponent.createObject(mapView, {
                "coordinate": mapView.center,
                "dropAnimation": true,
            })
            addMapItem(_marker)
        }

        function clearLocation() {
            root._changed = true
            if (_marker) {
                removeMapItem(_marker)
                _marker.destroy()
                _marker = null
            }
        }
    }

    Component {
        id: markerComponent
        Marker {
            onDragFinished: {
                console.log("Drag finished")
                var pos = Qt.point(sourceItem.x, sourceItem.y)
                var c = mapView.toCoordinate(pos)
                console.log("Item coordinate: " + c)
                if (c != coordinate) {
                    root._changed = true
                }
                mapView.gesture.enabled = true
            }
            onDragStarted: {
                console.log("drag started")
                mapView.gesture.enabled = false
            }
        }
    }

    function saveLocation() {
        if (!_marker) {
            writer.setLocation(photoId, Utils.geo(null))
        } else {
            var c = _marker.coordinate
            console.log("Item coordinate: " + c)
            writer.setLocation(photoId, Utils.geo(c))
        }
    }
}
