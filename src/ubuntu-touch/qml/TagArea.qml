import Imaginario 1.0
import QtQuick 2.0
import Ubuntu.Components 1.3

Item {
    id: root

    property string name: ""
    property var tagModel
    property bool isDeleted: false

    signal changed()

    property Item __tagEditPage: null
    property var __selectedTags: []

    visible: !isDeleted

    Rectangle {
        id: areaBorder
        anchors.fill: parent
        border { color: "green"; width: units.dp(1) }
        opacity: 0.8
        color: "transparent"
    }

    MouseArea {
        id: moveMouseArea
        anchors.fill: parent
        drag.target: root
        drag.onActiveChanged: if (!drag.active) root.changed()
    }

    Image {
        anchors { horizontalCenter: areaBorder.right; verticalCenter: areaBorder.top }
        width: units.gu(3)
        height: width
        source: "qrc:/icons/remove-tag"
        sourceSize.width: width

        MouseArea {
            anchors.fill: parent
            onClicked: { root.isDeleted = true; root.changed() }
        }
    }

    Rectangle {
        id: resizeHandle
        anchors { right: areaBorder.right; bottom: areaBorder.bottom }
        width: units.gu(1)
        height: units.gu(1)
        color: areaBorder.border.color
        opacity: areaBorder.opacity
        onXChanged: if (resizeMouseArea.drag.active) root.width = x + width
        onYChanged: if (resizeMouseArea.drag.active) root.height = y + height

        MouseArea {
            id: resizeMouseArea
            anchors.centerIn: resizeHandle
            width: units.gu(3)
            height: units.gu(3)
            drag.target: resizeHandle
            drag.onActiveChanged: if (!drag.active) root.changed()
            onPressed: {
                resizeHandle.anchors.right = undefined
                resizeHandle.anchors.bottom = undefined
            }
        }
    }

    Rectangle {
        id: nameArea
        anchors { horizontalCenter: parent.horizontalCenter;
            top: parent.bottom; topMargin: units.gu(0.5) }
        height: nameLabel.height + units.gu(1)
        width: nameLabel.width + units.gu(2)
        opacity: 0.8
        color: "black"
        radius: units.gu(2)
        border { color: nameLabel.color; width: units.dp(1) }

        Label {
            id: nameLabel
            anchors.centerIn: nameArea
            text: root.name ? root.name : i18n.tr("?")
            color: Theme.palette.normal.overlayText
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                __tagEditPage = pageStack.push(Qt.resolvedUrl("TagEditSheet.qml"), {
                    "title": i18n.tr("Pick a name"),
                    "model": tagModel,
                    "parentTag": tagModel.peopleTag,
                    "multiSelection": false,
                    "selectedTags": __selectedTags
                })
            }
        }
    }

    Connections {
        target: __tagEditPage
        onDone: {
            var tagNames = tagModel.tagNames(__tagEditPage.selectedTags)
            root.name = tagNames.length > 0 ? tagNames[0] : ""
            root.changed()
            pageStack.pop()
        }
    }

    onNameChanged: computeSelectedTags()
    onTagModelChanged: computeSelectedTags()

    function computeSelectedTags() {
        if (tagModel && name) {
            __selectedTags = [ tagModel.tag(name) ]
        }
    }
}
