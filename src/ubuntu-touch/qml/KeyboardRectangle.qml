import QtQuick 2.0

Item {
    id: root

    property Item flickable: null

    anchors { left: parent.left; right: parent.right; bottom: parent.bottom }
    height: Qt.inputMethod.keyboardRectangle.height

    Connections {
        target: Qt.inputMethod
        onVisibleChanged: if (visible) scrollToField()
        onAnimatingChanged: scrollToField()
        onCursorRectangleChanged: if (!flickable.moving) scrollToField()
    }

    Connections {
        target: flickable
        onHeightChanged: scrollToField()
    }

    function scrollToField() {
        if (Qt.inputMethod.animating) return
        var cursorRectangle = Qt.inputMethod.cursorRectangle
        var top = flickable.mapFromItem(null, 0, cursorRectangle.y).y
        var bottom = top + cursorRectangle.height
        if (bottom > flickable.height) {
            flickable.contentY += bottom - flickable.height
        }
        if (top < 0) {
            flickable.contentY += top
        }
    }
}
