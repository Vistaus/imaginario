import Imaginario 1.0
import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem
import Ubuntu.Components.Popups 1.3
import Ubuntu.Keyboard 0.1

Page {
    id: root

    property var photoIds: []
    property alias multiSelection: tagView.multiSelection
    property alias selectedTags: tagView.selectedTags
    property alias deselectedTags: tagView.deselectedTags
    property alias model: tagView.model
    property alias parentTag: tagView.parentTag

    signal done

    header: PageHeader {
        title: i18n.tr("Edit tags")
        flickable: flick
        extension: TagSections {
            tagModel: tagView.model
            parentTag: root.parentTag
            onParentTagChanged: if (parentTag != root.parentTag) {
                tagView.parentTag = parentTag
                tagView.model.parentTag = parentTag
                tagView.model.allTags = (selectedIndex == 0)
            }
        }
    }

    Flickable {
        id: flick
        anchors {
            left: parent.left; right: parent.right
            top: parent.top; bottom: keyboardRectangle.top
        }
        contentHeight: column.height
        Column {
            id: column
            anchors {
                left: parent.left
                right: parent.right
            }

            TagViewEditable {
                id: tagView

                anchors.left: parent.left
                anchors.right: parent.right
                textColor: Theme.palette.normal.overlayText
            }

            ListItem.SingleControl {
                highlightWhenPressed: false
                height: units.gu(6)
                control: Button {
                    text: i18n.tr("Done")
                    anchors {
                        fill: parent
                        margins: units.gu(1)
                    }
                    onClicked: root.done()
                }
            }
        }
    }

    KeyboardRectangle {
        id: keyboardRectangle
        flickable: flick
    }
}
