import Imaginario 1.0
import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem

Page {
    id: root

    property bool importTags: false
    property var tags: []
    property string applicationName: ""

    signal done

    header: PageHeader {
        title: applicationName
        flickable: flick
        leadingActionBar.actions: [
            Action {
                iconName: "back"
                onTriggered: {
                    saveChanges()
                    pageStack.pop()
                }
            }
        ]
    }

    TagModel {
        id: tagModel
        allTags: true
    }

    Flickable {
        id: flick
        anchors.fill: parent
        contentHeight: contentItem.childrenRect.height

        Column {
            anchors { left: parent.left; right: parent.right; margins: units.gu(1) }

            ListItem.Standard {
                text: i18n.tr("Import EXIF/XMP tags")
                control: CheckBox {
                    id: wImportTags
                    checked: root.importTags
                }
            }

            ListItem.Header {
                text: i18n.tr("Auto-tag imported images:")
            }

            TagViewEditable {
                id: tagView

                anchors.left: parent.left
                anchors.right: parent.right
                model: tagModel
                selectedTags: tagsFromNames(root.tags)
                textColor: Theme.palette.normal.baseText
            }
        }
    }

    function tagsFromNames(names) {
        var tags = []
        for (var i = 0; i < names.length; i++) {
            tags.push(tagModel.tag(names[i]))
        }
        return tags
    }

    function saveChanges() {
        root.tags = tagModel.tagNames(tagView.selectedTags)
        root.importTags = wImportTags.checked
        root.done()
    }
}
