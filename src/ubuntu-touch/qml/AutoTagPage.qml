import Imaginario 1.0
import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem
import Ubuntu.Content 1.3
import "Ubuntu.js" as Ubuntu

Page {
    id: root

    property string tagData: ""

    signal done()

    property var __autoTagData: ({})
    property Item __sourcePage: null

    header: PageHeader {
        title: i18n.tr("Auto-tag based on source")
        leadingActionBar.actions: [
            Action {
                iconName: "back"
                onTriggered: {
                    root.tagData = buildTagDataString()
                    root.done()
                    pageStack.pop()
                }
            }
        ]
    }

    ContentPeerModel {
        id: peerModel
        contentType: ContentType.Pictures
        handler: ContentHandler.Source
        onPeersChanged: peerModelTimer.start()
    }

    Timer {
        id: peerModelTimer
        interval: 100
        onTriggered: updateModel(peerModel.peers)
    }

    ListView {
        id: appList
        anchors.fill: parent
        model: __autoTagData

        delegate: ListItem.Subtitled {
            iconSource: "image://content-hub/" + modelData.appId
            fallbackIconName: "stock_application"
            text: modelData.name
            subText: modelData.tags.join(", ")
            progression: true
            onClicked: {
                appList.currentIndex = index
                __sourcePage = pageStack.push(Qt.resolvedUrl("SourceOptionsPage.qml"), {
                    "importTags": modelData.importTags,
                    "tags": modelData.tags,
                    "applicationName": modelData.name,
                })
            }
        }
    }

    Connections {
        target: __sourcePage
        onDone: {
            var tmp = __autoTagData
            tmp[appList.currentIndex].tags = __sourcePage.tags
            tmp[appList.currentIndex].importTags = __sourcePage.importTags
            __autoTagData = tmp
        }
    }

    function updateModel(peers) {
        var autoTagData = []

        var savedApps = {}
        try {
            savedApps = JSON.parse(tagData) 
        } catch (e) {}
        var unusedApps = Object.keys(savedApps)
        for (var i = 0; i < peers.length; i++) {
            var peer = peers[i]
            var shortAppId = Ubuntu.shortAppId(peer.appId)
            var tags = []
            var importTags = false
            if (shortAppId in savedApps) {
                tags = savedApps[shortAppId].tags
                importTags = savedApps[shortAppId].importTags
                unusedApps.splice(unusedApps.indexOf(shortAppId), 1)
            }
            autoTagData.push({
                "appId": peer.appId,
                "name": peer.name,
                "tags": tags,
                "importTags": importTags,
            })
        }

        for (var i = 0; i < unusedApps.length; i++) {
            var data = savedApps[unusedApps[i]]
            autoTagData.push(data)
        }

        __autoTagData = autoTagData
    }

    function buildTagDataString() {
        var savedApps = {}
        for (var i = 0; i < __autoTagData.length; i++) {
            var data = __autoTagData[i]
            var tags = []
            for (var t in data.tags) { tags.push(data.tags[t]) }
            savedApps[Ubuntu.shortAppId(data.appId)] = {
                "appId": data.appId,
                "name": data.name,
                "tags": tags,
                "importTags": data.importTags,
            }
        }
        return JSON.stringify(savedApps)
    }
}
