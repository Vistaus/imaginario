import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.Pickers 1.3
import Ubuntu.Components.Popups 1.3

Button {
    id: root

    property bool hasResetButton: false
    property bool endOfDay: false
    property var date: __invalidDate
    property var minimum: {
        var d = new Date();
        d.setFullYear(d.getFullYear() - 50);
        return d;
    }

    property var __invalidDate: new Date(NaN)

    text: date.isValid() ? Qt.formatDate(date, Qt.DefaultLocaleLongDate) : i18n.tr("Pick a date")
    onClicked: {
        var dateInitializer = date.isValid() ? date : new Date()
        PopupUtils.open(dialogComponent, root, { "date": dateInitializer })
    }

    Component {
        id: dialogComponent

        Dialog {
            id: dialog

            title: i18n.tr("Enter a date")
            property alias date: picker.date

            DatePicker {
                id: picker
                anchors.left: parent.left
                anchors.right: parent.right
                minimum: root.minimum
            }

            Button {
                text: i18n.tr("Clear date")
                visible: root.hasResetButton
                onClicked: { root.date = __invalidDate; PopupUtils.close(dialog) }
            }

            Button {
                text: i18n.tr("Done")
                onClicked: {
                    var chosenDate = picker.date
                    if (root.endOfDay) {
                        chosenDate.setHours(23, 59, 59, 999)
                    } else {
                        chosenDate.setHours(0, 0, 0, 0)
                    }
                    root.date = chosenDate
                    PopupUtils.close(dialog)
                }
            }
        }
    }
}
