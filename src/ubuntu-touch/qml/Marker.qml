import QtLocation 5.0
import QtPositioning 5.3
import QtQuick 2.0
import Ubuntu.Components 1.3

MapQuickItem {
    id: root

    property bool dropAnimation: false
    signal dragStarted
    signal dragFinished

    anchorPoint: Qt.point(marker.width / 2, marker.height)
    sourceItem: Item {
        id: marker
        opacity: 0.7
        width: units.gu(10)
        height: width
        state: root.dropAnimation ? "hanging" : "grounded"

        Component.onCompleted: state = "grounded"
        states: [
            State {
                name: "hanging"
                PropertyChanges { target: markerIcon; y: -units.gu(20) }
            },
            State {
                name: "grounded"
                PropertyChanges { target: markerIcon; y: marker.height - markerIcon.height }
            }
        ]

        transitions: [
            Transition {
                NumberAnimation { target: markerIcon; properties: "y"; easing.type: Easing.OutBounce; duration: 1000 }
            }
        ]

        Image {
            id: markerIcon
            fillMode: Image.PreserveAspectFit
            width: parent.width
            source: "qrc:/icons/geotag-handle"
            sourceSize.width: marker.width

            MouseArea {
                id: mouseArea
                anchors.fill: parent
                drag.filterChildren: true
                drag.onActiveChanged: {
                    console.log("Drag active: " + drag.active)
                    if (!drag.active) {
                        root.dragFinished()
                        drag.target = null
                    }
                }
                onPressed: console.log("item pressed")
                onPressAndHold: { drag.target = root; root.dragStarted() }
            }
        }
        Image {
            anchors { horizontalCenter: parent.horizontalCenter; verticalCenter: parent.bottom }
            fillMode: Image.PreserveAspectFit
            width: parent.width / 3
            source: "qrc:/icons/geotag-handle-in"
            sourceSize.width: marker.width
        }
    }
}
