import Imaginario 1.0
import QtQuick 2.0
import Ubuntu.Components 1.3

Page {
    id: root

    header: PageHeader {
        title: i18n.tr("Organize tags")
        flickable: flick
        trailingActionBar.actions: [
            Action {
                iconName: "edit"
                enabled: tagView.selectedTags.length > 0
                onTriggered: {
                    if (tagView.selectedTags.length > 1) {
                        pageStack.push(Qt.resolvedUrl("TagsEditPage.qml"), {
                            "tags": tagView.selectedTags,
                            "tagModel": tagView.model,
                        })
                    } else {
                        /* TODO: remove the loop once we use Q_GADGET:
                         * TagEditPage will receive the tag, not the index */
                        var selectedIndex = 0
                        var selectedTag = tagView.selectedTags[0]
                        var l = tagView.model.count
                        for (var i = 0; i < l; i++) {
                            var t = tagView.model.get(i, "tag")
                            if (tagView.model.get(i, "tag") == selectedTag) {
                                selectedIndex = i
                                break
                            }
                        }
                        pageStack.push(Qt.resolvedUrl("TagEditPage.qml"), {
                            "index": selectedIndex,
                            "tagModel": tagView.model,
                        })
                    }
                }
            },
            Action {
                iconName: "delete"
                enabled: tagView.selectedTags.length > 0
                onTriggered: {
                    PopupUtils.open(Qt.resolvedUrl("TagDeleteConfirmation.qml"), pageStack.currentPage, {
                        "tags": tagView.selectedTags,
                    })
                }
            }
        ]
        extension: TagSections {
            tagModel: tagModel
            onParentTagChanged: {
                tagView.selectedTags = []
                tagView.parentTag = parentTag
                tagView.model.parentTag = parentTag
                tagView.model.allTags = (selectedIndex == 0)
            }
        }
    }

    TagModel {
        id: tagModel
        allTags: true
        hideUnused: false
    }

    Flickable {
        id: flick
        anchors {
            left: parent.left; right: parent.right
            top: parent.top; bottom: keyboardRectangle.top
        }
        contentHeight: contentItem.childrenRect.height

        Column {
            anchors { left: parent.left; right: parent.right; margins: units.gu(1) }

            TagViewEditable {
                id: tagView

                anchors.left: parent.left
                anchors.right: parent.right
                model: tagModel
                textColor: Theme.palette.normal.overlayText
            }
        }
    }

    KeyboardRectangle {
        id: keyboardRectangle
        flickable: flick
    }
}
