import Imaginario 1.0
import QtQuick 2.0
import Ubuntu.Components 1.3

Button {
    id: root

    property var model
    property var location0
    property var location1
    property bool hasArea: Object.getOwnPropertyNames(__area.location0).length > 0

    property var __invalidLocation: Utils.geo(null)
    property var __area
    property Item __page: null

    text: i18n.tr("Open map")
    onClicked: {
        __area = {
            "location0": Utils.geoFields(location0),
            "location1": Utils.geoFields(location1),
        }
        __page = pageStack.push(Qt.resolvedUrl("AreaChooserPage.qml"), {
            "area": __area,
            "photoModel": model,
        })
    }

    Connections {
        target: __page
        onDone: {
            console.log("Updating locations")
            __area = __page.area
            location0 = Utils.geo(__area.location0)
            location1 = Utils.geo(__area.location1)
            pageStack.pop()
        }
    }
}
