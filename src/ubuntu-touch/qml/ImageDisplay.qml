import Imaginario 1.0
import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

Rectangle {
    id: root

    property int initialIndex: -1
    property alias currentIndex: display.currentIndex
    property alias model: display.model

    signal done

    property int _maxDimension: Math.max(width, height)
    property bool __showTagArea: Settings.showAreaTags
    property var __writer: Writer { embed: Settings.embed }

    color: "black"
    opacity: 0
    anchors.fill: parent
    states: [
        State {
            name: "opened"
            PropertyChanges { target: root; opacity: 1 }
        }
    ]

    transitions: [
        Transition {
            to: "opened"
            NumberAnimation { properties: "opacity"; duration: 1000 }
        },
        Transition {
            from: "opened"
            SequentialAnimation {
                NumberAnimation { properties: "opacity"; duration: 200 }
                ScriptAction { script: root.done() }
            }
        }
    ]

    Component.onCompleted: {
        display.positionViewAtIndex(initialIndex, ListView.Beginning)
    }

    function show() {
        state = "opened"
    }

    function hide() {
        state = ""
    }

    ListView {
        id: display
        anchors.fill: parent
        currentIndex: initialIndex
        orientation: ListView.Horizontal
        snapMode: ListView.SnapOneItem
        highlightFollowsCurrentItem: true
        highlightRangeMode: ListView.StrictlyEnforceRange
        onCurrentIndexChanged: {
            console.log("current index: " + currentIndex)
            tagAreas.save()
            if (currentIndex >= 0) {
                tagAreaModel.photoId = model.get(display.currentIndex, "photoId")
            } else {
                tagAreaModel.photoId = -1
            }
        }
        onCountChanged: if (count == 0) { root.hide() }
        spacing: units.gu(3)
        delegate: Image {
            property bool hasVersions: model.hasVersions
            property bool isDefaultVersion: model.isDefaultVersion
            property string title: model.title || model.fileName
            width: ListView.view.width
            height: ListView.view.height
            fillMode: Image.PreserveAspectFit
            smooth: true
            source: model.url
            asynchronous: true
            sourceSize { width: _maxDimension; height: _maxDimension }
        }

        ImageZoom {
            id: imageZoom
            anchors.fill: parent
            initialSourceSize: _maxDimension
            source: display.currentItem ? display.currentItem.source : ""
            unscaledDelegate: display.currentItem
            imageSize: display.model.get(display.currentIndex, "size")
            onClicked: { topPanel.show(); bottomPanel.show(); }
            onPressAndHold: if (display.currentItem && tagAreas.visible) {
                tagAreas.handlePressAndHold(mapToItem(tagAreas,
                                                      mouse.x, mouse.y))
            }
        }

        Item {
            anchors.fill: parent
            visible: root.__showTagArea && !display.moving && imageZoom.contentScale == 1.0

            TagAreas {
                id: tagAreas
                width: display.currentItem ? display.currentItem.paintedWidth : 0
                height: display.currentItem ? display.currentItem.paintedHeight : 0
                model: tagAreaModel
                photoId: tagAreaModel.photoId
                writer: __writer
            }

            FaceDetect {
                enabled: root.__showTagArea
                model: tagAreaModel
            }

            TagAreaModel {
                id: tagAreaModel
            }
        }
    }

    ImageDisplayPanel {
        id: topPanel
        position: "top"

        Label {
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.right: dateLabel.left
            anchors.margins: units.gu(1)
            fontSize: "large"
            color: "white"
            text: display.currentItem.title
            textFormat: Text.PlainText
            elide: Text.ElideRight
        }

        Label {
            id: dateLabel
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.margins: units.gu(1)
            color: "#eee"
            text: Qt.formatDateTime(model.get(currentIndex, "time"))
        }
    }

    ImageDisplayPanel {
        id: bottomPanel
        position: "bottom"

        property list<Action> actions: [
            Action {
                iconName: "back"
                onTriggered: { root.hide(); display.currentIndex = -1 }
            },
            EditTagsAction {
                photoIds: [ model.get(currentIndex, "photoId") ]
            },
            Action {
                text: i18n.tr("View/edit location")
                iconName: "location"
                onTriggered: pageStack.push(Qt.resolvedUrl("GeoLocationSheet.qml"), {
                    "location": display.model.get(display.currentIndex, "location"),
                    "photoId": model.get(currentIndex, "photoId"),
                })
                visible: model.photoVersions == 0
            },
            Action {
                text: i18n.tr("View image versions")
                iconSource: "qrc:/icons/versions"
                onTriggered: pageStack.push(Qt.resolvedUrl("VersionsPage.qml"), {
                    "photoId": model.get(currentIndex, "photoId"),
                })
                enabled: display.currentItem && display.currentItem.hasVersions
                visible: enabled && model.photoVersions == 0
            },
            Action {
                text: i18n.tr("Make version")
                iconSource: "qrc:/icons/version-add"
                onTriggered: pageStack.push(Qt.resolvedUrl("MakeVersionPage.qml"), {
                    "photoId": model.get(currentIndex, "photoId"),
                })
                enabled: display.currentItem && !display.currentItem.hasVersions
                visible: enabled && model.photoVersions == 0
            },
            Action {
                text: i18n.tr("Set as master version")
                iconSource: "qrc:/icons/version-best"
                enabled: display.currentItem && !display.currentItem.isDefaultVersion
                visible: model.photoVersions != 0
                onTriggered: __writer.makeDefaultVersion(model.get(currentIndex, "photoId"));
            },
            Action {
                text: i18n.tr("Not a version")
                iconSource: "qrc:/icons/version-remove"
                visible: model.photoVersions != 0
                onTriggered: {
                    __writer.removeVersion(model.get(currentIndex, "photoId"))
                    pageStack.pop(); pageStack.pop()
                }
            },
            Action {
                text: i18n.tr("Tag people")
                parameterType: Action.Object
                iconSource: "qrc:/icons/face-tag" + (Settings.showAreaTags ? "" : "-off")
                onTriggered: {
                    var popup = PopupUtils.open(Qt.resolvedUrl("FaceTagPopup.qml"), value, {
                        "model": tagAreaModel,
                    })
                    bottomPanel.lock()
                    popup.visibleChanged.connect(function () {
                        if (!popup.visible) bottomPanel.unlock()
                    })
                    popup.detectionRequested.connect(function () {
                        tagAreaModel.detectFaces()
                    })
                }
            },
            Action {
                text: i18n.tr("Edit title and description")
                iconName: "edit"
                onTriggered: pageStack.push(Qt.resolvedUrl("TitleEditSheet.qml"), {
                    "model": root.model,
                    "selectedIndex": currentIndex,
                })
            },
            ShareAction {
                model: root.model
                selectedIndexes: [ currentIndex ]
            },
            DeleteAction {
                model: root.model
                selectedIndexes: [ currentIndex ]
            }
        ]

        Row {
            anchors.fill: parent
            anchors.margins: units.gu(1)
            spacing: units.gu(1)

            Repeater {
                model: bottomPanel.actions

                PanelButtonDelegate {}
            }
        }
    }
}
