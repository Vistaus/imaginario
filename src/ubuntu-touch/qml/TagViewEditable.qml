import Imaginario 1.0
import QtQuick 2.0
import "TagActions.js" as TagActions
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem
import Ubuntu.Components.Popups 1.3
import Ubuntu.Keyboard 0.1

Column {
    id: column

    property alias model: tagView.model
    property alias selectedTags: tagView.selectedTags
    property alias deselectedTags: tagView.deselectedTags
    property alias textColor: tagView.textColor
    property alias multiSelection: tagView.multiSelection
    property var parentTag: tagView.model.uncategorizedTag

    anchors { left: parent.left; right: parent.right }
    spacing: units.gu(1)

    TagView {
        id: tagView

        anchors.left: parent.left
        anchors.right: parent.right
        onPressAndHold: TagActions.contextMenu(pressedItem, tagView.model, index)
    }

    Label {
        anchors { left: parent.left; right: parent.right; margins: units.gu(3) }
        height: units.gu(8)
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        visible: model.count == 0
        text: i18n.tr("No tags")
    }

    TextField {
        id: tagTextEntry
        anchors { left: parent.left; right: parent.right; margins: units.gu(1) }
        hasClearButton: true
        placeholderText: i18n.tr("Create a new tag")
        Keys.onReturnPressed: { createTag(text); text = "" }
        inputMethodHints: Qt.ImhNoAutoUppercase
        InputMethod.extensions: { "enterKeyText": qsTr("Add tag") }

        function createTag(text) {
            var tag = writer.createTag(text, root.parentTag)
            if (tagView.multiSelection) {
                var tags = tagView.selectedTags
                tags.push(tag)
                tagView.selectedTags = tags
            } else {
                tagView.selectedTags = [ tag ]
            }
        }
    }

    Writer { id: writer }
}
