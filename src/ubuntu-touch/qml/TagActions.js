function contextMenu(caller, model, index) {
    /* We handle the deletionRequested signal here and not in
     * TagActions.qml, because the ActionSelectionPopover gets
     * destroyed as soon as an action is chosen. */
    var popup = PopupUtils.open(Qt.resolvedUrl("TagActions.qml"),
                                caller, {
                                    "tagModel": tagView.model,
                                    "index": index,
                                })
    popup.deletionRequested.connect(function () {
        PopupUtils.open(Qt.resolvedUrl("TagDeleteConfirmation.qml"), caller, {
            "tags": [ model.get(index, "tag") ],
        })
    })
}
