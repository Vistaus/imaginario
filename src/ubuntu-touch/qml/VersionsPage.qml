import Imaginario 1.0
import QtQuick 2.0
import Ubuntu.Components 1.3

Page {
    id: root

    property alias photoId: photoModel.photoVersions

    header: PageHeader {
        title: i18n.tr("Image versions")
        flickable: imageView
    }

    PhotoModel {
        id: photoModel
    }

    ImageView {
        id: imageView
        anchors.fill: parent
        model: photoModel
        onItemClicked: pageStack.push(Qt.resolvedUrl("DisplayPage.qml"), {
            "model": photoModel,
            "currentIndex": index,
        })
    }
}
