import Imaginario 1.0
import QtQuick 2.0
import Ubuntu.Components.Popups 1.3

TagEditSheet {
    id: root

    model: TagModel {
        id: tagModel
        allTags: true
        onCountChanged: console.log("Tag count changed: " + count)
        photosForUsageCount: root.photoIds
    }

    Writer {
        id: writer
        embed: Settings.embed
    }

    onDone: {
        var addTags = selectedTags
        var removeTags = deselectedTags
        writer.changePhotoTags(photoIds, addTags, removeTags)
        pageStack.pop()
    }
}
