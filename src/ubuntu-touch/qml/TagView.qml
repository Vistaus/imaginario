import QtQuick 2.0

Item {
    id: flow

    property int rowSpacing: 2
    property int wordSpacing: Math.round(units.gu(1) * 1.5)
    property int maxHeight: units.gu(4)
    property var selectedTags: []
    property var deselectedTags: []
    property alias model: repeater.model
    property alias count: repeater.count
    property var textColor: "black"
    property bool multiSelection: true

    signal pressAndHold(var mouse, int index, var pressedItem)

    height: childrenRect.height
    onWidthChanged: reflowTimer.start()

    Repeater {
        id: repeater
        delegate: Item {
            property bool tristate: (flow.model.photosForUsageCount != undefined) &&
                model.usageCount > 0 &&
                model.usageCount < flow.model.photosForUsageCount.length
            property bool halfSelected: tristate && !flow.isSelected(index) && !flow.isDeselected(index)
            width: label.width
            implicitHeight: label.height

            Text {
                id: label
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                text: model.name + (halfSelected ? ('(' + model.usageCount + ')') : '')
                color: flow.textColor
                font.pixelSize: Math.round(Math.max(flow.maxHeight * model.popularity, units.gu(1) * 1.5))
            }

            Rectangle {
                anchors.fill: label
                color: "blue"
                opacity: flow.isSelected(index) ? 0.3 : 0.1
                visible: flow.isSelected(index) || (model.usageCount > 0 && !flow.isDeselected(index))
            }

            MouseArea {
                anchors.fill: parent
                onClicked: flow.toggleSelection(model.tag, tristate)
                onPressAndHold: flow.pressAndHold(mouse, index, label)
            }
        }
        onItemAdded: reflowTimer.start()
        onItemRemoved: reflowTimer.start()
    }

    Timer {
        id: reflowTimer
        interval: 10
        onTriggered: flow.reflow()
    }

    Component.onCompleted: computeInitialSelection()

    function computeInitialSelection() {
        if (!model.photosForUsageCount) return
        var numItems = model.photosForUsageCount.length
        if (numItems == 0) return
        var newSelectedList = []
        for (var i = 0; i < model.count; i++) {
            if (model.get(i, "usageCount") == numItems) {
                newSelectedList.push(model.get(i, "tag"))
            }
        }
        selectedTags = newSelectedList
    }

    function isSelected(index) {
        return selectedTags.indexOf(model.get(index, "tag")) >= 0
    }

    function isDeselected(index) {
        return deselectedTags.indexOf(model.get(index, "tag")) >= 0
    }

    function toggleSelection(tag, tristate) {
        var i = selectedTags.indexOf(tag)
        if (!multiSelection && i < 0) {
            selectedTags = [ tag ]
            return
        }
        var j = deselectedTags.indexOf(tag)
        var newSelectedList = selectedTags
        var newDeselectedList = deselectedTags
        if (i >= 0) {
            newSelectedList.splice(i, 1)
            newDeselectedList.push(tag)
        } else if (j >= 0) {
            newDeselectedList.splice(j, 1)
            if (!tristate) newSelectedList.push(tag)
        } else {
            newSelectedList.push(tag)
        }
        selectedTags = newSelectedList
        deselectedTags = newDeselectedList
    }

    function reflow() {
        var rows = []
        var rowWidth = 0
        var rowHeight = 0
        var rowIndex = 0
        var rowItems = []
        /* First, compute the lines' widths */
        for (var i = 0; i < repeater.count; i++) {
            var item = repeater.itemAt(i)
            if (!item) continue;
            if (rowWidth + item.width + flow.wordSpacing > flow.width) {
                rows[rowIndex] = {
                    "width": rowWidth,
                    "height": rowHeight,
                    "items": rowItems
                }
                rowIndex++
                rowWidth = 0
                rowHeight = 0
                rowItems = []
            }
            rowWidth += item.width + flow.wordSpacing
            rowItems.push(item)
            if (item.implicitHeight > rowHeight) rowHeight = item.implicitHeight
        }

        if (rowItems.length > 0) {
            rows[rowIndex] = {
                "width": rowWidth,
                "height": rowHeight,
                "items": rowItems
            }
        }

        var y = 0
        for (var i = 0; i < rows.length; i++) {
            var row = rows[i]
            var x = Math.round((flow.width - row.width) / 2)
            for (var j = 0; j < row.items.length; j++) {
                row.items[j].x = x
                row.items[j].y = y
                row.items[j].height = row.height
                x += row.items[j].width + flow.wordSpacing
            }
            y += row.height + flow.rowSpacing
        }
    }
}
