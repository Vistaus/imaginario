import QtQuick 2.0
import Ubuntu.Components 1.3

Page {
    id: root

    property alias model: imageView.model

    signal itemSelected(int photoId)

    ImageView {
        id: imageView
        anchors.fill: parent
        onItemClicked: root.itemSelected(model.get(index, "photoId"))
    }
}
