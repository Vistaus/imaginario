import Imaginario 1.0
import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

Dialog {
    id: root

    property var tags: []

    property string __tagName: Utils.tagName(tags[0])

    title: i18n.tr("Tag deletion")
    text: i18n.tr("Confirm deletion of the <b>%1</b> tag?<br><br>The tag will also be removed from any photos using it.",
                  "Confirm deletion of the selected tags?<br><br>The tags will also be removed from any photos using them.",
                  tags.length).arg(__tagName)

    Writer {
        id: writer
        embed: Settings.embed
    }

    Button {
        text: i18n.tr("Cancel")
        onClicked: setConfirmed(false)
    }

    Button {
        text: i18n.tr("Delete tag", "Delete tags", tags.length)
        color: "red"
        onClicked: setConfirmed(true)
    }

    function setConfirmed(confirmed) {
        console.log("Closing... confirmed: " + confirmed)
        if (confirmed) {
            var l = tags.length
            for (var i = 0; i < l; i++) {
                writer.deleteTag(tags[i])
            }
        }
        PopupUtils.close(root)
    }
}
