import Imaginario 1.0
import QtQuick 2.0
import QtQuick.Window 2.0
import Ubuntu.Components 1.3

Page {
    id: root

    property alias initialIndex: display.initialIndex
    property alias currentIndex: display.currentIndex
    property alias model: display.model

    property var _oldVisibility: Window.AutomaticVisibility

    header: Item {}
    visible: false
    onVisibleChanged: if (visible) {
        if (window.visibility != Window.FullScreen) {
            _oldVisibility = window.visibility
            window.visibility = Window.FullScreen
        }
        display.show()
    }

    ImageDisplay {
        id: display
        onDone: {
            if (root._oldVisibility != Window.AutomaticVisibility) {
                window.visibility = root._oldVisibility
            }
            pageStack.pop()
        }
    }
}
