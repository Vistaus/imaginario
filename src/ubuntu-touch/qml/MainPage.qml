import QtQuick 2.0
import Ubuntu.Components 1.3

Page {
    id: root

    property alias model: imageView.model

    signal importRequested
    signal itemClicked(int index)

    visible: false

    header: PageHeader {
        title: i18n.tr("Imaginario")
        flickable: imageView
    }

    property list<Action> __importActions: [
        Action {
            iconName: "search"
            text: i18n.tr("Filter")
            onTriggered: pageStack.push(Qt.resolvedUrl("FilterSheet.qml"),
                { "model": root.model })
        }
    ]

    property list<Action> __viewActions: [
        Action {
            iconName: "search"
            text: i18n.tr("Filter")
            onTriggered: pageStack.push(Qt.resolvedUrl("FilterSheet.qml"),
                { "model": root.model })
        },
        Action {
            iconName: "select"
            text: i18n.tr("Select")
            onTriggered: imageView.selectMode = true
        },
        Action {
            iconName: "add"
            text: i18n.tr("Import items")
            onTriggered: root.importRequested()
            visible: !root.model.hasFilter
        },
        Action {
            iconName: "settings"
            text: i18n.tr("Settings")
            onTriggered: pageStack.push(Qt.resolvedUrl("SettingsPage.qml"), {})
            visible: !root.model.hasFilter
        }
    ]

    property Action __filteredBackAction: Action {
        text: i18n.tr("Back")
        iconName: "back"
        onTriggered: root.model.clearFilter()
    }

    property list<Action> __selectActions: [
        EditTagsAction {
            photoIds: imageView.getSelectedIds()
        },
        ShareAction {
            model: photoModel
            selectedIndexes: imageView.selectedIndexes
        },
        DeleteAction {
            model: photoModel
            selectedIndexes: imageView.selectedIndexes
        }
    ]
    property Action __selectBackAction: Action {
        text: i18n.tr("Back")
        iconName: "back"
        onTriggered: imageView.selectMode = false
    }

    states: [
        State {
            name: "import"
            when: photoModel.count === 0 && !root.model.hasFilter

            PropertyChanges {
                target: root.header.trailingActionBar
                actions: root.__importActions
            }
        },
        State {
            name: "view"
            when: !imageView.selectMode && !root.model.hasFilter
            PropertyChanges {
                target: root.header.trailingActionBar
                actions: root.__viewActions
            }
        },
        State {
            name: "filtered"
            extend: "view"
            when: !imageView.selectMode && root.model.hasFilter
            PropertyChanges { target: root; title: root.model.filterName }
            PropertyChanges {
                target: root.header.trailingActionBar
                actions: root.__viewActions
            }
            PropertyChanges {
                target: root.header.leadingActionBar
                actions: root.__filteredBackAction
            }
        },
        State {
            name: "select"
            when: imageView.selectMode
            PropertyChanges { target: root; title: i18n.tr("Select items (%1)").arg(imageView.selectedIndexes.length) }
            PropertyChanges {
                target: root.header.trailingActionBar
                actions: root.__selectActions
            }
            PropertyChanges {
                target: root.header.leadingActionBar
                actions: root.__selectBackAction
            }
        }
    ]

    ImportPrompt {
        id: importPrompt
        visible: photoModel.count === 0
        onImportRequested: root.importRequested()
    }

    ImageView {
        id: imageView
        visible: !importPrompt.visible
        model: photoModel
        onItemClicked: root.itemClicked(index)
    }
}
