import Imaginario 1.0
import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem
import Ubuntu.Components.Popups 1.3
import Ubuntu.Components.Themes.Ambiance 1.3
import Ubuntu.Keyboard 0.1

Page {
    id: root

    property var tagModel: null
    property int index: -1

    title: i18n.tr("Edit tag")

    property string __tagName: tagModel.get(index, "name")

    flickable: flick

    Flickable {
        id: flick

        anchors {
            left: parent.left; right: parent.right
            top: parent.top; bottom: keyboardRectangle.top
        }
        contentHeight: column.height

        Column {
            id: column
            anchors {
                left: parent.left
                right: parent.right
            }

            TextField {
                id: tagTextEntry
                anchors {
                    left: parent.left; right: parent.right
                    margins: units.gu(4)
                }
                style: TextFieldStyle {
                    overlaySpacing: 0
                    frameSpacing: 0
                    background: Item {}
                }
                font.pixelSize: FontUtils.sizeToPixels("x-large")
                hasClearButton: false
                text: __tagName
                horizontalAlignment: Text.AlignHCenter
                Keys.onReturnPressed: Qt.inputMethod.hide()
                inputMethodHints: Qt.ImhNoAutoUppercase

                secondaryItem: Icon {
                    height: parent.height
                    width: height
                    name: "undo"
                    visible: tagTextEntry.text != __tagName
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            tagTextEntry.text = __tagName
                            Qt.inputMethod.hide()
                        }
                    }
                }

                function checkName() {
                    text = text.trim()
                    if (!text) {
                        PopupUtils.open(dialogComponent, root, {
                            "text": i18n.tr("Cannot set an empty name"),
                        })
                        return false
                    }
                    if (text == __tagName) return true
                    if (Utils.tagIsValid(tagModel.tag(text))) {
                        PopupUtils.open(dialogComponent, root, {
                            "text": i18n.tr("A tag with this name already exists"),
                        })
                        return false
                    }
                    return true
                }
            }

            Label {
                anchors { left: parent.left; right: parent.right }
                text: tagTextEntry.text != __tagName ?
                    i18n.tr("The tag will be renamed in all images") :
                    i18n.tr("Tap on the tag name above to edit it")
                fontSize: "x-small"
                horizontalAlignment: Text.AlignHCenter
            }

            ListItem.ThinDivider {}

            ListItem.ItemSelector {
                id: parentSelector
                text: i18n.tr("Parent tag:")

                function prepareModel() {
                    var options = []

                    var predefinedTags = [
                        tagModel.uncategorizedTag,
                        tagModel.peopleTag,
                        tagModel.placesTag,
                        tagModel.eventsTag,
                        tagModel.importedTagsTag
                    ]
                    if (predefinedTags.indexOf(tagModel.get(index, "tag")) >= 0) {
                        parentSelector.visible = false
                        return
                    }

                    var categories = tagModel.tagNames(predefinedTags)
                    for (var i = 0; i < categories.length; i++) {
                        options.push(categories[i])
                    }
                    var currentParent = tagModel.tagNames(tagModel.get(index, "parentTagId"))[0]
                    var parentIndex = options.indexOf(currentParent)
                    if (parentIndex < 0) {
                        parentIndex = options.length
                        options.push(currentParent)
                    }
                    parentSelector.model = options
                    parentSelector.selectedIndex = parentIndex
                }
            }

            Writer { id: writer }

            ListItem.SingleControl {
                highlightWhenPressed: false
                height: units.gu(6)
                control: Button {
                    text: i18n.tr("Done")
                    anchors {
                        fill: parent
                        margins: units.gu(1)
                    }
                    color: "green"
                    onClicked: if (root.save()) {
                        pageStack.pop()
                    }
                }
            }
        }
    }

    KeyboardRectangle {
        id: keyboardRectangle
        flickable: flick
    }

    Component {
        id: dialogComponent

        Dialog {
            id: dialog

            property alias message: label.text
            title: i18n.tr("Invalid tag name")

            Label {
                id: label
                anchors { left: parent.left; right: parent.right }
                wrapMode: Text.Wrap
                color: theme.palette.normal.backgroundText
            }

            Button {
                text: i18n.tr("OK")
                onClicked: {
                    tagTextEntry.text = __tagName
                    PopupUtils.close(dialog)
                }
            }
        }
    }

    Component.onCompleted: parentSelector.prepareModel()

    function save() {
        var updates = {}

        if (!tagTextEntry.checkName()) {
            return false
        }

        if (tagTextEntry.text != __tagName) {
            updates["name"] = tagTextEntry.text
        }

        if (parentSelector.selectedIndex >= 0) {
            var parentTagName = parentSelector.model[parentSelector.selectedIndex]
            var currentParent = tagModel.tagNames(tagModel.get(index, "parentTagId"))[0]
            if (parentTagName && parentTagName != currentParent) {
                updates["parent"] = tagModel.tag(parentTagName)
            }
        }

        writer.updateTag(tagModel.get(index, "tag"), updates)
        return true
    }
}
