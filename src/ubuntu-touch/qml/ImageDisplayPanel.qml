import QtQuick 2.0
import Ubuntu.Components 1.3

Item {
    id: root

    property string position: "top"

    property int yWhenOpened: position == "top" ? 0 : (parent.height - height)
    anchors.left: parent.left
    anchors.right: parent.right
    height: units.gu(6)
    y: position == "top" ? -height : parent.height

    states: [
        State {
            name: "exposed"
            PropertyChanges { target: root; y: yWhenOpened }
        }
    ]

    transitions: [
        Transition {
            NumberAnimation { properties: "y"; duration: 300 }
        }
    ]

    Rectangle {
        anchors.fill: parent
        color: "#222"
        opacity: 0.8
    }

    MouseArea {
        anchors.fill: parent
        z: 1
        onPressed: { panelTimer.restart(); mouse.accepted = false }
        onPositionChanged: panelTimer.restart()
    }

    Timer {
        id: panelTimer
        interval: 3000
        onTriggered: root.hide()
    }

    function show() {
        state = "exposed"
        panelTimer.restart()
    }

    function hide() {
        state = ""
    }

    function lock() {
        show()
        panelTimer.stop()
    }

    function unlock() {
        panelTimer.restart()
    }
}
