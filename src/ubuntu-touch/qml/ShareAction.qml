import QtQuick 2.0
import Ubuntu.Components 1.3

Action {
    property var model
    property var selectedIndexes

    text: i18n.tr("Share")
    iconName: "share"
    enabled: selectedIndexes.length > 0
    onTriggered: pageStack.push(Qt.resolvedUrl("ExportPage.qml"), {
        "model": model,
        "selectedIndexes": selectedIndexes
    })
}
