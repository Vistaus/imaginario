import Imaginario 1.0
import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem
import Ubuntu.Components.Popups 1.3
import Ubuntu.Components.Themes.Ambiance 1.3
import Ubuntu.Keyboard 0.1

Page {
    id: root

    property var tagModel: null
    property var tags: []

    title: i18n.tr("Edit tags")

    flickable: flick

    Flickable {
        id: flick

        anchors {
            left: parent.left; right: parent.right
            top: parent.top; bottom: keyboardRectangle.top
        }
        contentHeight: column.height

        Column {
            id: column
            anchors {
                left: parent.left
                right: parent.right
            }

            ListItem.Header {
                text: i18n.tr("Set a new parent for the selected tags")
            }

            ListItem.ItemSelector {
                id: parentSelector
                text: i18n.tr("Parent tag:")

                function prepareModel() {
                    var options = [
                        i18n.tr("don't change")
                    ]

                    var predefinedTags = [
                        tagModel.uncategorizedTag,
                        tagModel.peopleTag,
                        tagModel.placesTag,
                        tagModel.eventsTag,
                        tagModel.importedTagsTag
                    ]

                    var categories = tagModel.tagNames(predefinedTags)
                    for (var i = 0; i < categories.length; i++) {
                        options.push(categories[i])
                    }
                    parentSelector.model = options
                    parentSelector.selectedIndex = 0
                }
            }

            Writer { id: writer }

            ListItem.SingleControl {
                highlightWhenPressed: false
                height: units.gu(6)
                control: Button {
                    text: i18n.tr("Done")
                    anchors {
                        fill: parent
                        margins: units.gu(1)
                    }
                    color: "green"
                    onClicked: {
                        root.save()
                        pageStack.pop()
                    }
                }
            }
        }
    }

    KeyboardRectangle {
        id: keyboardRectangle
        flickable: flick
    }

    Component.onCompleted: parentSelector.prepareModel()

    function save() {
        var updates = {}

        if (parentSelector.selectedIndex > 0) {
            var parentTagName = parentSelector.model[parentSelector.selectedIndex]
            updates["parent"] = tagModel.tag(parentTagName)
        }

        var l = tags.length
        for (var i = 0; i < l; i++) {
            writer.updateTag(tags[i], updates)
        }
    }
}
