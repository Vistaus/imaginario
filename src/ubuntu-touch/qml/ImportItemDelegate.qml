import Imaginario 1.0
import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Content 0.1

Item {
    id: root

    property url itemUrl
    property var itemStatus
    property var duplicates
    property alias tags: tagList.tags
    property alias selectedTags: tagList.selectedTags
    readonly property int collapsedHeight: units.gu(11)

    signal ignoreItem()
    signal importAsNotDuplicate()
    signal importAsVersion(int photoId)
    signal importReplacing(int photoId)

    property Item __duplicateSheet: null

    implicitHeight: Math.max(collapsedHeight, label.implicitHeight + tagList.implicitHeight)

    Item {
        id: contents
        anchors.fill: parent
        anchors { leftMargin: units.gu(1); rightMargin: units.gu(1) }
        UbuntuShape {
            id: resImage
            width: height
            height: collapsedHeight - 2
            image: Image {
                anchors.fill: parent
                source: itemUrl
                sourceSize.height: parent.height
                fillMode: Image.PreserveAspectCrop
                smooth: true
                asynchronous: true
            }
        }

        Icon {
            id: statusIcon
            anchors { right: resImage.right; top: resImage.top }
            width: units.gu(4)
            height: width
            visible: false
        }

        Label {
            id: label
            anchors {
                top: parent.top; left: resImage.right; leftMargin: units.gu(1)
                right: parent.right
            }
            text: itemUrl.toString().split('/').pop()
            fontSize: "small"
            elide: Text.ElideRight
        }

        TagList {
            id: tagList
            anchors {
                top: label.bottom; left: label.left
                right: parent.right; bottom: parent.bottom
            }
        }

        StatusBanner {
            id: banner
        }

        ActivityIndicator {
            anchors.centerIn: parent
            running: itemStatus == Importer.Processing
        }
    }

    Behavior on height {
        NumberAnimation { duration: 500; easing.type: Easing.InOutQuad }
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        enabled: false
        onClicked: {
            if (root.state == "duplicate") {
                __duplicateSheet = pageStack.push(Qt.resolvedUrl("DuplicateSheet.qml"), {
                    "duplicates": duplicates,
                })
            } else if (root.state == "failed") {
                root.ignoreItem()
            }
        }
    }

    Connections {
        target: __duplicateSheet
        onIgnoreItem: root.ignoreItem()
        onImportAsNotDuplicate: root.importAsNotDuplicate()
        onImportAsVersion: root.importAsVersion(photoId)
        onImportReplacing: root.importReplacing(photoId)
    }

    states: [
        State {
            name: "done"
            when: itemStatus == Importer.Done
            PropertyChanges { target: banner; title: i18n.tr("Imported") }
            PropertyChanges {
                target: statusIcon
                visible: true
                color: "green"
                name: "ok"
            }
        },
        State {
            name: "duplicate"
            when: itemStatus == Importer.DuplicateCheck
            PropertyChanges {
                target: banner; title: i18n.tr("Duplicate check")
                message: i18n.tr("This item is likely a duplicate; tap to decide what to do with it.")
            }
            PropertyChanges { target: mouseArea; enabled: true }
            PropertyChanges {
                target: statusIcon
                visible: true
                color: "yellow"
                source: "qrc:/icons/versions"
            }
        },
        State {
            name: "failed"
            when: itemStatus == Importer.Failed
            PropertyChanges {
                target: banner; title: i18n.tr("Failed")
                message: i18n.tr("This item cannot be imported. Tap to ignore it.")
            }
            PropertyChanges { target: mouseArea; enabled: true }
            PropertyChanges {
                target: statusIcon
                visible: true
                color: "red"
                name: "cancel"
            }
        },
        State {
            name: "ignored"
            when: itemStatus == Importer.Ignored
            PropertyChanges {
                target: banner; title: i18n.tr("Ignored")
                message: i18n.tr("This item won't be imported")
            }
            PropertyChanges {
                target: statusIcon
                visible: true
                color: "red"
                name: "close"
            }
        }
    ]

    transitions: [
        Transition {
            to: "done"
            SequentialAnimation {
                PropertyAction { target: root; property: "GridView.delayRemove"; value: true }
                PropertyAction { target: statusIcon }
                PropertyAction { target: banner }
                PropertyAction { target: root; property: "GridView.delayRemove"; value: false }
            }
        },
        Transition {
            to: "failed"
            SequentialAnimation {
                RotationAnimation { target: resImage; property: "rotation"; to: 20; easing.type: Easing.OutQuad; duration: 50 }
                SequentialAnimation {
                    loops: 5
                    RotationAnimation { target: resImage; property: "rotation"; to: -20; easing.type: Easing.InOutQuad; duration: 50 }
                    RotationAnimation { target: resImage; property: "rotation"; to: 20; easing.type: Easing.InOutQuad; duration: 50 }
                }
                RotationAnimation { target: resImage; property: "rotation"; to: 0; easing.type: Easing.InQuad; duration: 50 }
            }
        }
    ]
}
