import Imaginario 1.0
import QtQuick 2.3
import Ubuntu.Components 1.3
import Ubuntu.Keyboard 0.1

Page {
    id: root

    property var model
    property var selectedIndex: -1

    property string _storedTitle: model.get(selectedIndex, "title")
    property string _storedDescription: model.get(selectedIndex, "description")

    header: PageHeader {
        title: i18n.tr("Edit title and description")
        flickable: flick
        leadingActionBar.actions: [
            Action {
                iconName: "back"
                onTriggered: {
                    saveChanges()
                    pageStack.pop()
                }
            }
        ]
        trailingActionBar.actions: [
            Action {
                iconName: "undo"
                text: i18n.tr("Undo")
                visible: _storedTitle != titleField.text || _storedDescription != descriptionField.text
                onTriggered: {
                    titleField.text = _storedTitle
                    descriptionField.text = _storedDescription
                }
            }
        ]
    }

    /* Save text if application suspended */
    Connections {
        target: Qt.application
        onStateChanged: {
            if (Qt.application.state == Qt.ApplicationSuspended) saveChanges()
        }
    }

    Writer {
        id: writer
        embed: Settings.embed
    }

    Flickable {
        id: flick
        anchors {
            left: parent.left; right: parent.right
            top: parent.top; bottom: keyboardRectangle.top
        }
        contentHeight: contentItem.childrenRect.height

        Column {
            anchors { left: parent.left; right: parent.right; margins: units.gu(1) }
            spacing: units.gu(1)

            Column {
                anchors { left: parent.left; right: parent.right }
                Label {
                    anchors { left: parent.left; right: parent.right }
                    text: i18n.tr("Title")
                }
                TextField {
                    id: titleField
                    anchors { left: parent.left; right: parent.right }
                    text: _storedTitle
                    placeholderText: i18n.tr("Enter a title")
                    KeyNavigation.tab: descriptionField
                    Keys.onReturnPressed: descriptionField.forceActiveFocus()
                }
            }

            Column {
                anchors { left: parent.left; right: parent.right }
                Label {
                    anchors { left: parent.left; right: parent.right }
                    text: i18n.tr("Description")
                }
                TextArea {
                    id: descriptionField
                    anchors { left: parent.left; right: parent.right }
                    text: _storedDescription
                    maximumLineCount: 0
                    placeholderText: i18n.tr("Enter a description")
                }
            }
        }
    }

    KeyboardRectangle {
        id: keyboardRectangle
        flickable: flick
    }

    function saveChanges() {
        var photoId = model.get(selectedIndex, "photoId")
        console.log("Saving changes")
        if (titleField.text != _storedTitle) {
            writer.setTitle(photoId, titleField.text)
        }
        if (descriptionField.text != _storedDescription) {
            writer.setDescription(photoId, descriptionField.text)
        }
    }
}
