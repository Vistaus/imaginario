import QtQuick 2.4
import Ubuntu.Components 1.3

Item {
    id: dialogForegroundStyle

    UbuntuShape {
        id: background
        anchors.fill: parent
        backgroundColor: "#221E1C"
    }
}
