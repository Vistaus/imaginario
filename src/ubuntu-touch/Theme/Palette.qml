import Ubuntu.Components 1.3
import Ubuntu.Components.Themes.SuruDark 1.3 as SuruDark

SuruDark.Palette {
    normal.overlay: "#ee221E1C"
    normal.overlayText: "#ffffff"
}
