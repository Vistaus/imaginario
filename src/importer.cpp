/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "database.h"
#include "duplicate_detector.h"
#include "importer.h"
#include "metadata.h"
#include "thumbnailer.h"
#include "utils.h"

#include <QDebug>
#include <QDir>
#include <QFileInfo>
#include <QFutureWatcher>
#include <QMetaObject>
#include <QMimeDatabase>
#include <QMimeType>
#include <QMutex>
#include <QWaitCondition>
#include <QtConcurrent>
#include <climits>

using namespace Imaginario;

namespace Imaginario {

class ImporterPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(Importer)

public:
    class Item {
    public:
        Item(const DuplicateDetector::RawPair &rawPair,
             const Metadata::ImportData &metadata,
             const QStringList &tags, bool importTags):
            rawUrl(rawPair.raw),
            editedUrl(rawPair.edited),
            metadata(metadata),
            addedTags(tags),
            tags(tags),
            status(Importer::Waiting),
            duplicateAction(Importer::DuplicateCheckNeeded),
            duplicatePhotoId(0),
            importTags(importTags)
        {
            if (importTags) {
                this->tags.append(ownTags());
            }
        }

        QStringList ownTags() const;

        QUrl rawUrl;
        QUrl editedUrl;
        Metadata::ImportData metadata;
        QStringList addedTags;
        QStringList tags;
        Importer::ItemStatus status;
        QList<PhotoId> duplicates;
        Importer::DuplicateAction duplicateAction;
        PhotoId duplicatePhotoId;
        bool importTags;
    };

    struct ThreadData {
        Metadata metadata;
        bool writeXmp;
        Database *db;
        RollId rollId;
        QDir destDir;
        Thumbnailer thumbnailer;
    };

    ImporterPrivate(Importer *q);
    ~ImporterPrivate();

    PhotoId importUrl(ThreadData &d, const Item &item,
                      const QUrl &url, const QList<Tag> &tags) const;
    void exec();
    inline bool checkInput();
    void removeItem(int index, int modelIndex);
    int modelIndexFromIndex(int index) const;
    int modelIndexToIndex(int index) const;
    void setStatus(int index, Importer::ItemStatus status);
    inline bool mustClear(Importer::ItemStatus status);
    bool fileIsSupported(const QString &fileName) const;
    QList<QUrl> filterItems(const QList<QUrl> &items);

private Q_SLOTS:
    void doSetStatus(int index, Importer::ItemStatus status);

private:
    DuplicateDetector m_detector;
    QHash<int, QByteArray> m_roles;
    RollId m_lastRollId;
    QString m_destination;
    Tag m_parentTag;
    bool m_embed;
    bool m_writeXmp;
    bool m_copyFiles;
    QStringList m_commonTags;
    QList<int> m_removedIndexes;
    QList<Item> m_items;
    int m_importedCount;
    int m_failedCount;
    int m_ignoredCount;
    int m_unsupportedCount;
    Importer::ClearStatuses m_autoClear;
    Importer::RawPolicy m_rawPolicy;
    QFuture<void> m_execFuture;
    QFutureWatcher<void> m_watcher;
    QMutex m_terminationMutex;
    QWaitCondition m_terminationCondition;
    bool m_terminationRequested;
    QMimeDatabase m_mimeDatabase;
    mutable Importer *q_ptr;
};

} // namespace

QStringList ImporterPrivate::Item::ownTags() const {
    QStringList tags = metadata.tags;
    for (const Metadata::RegionInfo &r: metadata.regions) {
        tags.append(r.name);
    }
    return tags;
}

ImporterPrivate::ImporterPrivate(Importer *q):
    m_lastRollId(0),
    m_embed(false),
    m_writeXmp(true),
    m_copyFiles(false),
    m_importedCount(0),
    m_failedCount(0),
    m_ignoredCount(0),
    m_unsupportedCount(0),
    m_autoClear(0),
    m_rawPolicy(Importer::EditedIsMaster),
    m_terminationRequested(false),
    q_ptr(q)
{
    qRegisterMetaType<Importer::ItemStatus>("Importer::ItemStatus");
    qRegisterMetaType<Importer::RawPolicy>();
    m_roles[Importer::UrlRole] = "url";
    m_roles[Importer::OwnTags] = "ownTags";
    m_roles[Importer::AddedTags] = "addedTags";
    m_roles[Importer::CurrentTags] = "currentTags";
    m_roles[Importer::StatusRole] = "status";
    m_roles[Importer::DuplicateListRole] = "duplicateList";
    m_roles[Importer::DuplicateActionRole] = "duplicateAction";
    m_roles[Importer::IsPairRole] = "isPair";

    QObject::connect(&m_watcher, SIGNAL(started()),
                     q, SIGNAL(isRunningChanged()));
    QObject::connect(&m_watcher, SIGNAL(finished()),
                     q, SIGNAL(isRunningChanged()));
    QObject::connect(&m_watcher, &QFutureWatcher<void>::finished,
                     [=]() {
        Q_EMIT q->finished(m_lastRollId);
    });
}

ImporterPrivate::~ImporterPrivate()
{
    m_terminationMutex.lock();
    if (m_execFuture.isRunning()) {
        m_terminationRequested = true;
        m_terminationCondition.wait(&m_terminationMutex, 10000); // 10 seconds
    }
    m_terminationMutex.unlock();
}

PhotoId ImporterPrivate::importUrl(ThreadData &d,
                                   const Item &item,
                                   const QUrl &url,
                                   const QList<Tag> &tags) const
{
    if (!url.isLocalFile()) {
        return -1;
    }

    QFileInfo fileInfo(url.toLocalFile());

    if (m_copyFiles) {
        QString destination = makeFileVersion(d.destDir, fileInfo);
        bool ok = QFile::copy(fileInfo.filePath(), destination);
        if (Q_UNLIKELY(!ok)) {
            return -1;
        }
        fileInfo = QFileInfo(destination);
    }

    if (Q_UNLIKELY(!fileInfo.isReadable())) {
        return -1;
    }

    const Metadata::ImportData &data = item.metadata;

    Database::Photo photo;
    photo.setBaseUri(QUrl::fromLocalFile(fileInfo.absolutePath()).toString());
    photo.setFileName(fileInfo.fileName());
    photo.setDescription(data.title);
    photo.setTime(data.time);
    photo.setRating(data.rating);
    photo.setLocation(data.location);

    PhotoId id = d.db->addPhoto(photo, d.rollId);
    if (Q_UNLIKELY(id < 0)) {
        return -1;
    }

    d.db->addTags(id, tags);
    if (item.importTags) {
        // Write tag areas
        for (const Metadata::RegionInfo &r: data.regions) {
            Tag t = d.db->tag(r.name);
            if (t.isValid()) {
                d.db->addTagWithArea(id, t, r.area);
            }
        }
    }

    d.thumbnailer.create(photo.url());
    if (d.writeXmp) {
        d.metadata.writeTags(fileInfo.filePath(), Tag::tagNames(tags));
    }
    return id;
}

void ImporterPrivate::exec()
{
    if (Q_UNLIKELY(!checkInput())) {
        for (int i = 0; i < m_items.count(); i++) {
            setStatus(i, Importer::Failed);
        }
        m_lastRollId = -1;
        return;
    }

    Database *db = Database::instance();
    db->transaction();

    QList<Tag> commonTags;
    Q_FOREACH(const QString &tagName, m_commonTags) {
        Tag tag = db->tag(tagName);
        if (!tag.isValid()) {
            tag = db->createTag(m_parentTag, tagName, QString());
        }
        commonTags.append(tag);
    }

    ThreadData threadData;
    threadData.destDir = QDir(m_destination); /* can be invalid, if m_copyFiles
                                                 is false */
    threadData.db = db;
    threadData.rollId = m_lastRollId > 0 ? m_lastRollId : db->createRoll();
    threadData.metadata.setEmbed(m_embed);
    threadData.writeXmp = m_writeXmp;

    int importedCount = 0;
    for (int i = 0; i < m_items.count(); i++) {
        m_terminationMutex.lock();
        if (m_terminationRequested) {
            db->rollback();
            m_lastRollId = 0;
            m_terminationCondition.wakeAll();
            m_terminationMutex.unlock();
            return;
        }
        m_terminationMutex.unlock();

        Item &item = m_items[i];
        if (item.status != Importer::Waiting) continue;

        setStatus(i, Importer::Processing);
        QUrl masterUrl;
        QUrl secondaryUrl;
        if (item.editedUrl.isEmpty() ||
            (m_rawPolicy == Importer::RawIsMaster && !item.rawUrl.isEmpty())) {
            masterUrl = item.rawUrl;
            secondaryUrl = item.editedUrl;
        } else {
            masterUrl = item.editedUrl;
            secondaryUrl = item.rawUrl;
        }

        if (item.duplicateAction == Importer::DuplicateCheckNeeded &&
            m_detector.checkFile(masterUrl)) {
            item.duplicates.clear();
            Q_FOREACH(const MaybeDuplicate &dup,
                      m_detector.duplicatesFor(masterUrl)) {
                item.duplicates.append(dup.id);
            }
            setStatus(i, Importer::DuplicateCheck);
            continue;
        }

        QList<Tag> tags = commonTags;

        if (!item.tags.isEmpty()) {
            Q_FOREACH(const QString &tagName, item.tags) {
                Tag tag = db->tag(tagName);
                if (!tag.isValid()) {
                    tag = db->createTag(m_parentTag, tagName, QString());
                }
                tags.append(tag);
            }
        }

        if (item.duplicateAction == Importer::ImportAsVersion ||
            item.duplicateAction == Importer::ImportReplacing) {
            tags += db->tags(item.duplicatePhotoId);
        }

        PhotoId id = importUrl(threadData, item, masterUrl, tags);
        if (Q_UNLIKELY(id < 0)) {
            setStatus(i, Importer::Failed);
            continue;
        }

        if (!secondaryUrl.isEmpty()) {
            PhotoId idSec = importUrl(threadData, item, secondaryUrl, tags);
            if (Q_UNLIKELY(idSec < 0)) {
                setStatus(i, Importer::Failed);
                continue;
            }
            db->makeVersion(idSec, id);
        }

        if (item.duplicateAction == Importer::ImportAsVersion) {
            db->makeVersion(id, item.duplicatePhotoId);
        } else if (item.duplicateAction == Importer::ImportReplacing) {
            db->deletePhoto(item.duplicatePhotoId);
        }

        setStatus(i, Importer::Done);
        importedCount++;
    }

    if (importedCount > 0) {
        db->commit();
        m_lastRollId = threadData.rollId;
    } else {
        db->rollback();
        m_lastRollId = 0;
    }
}

bool ImporterPrivate::checkInput()
{
    if (Q_UNLIKELY(m_copyFiles &&
                   (m_destination.isEmpty() ||
                    !QDir::root().mkpath(m_destination)))) {
        return false;
    }

    return true;
}

void ImporterPrivate::removeItem(int index, int modelIndex)
{
    Q_Q(Importer);
    q->beginRemoveRows(QModelIndex(), modelIndex, modelIndex);
    int i;
    for (i = 0; i < m_removedIndexes.count(); i++) {
        if (m_removedIndexes[i] > index) break;
    }
    m_removedIndexes.insert(i, index);
    q->endRemoveRows();
}

int ImporterPrivate::modelIndexFromIndex(int index) const
{
    /* "index" is the index the item had before any deletion took place. This
     * function returns the real index, taking the removed items into account.
     */
    int i;
    for (i = 0; i < m_removedIndexes.count(); i++) {
        if (m_removedIndexes[i] > index) break;
    }
    return index - i;
}

int ImporterPrivate::modelIndexToIndex(int index) const
{
    int i;
    int nextRemovedIndex = 0;
    for (i = 0; i < m_items.count(); i++) {
        if (m_removedIndexes.count() > nextRemovedIndex &&
            i == m_removedIndexes[nextRemovedIndex]) {
            nextRemovedIndex++;
            continue;
        }

        index--;
        if (index < 0) break;
    }
    return i;
}

void ImporterPrivate::setStatus(int index, Importer::ItemStatus status)
{
    QMetaObject::invokeMethod(this, "doSetStatus", Qt::QueuedConnection,
                              Q_ARG(int, index),
                              Q_ARG(Importer::ItemStatus, status));
}

void ImporterPrivate::doSetStatus(int index, Importer::ItemStatus status)
{
    Q_Q(Importer);
    Item &item = m_items[index];
    item.status = status;
    if (status == Importer::Done) {
        m_importedCount++;
        Q_EMIT q->importedCountChanged();
    } else if (status == Importer::Failed) {
        m_failedCount++;
        Q_EMIT q->failedCountChanged();
    } else if (status == Importer::Ignored) {
        m_ignoredCount++;
        Q_EMIT q->ignoredCountChanged();
    }

    int modelIndex = modelIndexFromIndex(index);
    QModelIndex idx = q->index(modelIndex, 0);
    Q_EMIT q->dataChanged(idx, idx);

    if (mustClear(status)) {
        removeItem(index, modelIndex);
    }
}

bool ImporterPrivate::mustClear(Importer::ItemStatus status)
{
    switch (status) {
    case Importer::Done: return m_autoClear & Importer::ClearDone;
    case Importer::Failed: return m_autoClear & Importer::ClearFailed;
    case Importer::Ignored: return m_autoClear & Importer::ClearIgnored;
    default: return false;
    }
}

bool ImporterPrivate::fileIsSupported(const QString &fileName) const
{
    /* We let through the most common extensions; for the rest, we use
     * QMimeDatabase and accept only image types. */
    int index = fileName.lastIndexOf('.');
    if (index > 0) {
        QByteArray extension = fileName.mid(index + 1).toUtf8().toLower();
        if (extension == "jpg" ||
            extension == "jpeg" ||
            extension == "png" ||
            /* QMimeType does not know about heif or heic: */
            extension == "heic" ||
            extension == "heif") {
            return true;
        }
    }
    QMimeType mimeType = m_mimeDatabase.mimeTypeForFile(fileName);
    return mimeType.name().startsWith("image/");
}

QList<QUrl> ImporterPrivate::filterItems(const QList<QUrl> &items)
{
    /* This function will take the list and remove all the items
     * that we don't want to import. */
    QList<QUrl> ret;
    for (const QUrl &url: items) {
        if (fileIsSupported(url.toLocalFile())) {
            ret.append(url);
        } else {
            m_unsupportedCount++;
        }
    }
    return ret;
}

Importer::Importer(QObject *parent):
    QAbstractListModel(parent),
    d_ptr(new ImporterPrivate(this))
{
    QObject::connect(this, SIGNAL(modelReset()),
                     this, SIGNAL(countChanged()));
    QObject::connect(this, SIGNAL(rowsRemoved(const QModelIndex&,int,int)),
                     this, SIGNAL(countChanged()));
    QObject::connect(this, SIGNAL(rowsInserted(const QModelIndex&,int,int)),
                     this, SIGNAL(countChanged()));
}

Importer::~Importer()
{
    delete d_ptr;
}

bool Importer::isRunning() const
{
    Q_D(const Importer);
    return d->m_execFuture.isRunning();
}

#define RETURN_IF_BUSY() \
    if (Q_UNLIKELY(!d->m_execFuture.isFinished())) { \
        qWarning() << "Importer: still busy!"; \
        return; \
    }

void Importer::setEmbed(bool embed)
{
    Q_D(Importer);
    RETURN_IF_BUSY();
    if (d->m_embed == embed) return;
    d->m_embed = embed;
    Q_EMIT embedChanged();
}

bool Importer::embed() const
{
    Q_D(const Importer);
    return d->m_embed;
}

void Importer::setWriteXmp(bool writeXmp)
{
    Q_D(Importer);

    if (d->m_writeXmp == writeXmp) return;
    d->m_writeXmp = writeXmp;
    Q_EMIT writeXmpChanged();
}

bool Importer::writeXmp() const
{
    Q_D(const Importer);
    return d->m_writeXmp;
}

void Importer::setCopyFiles(bool copyFiles)
{
    Q_D(Importer);
    RETURN_IF_BUSY();
    if (d->m_copyFiles == copyFiles) return;
    d->m_copyFiles = copyFiles;
    Q_EMIT copyFilesChanged();
}

bool Importer::copyFiles() const
{
    Q_D(const Importer);
    return d->m_copyFiles;
}

void Importer::setDestination(const QString &destination)
{
    Q_D(Importer);
    RETURN_IF_BUSY();
    if (d->m_destination == destination) return;
    d->m_destination = destination;
    Q_EMIT destinationChanged();
}

QString Importer::destination() const
{
    Q_D(const Importer);
    return d->m_destination;
}


void Importer::setParentTag(Tag parent)
{
    Q_D(Importer);
    RETURN_IF_BUSY();
    if (d->m_parentTag == parent) return;
    d->m_parentTag = parent;
    Q_EMIT parentTagChanged();
}

Tag Importer::parentTag() const
{
    Q_D(const Importer);
    return d->m_parentTag;
}


void Importer::setCommonTags(const QStringList &tags)
{
    Q_D(Importer);
    RETURN_IF_BUSY();
    if (d->m_commonTags == tags) return;
    d->m_commonTags = tags;
    Q_EMIT commonTagsChanged();
}

QStringList Importer::commonTags() const
{
    Q_D(const Importer);
    return d->m_commonTags;
}

int Importer::importedCount() const
{
    Q_D(const Importer);
    return d->m_importedCount;
}

int Importer::failedCount() const
{
    Q_D(const Importer);
    return d->m_failedCount;
}

int Importer::ignoredCount() const
{
    Q_D(const Importer);
    return d->m_ignoredCount;
}

int Importer::unsupportedCount() const
{
    Q_D(const Importer);
    return d->m_unsupportedCount;
}

void Importer::setAutoClear(ClearStatuses statuses)
{
    Q_D(Importer);
    d->m_autoClear = statuses;
    Q_EMIT autoClearChanged();
}

Importer::ClearStatuses Importer::autoClear() const
{
    Q_D(const Importer);
    return d->m_autoClear;
}

void Importer::setRawPolicy(RawPolicy policy)
{
    Q_D(Importer);
    d->m_rawPolicy = policy;
    Q_EMIT rawPolicyChanged();
}

Importer::RawPolicy Importer::rawPolicy() const
{
    Q_D(const Importer);
    return d->m_rawPolicy;
}

void Importer::exec()
{
    Q_D(Importer);
    RETURN_IF_BUSY();
    d->m_execFuture = QtConcurrent::run(d, &ImporterPrivate::exec);
    d->m_watcher.setFuture(d->m_execFuture);
}

void Importer::clear()
{
    Q_D(Importer);
    RETURN_IF_BUSY();

    d->m_importedCount = 0;
    Q_EMIT importedCountChanged();
    d->m_failedCount = 0;
    Q_EMIT failedCountChanged();
    d->m_ignoredCount = 0;
    Q_EMIT ignoredCountChanged();
    d->m_unsupportedCount = 0;
    Q_EMIT unsupportedCountChanged();

    beginResetModel();
    d->m_lastRollId = 0;
    d->m_removedIndexes.clear();
    d->m_items.clear();
    endResetModel();
}

void Importer::addItems(const QList<QUrl> &items, const QStringList &tagNames,
                        bool importTags)
{
    Q_D(Importer);
    RETURN_IF_BUSY();

    Metadata metadata;
    int index = d->m_items.count();
    QList<QUrl> filtered = d->filterItems(items);
    DuplicateDetector::RawPairs pairs = d->m_detector.makeRawPairs(filtered);
    beginInsertRows(QModelIndex(), index, index + pairs.count() - 1);
    d->m_removedIndexes.clear();
    Q_FOREACH(const DuplicateDetector::RawPair &pair, pairs) {
        const QUrl &url = pair.edited.isEmpty() ? pair.raw : pair.edited;
        Metadata::ImportData data;
        metadata.readImportData(url.toLocalFile(), data);
        d->m_items.append(ImporterPrivate::Item(pair, data, tagNames, importTags));
    }
    endInsertRows();
    Q_EMIT unsupportedCountChanged();
}

#define RETURN_IF_ROW_INVALID(row) \
    if (Q_UNLIKELY(row < 0 || row >= d->m_items.count())) { \
        qWarning() << "Importer: invalid row" << row; \
        return; \
    }

void Importer::setItemTags(int row, const QStringList &tags)
{
    Q_D(Importer);
    row = d->modelIndexToIndex(row);
    RETURN_IF_ROW_INVALID(row);

    ImporterPrivate::Item &item = d->m_items[row];
    item.tags = tags;
}

void Importer::ignoreItem(int row)
{
    Q_D(Importer);
    row = d->modelIndexToIndex(row);
    RETURN_IF_ROW_INVALID(row);
    d->doSetStatus(row, Importer::Ignored);
}

void Importer::importAsNotDuplicate(int row)
{
    Q_D(Importer);
    row = d->modelIndexToIndex(row);
    RETURN_IF_ROW_INVALID(row);

    ImporterPrivate::Item &item = d->m_items[row];
    item.duplicateAction = Importer::ImportAsNew;
    d->doSetStatus(row, Importer::Waiting);
}

void Importer::importAsVersion(int row, int photoId)
{
    Q_D(Importer);
    row = d->modelIndexToIndex(row);
    RETURN_IF_ROW_INVALID(row);

    ImporterPrivate::Item &item = d->m_items[row];
    item.duplicateAction = Importer::ImportAsVersion;
    item.duplicatePhotoId = photoId;
    d->doSetStatus(row, Importer::Waiting);
}

void Importer::importReplacing(int row, int photoId)
{
    Q_D(Importer);
    row = d->modelIndexToIndex(row);
    RETURN_IF_ROW_INVALID(row);

    ImporterPrivate::Item &item = d->m_items[row];
    item.duplicateAction = Importer::ImportReplacing;
    item.duplicatePhotoId = photoId;
    d->doSetStatus(row, Importer::Waiting);
}

QVariant Importer::get(int row, const QString &roleName) const
{
    int role = roleNames().key(roleName.toLatin1(), -1);
    return data(index(row, 0), role);
}

int Importer::rowCount(const QModelIndex &parent) const
{
    Q_D(const Importer);
    Q_UNUSED(parent);
    return d->m_items.count() - d->m_removedIndexes.count();
}

QVariant Importer::data(const QModelIndex &index, int role) const
{
    Q_D(const Importer);

    int row = d->modelIndexToIndex(index.row());
    if (row < 0 || row >= d->m_items.count()) return QVariant();

    const ImporterPrivate::Item &item = d->m_items[row];
    switch (role) {
    case UrlRole:
        return item.editedUrl.isEmpty() ? item.rawUrl : item.editedUrl;
    case OwnTags:
        return item.ownTags();
    case AddedTags:
        return item.addedTags;
    case CurrentTags:
        return item.tags;
    case StatusRole:
        return item.status;
    case DuplicateListRole:
        return QVariant::fromValue(item.duplicates);
    case DuplicateActionRole:
        return item.duplicateAction;
    case IsPairRole:
        return !item.editedUrl.isEmpty() && !item.rawUrl.isEmpty();
    default:
        qWarning() << "Unknown role ID:" << role;
        return QVariant();
    }
}

QHash<int, QByteArray> Importer::roleNames() const
{
    Q_D(const Importer);
    return d->m_roles;
}

#include "importer.moc"
