/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "database.h"
#include "types.h"
#include "utils.h"

#include <QDateTime>
#include <QDebug>
#include <QDir>
#include <QFileInfo>
#include <QFutureWatcher>
#include <QJSEngine>
#include <QJSValue>
#include <QRegularExpression>
#include <QSequentialIterable>
#include <QtConcurrent>

using namespace Imaginario;

static const QString keyLatitude = QStringLiteral("latitude");
static const QString keyLongitude = QStringLiteral("longitude");

Utils::Utils(QObject *parent):
    QObject(parent)
{
}

Utils::~Utils()
{
}

QVariantMap Utils::geoFields(const GeoPoint &p) const
{
    QVariantMap map;
    if (p.isValid()) {
        map[keyLatitude] = p.lat;
        map[keyLongitude] = p.lon;
    }
    return map;
}

GeoPoint Utils::geo(const QJSValue &value) const
{
    GeoPoint p;
    if (value.isObject() &&
        value.hasProperty(keyLatitude) &&
        value.hasProperty(keyLongitude)) {
        p.lat = value.property(keyLatitude).toNumber();
        p.lon = value.property(keyLongitude).toNumber();
    }
    return p;
}

bool Utils::tagIsValid(Tag tag) const
{
    return tag.isValid();
}

QString Utils::tagName(Tag tag) const
{
    return tag.name();
}

namespace Imaginario {

template <typename T>
T extractValue(const QVariant &v, bool *ok)
{
    if (Q_UNLIKELY(!v.canConvert<T>())) {
        *ok = false;
        return T();
    }
    *ok = true;
    return v.value<T>();
}

template <> Tag extractValue(const QVariant &v, bool *ok)
{
    Tag tag;

    Database *db = Database::instance();
    if (v.type() == QVariant::String) {
        tag = db->tag(v.toString());
    } else if (v.canConvert<Tag>()) {
        tag = v.value<Tag>();
    } else if (v.canConvert<int>()) {
        tag = Tag(v.toInt());
    }
    *ok = tag.isValid();
    return tag;
}

template <typename T>
QList<T> parseList(const QVariant &variant, bool *pOk)
{
    QList<T> result;
    bool succeeded = true;

    if (variant.canConvert<QVariantList>()) {
        QVariant variantList;
        if (variant.userType() == qMetaTypeId<QJSValue>()) {
            variantList = variant.value<QJSValue>().toVariant();
        } else {
            variantList = variant;
        }

        QSequentialIterable iterable = variantList.value<QSequentialIterable>();
        Q_FOREACH(const QVariant &v, iterable) {
            bool ok = false;
            T value = extractValue<T>(v, &ok);
            if (ok) {
                result.append(value);
            } else if (v.isValid()) {
                if (!pOk) qWarning() << "Cannot parse" << v;
                succeeded = false;
            }
        }
    } else {
        bool ok = false;
        T value = extractValue<T>(variant, &ok);
        if (ok) {
            result.append(value);
        } else if (variant.isValid()) {
            if (!pOk) qWarning() << "Cannot parse" << variant;
            succeeded = false;
        }
    }

    if (pOk) *pOk = succeeded;
    return result;
}

template <> QList<QList<Tag>>
parseList(const QVariant &variant, bool *pOk)
{
    QList<QList<Tag>> result;

    bool succeeded = true;
    bool ok = true;
    QList<Tag> tagList = parseList<Tag>(variant, &ok);
    if (ok) {
        if (!tagList.isEmpty()) {
            result.append(tagList);
        }
    } else if (variant.canConvert<QVariantList>()) {
        QVariant variantList;
        if (variant.userType() == qMetaTypeId<QJSValue>()) {
            variantList = variant.value<QJSValue>().toVariant();
        } else {
            variantList = variant;
        }

        QSequentialIterable iterable = variantList.value<QSequentialIterable>();
        Q_FOREACH(const QVariant &v, iterable) {
            bool ok = false;
            tagList = parseList<Tag>(v, &ok);
            if (ok) {
                result.append(tagList);
            } else if (v.isValid()) {
                if (!pOk) qWarning() << "Cannot parse" << v;
                succeeded = false;
            }
        }
    }

    if (pOk) *pOk = succeeded;
    return result;
}

bool Utils::pathIsWritable(const QString &path) const
{
    QFileInfo info(path);
    return info.isWritable();
}

bool Utils::urlIsWritable(const QUrl &url) const
{
    return pathIsWritable(url.toLocalFile());
}

QDateTime Utils::fileTime(const QUrl &url) const
{
    QFileInfo info(url.toLocalFile());
    return info.lastModified();
}

QString Utils::fileName(const QUrl &url) const
{
    QFileInfo info(url.toLocalFile());
    return info.fileName();
}

QList<QUrl> Utils::findFiles(const QUrl &dirUrl, bool recursive) const
{
    QList<QUrl> files;

    QDir dir(dirUrl.toLocalFile());
    if (Q_UNLIKELY(!dir.exists())) return files;

    auto list = dir.entryInfoList(QDir::NoDotAndDotDot |
                                  QDir::Dirs | QDir::Files,
                                  QDir::Name);
    for (const QFileInfo &info: list) {
        if (info.isDir()) {
            if (recursive) {
                files += findFiles(QUrl::fromLocalFile(info.filePath()), true);
            }
            continue;
        }

        files.append(QUrl::fromLocalFile(info.filePath()));
    }

    return files;
}

void Utils::findFiles(const QUrl &dirUrl, bool recursive,
                      const QJSValue &callback)
{
    auto *watcher = new QFutureWatcher<QList<QUrl>>(this);
    QObject::connect(watcher, &QFutureWatcher<QList<QUrl>>::finished,
                     this, [this,watcher,callback]() {
        QList<QUrl> files = watcher->result();
        QJSValue cbCopy(callback); // needed as callback is captured as const
        QJSEngine *engine = qjsEngine(this);
        cbCopy.call(QJSValueList { engine->toScriptValue(files) });
        watcher->deleteLater();
    });
    watcher->setFuture(QtConcurrent::run(this, &Utils::findFiles,
                                         dirUrl, recursive));
}

QUrl Utils::urlForArea(const QUrl &url, const QJsonObject &area) const
{
    QUrlQuery query(url);
    QStringList keys {
        QStringLiteral("x"),
        QStringLiteral("y"),
        QStringLiteral("width"),
        QStringLiteral("height"),
    };
    for (const QString &key: keys) {
        const QJsonValue v = area.value(key);
        if (!v.isDouble()) return url;

        query.addQueryItem(key, QString::number(v.toDouble()));
    }

    QUrl ret(url);
    ret.setQuery(query);
    return ret;
}

QRectF Utils::areaFromUrl(const QUrl &url)
{
    if (!url.hasQuery()) return QRectF();

    QUrlQuery query(url);
    bool ok = true;

    double x = query.queryItemValue("x").toDouble(&ok);
    if (!ok) return QRectF();
    double y = query.queryItemValue("y").toDouble(&ok);
    if (!ok) return QRectF();
    double width = query.queryItemValue("width").toDouble(&ok);
    if (!ok) return QRectF();
    double height = query.queryItemValue("height").toDouble(&ok);
    if (!ok) return QRectF();

    return (width > 0 && height > 0) ? QRectF(x, y, width, height) : QRectF();
}

/* Transform a relative area (with all coordinates in the 0.0-1.0 range) into a
 * real rectangle, by multiplying it to the given size. */
QRect Utils::scaleArea(const QRectF &relativeArea, const QSize &size)
{
    return QRect(relativeArea.x() * size.width(),
                 relativeArea.y() * size.height(),
                 relativeArea.width() * size.width(),
                 relativeArea.height() * size.height());
}

template QList<int> parseList(const QVariant &variant, bool *ok);
template QList<Tag> parseList(const QVariant &variant, bool *ok);

QString makeFileVersion(const QDir &destDir, const QFileInfo &fileInfo)
{
    QString destination = destDir.filePath(fileInfo.fileName());
    if (QFile::exists(destination)) {
        int i = 1;
        QString baseName = destDir.filePath(fileInfo.completeBaseName());
        QString suffix = fileInfo.suffix();
        /* Strip any (x) at the end of the base name */
        QRegularExpression regExp(" \\((\\d+)\\)$");
        auto match = regExp.match(baseName);
        if (match.hasMatch()) {
            i = match.captured(1).toInt() + 1;
            baseName.truncate(match.capturedStart(0));
        }
        do {
            destination = QString("%1 (%2).%3").
                arg(baseName).arg(i++).arg(suffix);
        } while (QFile::exists(destination) && i < INT_MAX);
    }

    return destination;
}

} // namespace
