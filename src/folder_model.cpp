/*
 * Copyright (C) 2016-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "folder_model.h"

#include <QDebug>
#include <QDir>
#include <QStorageInfo>
#include <cmath>

using namespace Imaginario;

namespace Imaginario {

class FolderModelPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(FolderModel)

public:
    FolderModelPrivate(FolderModel *q);
    ~FolderModelPrivate();

    QString displayName(const QStorageInfo &info) const;

private Q_SLOTS:
    void update();

private:
    QHash<int, QByteArray> m_roles;
    QList<QStorageInfo> m_devices;
    FolderModel *q_ptr;
};

} // namespace

FolderModelPrivate::FolderModelPrivate(FolderModel *q):
    QObject(q),
    q_ptr(q)
{
    m_roles[Qt::DisplayRole] = "displayName";
    m_roles[FolderModel::PathRole] = "path";

    update();
}

FolderModelPrivate::~FolderModelPrivate()
{
}

void FolderModelPrivate::update()
{
    Q_Q(FolderModel);

    q->beginResetModel();
    m_devices.clear();
    QList<QStorageInfo> allDevices = QStorageInfo::mountedVolumes();

    for (const QStorageInfo &info: allDevices) {
        QDir dir(info.rootPath());
        if (dir.cd("DCIM")) {
            m_devices.append(info);
        }
    }

    q->endResetModel();
}

QString FolderModelPrivate::displayName(const QStorageInfo &info) const
{
    QString name(info.displayName());
    if (name.isEmpty() || name[0] == '/') {
        // Display the device in terms of its size
        double megasFloat = info.bytesTotal() / double(1000 * 1000);
        qint64 megas = std::round(megasFloat);
        if (megas < 1000) {
            return tr("%1 MB Volume").arg(megas);
        }
        double gigasFloat = megasFloat / 1000;
        qint64 gigas = std::round(gigasFloat);
        if (gigas < 1000) {
            return tr("%1 GB Volume").arg(gigas);
        }
        qint64 teras = std::round(gigasFloat / 1000);
        return tr("%1 TB Volume").arg(teras);
    }
    return name;
}

FolderModel::FolderModel(QObject *parent):
    QAbstractListModel(parent),
    d_ptr(new FolderModelPrivate(this))
{
    QObject::connect(this, SIGNAL(modelReset()),
                     this, SIGNAL(countChanged()));
}

FolderModel::~FolderModel()
{
}

QVariant FolderModel::get(int row, const QString &roleName) const
{
    int role = roleNames().key(roleName.toLatin1(), -1);
    return data(index(row, 0), role);
}

int FolderModel::rowCount(const QModelIndex &parent) const
{
    Q_D(const FolderModel);
    Q_UNUSED(parent);
    return d->m_devices.count();
}

QVariant FolderModel::data(const QModelIndex &index, int role) const
{
    Q_D(const FolderModel);

    int row = index.row();
    if (row < 0 || row >= d->m_devices.count()) return QVariant();

    const QStorageInfo &e = d->m_devices[row];
    switch (role) {
    case Qt::DisplayRole:
        return d->displayName(e);
    case PathRole:
        return e.rootPath() + "/DCIM";
    default:
        qWarning() << "Unknown role ID:" << role;
        return QVariant();
    }
}

QHash<int, QByteArray> FolderModel::roleNames() const
{
    Q_D(const FolderModel);
    return d->m_roles;
}

#include "folder_model.moc"
