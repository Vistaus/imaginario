import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.0
import "."

Dialog {
    id: root

    property var imageView: null
    property bool deleteFromDrive: false

    title: qsTr("Delete confirmation")
    standardButtons: StandardButton.Ok | StandardButton.Cancel

    ColumnLayout {
        Label {
            Layout.fillWidth: true
            Layout.fillHeight: true
            text: (deleteFromDrive ? qsTr("Delete the %1 selected images from the disk?") : qsTr("Remove the %1 selected images from the catalogue?")).arg(imageView.selectedIndexes.length)
        }
    }

    onAccepted: {
        if (deleteFromDrive) {
            GlobalWriter.deletePhotos(imageView.getSelectedIds())
        }
        imageView.model.deletePhotos(imageView.selectedIndexes)
        root.close()
    }
}
