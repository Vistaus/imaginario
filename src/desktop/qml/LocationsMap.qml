import Imaginario 1.0
import QtLocation 5.0
import QtPositioning 5.3
import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1

Item {
    id: root
    property var selectedIndexes
    property alias photoModel: locationModel.model

    implicitWidth: 200
    implicitHeight: header.implicitHeight + mapView.implicitHeight

    Component.onCompleted: fitViewport()

    LocationModel {
        id: locationModel
        zoomLevel: mapView.zoomLevel
    }

    Plugin {
        id: locationPlugin
        preferred: [ "osm" ]
    }

    RowLayout {
        id: header
        anchors { left: parent.left; right: parent.right }
        Label {
            Layout.fillWidth: true
            text: qsTr("%1 non geotagged items", "", locationModel.nonGeoTaggedCount).arg(locationModel.nonGeoTaggedCount)
            wrapMode: Text.Wrap
        }
        Button {
            visible: locationModel.nonGeoTaggedCount > 0
            enabled: !photoModel.nonGeoTagged
            text: qsTr("Show")
            onClicked: {
                photoModel.nonGeoTagged = true
                var invalidLocation = Utils.geo(null)
                photoModel.location0 = invalidLocation
                photoModel.location1 = invalidLocation
            }
        }
    }

    Map {
        id: mapView
        anchors { left: parent.left; right: parent.right; top: header.bottom; bottom: parent.bottom }
        implicitHeight: Math.max(200, width)
        minimumZoomLevel: 1.0
        plugin: locationPlugin
        gesture.enabled: true

        MapItemView {
            id: itemView
            model: locationModel
            delegate: MapQuickItem {
                anchorPoint: Qt.point(sourceItem.width / 2, sourceItem.height)
                coordinate: QtPositioning.coordinate(model.location.lat, model.location.lon)
                sourceItem: Item {
                    width: 40
                    height: 20
                    Image {
                        anchors { left: parent.left; right: parent.right; bottom: parent.bottom }
                        fillMode: Image.PreserveAspectFit
                        source: "qrc:/icons/geotag-handle"
                        sourceSize.width: width
                        Rectangle {
                            anchors { fill: countLabel; margins: -1 }
                            radius: 6
                            color: "white"
                        }
                        Text {
                            id: countLabel
                            anchors { horizontalCenter: parent.horizontalCenter; top: parent.top; margins: 9 }
                            text: model.count
                        }
                    }
                }
            }
        }
    }

    Button {
        anchors { right: parent.right; top: mapView.top; margins: 4 }
        text: qsTr("Apply filter")
        onClicked: applyFilter()
    }

    Connections {
        target: root.photoModel
        onModelReset: fitViewport()
    }

    Timer {
        id: viewportTimer
        onTriggered: mapView.fitViewportToMapItems()
        interval: 300
        repeat: false
    }

    function fitViewport() {
        if (photoModel && !photoModel.nonGeoTagged) {
            mapView.zoomLevel = 12
            viewportTimer.start()
        }
    }

    function applyFilter() {
        var c0 = mapView.toCoordinate(Qt.point(0,0))
        var c1 = mapView.toCoordinate(Qt.point(mapView.width, mapView.height))
        photoModel.location0 = Utils.geo(c0)
        photoModel.location1 = Utils.geo(c1)
    }
}
