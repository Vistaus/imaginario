import QtQuick 2.5
import QtQuick.Controls 1.4

Label {
    id: root

    property var photoModel: null

    property bool filterActive: photoModel.roll0 != -1 || photoModel.roll1 != -1

    visible: filterActive
    text: qsTr("Import roll")
}
