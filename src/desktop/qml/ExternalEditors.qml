import Imaginario 1.0
import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import QtQuick.Window 2.2
import "popupUtils.js" as PopupUtils

ColumnLayout {
    id: root

    TableView {
        id: tableView
        Layout.fillWidth: true
        Layout.fillHeight: true

        TableViewColumn {
            role: "actionName"
            title: qsTr("Command title")
        }

        TableViewColumn {
            role: "details"
            title: qsTr("Command to be executed")
        }

        model: editorsModel
        onDoubleClicked: root.openEditor({ "row": row, "model": editorsModel.helpers })
    }

    ListModel {
        id: editorsModel
        property var helpers: []
        property var initialized: false

        onHelpersChanged: {
            updateControls()
            if (initialized) {
                Settings.helpers = helpers
            }
        }

        Component.onCompleted: {
            helpers = Settings.helpers
            if (!helpers || helpers.length == 0) {
                helpers = helperModel.defaultHelpersData
            }
            initialized = true
        }

        function updateControls() {
            clear()
            var rows = helpers.length
            for (var row = 0; row < rows; row++) {
                var data = helpers[row]
                if (data.command) {
                    data.details = data.command
                } else {
                    data.details = qsTr('<i>Find:</i> %1').arg(data.possibleNames.join(' / '))
                }
                append(data)
            }
            tableView.resizeColumnsToContents()
        }
    }

    RowLayout {
        Layout.fillWidth: true

        Button {
            iconName: "list-add"
            text: qsTr("Add command...")
            onClicked: {
                root.openEditor({
                    "model": editorsModel.helpers,
                })
            }
        }

        Button {
            iconName: "document-properties"
            text: qsTr("Edit selected...")
            enabled: tableView.selection.count > 0
            onClicked: {
                var rows = []
                tableView.selection.forEach(function(rowIndex) { rows.push(rowIndex) })
                root.openEditor({
                    "model": editorsModel.helpers,
                    "row": rows[0],
                })
            }
        }

        Button {
            iconName: "list-remove"
            text: qsTr("Remove command")
            onClicked: {
                var rows = []
                tableView.selection.forEach(function(rowIndex) { rows.push(rowIndex) })
                if (rows.length == 1) {
                    var model = editorsModel.helpers
                    model.splice(rows[0], 1)
                    editorsModel.helpers = model
                }
            }
        }

        Button {
            iconName: "document-revert"
            text: qsTr("Restore defaults")
            enabled: !Settings.helpers || Settings.helpers.length > 0
            onClicked: {
                editorsModel.helpers = helperModel.defaultHelpersData
                Settings.helpers = []
            }
        }
    }

    HelperModel { id: helperModel }

    Timer {
        repeat: false
        interval: 10
        running: true
        onTriggered: tableView.resizeColumnsToContents()
    }

    function openEditor(params) {
        params["helperModel"] = helperModel
        var dlg = PopupUtils.open(Qt.resolvedUrl("ExternalEditorEditDialog.qml"), root, params)
        dlg.accepted.connect(function() {
            editorsModel.helpers = dlg.model
        })
    }
}
