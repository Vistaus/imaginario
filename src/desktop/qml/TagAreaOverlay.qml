import Imaginario 1.0
import QtQuick 2.5
import "."

MouseArea {
    id: root

    property var imageView: null
    property var currentAreaOrigin: null
    property var currentArea: null
    property var currentItem: null
    // mapping of model ids to lists of areas
    property var editingAreas: ({})
    property bool hasAreas: Object.keys(editingAreas).length > 0

    property var _currentIndex: null
    property var _currentImageRect: null

    onPressed: {
        if (mouse.button != Qt.LeftButton) return
        var viewX = mouseX + imageView.contentX
        var viewY = mouseY + imageView.contentY
        currentItem = imageView.itemAt(viewX, viewY)
        if (!currentItem) return

        _currentImageRect = currentItem.getImageRect()
        currentAreaOrigin = root.mapToItemWithinImage(mouse.x, mouse.y)
        _currentIndex = imageView.indexAt(viewX, viewY)

        /* We don't support writing the same tag twice into the image, and
         * consequently it doesn't make sense for the user to select more than
         * one area per image. Therefore, clear any existing areas. */
        if (_currentIndex in editingAreas) {
            var tmp = editingAreas
            tmp[_currentIndex] = []
            editingAreas = tmp
        }

        mouse.accepted = true
    }

    onPositionChanged: {
        if (!currentAreaOrigin) return
        if (mouse.buttons != Qt.LeftButton) return

        if (!currentArea) {
            currentArea = areaComponent.createObject(currentItem, {
                "x": currentAreaOrigin.x,
                "y": currentAreaOrigin.y,
            })
        }

        var pos = root.mapToItemWithinImage(mouse.x, mouse.y)
        if (pos.x < currentAreaOrigin.x) {
            currentArea.x = pos.x
            currentArea.width = currentAreaOrigin.x - pos.x
        } else {
            currentArea.width = pos.x - currentArea.x
        }

        if (pos.y < currentAreaOrigin.y) {
            currentArea.y = pos.y
            currentArea.height = currentAreaOrigin.y - pos.y
        } else {
            currentArea.height = pos.y - currentArea.y
        }
    }

    onReleased: {
        if (currentArea) {
            var area = {
                "x": (currentArea.x - _currentImageRect.x) / _currentImageRect.width,
                "y": (currentArea.y - _currentImageRect.y) / _currentImageRect.height,
                "width": currentArea.width / _currentImageRect.width,
                "height": currentArea.height / _currentImageRect.height,
            }
            var areaList = [ area ]
            var tmp = editingAreas
            tmp[_currentIndex] = areaList
            editingAreas = tmp
            currentArea.destroy()
        }
        currentAreaOrigin = null
        currentArea = null
    }

    Component {
        id: areaComponent
        Rectangle {
            id: rect
            border { width: 2; color: "yellow" }
            color: "transparent"
            opacity: 0.8
        }
    }

    function mapToItemWithinImage(x, y) {
        var pos = root.mapToItem(currentItem, x, y)
        pos.x = Math.max(_currentImageRect.x, Math.min(pos.x, _currentImageRect.x + _currentImageRect.width))
        pos.y = Math.max(_currentImageRect.y, Math.min(pos.y, _currentImageRect.y + _currentImageRect.height))
        return pos
    }

    function deleteArea(itemIndex, tag) {
        if (!tag.valid) {
            console.log("Delete editing area without tag")
            var tmp = editingAreas
            tmp[itemIndex] = []
            editingAreas = tmp
        } else {
            console.log("Delete editing area with tag: " + tag)
            var photoId = imageView.model.get(itemIndex, "photoId")
            GlobalWriter.changePhotoTags([photoId], [], [tag])
        }
    }

    function clear() {
        editingAreas = {}
    }

    function assignTag(tag) {
        if (!tag.icon) {
            console.log("Tag has no icon; creating one")
            var firstItemUrl = ""
            for (var itemIndex in editingAreas) {
                firstItemUrl = imageView.model.get(itemIndex, "url")
                var areaList = editingAreas[itemIndex]
                if (areaList.length > 0) {
                    // Use the first tag area
                    var area = areaList[0]
                    firstItemUrl = Utils.urlForArea(firstItemUrl, area);
                }
                break
            }
            if (firstItemUrl) {
                GlobalWriter.updateTag(tag, {"icon": firstItemUrl})
            }
        }
        for (var itemIndex in editingAreas) {
            var regions = []
            var areaList = editingAreas[itemIndex]
            for (var i = 0; i < areaList.length; i++) {
                var tagArea = areaList[i]

                console.log("added region")
                regions.push({
                    "tag": tag,
                    "rect": Qt.rect(tagArea.x, tagArea.y,
                                    tagArea.width, tagArea.height),
                })
            }
            if (regions.length > 0) {
                var photoId = imageView.model.get(itemIndex, "photoId")
                GlobalWriter.addRegions(photoId, regions)
            }
        }

        editingAreas = {}
    }
}
