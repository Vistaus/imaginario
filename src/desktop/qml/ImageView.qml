import Imaginario 1.0
import QtQuick 2.7

GridView {
    id: root

    property bool selectMode: false
    property var selectedIndexes: []
    property int itemSize: 192
    property bool dragActive: false
    property int firstVisibleIndex: -1
    property bool tagAreaMode: false
    property alias tagAreaEditor: tagAreaOverlay

    signal itemClicked(int index)
    signal itemActivated(int index)
    signal rightClicked()

    property int _itemMaxSize: 256
    property int _itemsPerRow: width / itemSize
    property var __modifiers: 0
    property int __previousIndex: -1
    property var __weakHighlight: Qt.rgba(palette.highlight.r, palette.highlight.g, palette.highlight.b, palette.highlight.a * 0.2)

    activeFocusOnTab: true
    keyNavigationEnabled: true

    cellWidth: Math.floor(width / _itemsPerRow)
    cellHeight: cellWidth - (Settings.showTags ? 0 : 18)

    highlightRangeMode: dragActive ? GridView.ApplyRange : GridView.NoHighlightRange
    preferredHighlightBegin: 40
    preferredHighlightEnd: height - 40

    delegate: basicDelegate
    onSelectModeChanged: if (!selectMode) clearSelection()
    onCountChanged: clearSelection()
    onMovementEnded: updateFirstItem()

    /* The following is needed because movements caused by the scrolled window
     * or anyway by immediate positioning do not trigger the movementEnded()
     * signal
     */
    onContentYChanged: updateFirstTimer.restart()
    Timer {
        id: updateFirstTimer
        interval: 300
        onTriggered: updateFirstItem()
    }

    Keys.onPressed: {
        __modifiers = event.modifiers
        if (count > 0) {
            if (event.key == Qt.Key_Home) currentIndex = 0
            else if (event.key == Qt.Key_End) currentIndex = count -1
        }
    }
    Keys.onReleased: __modifiers = event.modifiers
    onCurrentIndexChanged: if (!dragActive && currentIndex != __previousIndex) {
        select(currentIndex, __modifiers)
    }

    Component {
        id: basicDelegate
        ImageViewDelegate {
            width: GridView.view.cellWidth
            height: GridView.view.cellHeight
            backgroundColor: root.backgroundColor(index)
            url: model.url
            photoId: model.photoId
            rating: model.rating
            showTags: Settings.showTags
            showAreaTags: Settings.showAreaTags || root.tagAreaMode
            showRating: Settings.showRating
            tagAreaMode: root.tagAreaMode
            editingAreas: index in tagAreaOverlay.editingAreas ? tagAreaOverlay.editingAreas[index] : []

            Keys.onReturnPressed: root.itemActivated(index)
            onClicked: root.handleItemClick(index, mouse)
            onDoubleClicked: root.itemActivated(index)
            onAreaDeletionRequested: tagAreaOverlay.deleteArea(index, tag)
        }
    }

    Connections {
        target: model
        onModelReset: updateFirstTimer.restart()
    }
    onModelChanged: updateFirstTimer.restart()

    MouseArea {
        anchors.fill: parent
        acceptedButtons: Qt.RightButton
        onWheel: {
            if (wheel.modifiers & Qt.ControlModifier) {
                if (wheel.angleDelta.y > 0) {
                    zoomIn()
                } else {
                    zoomOut()
                }
            } else {
                wheel.accepted = false
            }
        }
        onClicked: root.rightClicked()
    }

    TagAreaOverlay {
        id: tagAreaOverlay
        anchors.fill: parent
        visible: root.tagAreaMode
        imageView: root
        z: -1
    }

    SystemPalette { id: palette }

    function backgroundColor(index) {
        return root.isSelected(index) ? palette.highlight :
            (root.dragActive && parent.GridView.isCurrentItem ? root.__weakHighlight : palette.base)
    }

    function handleItemClick(index, mouse) {
        root.forceActiveFocus(Qt.MouseFocusReason)
        if (root.selectMode) {
            root.select(index, mouse.modifiers)
            root.currentIndex = index
        } else {
            root.itemClicked(index)
        }
    }

    function isSelected(index) {
        return selectedIndexes.indexOf(index) >= 0
    }

    function select(index, modifiers) {
        if (index < 0 || index >= model.rowCount()) return
        var s = selectedIndexes
        if (modifiers & Qt.ShiftModifier) {
            forCurrentToIndex(index, function(i) {
                if (s.indexOf(i) < 0) s.push(i)
            })
        } else if (modifiers & Qt.ControlModifier) {
            toggleSelection(index)
        } else {
            s = [ index ]
        }
        selectedIndexes = s
        __previousIndex = index
    }

    function forCurrentToIndex(index, callback) {
        if (index > __previousIndex) {
            for (var i = __previousIndex; i <= index; i++) {
                callback(i)
            }
        } else {
            for (var i = index; i <= __previousIndex; i++) {
                callback(i)
            }
        }
    }

    function toggleSelection(index) {
        var i = selectedIndexes.indexOf(index)
        var newList = selectedIndexes
        if (i < 0) {
            newList.push(index)
        } else {
            newList.splice(i, 1)
        }
        selectedIndexes = newList
    }

    function clearSelection() {
        selectedIndexes = []
    }

    function selectAll() {
        var all = Array.apply(0, Array(count)).map(function(e, i) {
            return i;
        });
        selectedIndexes = all
    }

    function getSelectedIds() {
        var selectedIds = []
        for (var i = 0; i < selectedIndexes.length; i++) {
            selectedIds.push(model.get(selectedIndexes[i], "photoId"))
        }
        return selectedIds
    }

    function selectedUrls() {
        var urls = []
        var length = selectedIndexes.length
        for (var i = 0; i < length; i++) {
            urls.push(model.get(selectedIndexes[i], "url"))
        }
        return urls
    }

    function zoomIn() {
        root.itemSize = Math.min(root._itemMaxSize, root.itemSize * 6 / 5)
    }

    function zoomOut() {
        root.itemSize = Math.max(128, root.itemSize * 5 / 6)
    }

    function updateFirstItem() {
        var x_step = cellWidth / 2;
        var y_step = cellHeight / 2;
        for (var y = 0; y < 3; y++) {
            for (var x = 1; x < 3; x++) {
                var index = indexAt(x_step * x, cellHeight - 10 + y_step * y + contentY);
                if (index != -1) {
                    root.firstVisibleIndex = index;
                    return;
                }
            }
        }
        root.firstVisibleIndex = -1;
    }
}

