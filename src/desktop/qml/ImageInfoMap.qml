import Imaginario 1.0
import QtLocation 5.0
import QtPositioning 5.3
import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1

Map {
    id: root

    property var selectedIndexes: []
    property var photoModel: null
    property var empty: listModel.count == 0

    implicitHeight: Math.max(200, width)
    minimumZoomLevel: 2.0
    maximumZoomLevel: 16
    plugin: locationPlugin
    gesture.enabled: true

    onSelectedIndexesChanged: listModel.update()
    onPhotoModelChanged: listModel.update()

    Plugin {
        id: locationPlugin
        preferred: [ "osm" ]
    }

    ListModel {
        id: listModel
        dynamicRoles: true
        function update() {
            var indexes = root.selectedIndexes
            var m = root.photoModel
            listModel.clear()
            if (!m || !indexes) return
            root.zoomLevel = 8
            var l = indexes.length
            for (var i = 0; i < l; i++) {
                var row = root.selectedIndexes[i]
                var location = m.get(row, "location")
                if (!location.valid) continue
                listModel.append({
                    "location": location,
                })
            }
        }
    }

    MapItemView {
        id: itemView
        autoFitViewport: true
        model: listModel
        delegate: MapQuickItem {
            anchorPoint: Qt.point(sourceItem.width / 2, sourceItem.height)
            coordinate: QtPositioning.coordinate(location.lat, location.lon)
            sourceItem: Item {
                width: 40
                height: 20
                Image {
                    anchors { left: parent.left; right: parent.right; bottom: parent.bottom }
                    fillMode: Image.PreserveAspectFit
                    source: "qrc:/icons/geotag-handle"
                    sourceSize.width: width
                }
            }
        }
    }

    Connections {
        target: root.photoModel
        onModelReset: listModel.update()
    }
}
