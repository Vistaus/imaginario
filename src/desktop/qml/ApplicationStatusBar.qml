import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.0

StatusBar {
    id: root

    property var photoModel: null
    property var imagePanel: null

    RowLayout {
        anchors.fill: parent

        Label {
            id: statusLabel

            Layout.fillWidth: true
            horizontalAlignment: Text.AlignHCenter
            text: qsTr("%n elements", "", photoModel.count)
            elide: Text.ElideRight
        }
    }

    states: [
        State {
            name: "info"
            when: imagePanel.tagAreaMode
            PropertyChanges {
                target: statusLabel
                text: qsTr("Editing area tags (press 'F' to cancel): draw rectangles over faces or objects, then type a tag name")
            }
        }
    ]
}
