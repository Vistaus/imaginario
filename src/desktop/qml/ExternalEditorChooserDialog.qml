import Imaginario 1.0
import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1

Dialog {
    id: root

    property alias model: view.model
    property alias selectedRow: view.currentRow

    title: qsTr("Select helper application")
    standardButtons: StandardButton.Ok | StandardButton.Cancel
    width: 800
    height: 300
    modality: Qt.ApplicationModal

    onModelChanged: console.log("model length: " + model.length)
    ColumnLayout {
        anchors.fill: parent

        Label {
            Layout.fillWidth: true
            text: qsTr("Select an application")
        }

        TableView {
            id: view
            Layout.fillWidth: true
            Layout.fillHeight: true

            headerVisible: false

            onDoubleClicked: { root.accepted(); root.close() }

            TableViewColumn {
                role: "icon"
                width: 32
                delegate: Item {
                    Image {
                        anchors {
                            right: parent.right
                            top: parent.top; bottom: parent.bottom
                        }
                        sourceSize { width: parent.height; height: parent.height }
                        fillMode: Image.PreserveAspectFit
                        source: styleData.value
                    }
                }
            }

            TableViewColumn { role: "actionName" }
        }
    }
}
