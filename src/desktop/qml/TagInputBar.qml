import Imaginario 1.0
import QtQml 2.2
import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.0
import "."

FocusScope {
    id: root

    property var selectedIndexes: []
    property var model: null
    property var parentTag: null

    property bool __visibilityRequested: false
    implicitHeight: tagInputRow.implicitHeight
    visible: enabled && __visibilityRequested
    onSelectedIndexesChanged: if (visible) tagInputRow.setup();

    RowLayout {
        id: tagInputRow

        anchors.fill: parent

        property var __selectedPhotos: []
        property var __oldCommonTags: []
        property var parentTag: root.parentTag ? root.parentTag : tagModel.uncategorizedTag

        Label {
            text: qsTr("Tags:")
        }

        TagInput {
            id: tagInput
            Layout.fillWidth: true
            model: tagModel
            Keys.onEscapePressed: root.__visibilityRequested = false
            Keys.onReturnPressed: tagInputRow.save()
        }

        TagModel {
            id: tagModel
            onCountChanged: if (allTags) tagInputRow.continueSetup()
        }

        function setup() {
            tagInput.text = ""
            tagInput.enabled = false
            tagModel.allTags = false

            var photoIds = []
            var length = selectedIndexes.length
            for (var i = 0; i < length; i++) {
                photoIds.push(model.get(selectedIndexes[i], "photoId"))
            }
            __selectedPhotos = photoIds
            tagModel.photosForUsageCount = photoIds
            if (photoIds.length > 0) tagModel.allTags = true
        }

        function continueSetup() {
            /* The model is ready; find common tags */
            var commonTags = []
            var numPhotos = __selectedPhotos.length
            var length = tagModel.count
            for (var i = 0; i < length; i++) {
                var usageCount = tagModel.get(i, "usageCount")
                if (usageCount == numPhotos) {
                    var name = tagModel.get(i, "name")
                    commonTags.push(name)
                }
            }

            __oldCommonTags = commonTags
            tagInput.tags = commonTags
            tagInput.enabled = true
        }

        function save() {
            var commonTags = tagInput.getTags()
            var addedTags = []
            var removedTags = []
            for (var i = 0; i < commonTags.length; i++) {
                var tag = commonTags[i]
                if (__oldCommonTags.indexOf(tag) < 0 && addedTags.indexOf(tag) < 0) {
                    if (!tagModel.tag(tag).valid) {
                        var icon = model.get(selectedIndexes[0], "url")
                        GlobalWriter.createTag(tag, parentTag, icon)
                    }
                    addedTags.push(tag)
                }
            }
            for (var i = 0; i < __oldCommonTags.length; i++) {
                var tag = __oldCommonTags[i]
                if (commonTags.indexOf(tag) < 0) {
                    removedTags.push(tag)
                }
            }
            GlobalWriter.changePhotoTags(__selectedPhotos, addedTags, removedTags)
            tagInput.tags = commonTags
        }
    }

    function show() {
        tagInputRow.setup()
        __visibilityRequested = true
        if (enabled) tagInput.forceActiveFocus()
    }
}
