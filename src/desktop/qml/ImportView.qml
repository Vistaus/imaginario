import Imaginario 1.0
import QtQuick 2.7
import QtQuick.Controls 1.4

ScrollView {
    id: root

    property alias model: gridView.model
    property alias currentIndex: gridView.currentIndex

    verticalScrollBarPolicy: Qt.ScrollBarAlwaysOn
    implicitWidth: 256

    GridView {
        id: gridView
        clip: true

        property int itemSize: 128
        property int _itemMaxSize: 256
        property int _itemsPerRow: Math.max(width / itemSize, 1)

        cellWidth: width / _itemsPerRow
        cellHeight: cellWidth
        activeFocusOnTab: true
        keyNavigationEnabled: true
        focus: true

        delegate: Item {
            property bool selected: GridView.isCurrentItem
            width: GridView.view.cellWidth
            height: GridView.view.cellHeight
            Rectangle {
                anchors { fill: parent; margins: 2 }
                border { width: parent.activeFocus ? 1 : 0; color: "dimgrey" }
                color: parent.selected ? palette.highlight : palette.base

                ImportDelegate {
                    id: image
                    anchors.fill: parent
                    source: model.url
                    isPair: model.isPair
                    itemMaxSize: gridView._itemMaxSize
                    onClicked: gridView.currentIndex = index
                }

                Loader {
                    id: brokenImage
                    anchors.fill: parent
                    active: image.brokenImage
                    Component.onCompleted: setSource(Qt.resolvedUrl("ImportDelegateBroken.qml"), {
                        "itemMaxSize": gridView._itemMaxSize,
                        "fileName": Utils.fileName(model.url),
                    });
                    onLoaded: image.visible = false
                }
            }
        }

        SystemPalette { id: palette }
    }
}
