import QtQuick 2.4
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Column {
    id: root

    property alias expanded: checkBox.checked
    property alias title: checkBox.text
    property alias source: loader.source
    property alias contentItem: loader.item

    spacing: 8

    CheckBox {
        id: checkBox
        anchors { left: parent.left; right: parent.right }
        style: CheckBoxStyle {
            spacing: 6
            indicator: Item {
                implicitWidth: 18
                implicitHeight: label.implicitHeight
                Label {
                    id: label
                    horizontalAlignment: Text.AlignRight
                    text: control.checked ? "\u25bc" : "\u25b6"
                    font.pointSize: 8
                    style: Text.PlainText
                    anchors.fill: parent
                }
            }
            label: Label {
                text: control.text
                elide: Text.ElideRight
            }
        }
    }

    Loader {
        id: loader
        anchors { left: parent.left; right: parent.right }
        active: checkBox.checked
        visible: active
    }
}
