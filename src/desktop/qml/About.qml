import QtQml 2.2
import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Window 2.2

Popup {
    Rectangle {
        anchors.centerIn: parent
        width: Math.min(500 * Screen.devicePixelRatio, parent.width - 40)
        height: Math.min(540 * Screen.devicePixelRatio, parent.height - 30)
        color: palette.window
        radius: 20
        border { width: 1; color: palette.windowText }

        ScrollView {
            id: view
            anchors { fill: parent; margins: 20 }

            ColumnLayout {
                width: view.width - 20
                spacing: 20

                Image {
                    Layout.fillWidth: true
                    fillMode: Image.PreserveAspectFit
                    source: "qrc:/icons/imaginario"
                    sourceSize.width: width / 2
                }

                Label {
                    Layout.fillWidth: true
                    textFormat: Text.StyledText
                    text: qsTr("Imaginario version %1<br/><font size=\"1\">Copyright ⓒ 2015-2019 mardy.it</font>").arg(Qt.application.version)
                    horizontalAlignment: Text.AlignHCenter
                }

                Label {
                    Layout.fillWidth: true
                    wrapMode: Text.WordWrap
                    text: qsTr("Imaginario is free software, distributed under the <a href=\"https://www.gnu.org/licenses/gpl-3.0.en.html\">GPL-v3 license</a>. Among other things, this means that you can share it with your friends for free, or resell it for a fee.")
                    onLinkActivated: Qt.openUrlExternally(link)
                }

                Label {
                    Layout.fillWidth: true
                    wrapMode: Text.WordWrap
                    textFormat: Text.StyledText
                    text: qsTr("If you got Imaginario for free, please consider making a small donation to its author:<br/><ul><li>Paypal: <a href=\"https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=SUEP2EDA8VMRA&source=url\">click here to donate a free amount</a></li><li>Donate via <a href=\"https://money.yandex.ru/quickpay/shop-widget?writer=seller&targets=Imaginario&targets-hint=&default-sum=&button-text=14&payment-type-choice=on&mobile-payment-type-choice=on&hint=&successURL=&quickpay=shop&account=410015419471966\">Yandex Money</a></li></ul>")
                    onLinkActivated: Qt.openUrlExternally(link)
                }
            }
        }
    }

    SystemPalette { id: palette }
}
