import Imaginario 1.0
import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.0
import QtQuick.Window 2.2
import "."

Dialog {
    id: root

    property var photoModel: null

    title: qsTr("Find by rating")
    standardButtons: StandardButton.Apply | StandardButton.Cancel

    onApply: root.save()

    GridLayout {
        columns: 2
        columnSpacing: 20
        rowSpacing: 4

        Label {
            verticalAlignment: Text.AlignVCenter
            text: qsTr("Minimum rating:")
        }

        Label {
            verticalAlignment: Text.AlignVCenter
            text: qsTr("Maximum rating:")
        }

        RatingControl {
            id: rating0
            rating: photoModel.rating0
            height: 20
        }

        RatingControl {
            id: rating1
            rating: photoModel.rating1
            height: 20
        }
    }

    function save() {
        photoModel.rating0 = rating0.rating
        photoModel.rating1 = rating1.rating
        close()
    }
}
