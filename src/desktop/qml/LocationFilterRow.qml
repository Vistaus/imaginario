import QtQuick 2.5
import QtQuick.Controls 1.4

Label {
    id: root

    property var photoModel: null

    property bool filterActive: photoModel.location0 !== photoModel.location1 || photoModel.nonGeoTagged

    visible: filterActive
    text: photoModel.nonGeoTagged ? qsTr("Non geotagged") : qsTr("Location filter")
}
