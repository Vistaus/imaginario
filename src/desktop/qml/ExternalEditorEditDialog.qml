import Imaginario 1.0
import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import "popupUtils.js" as PopupUtils

Dialog {
    id: root

    property var model: null
    property int row: -1
    property var helperModel: null

    title: row < 0 ? qsTr("Add external command") : qsTr("Edit external command")
    standardButtons: StandardButton.Save | StandardButton.Cancel
    width: 800
    height: 300
    modality: Qt.ApplicationModal

    property var _commandData: (model && row >= 0) ? model[row] : _defaultData
    property var _defaultData: {
        "actionName": "",
        "command": "",
        "canBatch": false,
        "readOnly": true,
        "makeVersion": false,
        "mimeTypes": [ "image/*" ],
    }

    ColumnLayout {
        anchors.fill: parent

        GridLayout {
            Layout.fillWidth: true

            columns: 2
            columnSpacing: 4
            rowSpacing: 4

            Label {
                text: qsTr("Command name")
            }

            RowLayout {
                TextField {
                    id: actionNameControl
                    Layout.fillWidth: true
                    placeholderText: qsTr("Command name (as shown in the menu)")
                    text: _commandData.actionName
                }
                Button {
                    text: qsTr("Choose…")
                    onClicked: root.openAppSelector()
                }
            }

            Label {
                text: qsTr("Command to be executed")
            }

            TextField {
                id: commandControl
                Layout.fillWidth: true
                placeholderText: qsTr("Actual command to be executed")
                text: _commandData.command
            }
        }

        GridLayout {
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignTop
            columns: 2
            columnSpacing: 20
            rowSpacing: 4

            CheckBox {
                id: canBatchControl
                Layout.fillWidth: true
                Layout.preferredWidth: 200
                text: qsTr("Operates on multiple files at once")
                checked: _commandData.canBatch
            }

            CheckBox {
                id: readOnlyControl
                Layout.fillWidth: true
                Layout.preferredWidth: 200
                text: qsTr("Modifies file")
                checked: !_commandData.readOnly
            }

            CheckBox {
                id: makeVersionControl
                Layout.fillWidth: true
                text: qsTr("Make a new version")
                // TODO Uncomment when using QtQuick.Controls 1.7
                //tooltip: qsTr("Whether to make a copy of the input file before invoking the command")
                checked: _commandData.makeVersion
            }
        }

        GroupBox {
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.minimumHeight: 120
            title: qsTr("Supported MIME types")
            implicitWidth: gl.implicitWidth

            GridLayout {
                id: gl
                anchors.fill: parent
                columns: 3

                Repeater {
                    id: mimeRepeater
                    // deep copy:
                    model: JSON.parse(JSON.stringify(_commandData.mimeTypes))
                    CheckBox {
                        Layout.maximumWidth: parent * 2 / 5
                        Layout.minimumWidth: parent / 4
                        text: modelData
                        checked: true
                    }

                    function selectedTypes() {
                        var l = count
                        var types = []
                        for (var i = 0; i < l; i++) {
                            var checkBox = itemAt(i)
                            if (checkBox.checked) {
                                types.push(checkBox.text)
                            }
                        }
                        return types
                    }
                }

                Item {
                    Layout.columnSpan: 3
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Label {
                        anchors.centerIn: parent
                        width: parent.width * 4 / 5
                        wrapMode: Text.Wrap
                        visible: mimeRepeater.count == 0
                        text: qsTr("No MIME types selected: the action will be available for all file types")
                    }
                }
            }
        }

        RowLayout {
            Layout.fillWidth: true

            ComboBox {
                id: combo
                Layout.fillWidth: true
                editable: true
                model: MimeDatabase.allMimeNames
                onAccepted: parent.addMime()
                Component.onCompleted: currentIndex = -1
                // TODO: once on Qt 5.12: https://bugreports.qt.io/browse/QTBUG-69095
            }

            Button {
                text: qsTr("Add")
                enabled: combo.editText
                onClicked: parent.addMime()
            }

            function addMime() {
                var tmp = mimeRepeater.model
                tmp.push(combo.editText)
                mimeRepeater.model = tmp
                combo.currentIndex = -1
            }
        }
    }

    onAccepted: root.save()

    function save() {
        var data = {
            "actionName": actionNameControl.text,
            "command": commandControl.text,
            "possibleNames": _commandData.possibleNames,
            "canBatch": canBatchControl.checked,
            "readOnly": !readOnlyControl.checked,
            "makeVersion": makeVersionControl.checked,
            "mimeTypes": mimeRepeater.selectedTypes(),
        }
        if (row >= 0) {
            model[row] = data
        } else {
            model.push(data)
        }
    }

    function openAppSelector() {
        var dlg = PopupUtils.open(Qt.resolvedUrl("ExternalEditorChooserDialog.qml"),
            root,
            {
                "model": root.helperModel.callableApplications
            })
        dlg.accepted.connect(function() {
            if (dlg.selectedRow < 0) return
            var app = dlg.model[dlg.selectedRow]
            console.log("Selected app: " + JSON.stringify(app, null, 2))
            actionNameControl.text = app["actionName"]
            commandControl.text = app["command"]
            mimeRepeater.model = app["mimeTypes"]
        })
    }
}
