import Imaginario 1.0
import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1

ColumnLayout {
    id: root

    property var importer: null
    property var duplicateModel: null

    spacing: 8

    onDuplicateModelChanged: staticDupModel.copy(duplicateModel)

    ListModel {
        id: staticDupModel
        dynamicRoles: true // somehow needed for the duplicateList role

        function copy(filtered) {
            var l = filtered.count
            var source = filtered.sourceModel
            for (var i = 0; i < l; i++) {
                var index = filtered.mapToSource(filtered.index(i, 0))
                append({
                    "row": index.row,
                    "url": source.data(index, Importer.UrlRole).toString(),
                    "status": source.data(index, Importer.StatusRole),
                    "duplicateList": source.data(index, Importer.DuplicateListRole),
                    "duplicateAction": source.data(index, Importer.DuplicateActionRole),
                    "isPair": source.data(index, Importer.IsPairRole),
                    "dupId": 0,
                })
            }
        }

        function refresh(filtered) {
            var l = count
            var source = filtered.sourceModel
            for (var i = 0; i < l; i++) {
                var index = source.index(get(i).row, 0)
                set(i, {
                    "status": source.data(index, Importer.StatusRole),
                    "duplicateAction": source.data(index, Importer.DuplicateActionRole),
                })
            }
        }
    }

    Connections {
        target: duplicateModel.sourceModel
        onDataChanged: refreshTimer.start()
    }

    Timer {
        id: refreshTimer
        interval: 100
        onTriggered: staticDupModel.refresh(duplicateModel)
    }

    Label {
        anchors { left: parent.left; right: parent.right }
        text: qsTr("Duplicate items: <b>%1</b>").arg(staticDupModel.count)
        textFormat: Text.StyledText
        wrapMode: Text.Wrap
    }

    ScrollView {
        Layout.fillWidth: true
        Layout.fillHeight: true
        GridView {
            id: view

            property var selectedIndexes: []
            property int itemSize: 192

            property int __itemMaxSize: 256
            property int __itemsPerRow: width / itemSize
            property var __modifiers: 0
            property int __previousIndex: -1

            cellWidth: Math.floor(width / __itemsPerRow)
            cellHeight: cellWidth
            model: staticDupModel
            clip: true
            delegate: ImportDuplicateDelegate {
                width: GridView.view.cellWidth
                height: GridView.view.cellHeight

                backgroundColor: view.isSelected(index) ? palette.highlight : palette.base

                url: model.url
                isPair: model.isPair
                itemStatus: model.status
                itemDuplicateAction: model.duplicateAction
                duplicateList: model.duplicateList

                onClicked: view.handleItemClick(index, mouse)
                onDuplicatePhotoIdChanged: staticDupModel.setProperty(index, "dupId", duplicatePhotoId)
            }

            Keys.onPressed: {
                __modifiers = event.modifiers
                if (count > 0) {
                    if (event.key == Qt.Key_Home) currentIndex = 0
                    else if (event.key == Qt.Key_End) currentIndex = count -1
                }
            }
            Keys.onReleased: __modifiers = event.modifiers
            onCurrentIndexChanged: if (currentIndex != __previousIndex) {
                select(currentIndex, __modifiers)
            }

            function handleItemClick(index, mouse) {
                forceActiveFocus(Qt.MouseFocusReason)
                select(index, mouse.modifiers)
                currentIndex = index
            }

            function isSelected(index) {
                return selectedIndexes.indexOf(index) >= 0
            }

            function select(index, modifiers) {
                if (index < 0 || index >= model.count) return
                var s = selectedIndexes
                if (modifiers & Qt.ShiftModifier) {
                    forCurrentToIndex(index, function(i) {
                        if (s.indexOf(i) < 0) s.push(i)
                    })
                } else if (modifiers & Qt.ControlModifier) {
                    toggleSelection(index)
                } else {
                    s = [ index ]
                }
                selectedIndexes = s
                __previousIndex = index
            }

            function forCurrentToIndex(index, callback) {
                if (index > __previousIndex) {
                    for (var i = __previousIndex; i <= index; i++) {
                        callback(i)
                    }
                } else {
                    for (var i = index; i <= __previousIndex; i++) {
                        callback(i)
                    }
                }
            }

            function toggleSelection(index) {
                var i = selectedIndexes.indexOf(index)
                var newList = selectedIndexes
                if (i < 0) {
                    newList.push(index)
                } else {
                    newList.splice(i, 1)
                }
                selectedIndexes = newList
            }

            function clearSelection() {
                selectedIndexes = []
            }

            function selectAll() {
                var all = Array.apply(0, Array(count)).map(function(e, i) {
                    return i;
                });
                selectedIndexes = all
            }

            function forEachSelectedItem(callback, data) {
                var l = selectedIndexes.length
                for (var i = 0; i < l; i++) {
                    callback(selectedIndexes[i], data)
                }
            }

            function selectIf(callback, data) {
                var all = Array.apply(0, Array(count)).map(function(e, i) {
                    return i;
                });
                var filtered = all.filter(function(e, i) {
                    return callback(i, data);
                });
                selectedIndexes = filtered
            }
        }
    }

    RowLayout {
        Layout.fillWidth: true

        Button {
            text: qsTr("Select all")
            onClicked: view.selectAll()
        }

        Button {
            text: qsTr("Unselect all")
            onClicked: view.clearSelection()
        }

        Button {
            text: qsTr("Select undecided")
            tooltip: qsTr("Select all items which don't have an action set")
            onClicked: view.selectIf(function(index) {
                return staticDupModel.get(index).status == Importer.DuplicateCheck
            })
        }
    }

    Label {
        Layout.fillWidth: true
        text: qsTr("Action for the %1 selected items:").arg(view.selectedIndexes.length)
    }

    RowLayout {
        Layout.fillWidth: true

        Button {
            text: qsTr("&Ignore (don't import)")
            enabled: view.selectedIndexes.length > 0
            iconSource: "qrc:/icons/import_ignore"
            tooltip: qsTr("Items will not be imported")
            onClicked: view.forEachSelectedItem(function(index) {
                var row = staticDupModel.get(index).row
                importer.ignoreItem(row)
            })
        }

        Button {
            text: qsTr("Import as &new")
            enabled: view.selectedIndexes.length > 0
            iconSource: "qrc:/icons/import_as_new"
            tooltip: qsTr("Items will be imported with no relationship")
            onClicked: view.forEachSelectedItem(function(index) {
                var row = staticDupModel.get(index).row
                importer.importAsNotDuplicate(row)
            })
        }

        Button {
            text: qsTr("Import &replacing")
            enabled: view.selectedIndexes.length > 0
            iconSource: "qrc:/icons/import_replacing"
            tooltip: qsTr("Items will be imported replacing the existing ones")
            onClicked: view.forEachSelectedItem(function(index) {
                var row = staticDupModel.get(index).row
                var photoId = staticDupModel.get(index).dupId
                importer.importReplacing(row, photoId)
            })
        }

        Button {
            text: qsTr("Import as new &version")
            enabled: view.selectedIndexes.length > 0
            iconSource: "qrc:/icons/import_as_version"
            tooltip: qsTr("Items will be imported as a version")
            onClicked: view.forEachSelectedItem(function(index) {
                var row = staticDupModel.get(index).row
                var photoId = staticDupModel.get(index).dupId
                importer.importAsVersion(row, photoId)
            })
        }
    }

    SystemPalette { id: palette }
}
