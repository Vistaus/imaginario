import Imaginario 1.0
import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.0
import QtQuick.Window 2.2
import "popupUtils.js" as PopupUtils
import "."

Dialog {
    id: root

    property var tag: null

    title: qsTr("Delete confirmation")
    standardButtons: StandardButton.NoButton

    ColumnLayout {
        Label {
            Layout.fillHeight: true
            text: qsTr("Delete tag <b>%1</b>?").arg(tag.name)
        }

        RowLayout {
            Button {
                anchors.verticalCenter: parent.verticalCenter
                text: qsTr("Cancel")
                iconName: "cancel"
                onClicked: root.close()
            }

            Button {
                anchors.verticalCenter: parent.verticalCenter
                text: qsTr("Delete tag")
                iconName: "dialog-ok"
                onClicked: {
                    GlobalWriter.deleteTag(tag)
                    root.close()
                }
            }
        }
    }

    TagModel {
        id: tagModel
        allTags: true
    }
}
