import QtQuick 2.5
import QtQuick.Controls 1.4

Item {
    id: root

    property var photoModel: null

    property bool filterActive: outerRepeater.count > 0 || mouseArea.drag.active

    property var __tmpModel: null
    property int __dropIndex: -1

    Connections {
        target: photoModel
        onRequiredTagsChanged: updateModel()
    }

    Row {
        id: outerRow
        anchors.fill: parent
        spacing: 2

        Repeater {
            id: outerRepeater
            model: __tmpModel
            delegate: Rectangle {
                id: outerDelegate
                property int pos: index
                border { width: 1; color: pos == __dropIndex ? "black" : "transparent" }
                color: "transparent"
                height: outerRow.height
                width: innerRow.implicitWidth + 2

                Row {
                    id: innerRow
                    anchors { top: parent.top; bottom: parent.bottom }
                    x: 1
                    spacing: 2

                    Label {
                        anchors { top: parent.top; bottom: parent.bottom }
                        text: qsTr("or")
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        width: implicitWidth + 16
                        visible: index > 0
                    }

                    Repeater {
                        model: modelData
                        delegate: Rectangle {
                            property int index0: outerDelegate.pos
                            property int index1: index
                            anchors.verticalCenter: parent.verticalCenter
                            width: 32; height: 32
                            color: "white"
                            Image {
                                anchors.fill: parent
                                sourceSize { width: width; height: height }
                                source: modelData.iconSource
                            }
                        }
                    }
                }

                function tagAt(pt) {
                    var child = innerRow.childAt(pt.x, pt.y)
                    return child && child.hasOwnProperty("index1") ? child : null
                }
            }
        }

        function tagAt(x, y) {
            var row = childAt(x, y)
            if (!row) return null
            return row.tagAt(row.mapFromItem(outerRow, x, y))
        }
    }

    MouseArea {
        id: mouseArea
        property int index0: -1
        property int index1: -1
        anchors.fill: parent
        drag.axis: Drag.XAndYAxis
        drag.target: draggedImage
        drag.onActiveChanged: if (drag.active) {
            var tmp = __tmpModel
            if (tmp[index0].length == 1) {
                if (tmp.length == 1) {
                    tmp = []
                } else {
                    tmp.splice(index0, 1)
                }
            } else {
                tmp[index0].splice(index1, 1)
            }
            __tmpModel = tmp
        }
        onPressed: {
            var clickedTagItem = outerRow.tagAt(mouse.x, mouse.y)
            if (clickedTagItem) {
                var tag = photoModel.requiredTags[clickedTagItem.index0][clickedTagItem.index1]
                draggedImage.x = mouse.x
                draggedImage.y = mouse.y
                draggedImage.tag = tag
                index0 = clickedTagItem.index0
                index1 = clickedTagItem.index1
            } else {
                console.log("no tag clicked")
                mouse.accepted = false
            }
        }
        onReleased: { draggedImage.Drag.drop(); draggedImage.tag = null }
    }

    TagIcon {
        id: draggedImage
        width: 32; height: 32
        visible: Drag.active
        Drag.active: mouseArea.drag.active
        Drag.onActiveChanged: if (!Drag.active) {
            photoModel.requiredTags = __tmpModel
        }
    }

    DropArea {
        anchors.fill: parent
        keys: [ "x-imaginario/tag" ]
        onEntered: {
            console.log("entered")
            updateDropArea(drag)
            drag.acceptProposedAction()
        }
        onPositionChanged: updateDropArea(drag)
        onExited: {
            __dropIndex = -1
            var tmp = __tmpModel
            if (tmp.length > 0 && tmp[tmp.length - 1].length == 0) {
                tmp.pop()
                __tmpModel = tmp
            }
        }
        onDropped: {
            updateDropArea(drop)
            if (__dropIndex >= 0) {
                var tmp = __tmpModel
                tmp[__dropIndex].push(drop.source.tag)
                __tmpModel = tmp
                photoModel.requiredTags = __tmpModel
                drop.accept()
                __dropIndex = -1
            } else {
                console.warn("Dropindex: " + __dropIndex)
            }
        }

        function updateDropArea(drag) {
            var row = outerRow.childAt(drag.x, drag.y)
            var tmp = __tmpModel
            if (row) {
                __dropIndex = row.pos
                if (tmp.length > 0 && (row.pos != tmp.length - 1) && tmp[tmp.length - 1].length == 0) {
                    tmp.pop()
                    __tmpModel = tmp
                }
            } else {
                if (tmp.length == 0 || tmp[tmp.length - 1].length > 0) {
                    tmp.push([])
                    __tmpModel = tmp
                }
                __dropIndex = tmp.length - 1
            }
        }
    }

    function updateModel() {
        __tmpModel = photoModel.requiredTags.slice()
    }
}
