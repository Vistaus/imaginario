import Imaginario 1.0
import QtQuick 2.0
import QtQuick.Controls 1.4
import "importer.js" as ImporterDlg

FocusScope {
    id: root

    property alias model: imageView.model
    property alias selectMode: imageView.selectMode
    property alias selectedIndexes: imageView.selectedIndexes
    property alias currentIndex: imageView.currentIndex
    property alias firstVisibleIndex: imageView.firstVisibleIndex
    property alias tagAreaMode: imageView.tagAreaMode
    property alias tagAreaEditor: imageView.tagAreaEditor

    signal itemClicked(int index)
    signal itemActivated(int index)
    signal rightClicked()

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        enabled: !tagAreaMode

        ScrollView {
            anchors.fill: parent
            verticalScrollBarPolicy: Qt.ScrollBarAlwaysOn

            ImageView {
                id: imageView
                clip: true
                dragActive: dropArea.containsDrag

                onItemClicked: root.itemClicked(index)
                onItemActivated: root.itemActivated(index)
                onRightClicked: root.rightClicked()
            }
        }

        drag.target: draggedItem
        drag.axis: Drag.XAndYAxis
        drag.filterChildren: true
        onReleased: draggedItem.Drag.drop()
        Item {
            id: draggedItem
            Drag.active: mouseArea.drag.active
            Drag.dragType: Drag.Automatic
            Drag.supportedActions: Qt.CopyAction | Qt.LinkAction
            Drag.mimeData: {
                "text/uri-list": imageView.selectedUrls().join('\n'),
                "text/plain": imageView.selectedUrls().join('\n'),
                "x-imaginario/item": imageView.selectedUrls().join('\n'),
            }
            Drag.keys: [ "text/uri-list", "text/plain", "x-imaginario/item" ]
        }

        ImageViewDropArea {
            id: dropArea
            anchors.fill: parent
            view: imageView
            onImportRequested: ImporterDlg.open(root.model, urls)
        }
    }

    function selectAll() { imageView.selectAll() }
    function selectNone() { imageView.clearSelection() }

    function getSelectedIds() { return imageView.getSelectedIds() }
    function selectedUrls() { return imageView.selectedUrls() }

    function positionViewAtIndex(index, mode) {
        imageView.positionViewAtIndex(index, mode)
    }
    function zoomIn() { imageView.zoomIn() }
    function zoomOut() { imageView.zoomOut() }
}
