import Mardy 1.0
import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2

Dialog {
    id: root

    title: qsTr("Check Imaginario version")
    standardButtons: StandardButton.Ok
    width: 400
    height: 200

    Component.onCompleted: updater.start()

    ColumnLayout {
        anchors.fill: parent
        spacing: 20

        RowLayout {
            Layout.fillWidth: true
            visible: updater.status == Updater.Busy

            Label {
                Layout.fillWidth: true
                text: qsTr("Checking for updates…")
            }

            BusyIndicator {
                id: indicator
                running: true
            }
        }

        GridLayout {
            Layout.fillWidth: true
            columns: 2
            visible: updater.status == Updater.Failed

            Image {
                source: "qrc:/icons/update-failed"
                height: 32
                width: height
                sourceSize.width: width
            }

            Label {
                Layout.fillWidth: true
                text: qsTr("An error occurred while contacting the server")
                wrapMode: Text.WordWrap
            }

            Button {
                Layout.columnSpan: 2
                Layout.alignment: Qt.AlignHCenter
                text: qsTr("Retry")
                onClicked: updater.start()
            }
        }

        ColumnLayout {
            visible: updater.status == Updater.Succeeded
            spacing: 20

            Label {
                Layout.fillWidth: true
                textFormat: Text.StyledText
                wrapMode: Text.WordWrap
                text: qsTr("Latest version: <b>%1</b>").arg(updater.latestVersion.version)
                horizontalAlignment: Text.AlignHCenter
            }

            Label {
                id: updateLabel
                Layout.fillWidth: true
                visible: Object.keys(updater.latestVersion).length > 0 ?
                    updater.latestVersion.isNewer : false
                textFormat: Text.StyledText
                wrapMode: Text.WordWrap
                text: qsTr("You are running Imaginario %1; if you obtained Imaginario from an application store, visit it to update to the latest version. Otherwise, you can download the latest version by downloading it from <a href=\"http://imaginario.mardy.it\">imaginario.mardy.it</a>.").arg(Qt.application.version)
                onLinkActivated: Qt.openUrlExternally(link)
            }

            Label {
                Layout.fillWidth: true
                visible: !updateLabel.visible
                textFormat: Text.StyledText
                wrapMode: Text.WordWrap
                text: qsTr("Your installation of Imaginario is up to date.")
            }
        }
    }

    Updater {
        id: updater
        versionUrl: "http://imaginario.mardy.it/latestversion.json"
    }
}
