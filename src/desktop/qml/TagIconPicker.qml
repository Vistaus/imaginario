import Imaginario 1.0
import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.0
import SortFilterProxyModel 0.2

Dialog {
    id: root

    property var tag: null
    property string icon: ""

    title: qsTr("Choose an icon for tag %1").arg(tag.name)
    standardButtons: StandardButton.Ok | StandardButton.Cancel

    ColumnLayout {
        anchors.fill: parent
        Label {
            text: qsTr("From a tagged photo:")
        }

        GridView {
            id: view
            Layout.minimumHeight: 300
            Layout.minimumWidth: cellWidth * 3
            Layout.fillWidth: true
            Layout.fillHeight: true
            cellWidth: 96
            cellHeight: cellWidth
            clip: true
            model: PhotoModel {
                requiredTags: tag
            }
            delegate: Item {
                id: tagDelegate

                property var url: {
                    var u = model.url
                    if (areaModel.count > 0) {
                        u = Utils.urlForArea(u, areaModel.get(0, "rect"))
                    }
                    return u
                }

                width: GridView.view.cellWidth
                height: GridView.view.cellHeight

                Rectangle {
                    anchors { fill: parent; margins: 1 }
                    color: parent.GridView.isCurrentItem ? palette.highlight : palette.base
                    Image {
                        anchors { fill: parent; margins: 3 }
                        source: "image://item/" + tagDelegate.url
                        asynchronous: true
                        fillMode: Image.PreserveAspectCrop
                        smooth: true
                        sourceSize { width: width; height: height }
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            view.currentIndex = index
                            root.icon = tagDelegate.url
                        }
                    }
                }

                SortFilterProxyModel {
                    id: areaModel
                    filterRoleName: "tag"
                    filterValue: root.tag
                    sourceModel: TagAreaModel {
                        photoId: model.photoId
                    }
                }
            }
        }

        SystemPalette { id: palette }
    }
}
