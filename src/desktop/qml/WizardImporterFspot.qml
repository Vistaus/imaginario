import Imaginario 1.0
import QtQuick 2.5
import QtQuick.Controls 1.4

Column {
    id: root

    property var importer: null
    property bool done: false

    spacing: 8

    Label {
        anchors { left: parent.left; right: parent.right }
        text: qsTr("F-Spot version: <b>%1</b>").arg(importer.version)
        textFormat: Text.StyledText
        wrapMode: Text.Wrap
    }

    Label {
        anchors { left: parent.left; right: parent.right }
        text: qsTr("F-Spot database version: <b>%1</b>").arg(importer.dbVersion)
        textFormat: Text.StyledText
        wrapMode: Text.Wrap
    }

    Label {
        id: statusLabel
        property string name: ""

        anchors { left: parent.left; right: parent.right }
        text: qsTr("Current status: <b>%1</b>").arg(name)
        textFormat: Text.StyledText
        wrapMode: Text.Wrap
    }

    Button {
        enabled: importer.status == FspotImporter.Ready
        text: qsTr("Start import")
        onClicked: importer.exec()
    }

    ProgressBar {
        id: progressBar
        minimumValue: 0
        maximumValue: 1
        value: importer.progress
        visible: false
    }

    states: [
        State {
            name: "ready"
            when: importer.status == FspotImporter.Ready
            PropertyChanges { target: statusLabel; name: qsTr("Ready") }
        },
        State {
            name: "tags"
            extend: "photos"
            when: importer.status == FspotImporter.ImportingTags
            PropertyChanges { target: statusLabel; name: qsTr("Importing tags") }
            PropertyChanges { target: progressBar; indeterminate: true }
        },
        State {
            name: "rolls"
            extend: "tags"
            when: importer.status == FspotImporter.ImportingRolls
            PropertyChanges { target: statusLabel; name: qsTr("Importing rolls") }
        },
        State {
            name: "photos"
            when: importer.status == FspotImporter.ImportingPhotos
            PropertyChanges { target: statusLabel; name: qsTr("Importing photos") }
            PropertyChanges { target: progressBar; visible: true }
        },
        State {
            name: "done"
            when: importer.status == FspotImporter.Done
            PropertyChanges { target: statusLabel; name: qsTr("Import completed") }
            PropertyChanges { target: root; done: true }
        },
        State {
            name: "failed"
            when: importer.status == FspotImporter.Failed
            PropertyChanges { target: statusLabel; name: qsTr("Import failed") }
            PropertyChanges { target: root; done: true }
        }
    ]
}
