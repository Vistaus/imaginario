import QtQuick 2.4
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.0

Label {
    font.bold: true
    horizontalAlignment: Text.AlignRight
    Layout.alignment: Qt.AlignRight
}
