import Imaginario 1.0
import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.0

ColumnLayout {
    id: root

    property bool done: importer.count > 0
    property var importer: null
    property var tagModel: null

    enabled: !importer.loading

    GroupBox {
        id: toolBar
        Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
        Layout.minimumWidth: 400
        title: qsTr("Import from:")

        ColumnLayout {
            anchors.fill: parent
            spacing: 10

            Button {
                Layout.fillWidth: true
                text: qsTr("Choose folder...")
                onClicked: fileDialog.open()
            }

            Repeater {
                model: FolderModel {}
                Button {
                    Layout.fillWidth: true
                    text: model.displayName
                    onClicked: importer.importFolder = "file://" + model.path
                }
            }

            BusyIndicator {
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignHCenter
                running: importer.loading
                visible: running
            }
        }
    }

    GridLayout {
        Layout.fillWidth: true
        Layout.fillHeight: true
        columns: 2

        CheckBox {
            id: recursiveCtrl
            Layout.fillWidth: true
            Layout.minimumWidth: 256
            text: qsTr("Include subfolders")
            checked: importer.recursive
            onCheckedChanged: importer.recursive = checked
        }
    }

    FileDialog {
        id: fileDialog
        title: qsTr("Select import source")
        folder: shortcuts.pictures
        selectFolder: true
        onAccepted: importer.importFolder = fileUrl
    }
}
