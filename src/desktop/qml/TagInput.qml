import QtQuick 2.5
import QtQuick.Controls 1.4

TextField {
    id: root

    property var model: null
    property var tags: []

    property var __options: []
    property bool __completing: false
    property bool __cycling: false
    property int __lastCursorPosition: -1
    property int __lastIndex: -1
    property string __beforeTag: ""
    property string __afterTag: ""

    Keys.onTabPressed: {
        __completing = true
        autoComplete()
        __completing = false
    }
    Keys.onPressed: {
        if (__cycling && (event.key == Qt.Key_Comma || event.key == Qt.Key_Space)) {
            deselect()
        }
    }

    onTagsChanged: recomputeText()
    onActiveFocusChanged: recomputeText()
    onTextChanged: if (!__completing) __cycling = false
    onCursorPositionChanged: if (!__completing) __cycling = false

    function recomputeText() {
        text = tags.join(', ')
        if (activeFocus && text) text += ', '
    }

    function getTags() {
        var parts = text.split(',')
        var tags = []
        for (var i = 0; i < parts.length; i++) {
            var t = parts[i].trim()
            if (t) {
                tags.push(model.properCase(t))
            }
        }
        return tags
    }

    function autoComplete() {
        if (!__cycling) {
            __lastCursorPosition = cursorPosition
            var start = text.lastIndexOf(',', cursorPosition - 1) + 1
            var currentText = text.slice(start, cursorPosition)
            console.log("Examining: " + currentText)
            __options = model.findMatches(currentText)
            __options.push(currentText.trim())
            __lastIndex = -1
            __beforeTag = text.slice(0, start)
            __afterTag = text.slice(cursorPosition)
            __cycling = true
        }
        console.log("Options: " + JSON.stringify(__options))
        if (__options.length <= 1) return

        var newIndex = __lastIndex + 1
        if (newIndex >= __options.length) newIndex = 0

        __lastIndex = newIndex
        var completion = __options[newIndex]
        console.log("Completion: " + completion)

        text = __beforeTag 
        if (__beforeTag) text += ' '
        text += completion
        var pos = text.length
        if (__afterTag) {
            var rest = __afterTag.trim()
            if (!rest || rest[0] != ',') text += ', '
        } else {
            text += ', '
        }
        text += __afterTag
        select(__lastCursorPosition, pos)
    }
}
