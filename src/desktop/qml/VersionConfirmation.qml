import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.0

Dialog {
    id: root

    property string masterName
    property var fileNames: []

    signal confirmed()

    title: qsTr("Make version")
    standardButtons: StandardButton.NoButton

    property string __namesString: fileNames.join(qsTr(', '))

    ColumnLayout {
        anchors { left: parent.left; right: parent.right }

        Label {
            Layout.fillHeight: true
            Layout.fillWidth: true
            font.bold: true
            wrapMode: Text.Wrap
            text: qsTr('Really reparent %1 as version of "%2"?').arg(__namesString).arg(masterName)
        }

        Label {
            Layout.fillHeight: true
            Layout.fillWidth: true
            text: qsTr("This makes the photos appear as a single item in the library.")
        }

        RowLayout {
            Button {
                anchors.verticalCenter: parent.verticalCenter
                text: qsTr("Cancel")
                iconName: "cancel"
                onClicked: root.close()
            }

            Button {
                anchors.verticalCenter: parent.verticalCenter
                text: qsTr("Make version")
                iconName: "dialog-ok"
                onClicked: {
                    root.confirmed()
                    root.close()
                }
            }
        }
    }
}
