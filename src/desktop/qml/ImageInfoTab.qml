import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.0

Item {
    id: root

    property var selectedIndexes
    property var photoModel: null

    Layout.minimumWidth: 1
    implicitWidth: grid.implicitWidth
    implicitHeight: grid.implicitHeight

    Connections {
        /* This is needed in order to catch changes to the selected image and
         * update the view */
        target: photoModel
        onDataChanged: {
            console.log("model data changed, resetting info tab")
            var tmp = selectedIndexes
            selectedIndexes = []
            selectedIndexes = tmp
        }
    }

    GridLayout {
        id: grid

        anchors.fill: parent
        columns: 2
        visible: selectedIndexes.length > 0

        InfoLabel {
            text: qsTr("Name")
        }

        Item {
            Layout.fillWidth: true
            implicitWidth: Math.min(nameLabel.implicitWidth, 150)
            implicitHeight: nameLabel.implicitHeight
            Label {
                id: nameLabel
                anchors.fill: parent
                text: grid.computeName()
                elide: Text.ElideRight
            }
        }

        InfoLabel {
            text: qsTr("Version")
            visible: versionBox.visible
        }

        VersionSelector {
            id: versionBox
            Layout.fillWidth: true
            visible: selectedIndexes.length == 1
            photoId: selectedIndexes.length > 0 ?
                photoModel.get(selectedIndexes[0], "masterVersion") : -1
        }

        InfoLabel {
            text: qsTr("Title")
            visible: titleLabel.visible
        }

        Label {
            id: titleLabel
            Layout.fillWidth: true
            visible: text != ""
            text: grid.computeTitle()
            elide: Text.ElideRight
            wrapMode: Text.Wrap
        }

        InfoLabel {
            text: qsTr("Date")
        }

        Label {
            Layout.fillWidth: true
            text: grid.computeDate()
            elide: Text.ElideRight
        }

        InfoLabel {
            text: qsTr("Size")
            visible: sizeLabel.visible
        }

        Label {
            id: sizeLabel
            Layout.fillWidth: true
            visible: text != ""
            text: grid.computeSize()
            elide: Text.ElideRight
        }

        InfoLabel {
            id: ratingLabel
            text: qsTr("Rating")
            visible: selectedIndexes.length == 1
        }

        RatingControl {
            Layout.fillWidth: true
            height: ratingLabel.height
            visible: ratingLabel.visible
            rating: selectedIndexes.length > 0 ?
                photoModel.get(selectedIndexes[0], "rating") : 0
            editable: false
        }

        ImageInfoMap {
            Layout.columnSpan: 2
            Layout.fillWidth: true
            photoModel: root.photoModel
            selectedIndexes: root.selectedIndexes
            visible: !empty
        }

        function computeName() {
            if (selectedIndexes.length == 0) return ""
            return selectedIndexes.slice(0, 3).map(function(el, index, a) {
                return photoModel.get(el, "fileName")
            }).join(qsTr(', '))
        }

        function computeTitle() {
            if (selectedIndexes.length != 1) return ""
            return photoModel.get(selectedIndexes[0], "title")
        }

        function computeSize() {
            if (selectedIndexes.length != 1) return ""
            var s = photoModel.get(selectedIndexes[0], "size")
            return qsTr("%1x%2").arg(s.width).arg(s.height)
        }

        function computeDate() {
            if (selectedIndexes.length == 0) return ""
            var d = photoModel.get(selectedIndexes[0], "time")
            var dateMin = d
            var dateMax = d
            for (var i = 1; i < selectedIndexes.length; i++) {
                var d = photoModel.get(selectedIndexes[i], "time")
                if (d < dateMin) {
                    dateMin = d
                } else if (d > dateMax) {
                    dateMax = d
                }
            }
            if (dateMin == dateMax) {
                return Qt.formatDateTime(dateMax)
            } else {
                var text1 = Qt.formatDate(dateMin)
                var text2 = Qt.formatDate(dateMax)
                if (text1 == text2) {
                    return text1
                } else {
                    return qsTr("%1 - %2").arg(text1).arg(text2)
                }
            }
        }
    }

    Label {
        anchors.centerIn: parent
        text: qsTr("No image selected")
        visible: !grid.visible
    }
}
