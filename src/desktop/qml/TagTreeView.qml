import Imaginario 1.0
import QtQml 2.2
import QtQml.Models 2.2
import QtQuick 2.0
import QtQuick.Controls 1.4
import "popupUtils.js" as PopupUtils
import "."

MouseArea {
    id: root

    property alias tagView: treeView
    property var photoModel: null
    property var activeTag: treeView.currentIndex.valid ?
        treeView.model.data(treeView.currentIndex, TagModel.TagRole) : TagModel.uncategorizedTag

    signal itemsDropped(var tag)

    implicitWidth: treeView.implicitWidth
    drag.filterChildren: true
    drag.axis: Drag.XAndYAxis
    drag.target: null
    drag.threshold: 1

    onPressed: {
        var index = treeView.indexAt(mouse.x, mouse.y)
        if (!index.valid) {
            mouse.accepted = false
            drag.target = null
        } else {
            var pos = rootItem.mapFromItem(root, mouse.x, mouse.y)
            draggedTag.x = pos.x
            draggedTag.y = pos.y
            draggedTag.tag = tagModel.data(index, TagModel.TagRole)
            drag.target = draggedTag
        }
    }
    onReleased: draggedTag.Drag.drop()

    TreeView {
        id: treeView
        anchors.fill: parent
        model: TagModel {
            id: tagModel
            onModelReset: treeView.__currentRow = -1
        }
        headerVisible: false

        onDoubleClicked: findTag(index)

        // Workaround for https://bugreports.qt.io/browse/QTBUG-52381
        Component.onCompleted: __mouseArea.preventStealing = false

        MouseArea {
            anchors.fill: parent
            acceptedButtons: Qt.RightButton
            onClicked: {
                var index = parent.indexAt(mouse.x, mouse.y)
                if (index.valid) {
                    var view = treeView.__listView
                    /* TODO: remove once https://bugreports.qt.io/browse/QTBUG-52138 is fixed */
                    if (view) {
                        var row = view.indexAt(0, mouseY + view.contentY)
                        view.currentIndex = row
                    }
                }
                menu.popup()
            }
        }

        TableViewColumn {
            role: "iconAndName"
            title: "Tag"
        }

        Menu {
            id: menu

            MenuItem {
                text: qsTr("Find")
                enabled: treeView.currentIndex.valid
                onTriggered: treeView.findTag(treeView.currentIndex)
            }

            Menu {
                id: tagsMenu
                title: qsTr("Find with")
                enabled: treeView.currentIndex.valid && instantiator.count > 0

                Instantiator {
                    id: instantiator
                    model: photoModel.requiredTags
                    MenuItem {
                        text: modelData.map(function(el, index, array) {
                            return el.name
                        }).join(', ')
                        onTriggered: {
                            var tag = tagModel.data(treeView.currentIndex, TagModel.TagRole)
                            var tags = modelData
                            tags.push(tag)
                            var requiredTags = photoModel.requiredTags
                            requiredTags[index] = tags
                            photoModel.requiredTags = requiredTags
                        }
                    }
                    onObjectAdded: tagsMenu.insertItem(index, object)
                    onObjectRemoved: tagsMenu.removeItem(object)
                }
            }

            MenuSeparator {}
            MenuItem { action: actions.createTag }
            MenuSeparator {}
            MenuItem { action: actions.editTag }
            MenuItem { action: actions.deleteTag }
        }

        DropArea {
            id: dropArea
            anchors.fill: parent
            keys: [ "x-imaginario/tag", "x-imaginario/item" ]
            onEntered: updateDropArea(drag)
            onPositionChanged: updateDropArea(drag)
            onExited: disableDrop()
            onDropped: {
                var selectParent = line.visible
                disableDrop()

                var index = treeView.indexAt(drop.x, drop.y)
                if (!index.valid) return
                if (selectParent) {
                    index = index.parent
                }
                var dropTag = index.valid ? tagModel.data(index, TagModel.TagRole) : tagModel.tag("invalid tag")
                if (drop.keys.indexOf("x-imaginario/tag") >= 0) {
                    var tag = drop.source.tag
                    if (tag.parent != dropTag) {
                        var updates = {}
                        updates["parent"] = dropTag
                        GlobalWriter.updateTag(tag, updates)
                    }
                } else if (drop.keys.indexOf("x-imaginario/item") >= 0) {
                    root.itemsDropped(dropTag)
                }
            }

            function disableDrop() {
                line.y = -2
                highlight.parent = dropArea
                expandTimer.index = null
                expandTimer.stop()
            }

            function updateDropArea(drag) {
                var index = treeView.indexAt(drag.x, drag.y)
                if (index != expandTimer.index) {
                    expandTimer.index = index
                    expandTimer.restart()
                }
                var item = treeView.__listView.itemAt(drag.x, drag.y)
                if (!item) {
                    disableDrop()
                    return
                }
                var itemY = dropArea.mapFromItem(item, 0, 0).y
                var linePos = -2
                if (drag.y < itemY + 3) {
                    linePos = itemY - 1
                } else if (drag.y > itemY + item.height - 3) {
                    linePos = itemY + item.height - 1
                }
                line.y = linePos
                if (linePos > -2) {
                    highlight.parent = dropArea
                } else {
                    highlight.parent = item
                }
            }

            Timer {
                id: expandTimer
                property var index: null
                running: false; repeat: false; interval: 1000
                onTriggered: treeView.expand(index)
            }

            Rectangle {
                id: line
                anchors { left: parent.left; right: parent.right; margins: 4 }
                y: -2
                height: 2
                color: "black"
                visible: y >= 0
            }

            Rectangle {
                id: highlight
                border { width: 1; color: palette.highlight }
                color: Qt.rgba(border.color.r, border.color.g, border.color.b, 0.3)
                visible: parent != dropArea
                anchors.fill: parent
            }
        }

        function findTag(index) {
            var tag = tagModel.data(index, TagModel.TagRole)
            var tags = (photoModel.requiredTags && photoModel.requiredTags.length > 0) ? photoModel.requiredTags : []
            tags.push([tag])
            photoModel.requiredTags = tags
        }
    }

    TagIcon {
        id: draggedTag
        parent: rootItem
        z: 10
        visible: Drag.active
        Drag.active: root.drag.active
    }

    SystemPalette { id: palette }
}
