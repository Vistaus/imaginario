import Imaginario 1.0
import QtQuick 2.5
import QtQuick.Controls 1.4

WizardDialog {
    id: root

    title: qsTr("Welcome to Imaginario")

    panelImplicitWidth: 600
    panelImplicitHeight: 400

    property var __importers: [
        digikamImporter,
        fspotImporter,
        shotwellImporter
    ]

    WizardPage {
        WelcomeWizardIntro {}
    }

    WizardPage {
        Preferences {}
    }

    WizardPage {
        enabled: root.hasPhotosToImport()

        WizardImporterSelect {
            digikamImporter: __importers[0]
            fspotImporter: __importers[1]
            shotwellImporter: __importers[2]
        }

        function onConfirmed() {
            var importer = item.selectedImporter
            var index = __importers.indexOf(importer)
            if (index >= 0) {
                root.jumpTo(root.currentIndex + index + 2)
            } else {
                root.jumpTo(root.currentIndex + __importers.length + 2)
            }
        }
    }

    WizardPage {
        WizardImporterNone {}

        function onConfirmed() {
            root.jumpTo(root.currentIndex + __importers.length + 1)
        }
    }

    WizardPage {
        WizardImporterDigikam {
            importer: __importers[0]
        }
        function onConfirmed() {
            root.jumpTo(root.currentIndex + 3)
        }
    }

    WizardPage {
        WizardImporterFspot {
            importer: __importers[1]
        }
        function onConfirmed() {
            root.jumpTo(root.currentIndex + 2)
        }
    }

    WizardPage {
        WizardImporterShotwell {
            importer: __importers[2]
        }
    }

    WizardPage {
        WelcomeWizardFinished {}
    }

    DigikamImporter { id: digikamImporter }
    FspotImporter { id: fspotImporter }
    ShotwellImporter { id: shotwellImporter }

    function hasPhotosToImport() {
        for (var i = 0; i < __importers.length; i++) {
            if (__importers[i].count > 0) return true
        }
        return false
    }

}
