import Imaginario 1.0
import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1

GridLayout {
    id: root

    columns: 2
    columnSpacing: 4
    rowSpacing: 12

    Label {
        Layout.alignment: Qt.AlignTop
        Layout.preferredHeight: combo.implicitHeight
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignRight
        text: qsTr("When importing photos, copy them to:")
    }

    ColumnLayout {
        Layout.fillWidth: true
        spacing: 2
        ComboBox {
            id: combo
            Layout.minimumWidth: 200
            Layout.fillWidth: true
            model: [
                Settings.photoDir,
                qsTr("Choose folder...")
            ]

            onActivated: if (index == 1) {
                // The timer is a workaround for https://bugreports.qt.io/browse/QTBUG-51113
                timer.start()
                fileDialog.open()
            }
            Timer {
                id: timer
                interval: 10
                onTriggered: combo.currentIndex = 0
            }
        }

        HelpLabel {
            Layout.fillWidth: true
            text: qsTr("This is where your photos will be stored. If you don't wish to have them copied, but prefer to keep them in the original location, please activate the checkbox below.")
        }

        Label {
            Layout.fillWidth: true
            visible: !Utils.pathIsWritable(Settings.photoDir)
            text: qsTr("Target directory is not writable!")
            color: "red"
        }

        CheckBox {
            Layout.fillWidth: true
            text: qsTr("By default, don't copy photos")
            checked: !Settings.copyFiles
            onClicked: Settings.copyFiles = !checked
        }
    }

    Label {
        Layout.alignment: Qt.AlignTop | Qt.AlignRight
        Layout.preferredHeight: radioEmbed.implicitHeight
        verticalAlignment: Text.AlignVCenter
        text: qsTr("Store tags and descriptions")
    }

    ColumnLayout {
        Layout.fillWidth: true
        spacing: 2

        RadioButton {
            id: radioEmbed
            text: qsTr("Inside the image files when possible")
            checked: false
            exclusiveGroup: exifGroup
        }
        RadioButton {
            id: radioXmp
            text: qsTr("In XMP files next to the images")
            checked: false
            exclusiveGroup: exifGroup
        }
        RadioButton {
            id: radioDatabase
            text: qsTr("Only in the Imaginario database")
            checked: false
            exclusiveGroup: exifGroup
        }
        ExclusiveGroup {
            id: exifGroup
            onCurrentChanged: {
                Settings.embed = (current == radioEmbed)
                Settings.writeXmp = (current != radioDatabase)
            }
        }
    }

    // Just to prevent vertical stretching
    Item {
        implicitHeight: 0
        Layout.fillHeight: true
    }

    FileDialog {
        id: fileDialog
        title: qsTr("Select image directory")
        folder: shortcuts.pictures
        selectMultiple: false
        selectFolder: true
        onAccepted: Settings.photoDir = urlToLocalFile(fileUrl)

        function urlToLocalFile(url) {
            var s = url.toString()
            return s.indexOf("file://") == 0 ? s.substr(7) : s
        }
    }

    Component.onCompleted: {
        var activeRadio = Settings.writeXmp ? (Settings.embed ? radioEmbed : radioXmp) : radioDatabase
        activeRadio.checked = true
    }
}
