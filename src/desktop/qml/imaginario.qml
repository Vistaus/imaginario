import Imaginario 1.0
import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import "importer.js" as ImporterDlg
import "popupUtils.js" as PopupUtils
import "."

ApplicationWindow {
    id: root

    visible: true

    menuBar: MenuBar {
        Menu {
            title: qsTr("&Photo")
            MenuItem { action: actions.importPhotos }
            MenuItem {
                text: qsTr("Quit")
                shortcut: StandardKey.Quit
                onTriggered: Qt.quit()
            }
        }

        Menu {
            title: qsTr("&Edit")
            MenuItem { action: actions.copy }
            MenuItem {
                text: qsTr("Select all")
                shortcut: StandardKey.SelectAll
                onTriggered: imagePanel.selectAll()
            }
            MenuItem {
                text: qsTr("Select none")
                enabled: imagePanel.selectedIndexes.length > 0
                shortcut: StandardKey.Deselect
                onTriggered: imagePanel.selectNone()
            }
            MenuSeparator {}
            MenuItem { action: actions.refreshThumbnails }
            MenuItem { action: actions.deleteFromCatalog }
            MenuItem { action: actions.deleteFromDrive }
            MenuSeparator {}
            MenuItem {
                text: qsTr("Preferences...")
                onTriggered: PopupUtils.open(Qt.resolvedUrl("PreferencesDialog.qml"), this, {
                })
            }
        }

        Menu {
            title: qsTr("&View")
            MenuItem {
                text: qsTr("Fullscreen")
                shortcut: "F11"
                onTriggered: imagePanel.showFullscreen()
            }
            MenuSeparator {}
            Menu {
                title: qsTr("Thumbnail elements")
                MenuItem {
                    text: qsTr("Tags")
                    checkable: true
                    checked: Settings.showTags
                    onToggled: Settings.showTags = checked
                }
                MenuItem {
                    text: qsTr("Rating")
                    checkable: true
                    checked: Settings.showRating
                    onToggled: Settings.showRating = checked
                }
                MenuItem {
                    text: qsTr("Face/area tags")
                    checkable: true
                    checked: Settings.showAreaTags
                    onToggled: Settings.showAreaTags = checked
                }
            }
            MenuSeparator {}
            MenuItem { action: actions.zoomIn }
            MenuItem { action: actions.zoomOut }
        }

        Menu {
            title: qsTr("&Find")
            Menu {
                title: qsTr("By import roll")
                MenuItem {
                    text: qsTr("Last import roll")
                    onTriggered: photoModel.showLastImportRoll(rollModel)
                }
                MenuItem {
                    text: qsTr("Choose import rolls...")
                    onTriggered: PopupUtils.open(Qt.resolvedUrl("FindByRollDialog.qml"), this, {
                        photoModel: photoModel,
                        rollModel: rollModel,
                    })
                }
                MenuItem {
                    text: qsTr("Clear filter")
                    onTriggered: photoModel.clearRollFilter()
                    enabled: photoModel.hasRollFilter
                }
            }
            Menu {
                title: qsTr("By date")
                MenuItem {
                    text: qsTr("Set date filter...")
                    onTriggered: PopupUtils.open(Qt.resolvedUrl("FindByDateDialog.qml"), this, {
                        photoModel: photoModel,
                    })
                }
                MenuItem {
                    text: qsTr("Clear filter")
                    onTriggered: photoModel.clearTimeFilter()
                    enabled: photoModel.hasTimeFilter
                }
            }
            Menu {
                title: qsTr("By rating")
                MenuItem {
                    text: qsTr("Set rating filter...")
                    onTriggered: PopupUtils.open(Qt.resolvedUrl("FindByRatingDialog.qml"), this, {
                        photoModel: photoModel,
                    })
                }
                MenuItem {
                    text: qsTr("Clear filter")
                    onTriggered: photoModel.clearRatingFilter()
                    enabled: photoModel.hasRatingFilter
                }
            }
            MenuSeparator {}
            MenuItem {
                text: qsTr("Non geotagged photos")
                onTriggered: photoModel.nonGeoTagged = true
            }
            MenuItem {
                text: qsTr("Photos with tag areas")
                onTriggered: photoModel.areaTagged = true
            }
        }
        Menu {
            title: qsTr("&Tags")
            MenuItem { action: actions.createTag }
            MenuSeparator {}
            MenuItem { action: actions.editTag }
            MenuItem { action: actions.deleteTag }
            MenuSeparator {}
            MenuItem { action: actions.tagAreaMode }
        }
        Menu {
            title: qsTr("&Help")
            MenuItem {
                text: "Check for updates…"
                onTriggered: PopupUtils.open(Qt.resolvedUrl("UpdateDialog.qml"), root, {})
            }
            MenuSeparator {}
            MenuItem {
                text: "About…"
                onTriggered: PopupUtils.open(Qt.resolvedUrl("About.qml"), root, {})
            }

        }
    }

    Actions {
        id: actions
        photoModel: photoModel
        imageView: imagePanel
        tagView: tagView.tagView
    }

    toolBar: ToolBar {
        RowLayout {
            anchors.fill: parent
            ToolButton {
                Layout.fillHeight: true
                action: actions.importPhotos
            }
        }
    }

    statusBar: ApplicationStatusBar {
        photoModel: photoModel
        imagePanel: imagePanel
    }

    Component.onCompleted: {
        if (photoModel.count == 0) {
            PopupUtils.open(Qt.resolvedUrl("WelcomeDialog.qml"), this, {
            })
        }
    }

    contentItem {
        minimumWidth: 300; minimumHeight: 200
        implicitWidth: 800; implicitHeight: 600
    }

    SplitView {
        id: rootItem
        anchors.fill: parent
        orientation: Qt.Horizontal

        ColumnLayout {
            Layout.minimumWidth: 50
            Layout.maximumWidth: 800
            spacing: 4

            TagTreeView {
                id: tagView
                Layout.fillWidth: true
                Layout.fillHeight: true
                photoModel: photoModel
                onItemsDropped: imagePanel.handleDropOnTag(tag)
            }

            ExpandableTab {
                id: locationTab
                Layout.fillWidth: true
                title: qsTr("Location filter")
                source: Qt.resolvedUrl("LocationsMap.qml")
                Binding {
                    target: locationTab.contentItem
                    property: "photoModel"; value: imagePanel.model
                }
                Binding {
                    target: locationTab.contentItem
                    property: "selectedIndexes"; value: imagePanel.selectedIndexes
                }
            }

            ExpandableTab {
                id: tab
                Layout.fillWidth: true
                title: qsTr("Image information")
                source: Qt.resolvedUrl("ImageInfoTab.qml")
                Binding {
                    target: tab.contentItem
                    property: "photoModel"; value: imagePanel.model
                }
                Binding {
                    target: tab.contentItem
                    property: "selectedIndexes"; value: imagePanel.selectedIndexes
                }
                Connections {
                    target: imagePanel
                    onSelectedIndexesChanged: if (imagePanel.selectedIndexes.length == 1) {
                        tab.expanded = true
                    }
                }
            }
        }

        ImagePanel {
            id: imagePanel
            Layout.minimumWidth: 200
            Layout.maximumWidth: 4000 // Hack, or we get 0 width */
            Layout.fillWidth: true
            model: photoModel
            selectedTag: tagView.activeTag
        }
    }

    FilteredPhotoModel {
        id: photoModel
    }

    RollModel {
        id: rollModel
    }
}
