import Imaginario 1.0
import QtQuick 2.5

Item {
    id: root

    property bool selected: false
    property alias backgroundColor: background.color
    property var url
    property int photoId: -1
    property int rating: -1
    property alias showTags: tagRow.visible
    property bool showRating: false
    property bool showAreaTags: false
    property bool tagAreaMode: false
    property rect imageRect
    property var editingAreas

    signal clicked(var mouse)
    signal doubleClicked()
    signal areaDeletionRequested(var tag)

    property var __ratingItem: null
    property var __areaTagItem: null

    onShowRatingChanged: if (showRating) {
        __ratingItem = ratingComponent.createObject(root, {})
    } else if (__ratingItem) {
        __ratingItem.destroy();
    }

    onShowAreaTagsChanged: if (showAreaTags || editingAreas.length > 0) {
        if (!__areaTagItem) {
            __areaTagItem = tagAreaComponent.createObject(root, {})
        }
    } else if (__areaTagItem) {
        __areaTagItem.destroy();
    }

    Rectangle {
        id: background
        anchors { fill: parent; margins: 1 }
        border { width: parent.activeFocus ? 1 : 0; color: "dimgrey" }

        Image {
            id: image
            sourceSize { width: _itemMaxSize; height: _itemMaxSize }
            anchors {
                top: parent.top; bottom: Settings.showTags ? tagRow.top : parent.bottom
                left: parent.left; right: parent.right
                margins: 2
            }
            verticalAlignment: Image.AlignBottom
            source: "image://item/" + root.url.toString()
            fillMode: Image.PreserveAspectFit
            smooth: true
            asynchronous: true
            autoTransform: true
            onPaintedWidthChanged: root.imageRect = getImageRect()
        }

        MouseArea {
            anchors.fill: parent
            enabled: !root.tagAreaMode
            onClicked: root.clicked(mouse)
            onDoubleClicked: root.doubleClicked()
        }

        TagRow {
            id: tagRow
            anchors { horizontalCenter: parent.horizontalCenter; bottom: parent.bottom; margins: 2 }
            model: TagModel { photoId: root.photoId }
        }
    }

    Component {
        id: ratingComponent
        Row {
            id: ratingRow
            x: imageRect.x
            y: imageRect.y
            Repeater {
                model: root.rating
                Image {
                    width: 12
                    height: width
                    sourceSize { width: width; height: height }
                    source: "qrc:/icons/rating_gold"
                }
            }
        }
    }

    Component {
        id: tagAreaComponent
        TagAreaView {
            id: tagAreaView
            x: imageRect.x
            y: imageRect.y
            width: imageRect.width
            height: imageRect.height
            enabled: root.tagAreaMode
            photoId: root.photoId
            editingAreas: root.editingAreas
            onDeletionRequested: root.areaDeletionRequested(tag)
        }
    }

    function getImageRect() {
        return mapFromItem(image,
            (image.width - image.paintedWidth) / 2,
            image.height - image.paintedHeight,
            image.paintedWidth,
            image.paintedHeight)
    }
}
