import Imaginario 1.0
import QtQuick 2.5
import QtQuick.Controls 1.4
import SortFilterProxyModel 0.2

WizardDialog {
    id: root

    property var urls

    signal done(int rollId)

    title: qsTr("Import photos")
    panelImplicitWidth: 800
    panelImplicitHeight: 600
    modality: Qt.ApplicationModal // WindowModal doesn't seem to work

    property var __duplicateModel: duplicateModel
    property var __importer: importer
    property var __tagModel: tagModel

    Component.onCompleted: {
        if (urls) importer.addItems(urls)
    }

    WizardPage {
        ImportWizardIntro {
            importer: __importer
            onDoneChanged: if (done) {
                jumpTo(currentIndex + 1)
            }
        }
    }

    WizardPage {
        ImportWizardView {
            importer: __importer
            tagModel: __tagModel
        }

        function onConfirmed() {
            item.run()
            jumpTo(currentIndex + 2)
        }
    }

    WizardPage {
        ImportWizardDups {
            importer: __importer
            duplicateModel: __duplicateModel
        }

        function onConfirmed() {
            importer.exec()
            jumpTo(currentIndex + 1)
        }
    }

    WizardPage {
        property bool isLast: duplicateModel.count == 0
        property bool done: !importer.running
        ImportWizardProgress {
            importer: __importer
            duplicateModel: __duplicateModel
        }

        function onConfirmed() {
            if (duplicateModel.count > 0) {
                jumpTo(currentIndex - 1)
            } else {
                root.done(importer.lastRollId)
                root.close()
            }
        }
    }

    Importer {
        id: importer
        property var importFolder
        property bool loading: false
        property bool importTags: false
        property bool recursive: true // TODO have a setting for this
        property int lastRollId: -1
        embed: Settings.embed
        writeXmp: Settings.writeXmp
        copyFiles: Settings.copyFiles
        destination: Settings.photoDir
        parentTag: tagModel.importedTagsTag
        autoClear: Importer.ClearDone
        onFinished: if (lastRollId <= 0) { lastRollId = rollId }
        onRecursiveChanged: updateFiles()
        onImportFolderChanged: updateFiles()

        function updateFiles() {
            clear();
            loading = true
            Utils.findFiles(importFolder, recursive, function(files) {
                addItems(files, [], importTags)
                loading = false
            })
        }
    }

    SortFilterProxyModel {
        id: duplicateModel
        sourceModel: importer
        filterRoleName: "status"
        filterValue: Importer.DuplicateCheck
    }

    TagModel {
        id: tagModel
        allTags: true
    }
}
