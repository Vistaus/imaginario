import Imaginario 1.0
import QtQml 2.2
import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.0

FocusScope {
    id: root

    property var parentTag: null

    signal tagChosen(var tag)
    signal editAborted()

    implicitHeight: tagInputRow.implicitHeight

    RowLayout {
        id: tagInputRow

        anchors.fill: parent

        Label {
            text: qsTr("Tag for selected areas:")
        }

        TagInput {
            id: tagInput
            Layout.fillWidth: true
            model: tagModel
            focus: true
            Keys.onEscapePressed: root.editAborted()
            Keys.onReturnPressed: {
                var tags = getTags()
                if (tags.length > 0) {
                    var tagName = tags[0]
                    var tag = tagModel.tag(tagName)
                    if (!tag.valid) {
                        var parentTag = root.parentTag ?
                            root.parentTag : tagModel.uncategorizedTag
                        tag = GlobalWriter.createTag(tagName, parentTag)
                    }
                    root.tagChosen(tag)
                }
            }
        }

        TagModel {
            id: tagModel
            allTags: true
        }
    }
}
