import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.0

Item {
    id: root

    Column {
        anchors {
            left: parent.left; right: parent.right; margins: 16
            verticalCenter: parent.verticalCenter
        }
        spacing: 12

        Label {
            anchors { left: parent.left; right: parent.right }
            text: qsTr("No import sources found")
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
        }

        Label {
            anchors { left: parent.left; right: parent.right }
            text: qsTr("Couldn't find any application to import photos from. Currently Imaginario can import photos from Digikam, F-Spot and Shotwell.")
            textFormat: Text.StyledText
            wrapMode: Text.Wrap
        }
    }
}
