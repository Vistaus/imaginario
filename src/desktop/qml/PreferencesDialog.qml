import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1

Dialog {
    id: root

    title: qsTr("Preferences")
    standardButtons: StandardButton.Close
    width: 800
    height: 600
    modality: Qt.ApplicationModal // WindowModal doesn't seem to work

    TabView {
        anchors.fill: parent
        TabWithMargin {
            title: qsTr("General")
            Preferences {}
        }

        TabWithMargin {
            title: qsTr("External editors")
            ExternalEditors {}
        }
    }
}
