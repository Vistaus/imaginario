import Imaginario 1.0
import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.0
import "popupUtils.js" as PopupUtils
import "."

Dialog {
    id: root

    property var parentTag: tag ? tag.parent : null
    property var tag: null

    title: tag ? qsTr("Edit tag %1").arg(tag.name) : qsTr("Create new tag")
    standardButtons: StandardButton.Ok | StandardButton.Cancel

    property string tagIcon: tag ? tag.icon : ""
    property string tagIconSource: tag ? tag.iconSource : ""

    onVisibleChanged: if (visible) tagTextEntry.forceActiveFocus()
    onAccepted: root.save()

    GridLayout {
        columns: 2
        columnSpacing: 4
        rowSpacing: 4

        Label {
            id: iconLabel
            verticalAlignment: Text.AlignVCenter
            text: qsTr("Icon:")
            visible: tag
        }

        Button {
            visible: iconLabel.visible
            style: ButtonStyle {
                label: Rectangle {
                    implicitWidth: 64 + 2
                    implicitHeight: implicitWidth
                    Image {
                        anchors { fill: parent; margins: 1 }
                        fillMode: Image.PreserveAspectCrop
                        source: tagIconSource
                        sourceSize.width: width
                        sourceSize.height: height
                    }
                }
            }
            onClicked: {
                var item = PopupUtils.open(Qt.resolvedUrl("TagIconPicker.qml"), this, {
                    "tag": tag,
                    "icon": tag.icon,
                })
                item.accepted.connect(function() {
                    console.log("New icon: " + item.icon)
                    tagIcon = item.icon
                    tagIconSource = "image://item/" + item.icon
                })
            }

        }

        Label {
            verticalAlignment: Text.AlignVCenter
            text: qsTr("Name:")
        }

        TextField {
            id: tagTextEntry
            text: tag ? tag.name : ""
            validator: RegExpValidator { regExp: /[^,]*/ }

            function checkName() {
                text = text.trim()
                if (!text) {
                    /* TODO
                    PopupUtils.open(dialogComponent, root, {
                        "text": i18n.tr("Cannot set an empty name"),
                    })
                    */
                    return false
                }
                if (tag && text == tag.name) return true
                if (tagModel.tag(text).valid) {
                    /* TODO
                    PopupUtils.open(dialogComponent, root, {
                        "text": i18n.tr("A tag with this name already exists"),
                    })
                    */
                    return false
                }
                return true
            }
        }

        Label {
            verticalAlignment: Text.AlignVCenter
            text: qsTr("Parent tag:")
        }

        ComboBox {
            id: combo
            Layout.minimumWidth: 200
            Layout.fillWidth: true
            model: tagListModel
            textRole: "text"

            Component.onCompleted: {
                // FIXME introduce a way to create an invalid tag
                tagListModel.append({
                    "text": qsTr("No parent"),
                    "tag": tagModel.tag("invalid tag"),
                })
                for (var i = 0; i < tagModel.count; i++) {
                    var t = tagModel.get(i, "tag")
                    tagListModel.append({
                        "text": t.name,
                        "tag": t,
                    })
                    if (t == parentTag) { currentIndex = i + 1 }
                }
            }

            ListModel {
                id: tagListModel
                dynamicRoles: true
            }

            function getTag() {
                return tagListModel.get(currentIndex).tag
            }
        }
    }

    TagModel {
        id: tagModel
        allTags: true
    }

    function save() {
        var updates = {}

        if (!tagTextEntry.checkName()) {
            return false
        }

        var parentTag = combo.getTag()

        if (tag) {
            if (tagTextEntry.text != tag.name) {
                updates["name"] = tagTextEntry.text
            }

            if (tagIcon != tag.icon) {
                updates["icon"] = tagIcon
            }

            if (parentTag != tag.parent) {
                updates["parent"] = parentTag
            }

            GlobalWriter.updateTag(tag, updates)
        } else {
            GlobalWriter.createTag(tagTextEntry.text, parentTag)
        }
        return true
    }
}
