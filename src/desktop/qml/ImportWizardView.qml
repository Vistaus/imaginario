import Imaginario 1.0
import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.0

ColumnLayout {
    id: root

    property bool done: importer.count > 0
    property var importer: null
    property var tagModel: null

    Label {
        Layout.fillWidth: true
        text: qsTr("Found %1 files:", "", importer.count).arg(importer.count)
    }

    SplitView {
        Layout.fillWidth: true
        Layout.fillHeight: true

        ImportView {
            id: importView
            Layout.fillHeight: true
            Layout.minimumWidth: 128
            model: importer
            onVisibleChanged: forceActiveFocus()
        }

        Image {
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.minimumWidth: 256
            fillMode: Image.PreserveAspectFit
            smooth: true
            source: importer.get(importView.currentIndex, "url")
            asynchronous: true
            onSourceChanged: displayPanel.show()

            ImageDisplayPanel {
                id: displayPanel
                position: "bottom"
                height: fileNameLabel.implicitHeight + 8

                Label {
                    id: fileNameLabel
                    anchors { fill: parent; margins: 4 }
                    property string fileUrl: importer.get(importView.currentIndex, "url")
                    elide: Text.ElideMiddle
                    text: fileUrl.indexOf("file://") == 0 ? fileUrl.substr(7) : fileUrl
                    color: "white"
                }
            }

            MouseArea {
                anchors.fill: parent
                hoverEnabled: true
                onPositionChanged: displayPanel.show()
            }
        }
    }

    Label {
        Layout.fillWidth: true
        text: qsTr("%n file(s) are not supported and therefore are not going to be imported!",
                   "", importer.unsupportedCount)
        font.bold: true
        wrapMode: Text.Wrap
        visible: importer.unsupportedCount > 0
    }

    TagInput {
        id: tagInput
        Layout.fillWidth: true
        model: tagModel
    }

    GridLayout {
        Layout.fillWidth: true
        columns: 2

        ColumnLayout {
            CheckBox {
                id: copyFilesCtrl
                Layout.fillWidth: true
                Layout.preferredWidth: 200
                text: qsTr("Copy files to the Photos folder")
                checked: Settings.copyFiles
            }
            Label {
                Layout.fillWidth: true
                text: qsTr("Your photos might become inaccessible to Imaginario.")
                color: "red"
                visible: !Utils.urlIsWritable(importer.importFolder) && !copyFilesCtrl.checked
            }
        }

        CheckBox {
            id: importTagsCtrl
            Layout.fillWidth: true
            Layout.preferredWidth: 200
            text: qsTr("Import tags")
            checked: importer.importTags
            onCheckedChanged: importer.importTags = checked
        }
    }

    function run() {
        Settings.copyFiles = copyFilesCtrl.checked
        importer.commonTags = tagInput.getTags()
        importer.exec()
    }
}
