import QtQuick 2.5

Image {
    id: image

    property var tag: null

    width: 32; height: 32
    sourceSize { width: width; height: height }
    source: tag ? tag.iconSource : ""

    property string tagName: tag ? tag.name : ""
    Drag.supportedActions: Qt.CopyAction | Qt.MoveAction
    Drag.mimeData: {
        "x-imaginario/tag": tag,
        "text/plain": tagName,
    }
    Drag.keys: [ "x-imaginario/tag" ]

    Text {
        visible: image.status == Image.Error || image.status == Image.Null
        text: tagName
    }
}
