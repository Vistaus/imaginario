/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "database.h"
#include "job_database.h"
#include "job_executor.h"
#include "metadata.h"
#include "thumbnailer.h"

#include <QCoreApplication>
#include <QDebug>
#include <QMutex>
#include <QMutexLocker>
#include <QSet>
#include <QtConcurrent>

using namespace Imaginario;

namespace Imaginario {

static JobExecutor *m_instance = 0;

class JobExecutorPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(JobExecutor)

public:
    JobExecutorPrivate(JobExecutor *q);
    ~JobExecutorPrivate();

    void start();

    void addJobs(const QVector<Job> &jobs);
    bool startNextJob(Job &job);
    void endJob(const Job &job);
    void runJobs();

    /* Job functions; if needed, we could make this class totally agnostic
     * about the jobs to be performed. For the time being though, the class is
     * small enough that we can host a couple of job types in here. */
    void runPhotoJob(const Job &job);
    void runThumbnailJob(const Job &job);

private:
    friend class JobExecutor;
    QMutex m_dbMutex;
    QSet<PhotoId> m_activePhotoIds;
    QHash<Job::Type,JobExecutor::ExecutorFunc> m_executors;
    JobDatabase m_db;
    bool m_stopped;
    Metadata m_metadata;
    Thumbnailer m_thumbnailer;
    JobExecutor *q_ptr;
};

} // namespace

JobExecutorPrivate::JobExecutorPrivate(JobExecutor *q):
    m_stopped(false),
    m_metadata(0),
    q_ptr(q)
{
}

JobExecutorPrivate::~JobExecutorPrivate()
{
}

void JobExecutorPrivate::start()
{
    m_stopped = false;
    QtConcurrent::run(this, &JobExecutorPrivate::runJobs);
}

void JobExecutorPrivate::addJobs(const QVector<Job> &jobs)
{
    QMutexLocker locker(&m_dbMutex);
    m_db.transaction();
    bool ok = m_db.addJobs(jobs);
    if (ok) {
        m_db.commit();
        start();
    } else {
        m_db.rollback();
    }
}

bool JobExecutorPrivate::startNextJob(Job &job)
{
    QMutexLocker locker(&m_dbMutex);
    bool hasJob = false;
    for (int jobId = 0; m_db.next(job) && job.id() > jobId; jobId = job.id()) {
        if (job.inProgress()) continue;
        if (job.type() == Job::Photo) {
            PhotoId photoId = static_cast<const PhotoJob&>(job).photoId();
            if (m_activePhotoIds.contains(photoId)) continue;
            m_activePhotoIds.insert(photoId);
        }
        hasJob = true;
        break;
    }
    if (!hasJob) return false;

    return m_db.markJobInProgress(job.id());
}

void JobExecutorPrivate::endJob(const Job &job)
{
    QMutexLocker locker(&m_dbMutex);
    if (job.type() == Job::Photo) {
        PhotoId photoId = static_cast<const PhotoJob&>(job).photoId();
        m_activePhotoIds.remove(photoId);
    }

    m_db.removeJob(job.id());
}

void JobExecutorPrivate::runJobs()
{
    Job job;
    while (!m_stopped && startNextJob(job)) {
        const auto &func = m_executors.find(job.type());
        if (Q_LIKELY(func != m_executors.end())) {
            func.value()(job);
        } else {
            qWarning() << "Don't know how to process job type" << job.type();
        }
        endJob(job);
    }
}

void JobExecutorPrivate::runPhotoJob(const Job &j)
{
    const PhotoJob &job = static_cast<const PhotoJob&>(j);
    Database *db = Database::instance();
    QMutexLocker locker(&db->mutex());
    Database::Photo photo = db->photo(job.photoId());
    if (Q_UNLIKELY(!photo.isValid())) {
        qWarning() << Q_FUNC_INFO << "No such photo" << job.photoId();
        return;
    }

    Metadata::Changes changes;
    PhotoJob::Operations ops = job.operations();
    if (ops & PhotoJob::WriteTags) {
        changes.setTags(Tag::tagNames(db->tags(job.photoId())));
    }
    if (ops & PhotoJob::WriteRating) {
        changes.setRating(photo.rating());
    }
    if (ops & PhotoJob::WriteLocation) {
        changes.setLocation(photo.location().toGeoCoordinate());
    }
    if (ops & PhotoJob::WriteTitle) {
        changes.setTitle(photo.description());
    }
    if (ops & PhotoJob::WriteDescription) {
        changes.setDescription(job.description());
    }
    if (ops & PhotoJob::WriteRegions) {
        Metadata::Regions regions;
        Q_FOREACH(const TagArea &area, db->tagAreas(job.photoId())) {
            QString description = job.regionDescription(area.rect());
            regions.append(Metadata::RegionInfo(area.rect(),
                                                area.tag().name(),
                                                description));
        }
        changes.setRegions(regions);
    }
    locker.unlock();
    m_metadata.writeChanges(photo.url().toLocalFile(), changes);
}

void JobExecutorPrivate::runThumbnailJob(const Job &j)
{
    Q_Q(JobExecutor);

    const ThumbnailJob &job = static_cast<const ThumbnailJob&>(j);
    m_thumbnailer.create(job.uri());
    Q_EMIT q->thumbnailUpdated(job.uri());
}

JobExecutor::JobExecutor(QObject *parent):
    QObject(parent),
    d_ptr(new JobExecutorPrivate(this))
{
    using std::placeholders::_1;
    registerExecutor(Job::Photo,
                     std::bind(&JobExecutorPrivate::runPhotoJob, d_ptr, _1));
    registerExecutor(Job::Thumbnail,
                     std::bind(&JobExecutorPrivate::runThumbnailJob, d_ptr,
                               _1));

    QObject::connect(QCoreApplication::instance(), SIGNAL(aboutToQuit()),
                     this, SLOT(stop()));
}

JobExecutor::~JobExecutor()
{
    m_instance = 0;
    delete d_ptr;
}

JobExecutor *JobExecutor::instance()
{
    if (!m_instance) {
        m_instance = new JobExecutor;
    }
    return m_instance;
}

void JobExecutor::setEmbedMetadata(bool embed)
{
    Q_D(JobExecutor);
    d->m_metadata.setEmbed(embed);
}

bool JobExecutor::embedMetadata() const
{
    Q_D(const JobExecutor);
    return d->m_metadata.embed();
}

void JobExecutor::addJobs(const QVector<Job> &jobs)
{
    Q_D(JobExecutor);
    d->addJobs(jobs);
}

void JobExecutor::registerExecutor(Job::Type type, ExecutorFunc func)
{
    Q_D(JobExecutor);
    d->m_executors.insert(type, func);
}

void JobExecutor::stop()
{
    Q_D(JobExecutor);
    d->m_stopped = true;
}

void JobExecutor::start()
{
    Q_D(JobExecutor);
    d->start();
}

#include "job_executor.moc"
