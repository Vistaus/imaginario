/*
 * Copyright (C) 2019 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "macos.h"

#include <QDebug>
#include <QJsonObject>
#include <QUrl>

#import <ApplicationServices/ApplicationServices.h>
#import <CoreFoundation/CoreFoundation.h>
#import <Foundation/Foundation.h>

namespace Imaginario {

bool MacOS::runApp(const QString &app, const QStringList &files)
{
    CFURLRef appUrl = QUrl::fromLocalFile(app).toCFURL();

    CFMutableArrayRef cfaFiles =
        CFArrayCreateMutable(kCFAllocatorDefault,
                             files.count(),
                             &kCFTypeArrayCallBacks);
    for (const QString &file: files) {
        CFURLRef u = QUrl::fromLocalFile(file).toCFURL();
        CFArrayAppendValue(cfaFiles, u);
        CFRelease(u);
    }

    LSLaunchURLSpec inspec;
    inspec.appURL = appUrl;
    inspec.itemURLs = cfaFiles;
    inspec.asyncRefCon = NULL;
    inspec.launchFlags = kLSLaunchDefaults + kLSLaunchAndDisplayErrors;
    inspec.passThruParams = NULL;

    OSStatus ret;
    ret = LSOpenFromURLSpec(&inspec, NULL);
    CFRelease(appUrl);
    return ret == 0;
}

static QJsonArray getBundleMimeTypes(CFBundleRef bundle)
{
    QJsonArray ret;

    CFArrayRef typesArray =
        (CFArrayRef)CFBundleGetValueForInfoDictionaryKey(
            bundle,
            CFSTR("CFBundleDocumentTypes"));
    if (!typesArray) return ret;

    CFIndex nTypes = CFArrayGetCount(typesArray);
    for (int i = 0; i < nTypes; i++) {
        CFDictionaryRef typesDict =
            (CFDictionaryRef)CFArrayGetValueAtIndex(typesArray, i);
        CFArrayRef partialTypes =
            (CFArrayRef)CFDictionaryGetValue(typesDict,
                                             CFSTR("CFBundleTypeMIMETypes"));
        if (!partialTypes) continue;

        CFIndex nPartialTypes = CFArrayGetCount(partialTypes);
        for (int j = 0; j < nPartialTypes; j++) {
            CFStringRef typeName = (CFStringRef)CFArrayGetValueAtIndex(partialTypes, j);
            ret.append(QString::fromCFString(typeName));
        }
    }
    return ret;
}

QJsonArray MacOS::scanApps()
{
    QJsonArray ret;

    CFArrayRef apps =
        LSCopyAllRoleHandlersForContentType(CFSTR("public.jpeg"),
                                            kLSRolesViewer | kLSRolesEditor);
    if (!apps) return ret;

    CFIndex nApps = CFArrayGetCount(apps);
    for (int i = 0; i < nApps; i++) {
        CFStringRef cfBundleName = (CFStringRef)CFArrayGetValueAtIndex(apps, i);
        CFArrayRef appUrls =
            LSCopyApplicationURLsForBundleIdentifier(cfBundleName, NULL);
        CFIndex nAppUrls = CFArrayGetCount(appUrls);
        for (int j = 0; j < nAppUrls; j++) {
            CFURLRef appUrl = (CFURLRef)CFArrayGetValueAtIndex(appUrls, j);
            CFBundleRef bundle = CFBundleCreate(kCFAllocatorDefault, appUrl);
            CFStringRef name = (CFStringRef)CFBundleGetValueForInfoDictionaryKey(bundle, kCFBundleNameKey);
            if (!name) continue;

            CFStringRef icon = (CFStringRef)
                CFBundleGetValueForInfoDictionaryKey(
                    bundle, CFSTR("CFBundleIconFile"));
            NSString *sIcon = (__bridge NSString *)icon;
            NSString *extension = [sIcon pathExtension];
            CFStringRef extparam = [extension length] > 0 ? NULL: CFSTR("icns");
            CFURLRef iconUrl =
                CFBundleCopyResourceURL(bundle, icon, extparam, NULL);
            QJsonObject appInfo {
                { "actionName", QString::fromCFString(name) },
                { "command", QUrl::fromCFURL(appUrl).toLocalFile() },
                { "icon", iconUrl ? QUrl::fromCFURL(iconUrl).toString() : QString() },
                { "mimeTypes", getBundleMimeTypes(bundle) },
            };
            ret.append(appInfo);

            if (iconUrl) CFRelease(iconUrl);
            CFRelease(bundle);
        }
    }
    CFRelease(apps);

    return ret;
}

} // namespace
