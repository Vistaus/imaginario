/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGINARIO_WRITER_H
#define IMAGINARIO_WRITER_H

#include "types.h"

#include <QList>
#include <QObject>
#include <QStringList>
#include <QUrl>

class QJSValue;

namespace Imaginario {

class WriterPrivate;
class Writer: public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool writeXmp READ writeXmp WRITE setWriteXmp \
               NOTIFY writeXmpChanged)
    Q_PROPERTY(bool embed READ embed WRITE setEmbed NOTIFY embedChanged)

public:
    Writer(QObject *parent = 0);
    ~Writer();

    void setEmbed(bool embed);
    bool embed() const;

    void setWriteXmp(bool writeXmp);
    bool writeXmp() const;

    Q_INVOKABLE Tag createTag(const QString &name, Tag parentTag = Tag(),
                              const QString &icon = QString(""));
    Q_INVOKABLE void updateTag(Tag tag, const QVariantMap &changes);
    Q_INVOKABLE void deleteTag(Tag tag);
    Q_INVOKABLE void setPhotoTags(const QVariant &ids, const QVariant &tags);
    Q_INVOKABLE void changePhotoTags(const QVariant &ids,
                                     const QVariant &addTags,
                                     const QVariant &removeTags);
    Q_INVOKABLE void setRegions(PhotoId photoId, const QJSValue &regions);
    Q_INVOKABLE void addRegions(PhotoId photoId, const QJSValue &regions);
    Q_INVOKABLE void setRating(const QVariant &ids, int rating);
    Q_INVOKABLE void setLocation(const QVariant &ids, const GeoPoint &p);
    Q_INVOKABLE void setTitle(const QVariant &ids, const QString &title);
    Q_INVOKABLE void setDescription(const QVariant &ids, const QString &desc);
    Q_INVOKABLE void makeVersion(PhotoId photoId, PhotoId master);
    Q_INVOKABLE void makeDefaultVersion(PhotoId photoId);
    Q_INVOKABLE void removeVersion(PhotoId photoId);

    Q_INVOKABLE int createItem(const QUrl &url);
    Q_INVOKABLE void deletePhotos(const QVariant &photoIds);

    Q_INVOKABLE void rebuildThumbnails(const QVariant &ids);

Q_SIGNALS:
    void embedChanged();
    void writeXmpChanged();
    void thumbnailUpdated(int photoId);

private:
    WriterPrivate *d_ptr;
    Q_DECLARE_PRIVATE(Writer)
};

} // namespace

#endif // IMAGINARIO_WRITER_H
