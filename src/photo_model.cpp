/*
 * Copyright (C) 2014-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "database.h"
#include "metadata.h"
#include "photo_model.h"
#include "utils.h"

#include <QDebug>
#include <QImageReader>
#include <QMimeDatabase>
#include <QUrl>
#include <algorithm>

using namespace Imaginario;

namespace Imaginario {

class PhotoModelPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(PhotoModel)

public:
    PhotoModelPrivate(PhotoModel *q);
    ~PhotoModelPrivate();

    static int m_fragmentIncrement;

    void queueUpdate();
    int indexOfItem(PhotoId photoId) const;

private Q_SLOTS:
    void update();
    void onPhotoAdded(PhotoId id);
    void onPhotoDeleted(PhotoId id);
    void onPhotoChanged(PhotoId id);

private:
    QHash<int, QByteArray> m_roles;
    Database *m_db;
    Metadata m_metadata;
    QMimeDatabase m_mimeDb;
    bool m_updateQueued;
    bool m_skipNonDefaultVersions;
    bool m_updatingThumbnail;
    QDateTime m_time0;
    QDateTime m_time1;
    int m_roll0;
    int m_roll1;
    int m_rating0;
    int m_rating1;
    GeoPoint m_location0;
    GeoPoint m_location1;
    bool m_nonGeoTagged;
    bool m_areaTagged;
    QVariant m_requiredTagsVariant;
    QList<QList<Tag>> m_requiredTags;
    QVariant m_forbiddenTagsVariant;
    QList<Tag> m_forbiddenTags;
    QVariant m_excludedPhotosVariant;
    QList<PhotoId> m_excludedPhotos;
    QList<PhotoId> m_photoIds;
    PhotoId m_photoVersions;
    Qt::SortOrder m_sortOrder;
    QList<Database::Photo> m_photos;
    mutable PhotoModel *q_ptr;
};

} // namespace

int PhotoModelPrivate::m_fragmentIncrement = 1;

PhotoModelPrivate::PhotoModelPrivate(PhotoModel *q):
    QObject(q),
    m_db(Database::instance()),
    m_updateQueued(false),
    m_skipNonDefaultVersions(false),
    m_updatingThumbnail(false),
    m_roll0(-1),
    m_roll1(-1),
    m_rating0(-1),
    m_rating1(-1),
    m_nonGeoTagged(false),
    m_areaTagged(false),
    m_photoVersions(0),
    m_sortOrder(Qt::DescendingOrder),
    q_ptr(q)
{
    m_roles[PhotoModel::PhotoIdRole] = "photoId";
    m_roles[PhotoModel::UrlRole] = "url";
    m_roles[PhotoModel::FileNameRole] = "fileName";
    m_roles[PhotoModel::TitleRole] = "title";
    m_roles[PhotoModel::DescriptionRole] = "description";
    m_roles[PhotoModel::TimeRole] = "time";
    m_roles[PhotoModel::RollIdRole] = "rollId";
    m_roles[PhotoModel::RatingRole] = "rating";
    m_roles[PhotoModel::LocationRole] = "location";
    m_roles[PhotoModel::HasVersionsRole] = "hasVersions";
    m_roles[PhotoModel::IsDefaultVersionRole] = "isDefaultVersion";
    m_roles[PhotoModel::MasterVersionRole] = "masterVersion";
    m_roles[PhotoModel::SizeRole] = "size";
    m_roles[PhotoModel::MimeTypeRole] = "mimeType";

    QObject::connect(m_db, SIGNAL(photoDeleted(PhotoId)),
                     this, SLOT(onPhotoDeleted(PhotoId)));
    QObject::connect(m_db, SIGNAL(photoAdded(PhotoId)),
                     this, SLOT(onPhotoAdded(PhotoId)));
    QObject::connect(m_db, SIGNAL(photoChanged(PhotoId)),
                     this, SLOT(onPhotoChanged(PhotoId)));
    qRegisterMetaType<QList<PhotoId> >("QList<PhotoId>");
}

PhotoModelPrivate::~PhotoModelPrivate()
{
    /* This is unneeded, but it helps in making unit tests predictable */
    m_fragmentIncrement = 1;
}

void PhotoModelPrivate::queueUpdate()
{
    if (!m_updateQueued) {
        QMetaObject::invokeMethod(this, "update", Qt::QueuedConnection);
        m_updateQueued = true;
    }
}

int PhotoModelPrivate::indexOfItem(PhotoId photoId) const
{
    for (int i = 0; i < m_photos.count(); i++) {
        if (m_photos[i].id() == photoId) return i;
    }

    return -1;
}

void PhotoModelPrivate::update()
{
    Q_Q(PhotoModel);

    m_updateQueued = false;

    SearchFilters filters;
    filters.time0 = m_time0;
    filters.time1 = m_time1;
    filters.roll0 = m_roll0;
    filters.roll1 = m_roll1;
    filters.rating0 = m_rating0;
    filters.rating1 = m_rating1;
    filters.location0 = m_location0;
    filters.location1 = m_location1;
    filters.nonGeoTagged = m_nonGeoTagged;
    filters.requiredTags = m_requiredTags;
    filters.forbiddenTags = m_forbiddenTags;
    filters.areaTagged = m_areaTagged;
    filters.excludedPhotos = m_excludedPhotos;
    filters.skipNonDefaultVersions = m_skipNonDefaultVersions;

    q->beginResetModel();
    if (!m_photoIds.isEmpty()) {
        m_photos = m_db->photos(m_photoIds);
    } else if (m_photoVersions) {
        m_photos = m_db->photoVersions(m_photoVersions);
    } else {
        m_photos = m_db->findPhotos(filters, m_sortOrder);
    }
    q->endResetModel();
}

void PhotoModelPrivate::onPhotoAdded(PhotoId id)
{
    Q_UNUSED(id);
    queueUpdate();
}

void PhotoModelPrivate::onPhotoDeleted(PhotoId id)
{
    Q_UNUSED(id);
    queueUpdate();
}

void PhotoModelPrivate::onPhotoChanged(PhotoId id)
{
    Q_Q(PhotoModel);

    /* The photo could have been changed in such a way that it could be
     * a new fit for the model, or (if it currently is in the model) that it
     * should be removed from it.
     * We could:
     * A) Always queue a full update
     * B) Have a method to check if the photo fits the model, and act
     *    accordingly (but if it's a new valid photo, we don't want to mess
     *    with sorting, so we should update the whole model)
     *
     * For simplicty, we currently go for A.
     * We might also want to have a faster way to find out if a given photo ID
     * is in the model, because a linear scan doesn't scale.
     */
    int i = indexOfItem(id);
    if (i < 0) {
        queueUpdate();
        return;
    }

    const Database::Photo &p = m_photos[i] = m_db->photo(id);
    QModelIndex idx = q->index(i, 0);
    q->dataChanged(idx, idx);

    if (p.isNonDefaultVersion() && m_skipNonDefaultVersions) {
        q->beginRemoveRows(QModelIndex(), i, i);
        m_photos.removeAt(i);
        q->endRemoveRows();
    }
}

PhotoModel::PhotoModel(QObject *parent):
    QAbstractListModel(parent),
    d_ptr(new PhotoModelPrivate(this))
{
    QObject::connect(this, SIGNAL(modelReset()),
                     this, SIGNAL(countChanged()));
    QObject::connect(this, SIGNAL(rowsRemoved(const QModelIndex&,int,int)),
                     this, SIGNAL(countChanged()));
    QObject::connect(this, SIGNAL(rowsInserted(const QModelIndex&,int,int)),
                     this, SIGNAL(countChanged()));
}

PhotoModel::~PhotoModel()
{
    delete d_ptr;
}

void PhotoModel::setTime0(const QDateTime &time)
{
    Q_D(PhotoModel);
    d->m_time0 = time;
    d->queueUpdate();
    Q_EMIT time0Changed();
}

QDateTime PhotoModel::time0() const
{
    Q_D(const PhotoModel);
    return d->m_time0;
}

void PhotoModel::setTime1(const QDateTime &time)
{
    Q_D(PhotoModel);
    d->m_time1 = time;
    d->queueUpdate();
    Q_EMIT time1Changed();
}

QDateTime PhotoModel::time1() const
{
    Q_D(const PhotoModel);
    return d->m_time1;
}

void PhotoModel::setRoll0(int roll)
{
    Q_D(PhotoModel);
    d->m_roll0 = roll;
    d->queueUpdate();
    Q_EMIT roll0Changed();
}

int PhotoModel::roll0() const
{
    Q_D(const PhotoModel);
    return d->m_roll0;
}

void PhotoModel::setRoll1(int roll)
{
    Q_D(PhotoModel);
    d->m_roll1 = roll;
    d->queueUpdate();
    Q_EMIT roll1Changed();
}

int PhotoModel::roll1() const
{
    Q_D(const PhotoModel);
    return d->m_roll1;
}

void PhotoModel::setRating0(int rating)
{
    Q_D(PhotoModel);
    d->m_rating0 = rating;
    d->queueUpdate();
    Q_EMIT rating0Changed();
}

int PhotoModel::rating0() const
{
    Q_D(const PhotoModel);
    return d->m_rating0;
}

void PhotoModel::setRating1(int rating)
{
    Q_D(PhotoModel);
    d->m_rating1 = rating;
    d->queueUpdate();
    Q_EMIT rating1Changed();
}

int PhotoModel::rating1() const
{
    Q_D(const PhotoModel);
    return d->m_rating1;
}

void PhotoModel::setLocation0(const GeoPoint &location)
{
    Q_D(PhotoModel);
    d->m_location0 = location;
    d->queueUpdate();
    Q_EMIT location0Changed();
}

GeoPoint PhotoModel::location0() const
{
    Q_D(const PhotoModel);
    return d->m_location0;
}

void PhotoModel::setLocation1(const GeoPoint &location)
{
    Q_D(PhotoModel);
    d->m_location1 = location;
    d->queueUpdate();
    Q_EMIT location1Changed();
}

GeoPoint PhotoModel::location1() const
{
    Q_D(const PhotoModel);
    return d->m_location1;
}

void PhotoModel::setNonGeoTagged(bool nonGeoTagged)
{
    Q_D(PhotoModel);
    if (d->m_nonGeoTagged == nonGeoTagged) return;
    d->m_nonGeoTagged = nonGeoTagged;
    d->queueUpdate();
    Q_EMIT nonGeoTaggedChanged();
}

bool PhotoModel::nonGeoTagged() const
{
    Q_D(const PhotoModel);
    return d->m_nonGeoTagged;
}

void PhotoModel::setRequiredTags(const QVariant &tags)
{
    Q_D(PhotoModel);
    d->m_requiredTagsVariant = tags;
    d->m_requiredTags = parseList<QList<Tag>>(tags);
    d->queueUpdate();
    Q_EMIT requiredTagsChanged();
}

QVariant PhotoModel::requiredTags() const
{
    Q_D(const PhotoModel);
    return d->m_requiredTagsVariant;
}

void PhotoModel::setForbiddenTags(const QVariant &tags)
{
    Q_D(PhotoModel);
    d->m_forbiddenTagsVariant = tags;
    d->m_forbiddenTags = parseList<Tag>(tags);
    d->queueUpdate();
    Q_EMIT forbiddenTagsChanged();
}

QVariant PhotoModel::forbiddenTags() const
{
    Q_D(const PhotoModel);
    return d->m_forbiddenTagsVariant;
}

void PhotoModel::setAreaTagged(bool areaTagged)
{
    Q_D(PhotoModel);
    if (d->m_areaTagged == areaTagged) return;
    d->m_areaTagged = areaTagged;
    d->queueUpdate();
    Q_EMIT areaTaggedChanged();
}

bool PhotoModel::areaTagged() const
{
    Q_D(const PhotoModel);
    return d->m_areaTagged;
}

void PhotoModel::setExcludedPhotos(const QVariant &photos)
{
    Q_D(PhotoModel);
    d->m_excludedPhotosVariant = photos;
    d->m_excludedPhotos = parseList<PhotoId>(photos);
    d->queueUpdate();
    Q_EMIT excludedPhotosChanged();
}

QVariant PhotoModel::excludedPhotos() const
{
    Q_D(const PhotoModel);
    return d->m_excludedPhotosVariant;
}

void PhotoModel::setSkipNonDefaultVersions(bool skip)
{
    Q_D(PhotoModel);
    if (d->m_skipNonDefaultVersions == skip) return;
    d->m_skipNonDefaultVersions = skip;
    d->queueUpdate();
    Q_EMIT skipNonDefaultVersionsChanged();
}

bool PhotoModel::skipNonDefaultVersions() const
{
    Q_D(const PhotoModel);
    return d->m_skipNonDefaultVersions;
}

void PhotoModel::setPhotoIds(const QList<PhotoId> &photoIds)
{
    Q_D(PhotoModel);
    d->m_photoIds = photoIds;
    d->queueUpdate();
    Q_EMIT photoIdsChanged();
}

QList<PhotoId> PhotoModel::photoIds() const
{
    Q_D(const PhotoModel);
    return d->m_photoIds;
}

void PhotoModel::setPhotoVersions(PhotoId photoVersions)
{
    Q_D(PhotoModel);
    d->m_photoVersions = photoVersions;
    d->queueUpdate();
    Q_EMIT photoVersionsChanged();
}

PhotoId PhotoModel::photoVersions() const
{
    Q_D(const PhotoModel);
    return d->m_photoVersions;
}

void PhotoModel::setSortOrder(Qt::SortOrder order)
{
    Q_D(PhotoModel);
    d->m_sortOrder = order;
    d->queueUpdate();
    Q_EMIT sortOrderChanged();
}

Qt::SortOrder PhotoModel::sortOrder() const
{
    Q_D(const PhotoModel);
    return d->m_sortOrder;
}
QList<Database::Photo> PhotoModel::photos() const
{
    Q_D(const PhotoModel);
    return d->m_photos;
}

int PhotoModel::firstIndex(TimeOrder order, const QDateTime &time) const
{
    Q_D(const PhotoModel);

    const auto &photos = d->m_photos;
    auto olderThanCmp = [](const Database::Photo &a, const QDateTime &t) {
        return a.time() > t;
    };

    auto i =
        std::lower_bound(photos.begin(), photos.end(), time,
                         olderThanCmp);
    if (order == NewerThan) {
        if (i == photos.end() && !photos.isEmpty()) {
            i--;
        } else {
            while (i != photos.begin() && i->time() < time) i--;
            if (i == photos.begin() && i->time() < time) i = photos.end();
        }
    }
    return i != photos.end() ? (i - photos.begin()) : -1;
}

int PhotoModel::indexOfItem(PhotoId photoId) const
{
    Q_D(const PhotoModel);
    return d->indexOfItem(photoId);
}

void PhotoModel::deletePhotos(const QVariant &indexes)
{
    Q_D(PhotoModel);

    QList<PhotoId> ids;
    Q_FOREACH(int row, parseList<int>(indexes)) {
        if (Q_UNLIKELY(row < 0 || row >= d->m_photos.count())) return;
        ids.append(d->m_photos[row].id());
    }

    d->m_db->deletePhotos(ids);
}

void PhotoModel::forceItemRefresh(int row)
{
    Q_D(PhotoModel);
    d->m_updatingThumbnail = true;
    QModelIndex idx = index(row, 0);
    dataChanged(idx, idx);
    d->m_updatingThumbnail = false;
}

void PhotoModel::classBegin()
{
    Q_D(PhotoModel);
    d->m_updateQueued = true;
}

void PhotoModel::componentComplete()
{
    Q_D(PhotoModel);
    d->update();
}

QVariant PhotoModel::get(int row, const QString &roleName) const
{
    int role = roleNames().key(roleName.toLatin1(), -1);
    return data(index(row, 0), role);
}

int PhotoModel::rowCount(const QModelIndex &parent) const
{
    Q_D(const PhotoModel);
    Q_UNUSED(parent);
    return d->m_photos.count();
}

QVariant PhotoModel::data(const QModelIndex &index, int role) const
{
    Q_D(const PhotoModel);

    int row = index.row();
    if (row < 0 || row >= d->m_photos.count()) return QVariant();

    const Database::Photo &p = d->m_photos[row];
    switch (role) {
    case PhotoIdRole:
        return p.id();
    case UrlRole:
        {
            QUrl url = p.url();
            if (d->m_updatingThumbnail) {
                /* make sure that the fragment is always different: if
                 * forceItemRefresh() is called twice on the same item, we
                 * don't want to return the same URL. */
                url.setFragment(QString::number(d->m_fragmentIncrement++));
            }
            return url;
        }
    case FileNameRole:
        return p.fileName();
    case TitleRole:
        return p.description();
    case DescriptionRole:
        {
            Metadata::ImportData data;
            d->m_metadata.readImportData(p.url().toLocalFile(), data);
            return data.description;
        }
    case TimeRole:
        return p.time();
    case RollIdRole:
        return p.rollId();
    case RatingRole:
        return qMin(p.rating(), 5);
    case LocationRole:
        return QVariant::fromValue(p.location());
    case HasVersionsRole:
        return p.isDefaultVersion() || p.isNonDefaultVersion();
    case IsDefaultVersionRole:
        return p.isDefaultVersion();
    case MasterVersionRole:
        return d->m_db->masterVersion(p.id());
    case SizeRole:
        {
            QImageReader reader(p.url().toLocalFile());
            return reader.size();
        }
    case MimeTypeRole:
        return QVariant::fromValue(d->m_mimeDb.mimeTypeForFile(
            p.url().toLocalFile()));
    default:
        qWarning() << "Unknown role ID:" << role;
        return QVariant();
    }
}

QHash<int, QByteArray> PhotoModel::roleNames() const
{
    Q_D(const PhotoModel);
    return d->m_roles;
}

#include "photo_model.moc"
