/*
 * Copyright (C) 2016-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGINARIO_SHOTWELL_IMPORTER_H
#define IMAGINARIO_SHOTWELL_IMPORTER_H

#include "types.h"

#include <QObject>

namespace Imaginario {

class ShotwellImporterPrivate;
class ShotwellImporter: public QObject
{
    Q_OBJECT
    Q_ENUMS(Status)
    Q_PROPERTY(int dbVersion READ dbVersion CONSTANT)
    Q_PROPERTY(QString version READ version CONSTANT)
    Q_PROPERTY(Status status READ status NOTIFY statusChanged)
    Q_PROPERTY(int count READ count CONSTANT)
    Q_PROPERTY(double progress READ progress NOTIFY progressChanged)

public:
    enum Status {
        Initializing = 0,
        Ready,
        ImportingTags,
        ImportingPhotos,
        Done,
        Failed,
    };

    ShotwellImporter(QObject *parent = 0);
    ~ShotwellImporter();

    int dbVersion() const;
    QString version() const;

    Status status() const;

    int count() const;

    double progress() const;

    Q_INVOKABLE void exec();

Q_SIGNALS:
    void statusChanged(ShotwellImporter::Status status);
    void progressChanged();

private:
    ShotwellImporterPrivate *d_ptr;
    Q_DECLARE_PRIVATE(ShotwellImporter)
};

} // namespace

Q_DECLARE_METATYPE(Imaginario::ShotwellImporter::Status)

#endif // IMAGINARIO_SHOTWELL_IMPORTER_H
