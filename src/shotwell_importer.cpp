/*
 * Copyright (C) 2016-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "database.h"
#include "metadata.h"
#include "shotwell_importer.h"

#include <QDebug>
#include <QDir>
#include <QFileInfo>
#include <QMetaObject>
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlQuery>
#include <QStandardPaths>
#include <QtConcurrent>

using namespace Imaginario;

namespace Imaginario {

class ShotwellImporterPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(ShotwellImporter)

public:
    ShotwellImporterPrivate(ShotwellImporter *q);
    ~ShotwellImporterPrivate();

    void setStatus(ShotwellImporter::Status status);
    inline void setProgress(double progress);
    bool checkDatabase();

    bool importTags();
    bool importRolls();
    bool importPhotos();
    void exec();

private:
    QSqlDatabase m_db;
    ShotwellImporter::Status m_status;
    int m_count;
    double m_progress;
    int m_dbVersion;
    QString m_version;
    QHash<TagId,QList<int>> m_tagPhotos;
    QFuture<void> m_execFuture;
    mutable ShotwellImporter *q_ptr;
};

} // namespace

ShotwellImporterPrivate::ShotwellImporterPrivate(ShotwellImporter *q):
    m_db(QSqlDatabase::addDatabase("QSQLITE", "shotwell")),
    m_status(ShotwellImporter::Initializing),
    m_count(-1),
    m_progress(0.0),
    q_ptr(q)
{
    qRegisterMetaType<ShotwellImporter::Status>("ShotwellImporter::Status");
    if (checkDatabase()) {
        setStatus(ShotwellImporter::Ready);
    } else {
        setStatus(ShotwellImporter::Failed);
    }
}

ShotwellImporterPrivate::~ShotwellImporterPrivate()
{
    QString name = m_db.connectionName();
    m_db = QSqlDatabase();
    QSqlDatabase::removeDatabase(name);
}

void ShotwellImporterPrivate::setStatus(ShotwellImporter::Status status)
{
    Q_Q(ShotwellImporter);

    if (m_status == status) return;
    m_status = status;
    Q_EMIT q->statusChanged(status);
}

void ShotwellImporterPrivate::setProgress(double progress)
{
    Q_Q(ShotwellImporter);
    m_progress = progress;
    Q_EMIT q->progressChanged();
}

bool ShotwellImporterPrivate::checkDatabase()
{
    QString dbName =
        QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation) +
        "/shotwell/data/photo.db";
    m_db.setDatabaseName(dbName);
    if (!m_db.open()) return false;

    QSqlQuery q(m_db);
    q.exec("SELECT schema_version, app_version FROM VersionTable "
           "ORDER BY schema_version DESC LIMIT 1");
    if (Q_UNLIKELY(!q.next())) return false;
    m_dbVersion = q.value(0).toInt();
    m_version = q.value(1).toString();
    if (m_dbVersion < 17) return false;

    q.exec("SELECT COUNT(*) FROM PhotoTable");
    if (Q_UNLIKELY(!q.next())) return false;
    m_count = q.value(0).toInt();

    m_db.close();
    return true;
}

bool ShotwellImporterPrivate::importTags()
{
    setStatus(ShotwellImporter::ImportingTags);

    QSqlQuery q(m_db);
    q.setForwardOnly(true);
    bool ok = q.exec("SELECT name, photo_id_list FROM TagTable "
                     "ORDER BY name");
    if (Q_UNLIKELY(!ok)) return false;

    Database *db = Database::instance();

    while (q.next()) {
        QString name = q.value(0).toString();
        QString photoIdList = q.value(1).toString();

        QStringList names = name.split('/', QString::SkipEmptyParts);
        QStringList photoIdsText =
            photoIdList.split(',', QString::SkipEmptyParts);

        Tag parentTag = db->uncategorizedTag();
        Tag tag;
        for (const QString &tagName: names) {
            tag = db->tag(name);
            if (!tag.isValid()) {
                tag = db->createTag(parentTag, tagName, QString());
            }
            parentTag = tag;
        }

        if (Q_UNLIKELY(!tag.isValid())) continue;

        QList<int> photoIds;
        for (const QString &photoIdText: photoIdsText) {
            if (!photoIdText.startsWith("thumb")) continue;
            bool ok;
            int id = photoIdText.mid(5).toInt(&ok);
            if (Q_UNLIKELY(!ok)) continue;

            photoIds.append(id);
        }

        m_tagPhotos.insert(tag.id(), photoIds);
    }
    return true;
}

bool ShotwellImporterPrivate::importPhotos()
{
    setStatus(ShotwellImporter::ImportingPhotos);

    QSqlQuery q(m_db);
    q.setForwardOnly(true);
    bool ok = q.exec("SELECT id, filename, import_id, exposure_time, rating,"
                     " title "
                     "FROM PhotoTable ORDER BY import_id");
    if (Q_UNLIKELY(!ok)) return false;

    Database *db = Database::instance();
    Metadata metadata;

    uint lastImportId = 0;
    RollId rollId = -1;

    int importedPhotos = 0;
    while (q.next()) {
        importedPhotos++;
        if (importedPhotos % 10 == 0) {
            setProgress(double(importedPhotos) / m_count);
        }

        int i = 0;
        Database::Photo p;
        int id = q.value(i++).toInt();

        QFileInfo fileInfo(q.value(i++).toString());
        if (fileInfo.isAbsolute()) {
            // it's a local file
            p.setBaseUri(QString("file://") + fileInfo.path());
        } else {
            // let's hope this works somehow
            qWarning() << "Shotwell importer: relative file name!";
            p.setBaseUri(fileInfo.absolutePath());
        }
        p.setFileName(fileInfo.fileName());

        uint importId = q.value(i++).toUInt();

        QVariant timeV = q.value(i++);
        if (timeV.toInt() != -1) {
            p.setTime(QDateTime::fromTime_t(timeV.toUInt()));
        }

        p.setRating(q.value(i++).toInt());
        p.setDescription(q.value(i++).toString());

        Metadata::ImportData data;
        if (metadata.readImportData(p.filePath(), data)) {
            p.setLocation(data.location);
        }

        // Roll ID
        if (importId != lastImportId || rollId < 0) {
            QDateTime importTime = QDateTime::fromTime_t(importId);
            rollId = db->createRoll(importTime);
            if (Q_UNLIKELY(rollId < 0)) return false;
            lastImportId = importId;
        }

        // Add the photo
        PhotoId photoId = db->addPhoto(p, rollId);
        if (Q_UNLIKELY(photoId < 0)) return false;

        // Match tags
        QList<Tag> tags;
        for (auto i = m_tagPhotos.constBegin();
             i != m_tagPhotos.constEnd();
             i++) {
            if (i.value().contains(id)) {
                tags.append(Tag(i.key()));
            }
        }

        ok = db->setTags(photoId, tags);
        if (Q_UNLIKELY(!ok)) return false;
    }

    return true;
}

void ShotwellImporterPrivate::exec()
{
    if (!m_db.open()) return;

    m_tagPhotos.clear();

    Database *db = Database::instance();
    db->transaction();

    bool ok = importTags();
    if (Q_UNLIKELY(!ok)) {
        setStatus(ShotwellImporter::Failed);
        return;
    }

    ok = importPhotos();
    if (Q_UNLIKELY(!ok)) {
        setStatus(ShotwellImporter::Failed);
        return;
    }

    m_db.close();

    db->commit();
    setStatus(ShotwellImporter::Done);
}

ShotwellImporter::ShotwellImporter(QObject *parent):
    QObject(parent),
    d_ptr(new ShotwellImporterPrivate(this))
{
}

ShotwellImporter::~ShotwellImporter()
{
    delete d_ptr;
}

int ShotwellImporter::dbVersion() const
{
    Q_D(const ShotwellImporter);
    return d->m_dbVersion;
}

QString ShotwellImporter::version() const
{
    Q_D(const ShotwellImporter);
    return d->m_version;
}

ShotwellImporter::Status ShotwellImporter::status() const
{
    Q_D(const ShotwellImporter);
    return d->m_status;
}

double ShotwellImporter::progress() const
{
    Q_D(const ShotwellImporter);
    return d->m_progress;
}

void ShotwellImporter::exec()
{
    Q_D(ShotwellImporter);
    d->m_execFuture = QtConcurrent::run(d, &ShotwellImporterPrivate::exec);
}

int ShotwellImporter::count() const
{
    Q_D(const ShotwellImporter);
    return d->m_count;
}

#include "shotwell_importer.moc"
