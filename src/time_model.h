/*
 * Copyright (C) 2016-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGINARIO_TIME_MODEL_H
#define IMAGINARIO_TIME_MODEL_H

#include "types.h"

#include <QAbstractListModel>
#include <QDate>
#include <QVariant>

namespace Imaginario {

class TimeModelPrivate;
class TimeModel: public QAbstractListModel
{
    Q_OBJECT
    Q_ENUMS(Roles)
    Q_PROPERTY(int count READ rowCount NOTIFY countChanged)
    Q_PROPERTY(QVariant photoModel READ photoModel WRITE setPhotoModel \
               NOTIFY photoModelChanged)
    Q_PROPERTY(QDate date0 READ date0 NOTIFY rangeChanged)
    Q_PROPERTY(QDate date1 READ date1 NOTIFY rangeChanged)

public:
    enum Roles {
        TimeRole = Qt::UserRole + 1,
        CountRole,
        NormalizedCountRole,
    };

    TimeModel(QObject *parent = 0);
    ~TimeModel();

    void setPhotoModel(const QVariant &model);
    QVariant photoModel() const;

    QDate date0() const;

    QDate date1() const;

    Q_INVOKABLE QVariant get(int row, const QString &role) const;

    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index,
                  int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

Q_SIGNALS:
    void countChanged();
    void photoModelChanged();
    void rangeChanged();

private:
    TimeModelPrivate *d_ptr;
    Q_DECLARE_PRIVATE(TimeModel)
};

} // namespace

#endif // IMAGINARIO_TIME_MODEL_H
