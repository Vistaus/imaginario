/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "image_provider.h"

#include "icon_thumbnailer.h"
#include "job_executor.h"
#include "metadata.h"
#include "thumbnailer.h"
#include "utils.h"

#include <QDebug>
#include <QDir>
#include <QImageReader>
#include <QStandardPaths>

using namespace Imaginario;

namespace Imaginario {

class ImageProviderPrivate
{
private:
    friend class ImageProvider;
    Metadata m_metadata;
    Thumbnailer m_thumbnailer;
    IconThumbnailer m_iconThumbnailer;
    QDir m_tagIconDir;
};

} // namespace

ImageProvider::ImageProvider():
    QQuickImageProvider(QQmlImageProviderBase::Image),
    d_ptr(new ImageProviderPrivate)
{
    Q_D(ImageProvider);

    QString base =
        QStandardPaths::writableLocation(QStandardPaths::DataLocation);
    d->m_tagIconDir = base + QStringLiteral("/tag_icons");
}

ImageProvider::~ImageProvider()
{
    delete d_ptr;
}

static void crop(QImage &image, const QRectF &area)
{
    if (area.isValid()) {
        image = image.copy(Utils::scaleArea(area, image.size()));
    }
}

QImage ImageProvider::requestImage(const QString &id,
                                   QSize *size,
                                   const QSize &requestedSize)
{
    Q_D(ImageProvider);
    QUrl uri(id);
    QRectF desiredArea;

    QSize requestedLoadSize(requestedSize);
    if (id.startsWith("tag_icon:")) {
        uri = QUrl::fromLocalFile(d->m_tagIconDir.filePath(id.mid(9)));
    } else {
        desiredArea = Utils::areaFromUrl(uri);
        if (desiredArea.isValid()) {
            float scale = qMin(desiredArea.width(), desiredArea.height());
            requestedLoadSize /= scale;
        }
    }

    if (requestedSize.width() < 48 &&
        requestedSize.width() == requestedSize.height()) {
        QImage iconThumbnail = d->m_iconThumbnailer.load(uri);
        if (!iconThumbnail.isNull()) {
            if (size) {
                *size = iconThumbnail.size();
            }
            return iconThumbnail;
        }
    }

    QImage thumbnail = d->m_thumbnailer.load(uri, requestedLoadSize);
    if (!thumbnail.isNull()) {
        crop(thumbnail, desiredArea);
        /* FIXME: according to the QQuickImageProvider
         * documentation, we should set "size" to the size of the
         * original image.
         */
        return thumbnail;
    }

    QString filePath(uri.toLocalFile());
    QImage preview = d->m_metadata.loadPreview(filePath, size,
                                               requestedLoadSize);
    if (!preview.isNull()) {
        crop(preview, desiredArea);
        return preview;
    }

    /* Load the actual image */
    QImageReader reader(filePath);
#if (QT_VERSION >= QT_VERSION_CHECK(5, 5, 0))
    reader.setAutoTransform(true);
#endif
    QSize imageSize = reader.size();
    /* scale when loading, if possible */
    if (imageSize.isValid() && requestedLoadSize.isValid() &&
        (imageSize.width() > requestedLoadSize.width() ||
         imageSize.height() > requestedLoadSize.height())) {
        QSize scaledSize = imageSize.scaled(requestedLoadSize,
                                            Qt::KeepAspectRatio);
        reader.setScaledSize(scaledSize);

        if (requestedLoadSize.width() <= 512 &&
            requestedLoadSize.height() <= 512) {
            ThumbnailJob job(uri);
            JobExecutor::instance()->addJob(job);
        }
    }

    *size = imageSize;
    QImage image = reader.read();
    crop(image, desiredArea);
    return image;
}
