/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGINARIO_IMPORTER_H
#define IMAGINARIO_IMPORTER_H

#include "types.h"

#include <QAbstractListModel>
#include <QList>
#include <QStringList>
#include <QUrl>

namespace Imaginario {

class ImporterPrivate;
class Importer: public QAbstractListModel
{
    Q_OBJECT
    Q_ENUMS(ItemStatus DuplicateAction RawPolicy Roles)
    Q_FLAGS(ClearStatuses)
    Q_PROPERTY(int count READ rowCount NOTIFY countChanged)
    Q_PROPERTY(bool running READ isRunning NOTIFY isRunningChanged)
    Q_PROPERTY(bool writeXmp READ writeXmp WRITE setWriteXmp \
               NOTIFY writeXmpChanged)
    Q_PROPERTY(bool embed READ embed WRITE setEmbed NOTIFY embedChanged)
    Q_PROPERTY(bool copyFiles READ copyFiles WRITE setCopyFiles \
               NOTIFY copyFilesChanged)
    Q_PROPERTY(QString destination READ destination WRITE setDestination \
               NOTIFY destinationChanged)
    Q_PROPERTY(Tag parentTag READ parentTag WRITE setParentTag \
               NOTIFY parentTagChanged)
    Q_PROPERTY(QStringList commonTags READ commonTags WRITE setCommonTags \
               NOTIFY commonTagsChanged)
    Q_PROPERTY(int importedCount READ importedCount \
               NOTIFY importedCountChanged)
    Q_PROPERTY(int failedCount READ failedCount \
               NOTIFY failedCountChanged)
    Q_PROPERTY(int ignoredCount READ ignoredCount \
               NOTIFY ignoredCountChanged)
    Q_PROPERTY(int unsupportedCount READ unsupportedCount \
               NOTIFY unsupportedCountChanged)
    Q_PROPERTY(ClearStatuses autoClear READ autoClear WRITE setAutoClear \
               NOTIFY autoClearChanged)
    Q_PROPERTY(RawPolicy rawPolicy READ rawPolicy WRITE setRawPolicy \
               NOTIFY rawPolicyChanged)

public:
    enum ItemStatus {
        Waiting = 0,
        Processing,
        DuplicateCheck,
        Done,
        Failed,
        Ignored,
    };

    enum DuplicateAction {
        DuplicateCheckNeeded = 0,
        ImportAsNew,
        ImportReplacing,
        ImportAsVersion,
    };

    enum ClearStatus {
        ClearDone = 1 << 0,
        ClearIgnored = 1 << 1,
        ClearFailed = 1 << 2,
    };
    Q_DECLARE_FLAGS(ClearStatuses, ClearStatus)

    enum RawPolicy {
        EditedIsMaster = 0,
        RawIsMaster,
    };

    enum Roles {
        UrlRole = Qt::UserRole + 1,
        OwnTags,
        AddedTags,
        CurrentTags,
        StatusRole,
        DuplicateListRole,
        DuplicateActionRole,
        IsPairRole,
    };

    Importer(QObject *parent = 0);
    ~Importer();

    bool isRunning() const;

    void setEmbed(bool embed);
    bool embed() const;

    void setWriteXmp(bool writeXmp);
    bool writeXmp() const;

    void setCopyFiles(bool copyFiles);
    bool copyFiles() const;

    void setDestination(const QString &destination);
    QString destination() const;

    void setParentTag(Tag parent);
    Tag parentTag() const;

    void setCommonTags(const QStringList &tags);
    QStringList commonTags() const;

    int importedCount() const;
    int failedCount() const;
    int ignoredCount() const;
    int unsupportedCount() const;

    void setAutoClear(ClearStatuses statuses);
    ClearStatuses autoClear() const;

    void setRawPolicy(RawPolicy policy);
    RawPolicy rawPolicy() const;

    Q_INVOKABLE void exec();

    Q_INVOKABLE void clear();
    Q_INVOKABLE void addItems(const QList<QUrl> &urls,
                              const QStringList &tags = QStringList(),
                              bool importTags = false);
    Q_INVOKABLE void setItemTags(int row, const QStringList &tags);
    Q_INVOKABLE void ignoreItem(int row);
    Q_INVOKABLE void importAsNotDuplicate(int row);
    Q_INVOKABLE void importAsVersion(int row, int photoId);
    Q_INVOKABLE void importReplacing(int row, int photoId);

    Q_INVOKABLE QVariant get(int row, const QString &role) const;

    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index,
                  int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

Q_SIGNALS:
    void isRunningChanged();
    void countChanged();
    void embedChanged();
    void writeXmpChanged();
    void copyFilesChanged();
    void destinationChanged();
    void parentTagChanged();
    void commonTagsChanged();
    void importedCountChanged();
    void failedCountChanged();
    void ignoredCountChanged();
    void unsupportedCountChanged();
    void autoClearChanged();
    void rawPolicyChanged();
    void finished(int rollId);

private:
    ImporterPrivate *d_ptr;
    Q_DECLARE_PRIVATE(Importer)
};

} // namespace

Q_DECLARE_METATYPE(Imaginario::Importer::RawPolicy)
Q_DECLARE_METATYPE(Imaginario::Importer::ClearStatuses)
Q_DECLARE_OPERATORS_FOR_FLAGS(Imaginario::Importer::ClearStatuses)

#endif // IMAGINARIO_IMPORTER_H
