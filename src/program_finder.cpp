/*
 * Copyright (C) 2018-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "program_finder.h"

#include <QDebug>
#include <QDir>
#include <QIODevice>
#include <QJsonArray>
#include <QJsonObject>
#include <QLocale>
#include <QSettings>
#include <QStandardPaths>

#ifdef Q_OS_MACOS
#include "macos.h"
#endif

#if defined(Q_OS_UNIX) && !defined(Q_OS_MACOS)
#define SUPPORTS_DESKTOP_FILES
#endif

using namespace Imaginario;

namespace Imaginario {

class ProgramFinderPrivate
{
public:
    ProgramFinderPrivate();
    ~ProgramFinderPrivate();

#ifdef SUPPORTS_DESKTOP_FILES
    static bool readDesktopFile(QIODevice &device, QSettings::SettingsMap &map);
    static bool writeDesktopFile(QIODevice &device, const QSettings::SettingsMap &map);
    void scanDesktopFiles();
    QVariant localizedValue(const QSettings &settings, const QString &key) const;
#elif defined(Q_OS_MACOS)
    void scanMacOSApps();
    QString findMacOSApp(const QString &programName) const;
#endif

    QJsonObject find(const QString &programName) const;

private:
    friend class ProgramFinder;
    mutable QHash<QString, QJsonObject> m_cache;
    QHash<QString, QJsonObject> m_appInfoByCommand;
};

} // namespace

ProgramFinderPrivate::ProgramFinderPrivate()
{
#ifdef SUPPORTS_DESKTOP_FILES
    scanDesktopFiles();
#elif defined(Q_OS_MACOS)
    scanMacOSApps();
#endif
}

ProgramFinderPrivate::~ProgramFinderPrivate()
{
}

#ifdef SUPPORTS_DESKTOP_FILES
bool ProgramFinderPrivate::readDesktopFile(QIODevice &device,
                                           QSettings::SettingsMap &map)
{
    QByteArray line;
    QByteArray section;

    while ((line = device.readLine(1024)), !line.isEmpty()) {
        if (line[0] == '\n' || line[0] == '#') continue;
        if (line[0] == '[') {
            int closingBracket = line.indexOf(']');
            if (closingBracket < 0) {
                qWarning() << "Invalid line in desktop file:" << line;
                return false;
            }
            section = line.mid(1, closingBracket - 1);
        }

        /* Optimization: we only care about the "Desktop Entry" section, so:
         * - Ignore all keys not in this section
         * - Omit the section name from the key
         */
        if (section != "Desktop Entry") continue;

        int equal = line.indexOf('=');
        QByteArray key = line.left(equal).trimmed();

        /* Ignore non-standard keys */
        if (key.startsWith("X-")) continue;

        QByteArray value = line.mid(equal + 1).trimmed();
        map[key] = value;
    }
    return true;
}

bool ProgramFinderPrivate::writeDesktopFile(QIODevice &,
                                            const QSettings::SettingsMap &)
{
    qWarning() << Q_FUNC_INFO << "unimplemented";
    return true;
}

void ProgramFinderPrivate::scanDesktopFiles()
{
    const QSettings::Format desktopFormat =
        QSettings::registerFormat("desktop", readDesktopFile, writeDesktopFile);

    QStringList appDirs(QStandardPaths::standardLocations(
        QStandardPaths::ApplicationsLocation));

    for (const QString &appDir: appDirs) {
        QDir dir(appDir);
        QStringList entries = dir.entryList(QStringList{ "*.desktop" });
        for (const QString &fileName: entries) {
            QJsonObject appInfo;
            QString filePath = dir.filePath(fileName);
            QSettings desktopFile(filePath, desktopFormat);
            QStringList mimeTypes = desktopFile.value("MimeType")
                .toString().split(';', QString::SkipEmptyParts);
            /* If we know that the application does not support images, there's
             * no point in remembering it */
            if (!mimeTypes.isEmpty()) {
                bool supportsImages = false;
                QStringList filteredMimeTypes;
                for (const QString &mimeType: mimeTypes) {
                    if (mimeType.startsWith("image/")) {
                        supportsImages = true;
                        filteredMimeTypes.append(mimeType);
                    }
                }
                if (!supportsImages) continue;
                mimeTypes = filteredMimeTypes;
            }
            appInfo.insert("mimeTypes", QJsonArray::fromStringList(mimeTypes));
            QString command = desktopFile.value("Exec").toString();
            QString binaryName = command.split(' ').first().split('/').last();
            appInfo.insert("command", command);
            appInfo.insert("actionName",
                           localizedValue(desktopFile, "Name").toString());
            QString icon = desktopFile.value("Icon").toString();
            if (icon.startsWith('/')) {
                icon.prepend("file://");
            } else if (!icon.isEmpty()) {
                icon.prepend("image://theme/");
            }
            appInfo.insert("icon", icon);
            m_appInfoByCommand.insert(binaryName, appInfo);
            /* The binary name might include a version number; let's try also
             * splitting it out */
            int i = binaryName.indexOf('_');
            if (i > 0) {
                m_appInfoByCommand.insert(binaryName.left(i), appInfo);
            }
        }
    }
}

QVariant ProgramFinderPrivate::localizedValue(const QSettings &settings,
                                              const QString &key) const
{
    QVariant ret;

    QString localeName = QLocale::system().name();
    QString localizedKeyTemplate = QString("%1[%2]").arg(key);

    ret = settings.value(localizedKeyTemplate.arg(localeName));
    if (!ret.isNull()) return ret;

    ret = settings.value(localizedKeyTemplate.arg(localeName.split('_').
                                                  first()));
    if (!ret.isNull()) return ret;

    return settings.value(key);
}

#elif defined(Q_OS_MACOS)

void ProgramFinderPrivate::scanMacOSApps()
{
    QJsonArray apps = MacOS::scanApps();

    for (const QJsonValue &object: apps) {
        const QJsonObject app = object.toObject();
        QString command = app.value("actionName").toString();
        m_appInfoByCommand.insert(command, app);
    }
}

QString ProgramFinderPrivate::findMacOSApp(const QString &programName) const
{
    QStringList appDirs(QStandardPaths::standardLocations(
        QStandardPaths::ApplicationsLocation));

    QString searchedName(programName + ".app");
    for (const QString &appDir: appDirs) {
        QDir dir(appDir);
        if (dir.exists(searchedName)) {
            return dir.filePath(searchedName);
        }
    }
    return QString();
}
#endif

QJsonObject ProgramFinderPrivate::find(const QString &programName) const
{
    QJsonObject ret = m_cache.value(programName);
    if (!ret.isEmpty()) return ret;

    ret = m_appInfoByCommand.value(programName);

#ifdef Q_OS_MACOS
    if (ret.isEmpty()) {
        QString path = findMacOSApp(programName);
        if (!path.isEmpty()) {
            ret.insert("command", path);
        }
    }
#endif
    if (ret.isEmpty()) {
        QString path = QStandardPaths::findExecutable(programName);
        if (!path.isEmpty()) {
            ret.insert("command", path);
        }
    }

    if (!ret.isEmpty()) {
        m_cache.insert(programName, ret);
    }

    return ret;
}

ProgramFinder::ProgramFinder():
    d_ptr(new ProgramFinderPrivate())
{
}

ProgramFinder::~ProgramFinder()
{
}

QJsonObject ProgramFinder::find(const QString &programName) const
{
    Q_D(const ProgramFinder);
    return d->find(programName);
}

QJsonArray ProgramFinder::callableApplications() const
{
    Q_D(const ProgramFinder);

    QJsonArray ret;
    for (const QJsonObject &appInfo: d->m_appInfoByCommand) {
        /* There might be duplicates in the hash table; we should skip them */
        if (ret.contains(appInfo)) {
            continue;
        }
        ret.append(appInfo);
    }
    return ret;
}
