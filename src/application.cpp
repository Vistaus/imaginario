/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "application.h"
#include "i18n.h"
#include "job_executor.h"

#include <QDebug>
#include <QDir>
#include <QIcon>
#include <QThreadPool>

using namespace Imaginario;

namespace Imaginario {

class ApplicationPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(Application)

public:
    ApplicationPrivate(Application *q);

private Q_SLOTS:
    void onStateChanged(Qt::ApplicationState state);

private:
    Application *q_ptr;
};

} // namespace

ApplicationPrivate::ApplicationPrivate(Application *q):
    QObject(),
    q_ptr(q)
{

    QObject::connect(q_ptr,
                     SIGNAL(applicationStateChanged(Qt::ApplicationState)),
                     this, SLOT(onStateChanged(Qt::ApplicationState)));

}

void ApplicationPrivate::onStateChanged(Qt::ApplicationState state)
{
    JobExecutor *executor = JobExecutor::instance();
    /* Workaround for https://bugs.launchpad.net/bugs/1456706
     * TODO remove once above bug is fixed */
    if (QGuiApplication::platformName().startsWith("ubuntu") &&
        state == Qt::ApplicationInactive) {
        state = Qt::ApplicationSuspended;
    }

    if (state == Qt::ApplicationSuspended) {
        executor->stop();
    } else if (state == Qt::ApplicationActive) {
        executor->start();
    }
}

static inline int countDots(const char *s)
{
    int count = 0;
    for (int i = 0; s[i] != 0; i++) {
        if (s[i] == '.') count++;
    }
    return count;
}

Application::Application(int &argc, char **argv):
#ifdef DESKTOP_BUILD
    QApplication(argc, argv),
#else
    QGuiApplication(argc, argv),
#endif
    d_ptr(new ApplicationPrivate(this))
{
    if (countDots(APPLICATION_VERSION) > 1) {
        setApplicationDisplayName(tr("Imaginario (dev)"));
    } else {
        setApplicationDisplayName(tr("Imaginario"));
    }
    setApplicationName(APPLICATION_NAME);
    setApplicationVersion(APPLICATION_VERSION);
    setOrganizationName(QString());
    setOrganizationDomain(APPLICATION_NAME);
    setWindowIcon(QIcon(":/icons/imaginario"));

    installTranslator(new GettextTranslator(this));

    if (QIcon::themeName().isEmpty()) {
        QIcon::setThemeName("Desktop");
    }
}

Application::~Application()
{
    QThreadPool::globalInstance()->waitForDone(5000);
    delete d_ptr;
}

QString Application::dataPath() const
{
    QDir execDir(applicationDirPath());
    return QDir::cleanPath(execDir.filePath(RELATIVE_DATA_DIR)) + "/";
}

#include "application.moc"
