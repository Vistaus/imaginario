/*
 * Copyright (C) 2018-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGINARIO_PROGRAM_FINDER_H
#define IMAGINARIO_PROGRAM_FINDER_H

#include <QJsonArray>
#include <QJsonObject>
#include <QScopedPointer>
#include <QString>

namespace Imaginario {

class ProgramFinderPrivate;
class ProgramFinder
{
public:
    ProgramFinder();
    virtual ~ProgramFinder();

    QJsonObject find(const QString &programName) const;
    QJsonArray callableApplications() const;

private:
    QScopedPointer<ProgramFinderPrivate> d_ptr;
    Q_DECLARE_PRIVATE(ProgramFinder)
};

} // namespace

#endif // IMAGINARIO_PROGRAM_FINDER_H
