/*
 * Copyright (C) 2016-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "location_model.h"

#include <QAbstractItemModel>
#include <QDebug>

using namespace Imaginario;

namespace Imaginario {

struct ItemData {
    GeoPoint location;
    int rowId;
};

struct GroupData {
    GeoPoint averageLocation;
    QVector<ItemData> items;
};

class LocationModelPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(LocationModel)

public:
    LocationModelPrivate(LocationModel *q);
    ~LocationModelPrivate();

    void computeRoleIds();
    void computeAverageLocations();
    void fillModel();

private Q_SLOTS:
    void queueUpdate();
    void update();

private:
    QHash<int, QByteArray> m_roles;
    QAbstractItemModel *m_model;
    QStringList m_usedRoles;
    bool m_updateQueued;
    qreal m_zoomLevel;
    int m_nonGeoTaggedCount;
    QHash<int,QString> m_usedRolesIds;
    int m_locationRoleId;
    QHash<int,GroupData> m_groups;
    QList<int> m_usedGroups;
    LocationModel *q_ptr;
};

} // namespace

LocationModelPrivate::LocationModelPrivate(LocationModel *q):
    QObject(q),
    m_model(0),
    m_updateQueued(false),
    m_zoomLevel(-1),
    m_nonGeoTaggedCount(0),
    m_locationRoleId(-1),
    q_ptr(q)
{
    m_roles[LocationModel::LocationRole] = "location";
    m_roles[LocationModel::CountRole] = "count";
}

LocationModelPrivate::~LocationModelPrivate()
{
}

void LocationModelPrivate::queueUpdate()
{
    if (!m_updateQueued) {
        QMetaObject::invokeMethod(this, "update", Qt::QueuedConnection);
        m_updateQueued = true;
    }
}

void LocationModelPrivate::computeRoleIds()
{
    const auto roleMap = m_model->roleNames();
    for (auto i = roleMap.begin(); i != roleMap.end(); i++) {
        QString roleName = QString::fromLatin1(i.value());
        if (m_usedRoles.isEmpty() || m_usedRoles.contains(roleName)) {
            m_usedRolesIds.insert(i.key(), roleName);
        }
        if (i.value() == "location") {
            m_locationRoleId = i.key();
        }
    }
}

void LocationModelPrivate::computeAverageLocations()
{
    for (GroupData &gd: m_groups) {
        qreal sumLat = 0, sumLon = 0;
        for (const ItemData &id: gd.items) {
            sumLat += id.location.lat;
            sumLon += id.location.lon;
        }
        int count = gd.items.count();
        gd.averageLocation.lat = sumLat / count;
        gd.averageLocation.lon = sumLon / count;
    }
}

void LocationModelPrivate::fillModel()
{
    int zoomLevel = m_zoomLevel;
    int areas = (1 << zoomLevel) * 4;

    int rowCount = m_model->rowCount();
    for (int i = 0; i < rowCount; i++) {
        QModelIndex index = m_model->index(i, 0);
        QVariant data = m_model->data(index, m_locationRoleId);
        if (!data.isValid()) continue;

        GeoPoint geo = data.value<GeoPoint>();
        if (!geo.isValid()) { m_nonGeoTaggedCount++; continue; }

        int latUnits = geo.lat * areas / 180.0;
        int lonUnits = geo.lon * areas / 360.0;
        /* protect against overflows */
        latUnits = latUnits % areas;
        lonUnits = lonUnits % areas;

        /* Compute area Id */
        int areaId = lonUnits * areas + latUnits;
        m_groups[areaId].items.append({ geo, i });
    }

    computeAverageLocations();

    m_usedGroups = m_groups.keys();
}

void LocationModelPrivate::update()
{
    Q_Q(LocationModel);

    m_updateQueued = false;

    q->beginResetModel();
    m_usedRolesIds.clear();
    m_groups.clear();
    m_usedGroups.clear();
    m_nonGeoTaggedCount = 0;
    if (m_model) {
        computeRoleIds();
        fillModel();
    }
    q->endResetModel();
}

LocationModel::LocationModel(QObject *parent):
    QAbstractListModel(parent),
    d_ptr(new LocationModelPrivate(this))
{
    QObject::connect(this, SIGNAL(modelReset()),
                     this, SIGNAL(countChanged()));
}

LocationModel::~LocationModel()
{
    delete d_ptr;
}

void LocationModel::setModel(const QVariant &model)
{
    Q_D(LocationModel);

    QAbstractItemModel *m = model.value<QAbstractItemModel*>();
    if (m == d->m_model) return;

    if (d->m_model) {
        d->m_model->disconnect(d);
    }
    d->m_model = m;
    if (d->m_model) {
        QObject::connect(d->m_model, SIGNAL(modelReset()),
                         d, SLOT(queueUpdate()));
        QObject::connect(d->m_model,
                         SIGNAL(rowsRemoved(const QModelIndex&,int,int)),
                         d, SLOT(queueUpdate()));
        QObject::connect(d->m_model,
                         SIGNAL(dataChanged(const QModelIndex&,const QModelIndex&,const QVector<int>&)),
                         d, SLOT(queueUpdate()));
    }

    d->queueUpdate();
    Q_EMIT modelChanged();
}

QVariant LocationModel::model() const
{
    Q_D(const LocationModel);
    return QVariant::fromValue(d->m_model);
}

void LocationModel::setUsedRoles(const QStringList &roles)
{
    Q_D(LocationModel);
    d->m_usedRoles = roles;
    d->queueUpdate();
    Q_EMIT usedRolesChanged();
}

QStringList LocationModel::usedRoles() const
{
    Q_D(const LocationModel);
    return d->m_usedRoles;
}

void LocationModel::setZoomLevel(qreal zoomLevel)
{
    Q_D(LocationModel);
    d->m_zoomLevel = zoomLevel;
    d->queueUpdate();
    Q_EMIT zoomLevelChanged();
}

qreal LocationModel::zoomLevel() const
{
    Q_D(const LocationModel);
    return d->m_zoomLevel;
}

int LocationModel::nonGeoTaggedCount() const
{
    Q_D(const LocationModel);
    return d->m_nonGeoTaggedCount;
}

void LocationModel::classBegin()
{
    Q_D(LocationModel);
    d->m_updateQueued = true;
}

void LocationModel::componentComplete()
{
    Q_D(LocationModel);
    d->update();
}

QVariantList LocationModel::getItems(int row) const
{
    return data(index(row, 0), ItemRole).toList();
}

QVariant LocationModel::get(int row, const QString &roleName) const
{
    int role = roleNames().key(roleName.toLatin1(), -1);
    return data(index(row, 0), role);
}

int LocationModel::rowCount(const QModelIndex &parent) const
{
    Q_D(const LocationModel);
    Q_UNUSED(parent);
    return d->m_groups.count();
}

QVariant LocationModel::data(const QModelIndex &index, int role) const
{
    Q_D(const LocationModel);

    int row = index.row();
    if (Q_UNLIKELY(row < 0 || row >= d->m_usedGroups.count()))
        return QVariant();

    const GroupData &gd = d->m_groups[d->m_usedGroups[row]];

    switch (role) {
    case LocationRole:
        return QVariant::fromValue(gd.averageLocation);
    case CountRole:
        return gd.items.count();
    case ItemRole:
        {
            QVariantList items;
            for (const ItemData &id: gd.items) {
                QVariantMap itemData;
                QModelIndex sourceIndex = d->m_model->index(id.rowId, 0);
                for (auto i = d->m_usedRolesIds.begin();
                     i != d->m_usedRolesIds.end(); i++) {
                    itemData.insert(i.value(), sourceIndex.data(i.key()));
                }
                items.append(itemData);
            }
            return items;
        }
    default:
        qWarning() << "Unknown role ID:" << role;
        return QVariant();
    }
}

QHash<int, QByteArray> LocationModel::roleNames() const
{
    Q_D(const LocationModel);
    return d->m_roles;
}

#include "location_model.moc"
