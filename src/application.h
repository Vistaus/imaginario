/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGINARIO_APPLICATION_H
#define IMAGINARIO_APPLICATION_H

#ifdef DESKTOP_BUILD
#include <QApplication>
#else
#include <QGuiApplication>
#endif

namespace Imaginario {

class ApplicationPrivate;
class Application:
#ifdef DESKTOP_BUILD
    public QApplication
#else
    public QGuiApplication
#endif
{
    Q_OBJECT

public:
    Application(int &argc, char **argv);
    ~Application();

    QString dataPath() const;

private:
    ApplicationPrivate *d_ptr;
    Q_DECLARE_PRIVATE(Application)
};

} // namespace

#endif // IMAGINARIO_APPLICATION_H
