/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "face_detector.h"

#include <QDebug>
#include <QFileInfo>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace Imaginario;

namespace Imaginario {

class FaceDetectorPrivate
{
    Q_DECLARE_PUBLIC(FaceDetector)

public:
    FaceDetectorPrivate(const QString &dataPath, FaceDetector *q);
    ~FaceDetectorPrivate();

private:
    cv::CascadeClassifier m_classifier;
    bool m_isValid;
    FaceDetector *q_ptr;
};

} // namespace

FaceDetectorPrivate::FaceDetectorPrivate(const QString &dataPath,
                                         FaceDetector *q):
    m_isValid(true),
    q_ptr(q)
{
    QByteArray path =
        dataPath.toUtf8() + "haarcascade_frontalface_default.xml";
    bool ok = m_classifier.load(path.constData());
    if (Q_UNLIKELY(!ok)) {
        qWarning() << "Couldn't load classifier file!" << path;
        m_isValid = false;
    }
}

FaceDetectorPrivate::~FaceDetectorPrivate()
{
}

FaceDetector::FaceDetector(const QString &dataPath):
    d_ptr(new FaceDetectorPrivate(dataPath, this))
{
}

FaceDetector::~FaceDetector()
{
    delete d_ptr;
}

bool FaceDetector::isValid() const
{
    Q_D(const FaceDetector);
    return d->m_isValid;
}

QList<QRectF> FaceDetector::checkFile(const QUrl &url)
{
    Q_D(FaceDetector);

    QList<QRectF> ret;

    cv::Mat image = cv::imread(url.toLocalFile().toStdString());
    if (Q_UNLIKELY(image.empty())) return ret;

    /* If the image is too big, scale it down */
    int minSize = qMin(image.cols, image.rows);
    if (minSize > 1500) {
        cv::Mat resized;
        double scale = 1000.0 / minSize;
        cv::resize(image, resized, cv::Size(), scale, scale);
        image = resized;
    }
    cv::Mat grey;
    cv::cvtColor(image, grey, CV_BGR2GRAY);
    cv::equalizeHist(grey, grey);

    std::vector<cv::Rect> faces;
    d->m_classifier.detectMultiScale(grey, faces, 1.2, 2,
                                     CV_HAAR_SCALE_IMAGE,
                                     cv::Size(30, 30));
    for (size_t i = 0; i < faces.size(); i++) {
        const cv::Rect &r = faces[i];
        ret.append(QRectF(qreal(r.x) / image.cols,
                          qreal(r.y) / image.rows,
                          qreal(r.width) / image.cols,
                          qreal(r.height) / image.rows));
    }
    return ret;
}
