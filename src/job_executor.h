/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGINARIO_JOB_EXECUTOR_H
#define IMAGINARIO_JOB_EXECUTOR_H

#include "job.h"

#include <QObject>
#include <QVector>
#include <functional>

namespace Imaginario {

class JobExecutorPrivate;
class JobExecutor: public QObject
{
    Q_OBJECT

public:
    static JobExecutor *instance();
    ~JobExecutor();

    void setEmbedMetadata(bool embed);
    bool embedMetadata() const;

    void addJob(const Job &job) { addJobs(QVector<Job>() << job); }
    void addJobs(const QVector<Job> &jobs);

    typedef std::function<void(const Job&)> ExecutorFunc;

    void registerExecutor(Job::Type type, ExecutorFunc func);

    Q_INVOKABLE void start();
    Q_INVOKABLE void stop();

Q_SIGNALS:
    void thumbnailUpdated(QUrl uris);

protected:
    JobExecutor(QObject *parent = 0);

private:
    JobExecutorPrivate *d_ptr;
    Q_DECLARE_PRIVATE(JobExecutor)
};

} // namespace

#endif // IMAGINARIO_JOB_EXECUTOR_H
