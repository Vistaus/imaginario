/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGINARIO_TAG_AREA_MODEL_H
#define IMAGINARIO_TAG_AREA_MODEL_H

#include "types.h"

#include <QAbstractListModel>
#include <QJsonArray>
#include <QQmlParserStatus>

namespace Imaginario {

class TagAreaModelPrivate;
class TagAreaModel: public QAbstractListModel, public QQmlParserStatus
{
    Q_OBJECT
    Q_ENUMS(Roles)
    Q_INTERFACES(QQmlParserStatus)
    Q_PROPERTY(int count READ rowCount NOTIFY countChanged)
    Q_PROPERTY(PhotoId photoId READ photoId \
               WRITE setPhotoId NOTIFY photoIdChanged)
    Q_PROPERTY(bool canDetect READ canDetect CONSTANT)
    Q_PROPERTY(bool detecting READ isDetecting NOTIFY detectingChanged)
    Q_PROPERTY(QJsonArray editingAreas READ editingAreas WRITE setEditingAreas
               NOTIFY editingAreasChanged)

public:
    enum Roles {
        TagNameRole = Qt::UserRole + 1,
        RectRole,
        EditingRole,
        TagRole,
    };

    TagAreaModel(QObject *parent = 0);
    ~TagAreaModel();

    void setPhotoId(PhotoId photoId);
    PhotoId photoId() const;

    bool canDetect() const;
    bool isDetecting() const;

    void setEditingAreas(const QJsonArray &areas);
    QJsonArray editingAreas() const;

    Q_INVOKABLE bool detectFaces();
    Q_INVOKABLE void cancelDetection();

    Q_INVOKABLE QVariant get(int row, const QString &role) const;

    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index,
                  int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

    void classBegin() Q_DECL_OVERRIDE;
    void componentComplete() Q_DECL_OVERRIDE;

Q_SIGNALS:
    void countChanged();
    void photoIdChanged();
    void detectingChanged();
    void editingAreasChanged();

private:
    TagAreaModelPrivate *d_ptr;
    Q_DECLARE_PRIVATE(TagAreaModel)
};

} // namespace

#endif // IMAGINARIO_TAG_AREA_MODEL_H
