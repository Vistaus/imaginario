/*
 * Copyright (C) 2016-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "folder_model.h"

#include <QSignalSpy>
#include <QTest>

using namespace Imaginario;

class FolderModelTest: public QObject
{
    Q_OBJECT

public:
    static QVariant get(const QAbstractListModel *model, int row,
                        QString roleName);

private Q_SLOTS:
    void testProperties();
    // TODO: find a way to test it properly
};

QVariant FolderModelTest::get(const QAbstractListModel *model, int row,
                             QString roleName)
{
    QHash<int, QByteArray> roleNames = model->roleNames();

    int role = roleNames.key(roleName.toLatin1(), -1);
    return model->data(model->index(row), role);
}

void FolderModelTest::testProperties()
{
    FolderModel model;

    QVERIFY(model.property("count").toInt() >= 0);
}
QTEST_GUILESS_MAIN(FolderModelTest)

#include "tst_folder_model.moc"
