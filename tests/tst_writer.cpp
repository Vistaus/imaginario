/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fake_database.h"
#include "fake_job_executor.h"
#include "fake_metadata.h"
#include "metadata.h"
#include "test_utils.h"
#include "writer.h"

#include <QFileInfo>
#include <QMetaObject>
#include <QQmlComponent>
#include <QQmlEngine>
#include <QScopedPointer>
#include <QSignalSpy>
#include <QTemporaryDir>
#include <QTest>

using namespace Imaginario;

class WriterTest: public QObject
{
    Q_OBJECT

public:
    enum TagOperation {
        Add,
        Remove,
        Set,
    };

    WriterTest() {
        qRegisterMetaType<GeoPoint>("GeoPoint");
        qRegisterMetaType<PhotoId>("PhotoId");
        qRegisterMetaType<Metadata::Regions>("Metadata::Regions");
        qRegisterMetaType<Tag>("Tag");
    }

private Q_SLOTS:
    void testProperties();
    void testCreateTag();
    void testSetTags_data();
    void testSetTags();
    void testUpdateTags_data();
    void testUpdateTags();
    void testDeleteTag_data();
    void testDeleteTag();
    void testSetRegions_data();
    void testSetRegions();
    void testSetRating_data();
    void testSetRating();
    void testSetLocation_data();
    void testSetLocation();
    void testSetTitleAndDescription_data();
    void testSetTitleAndDescription();
    void testMakeVersion();
    void testMakeDefaultVersion_data();
    void testMakeDefaultVersion();
    void testRemoveVersion_data();
    void testRemoveVersion();
    void testDeletePhotos_data();
    void testDeletePhotos();
    void testNoXmp();
    void testRebuildThumbnails_data();
    void testRebuildThumbnails();
    void testCreateItem_data();
    void testCreateItem();

private:
    QVector<Job> addedJobs(const QList<QList<QVariant>> &spy) {
        QVector<Job> jobs;
        Q_FOREACH(const QList<QVariant> &args, spy) {
            jobs += args.at(0).value<QVector<Job>>();
        }
        return jobs;
    }
};

Q_DECLARE_METATYPE(WriterTest::TagOperation)

void WriterTest::testProperties()
{
    Database *db = Database::instance();

    Writer writer;

    writer.setProperty("embed", true);
    QCOMPARE(writer.property("embed").toBool(), true);

    QCOMPARE(writer.property("writeXmp").toBool(), true);
    writer.setProperty("writeXmp", false);
    QCOMPARE(writer.property("writeXmp").toBool(), false);

    delete db;
}

void WriterTest::testCreateTag()
{
    Database *db = Database::instance();

    Writer writer;

    Tag parent = writer.createTag("parent");
    QVERIFY(parent.isValid());
    QCOMPARE(parent, db->tag("parent"));

    Tag child = writer.createTag("child", parent, "baby");
    QVERIFY(child.isValid());
    QCOMPARE(child.name(), QString("child"));
    QCOMPARE(child.icon(), QString("baby"));
    QCOMPARE(child, db->tag("child"));

    // Create the same tag
    QCOMPARE(child, writer.createTag("child"));

    delete db;
}

void WriterTest::testSetTags_data()
{
    QTest::addColumn<QString>("fileName");
    QTest::addColumn<QStringList>("tagNames");
    QTest::addColumn<TagOperation>("operation");

    QTest::newRow("no file tags, no new tags") <<
        "image0.png" <<
        QStringList() <<
        Set;

    QTest::newRow("no file tags, new tags") <<
        "image0.png" <<
        (QStringList() << "blue" << "a cat") <<
        Set;

    QTest::newRow("file tags, no new tags") <<
        "image1.jpg" <<
        QStringList() <<
        Set;

    QTest::newRow("file tags, some new tags") <<
        "image1.jpg" <<
        (QStringList() << "big" << "a dog" << "red") <<
        Set;

    QTest::newRow("adding, no file tags, no new tags") <<
        "image0.png" <<
        QStringList() <<
        Add;

    QTest::newRow("adding, no file tags, new tags") <<
        "image0.png" <<
        (QStringList() << "blue" << "a cat") <<
        Add;

    QTest::newRow("adding, file tags, no new tags") <<
        "image1.jpg" <<
        QStringList() <<
        Add;

    QTest::newRow("adding, file tags, same tags") <<
        "image1.jpg" <<
        (QStringList() << "big" << "a dog" << "red") <<
        Add;

    QTest::newRow("adding, file tags, some new tags") <<
        "image1.jpg" <<
        (QStringList() << "tiny" << "a dog" << "blue") <<
        Add;

    QTest::newRow("removing, no file tags, no removed tags") <<
        "image0.png" <<
        QStringList() <<
        Remove;

    QTest::newRow("removing, file tags, no removed tags") <<
        "image1.jpg" <<
        QStringList() <<
        Remove;

    QTest::newRow("removing, file tags") <<
        "image1.jpg" <<
        (QStringList() << "a dog") <<
        Remove;
}

void WriterTest::testSetTags()
{
    QFETCH(QString, fileName);
    QFETCH(QStringList, tagNames);
    QFETCH(TagOperation, operation);

    /* Prepare some fake image metadata */
    QHash<QString,QStringList> photoTags;
    photoTags["image0.png"] = QStringList();
    photoTags["image1.jpg"] =
        QStringList() << "red" << "a dog" << "big";

    // Create the images in the DB, setup the metadata
    QDir dataDir(ITEMS_DIR);
    QString sourceFile = dataDir.absoluteFilePath(fileName);

    QScopedPointer<JobExecutor> executor(JobExecutor::instance());
    JobExecutorPrivate *executorMock =
        JobExecutorPrivate::mocked(executor.data());
    QSignalSpy addJobsCalled(executorMock,
                             SIGNAL(addJobsCalled(QVector<Imaginario::Job>)));

    Database *db = Database::instance();
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db);

    QStringList allTags;
    allTags << "red" << "green" << "blue" <<
        "a cat" << "a dog" << "a house" <<
        "tiny" << "medium" << "big";
    dbMock->setTags(allTags);

    QStringList fileNames = photoTags.keys();
    QList<Database::Photo> allPhotos;
    Q_FOREACH(const QString &name, fileNames) {
        Database::Photo photo;
        dbMock->setPhotoId(photo, fileNames.indexOf(name));
        photo.setBaseUri("file://" + dataDir.absolutePath());
        photo.setFileName(name);

        allPhotos.append(photo);
    }
    dbMock->setPhotos(allPhotos);
    Q_FOREACH(const Database::Photo photo, allPhotos) {
        db->setTags(photo.id(), photoTags[photo.fileName()]);
    }

    QSignalSpy setTagsCalled(dbMock,
                     SIGNAL(setTagsCalled(int,QList<Imaginario::Tag>)));
    QSignalSpy addTagsCalled(dbMock,
                     SIGNAL(addTagsCalled(int,QList<Imaginario::Tag>)));

    PhotoId photoId = fileNames.indexOf(fileName);
    QList<Tag> tags = db->tags(tagNames);

    Writer writer;
    QStringList expectedTagNames;
    QList<Tag> expectedTags;
    bool callExpected;
    if (operation == Add) {
        expectedTagNames = tagNames + photoTags[fileName];
        expectedTagNames.removeDuplicates();
        writer.changePhotoTags(photoId, tagNames, QStringList());
        callExpected = (expectedTagNames != photoTags[fileName]);
    } else if (operation == Remove) {
        expectedTagNames = photoTags[fileName];
        Q_FOREACH(const QString &tagName, tagNames) {
            expectedTagNames.removeOne(tagName);
        }
        writer.changePhotoTags(photoId, QStringList(), tagNames);
        callExpected = (expectedTagNames != photoTags[fileName]);
    } else {
        expectedTagNames = tagNames;
        writer.setPhotoTags(photoId, tagNames);
        callExpected = true;
    }
    expectedTags = db->tags(expectedTagNames);

    std::sort(expectedTags.begin(), expectedTags.end());
    expectedTagNames.sort();

    QCOMPARE(addJobsCalled.count(), callExpected ? 1 : 0);
    if (callExpected) {
        tags = db->tags(photoId);
        std::sort(tags.begin(), tags.end());
        QCOMPARE(tags, expectedTags);

        QVector<Job> jobs = addedJobs(addJobsCalled);
        QCOMPARE(jobs.count(), 1);
        PhotoJob job = static_cast<const PhotoJob &>(jobs.at(0));
        QCOMPARE(job.photoId(), photoId);
        QCOMPARE(job.operations(), PhotoJob::WriteTags);
    }

    delete db;
}

void WriterTest::testUpdateTags_data()
{
    QTest::addColumn<QString>("tagName");
    QTest::addColumn<QString>("changes");
    QTest::addColumn<int>("numPhotos");
    QTest::addColumn<QString>("expectedName");
    QTest::addColumn<QString>("expectedIcon");
    QTest::addColumn<QString>("expectedParent");

    QTest::newRow("no changes") <<
        "blue" <<
        "{}" <<
        0 <<
        "blue" <<
        "blueicon" <<
        "colours";

    QTest::newRow("name") <<
        "blue" <<
        "{ 'name': 'azzurro' }" <<
        0 <<
        "azzurro" <<
        "blueicon" <<
        "colours";

    QTest::newRow("icon") <<
        "red" <<
        "{ 'icon': 'rosso' }" <<
        0 <<
        "red" <<
        "rosso" <<
        "colours";

    QTest::newRow("parent") <<
        "green" <<
        "{ 'parent': createTag('colori') }" <<
        0 <<
        "green" <<
        "" <<
        "colori";
}

void WriterTest::testUpdateTags()
{
    QFETCH(QString, tagName);
    QFETCH(QString, changes);
    QFETCH(int, numPhotos);
    QFETCH(QString, expectedName);
    QFETCH(QString, expectedIcon);
    QFETCH(QString, expectedParent);

    /* Prepare some fake image metadata */
    QHash<QString,QStringList> photoTags;
    photoTags["image0.png"] =
        QStringList() << "blue";
    photoTags["image1.jpg"] =
        QStringList() << "red" << "a dog" << "big";
    photoTags["image2.jpg"] =
        QStringList() << "blue" << "red" << "green";

    QScopedPointer<JobExecutor> executor(JobExecutor::instance());
    JobExecutorPrivate *executorMock =
        JobExecutorPrivate::mocked(executor.data());
    QSignalSpy addJobsCalled(executorMock,
                             SIGNAL(addJobsCalled(QVector<Imaginario::Job>)));

    QScopedPointer<Database> db(Database::instance());
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db.data());

    QStringList allTags;
    allTags << "colours" <<
        "a cat" << "a dog" << "a house" <<
        "tiny" << "medium" << "big";
    dbMock->setTags(allTags);
    db->createTag(db->tag("colours"), "blue", "blueicon");
    db->createTag(db->tag("colours"), "red", "red icon");
    db->createTag(db->tag("colours"), "green", "");

    QStringList fileNames = photoTags.keys().mid(0, numPhotos);
    QList<Database::Photo> allPhotos;
    QDir dataDir(ITEMS_DIR);
    Q_FOREACH(const QString &name, fileNames) {
        Database::Photo photo;
        dbMock->setPhotoId(photo, fileNames.indexOf(name));
        photo.setBaseUri("file://" + dataDir.absolutePath());
        photo.setFileName(name);

        allPhotos.append(photo);
    }
    dbMock->setPhotos(allPhotos);
    Q_FOREACH(const Database::Photo photo, allPhotos) {
        db->setTags(photo.id(), photoTags[photo.fileName()]);
    }

    QSignalSpy tagChanged(db.data(), SIGNAL(tagChanged(Tag)));
    QSignalSpy findPhotosCalled(dbMock,
        SIGNAL(findPhotosCalled(Imaginario::SearchFilters,Qt::SortOrder)));

    QQmlEngine engine;
    qmlRegisterType<Writer>("MyTest", 1, 0, "Writer");
    QQmlComponent component(&engine);
    QByteArray changesText = changes.toUtf8();
    component.setData("import MyTest 1.0\n"
                      "Writer {\n"
                      "  function run(tag) {\n"
                      "    updateTag(tag, " + changesText + ")\n"
                      "  }\n"
                      "}",
                      QUrl());
    QObject *object = component.create();
    QVERIFY(object != 0);

    Tag changedTag = db->tag(tagName);
    QVariant tagVariant = QVariant::fromValue(changedTag);
    bool ok = QMetaObject::invokeMethod(object, "run",
                                        Q_ARG(QVariant, tagVariant));
    QVERIFY(ok);

    /* Verify that the DB has been updated */
    QCOMPARE(tagChanged.count(), 1);
    QCOMPARE(tagChanged.at(0).at(0).value<Tag>(), changedTag);

    QCOMPARE(changedTag.name(), expectedName);
    QCOMPARE(changedTag.parent(), db->tag(expectedParent));
    QCOMPARE(changedTag.icon(), expectedIcon);

    /* Verify that the correct metadata jobs have been created */
    if (tagName != expectedName) {
        QCOMPARE(findPhotosCalled.count(), 1);
        SearchFilters filters =
            findPhotosCalled.at(0).at(0).value<SearchFilters>();
        QCOMPARE(filters.time0, QDateTime());
        QCOMPARE(filters.time1, QDateTime());
        QCOMPARE(filters.roll0, -1);
        QCOMPARE(filters.roll1, -1);
        QCOMPARE(filters.requiredTags, QList<QList<Tag>>{QList<Tag>{changedTag}});
        QVERIFY(filters.forbiddenTags.isEmpty());
        QCOMPARE(filters.location0, GeoPoint());
        QCOMPARE(filters.location1, GeoPoint());
        QVERIFY(filters.excludedPhotos.isEmpty());
        QCOMPARE(filters.skipNonDefaultVersions, false);
    } else {
        QCOMPARE(findPhotosCalled.count(), 0);
    }
}

void WriterTest::testDeleteTag_data()
{
    QTest::addColumn<QString>("tagName");
    QTest::addColumn<int>("numPhotos");
    QTest::addColumn<QList<int> >("expectedPhotoIds");

    QTest::newRow("invalid tag") <<
        "beige" <<
        2 <<
        QList<int>();

    QTest::newRow("no photos") <<
        "green" <<
        2 <<
        (QList<int>() << 0 << 1); // mocked findPhotos bug

    QTest::newRow("one photo") <<
        "a dog" <<
        3 <<
        (QList<int>() << 0 << 1 << 2); // mocked findPhotos bug

    QTest::newRow("two photos") <<
        "red" <<
        3 <<
        (QList<int>() << 0 << 1 << 2); // mocked findPhotos bug
}

void WriterTest::testDeleteTag()
{
    QFETCH(QString, tagName);
    QFETCH(int, numPhotos);
    QFETCH(QList<int>, expectedPhotoIds);

    /* Prepare some fake image metadata */
    QHash<QString,QStringList> photoTags;
    photoTags["image0.png"] =
        QStringList() << "blue";
    photoTags["image1.jpg"] =
        QStringList() << "red" << "a dog" << "big";
    photoTags["image2.jpg"] =
        QStringList() << "blue" << "red" << "green";

    QScopedPointer<JobExecutor> executor(JobExecutor::instance());
    JobExecutorPrivate *executorMock =
        JobExecutorPrivate::mocked(executor.data());
    QSignalSpy addJobsCalled(executorMock,
                             SIGNAL(addJobsCalled(QVector<Imaginario::Job>)));

    QScopedPointer<Database> db(Database::instance());
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db.data());

    QStringList allTags;
    allTags << "red" << "green" << "blue" <<
        "a cat" << "a dog" << "a house" <<
        "tiny" << "medium" << "big";
    dbMock->setTags(allTags);

    QStringList fileNames = photoTags.keys().mid(0, numPhotos);
    QList<Database::Photo> allPhotos;
    QDir dataDir(ITEMS_DIR);
    Q_FOREACH(const QString &name, fileNames) {
        Database::Photo photo;
        dbMock->setPhotoId(photo, fileNames.indexOf(name));
        photo.setBaseUri("file://" + dataDir.absolutePath());
        photo.setFileName(name);

        allPhotos.append(photo);
    }
    dbMock->setPhotos(allPhotos);
    Q_FOREACH(const Database::Photo photo, allPhotos) {
        db->setTags(photo.id(), photoTags[photo.fileName()]);
    }

    QSignalSpy tagDeleted(db.data(), SIGNAL(tagDeleted(Tag)));
    QSignalSpy findPhotosCalled(dbMock,
        SIGNAL(findPhotosCalled(Imaginario::SearchFilters,Qt::SortOrder)));

    QQmlEngine engine;
    qmlRegisterType<Writer>("MyTest", 1, 0, "Writer");
    QQmlComponent component(&engine);
    component.setData("import MyTest 1.0\n"
                      "Writer {\n"
                      "  function run(tag) {\n"
                      "    deleteTag(tag)\n"
                      "  }\n"
                      "}",
                      QUrl());
    QObject *object = component.create();
    QVERIFY(object != 0);

    Tag deletedTag = db->tag(tagName);
    QVariant tagVariant = QVariant::fromValue(deletedTag);
    bool ok = QMetaObject::invokeMethod(object, "run",
                                        Q_ARG(QVariant, tagVariant));
    QVERIFY(ok);

    /* findPhotos() will always be called, because it's called
     * before actually deleting the tag */
    QCOMPARE(findPhotosCalled.count(), 1);
    SearchFilters filters =
        findPhotosCalled.at(0).at(0).value<SearchFilters>();
    QCOMPARE(filters.time0, QDateTime());
    QCOMPARE(filters.time1, QDateTime());
    QCOMPARE(filters.roll0, -1);
    QCOMPARE(filters.roll1, -1);
    QCOMPARE(filters.requiredTags, QList<QList<Tag>>{QList<Tag>{deletedTag}});
    QVERIFY(filters.forbiddenTags.isEmpty());
    QCOMPARE(filters.location0, GeoPoint());
    QCOMPARE(filters.location1, GeoPoint());
    QVERIFY(filters.excludedPhotos.isEmpty());
    QCOMPARE(filters.skipNonDefaultVersions, false);

    if (deletedTag.isValid()) {
        /* Verify that the DB has been updated */
        QCOMPARE(tagDeleted.count(), 1);
        QCOMPARE(tagDeleted.at(0).at(0).value<Tag>(), deletedTag);

        /* Verify that the right jobs have been created */
        QVector<Job> jobs = addedJobs(addJobsCalled);
        QCOMPARE(jobs.count(), expectedPhotoIds.count());
        QList<int> photoIds;
        for (int i = 0; i < jobs.count(); i++) {
            PhotoJob job = static_cast<const PhotoJob &>(jobs.at(i));
            photoIds.append(job.photoId());
            QCOMPARE(job.operations(), PhotoJob::WriteTags);
        }
        QCOMPARE(photoIds.toSet(), expectedPhotoIds.toSet());
    } else {
        QCOMPARE(tagDeleted.count(), 0);
    }
}

void WriterTest::testSetRegions_data()
{
    QTest::addColumn<QString>("method");
    QTest::addColumn<QString>("regionsString");
    QTest::addColumn<Metadata::Regions>("initialRegions");
    QTest::addColumn<Metadata::Regions>("expectedRegions");

    Metadata::Regions initial;
    Metadata::Regions regions;
    QTest::newRow("empty list") <<
        "setRegions" <<
        "[]" <<
        initial <<
        regions;

    initial.append(Metadata::RegionInfo(QRectF(0.6, 0.4, 0.1, 0.2),
                                        "Been there", QString()));
    QTest::newRow("empty list, initial regions") <<
        "setRegions" <<
        "[]" <<
        initial <<
        regions;

    regions.append(Metadata::RegionInfo(QRectF(0.4, 0.5, 0.1, 0.2),
                                        "Tom", "My friend"));
    QTest::newRow("single item, initial regions") <<
        "setRegions" <<
        "["
        "  {"
        "    \"tag\": createTag(\"Tom\"),"
        "    \"description\": \"My friend\","
        "    \"rect\": Qt.rect(0.4, 0.5, 0.1, 0.2)"
        "  }"
        "]" <<
        initial <<
        regions;
    initial.clear();
    regions.clear();

    regions.append(Metadata::RegionInfo(QRectF(0.4, 0.5, 0.1, 0.2),
                                        "Jim", "My friend"));
    QTest::newRow("single item") <<
        "setRegions" <<
        "["
        "  {"
        "    \"tag\": createTag(\"Jim\"),"
        "    \"description\": \"My friend\","
        "    \"rect\": Qt.rect(0.4, 0.5, 0.1, 0.2)"
        "  }"
        "]" <<
        initial <<
        regions;
    regions.clear();

    regions.append(Metadata::RegionInfo(QRectF(0.4, 0.5, 0.1, 0.2),
                                        "Sam", "A guy"));
    regions.append(Metadata::RegionInfo(QRectF(0.6, 0.5, 0.2, 0.2),
                                        "Dick", QString()));
    regions.append(Metadata::RegionInfo(QRectF(0.4, 0.8, 0.1, 0.1),
                                        "Harry", QString()));
    QTest::newRow("many items") <<
        "setRegions" <<
        "["
        "  {"
        "    \"tag\": createTag(\"Sam\"),"
        "    \"description\": \"A guy\","
        "    \"rect\": Qt.rect(0.4, 0.5, 0.1, 0.2)"
        "  },"
        "  {"
        "    \"tag\": createTag(\"Dick\"),"
        "    \"description\": \"\","
        "    \"rect\": Qt.rect(0.6, 0.5, 0.2, 0.2)"
        "  },"
        "  {"
        "    \"tag\": createTag(\"Harry\"),"
        "    \"rect\": Qt.rect(0.4, 0.8, 0.1, 0.1)"
        "  }"
        "]" <<
        initial <<
        regions;

    initial = {
        Metadata::RegionInfo(QRectF(0.6, 0.4, 0.1, 0.2),
                             "Been there", QString()),
    };
    regions = {
        Metadata::RegionInfo(QRectF(0.6, 0.4, 0.1, 0.2),
                             "Been there", QString()),
        Metadata::RegionInfo(QRectF(0.4, 0.5, 0.1, 0.2),
                             "Tom", "My friend"),
    };
    QTest::newRow("add regions") <<
        "addRegions" <<
        "["
        "  {"
        "    \"tag\": createTag(\"Tom\"),"
        "    \"description\": \"My friend\","
        "    \"rect\": Qt.rect(0.4, 0.5, 0.1, 0.2)"
        "  }"
        "]" <<
        initial <<
        regions;
}

void WriterTest::testSetRegions()
{
    QFETCH(QString, method);
    QFETCH(QString, regionsString);
    QFETCH(Metadata::Regions, initialRegions);
    QFETCH(Metadata::Regions, expectedRegions);

    // Create the images in the DB
    QDir dataDir(ITEMS_DIR);

    Database *db = Database::instance();
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db);

    QStringList allTags;
    allTags << "Tom" << "Been there" << "Jim" <<
        "Sam" << "Dick" << "Harry";
    dbMock->setTags(allTags);

    Database::Photo photo;
    photo.setBaseUri("file://" + dataDir.absolutePath());
    photo.setFileName("image0.png");
    PhotoId id = db->addPhoto(photo, 1);
    QVERIFY(id >= 0);

    photo.setFileName("image1.jpg");
    id = db->addPhoto(photo, 1);
    QVERIFY(id > 0);
    Q_FOREACH(const Metadata::RegionInfo &r, initialRegions) {
        db->addTagWithArea(id, db->tag(r.name), r.area);
    }

    QSignalSpy photoTagAreasChanged(db,
                                    SIGNAL(photoTagAreasChanged(PhotoId)));

    QScopedPointer<JobExecutor> executor(JobExecutor::instance());
    JobExecutorPrivate *executorMock =
        JobExecutorPrivate::mocked(executor.data());
    QSignalSpy addJobsCalled(executorMock,
                             SIGNAL(addJobsCalled(QVector<Imaginario::Job>)));

    QQmlEngine engine;
    qmlRegisterType<Writer>("MyTest", 1, 0, "Writer");
    QQmlComponent component(&engine);
    QByteArray regionsText = regionsString.toUtf8();
    component.setData("import MyTest 1.0\n"
                      "Writer {\n"
                      "  function run(photoId) {\n"
                      "    " + method.toUtf8() + "(photoId, " + regionsText + ")\n"
                      "  }\n"
                      "}",
                      QUrl());
    QObject *object = component.create();
    QVERIFY(object != 0);

    bool ok = QMetaObject::invokeMethod(object, "run",
                                        Q_ARG(QVariant, id));
    QVERIFY(ok);

    QVector<Job> jobs = addedJobs(addJobsCalled);
    QCOMPARE(jobs.count(), 1);
    PhotoJob job = static_cast<const PhotoJob &>(jobs.at(0));
    QCOMPARE(job.photoId(), id);
    QCOMPARE(job.operations(), PhotoJob::WriteRegions);

    QCOMPARE(photoTagAreasChanged.isEmpty(), expectedRegions.isEmpty());
    QVector<TagArea> tagAreas = db->tagAreas(id);
    QVector<TagArea> expectedTagAreas;
    Q_FOREACH(const Metadata::RegionInfo &r, expectedRegions) {
        expectedTagAreas.append(TagArea(r.area, db->tag(r.name)));
    }
    QCOMPARE(tagAreas.toList().toSet(), expectedTagAreas.toList().toSet());

    /* Compare region descriptions in the scheduled job */
    Metadata::Regions regions;
    Q_FOREACH(const Metadata::RegionInfo &r, expectedRegions) {
        regions.append(Metadata::RegionInfo(r.area, r.name,
                                            job.regionDescription(r.area)));
    }

    QCOMPARE(regions.toList().toSet(), expectedRegions.toList().toSet());

    delete object;
    delete db;
}

void WriterTest::testSetRating_data()
{
    QTest::addColumn<QString>("photoIdString");
    QTest::addColumn<int>("rating");
    QTest::addColumn<QList<int> >("photoIds");

    QTest::newRow("empty list") <<
        "[]" <<
        3 <<
        QList<int>();

    QTest::newRow("single item") <<
        "1" <<
        2 <<
        (QList<int>() << 1);

    QTest::newRow("list") <<
        "[1, 0]" <<
        4 <<
        (QList<int>() << 1 << 0);
}

void WriterTest::testSetRating()
{
    QFETCH(QString, photoIdString);
    QFETCH(int, rating);
    QFETCH(QList<int>, photoIds);

    // Create the images in the DB
    QDir dataDir(ITEMS_DIR);

    Database *db = Database::instance();
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db);

    Database::Photo photo;
    photo.setBaseUri("file://" + dataDir.absolutePath());
    photo.setFileName("image0.png");
    PhotoId id = db->addPhoto(photo, 1);
    QCOMPARE(id, 0);

    photo.setFileName("image1.jpg");
    id = db->addPhoto(photo, 1);
    QCOMPARE(id, 1);

    QSignalSpy setRatingCalled(dbMock,
                               SIGNAL(setRatingCalled(int,int)));

    QScopedPointer<JobExecutor> executor(JobExecutor::instance());
    JobExecutorPrivate *executorMock =
        JobExecutorPrivate::mocked(executor.data());
    QSignalSpy addJobsCalled(executorMock,
                             SIGNAL(addJobsCalled(QVector<Imaginario::Job>)));

    QQmlEngine engine;
    qmlRegisterType<Writer>("MyTest", 1, 0, "Writer");
    QQmlComponent component(&engine);
    QByteArray idsText = photoIdString.toUtf8();
    QByteArray ratingText = QByteArray::number(rating);
    component.setData("import MyTest 1.0\n"
                      "Writer {\n"
                      "  function run() {\n"
                      "    setRating(" + idsText + "," + ratingText + ")\n"
                      "  }\n"
                      "}",
                      QUrl());
    QObject *object = component.create();
    QVERIFY(object != 0);

    bool ok = QMetaObject::invokeMethod(object, "run");
    QVERIFY(ok);

    QCOMPARE(setRatingCalled.count(), photoIds.count());
    QVector<Job> jobs = addedJobs(addJobsCalled);
    QCOMPARE(jobs.count(), photoIds.count());
    for (int i = 0; i < photoIds.count(); i++) {
        QCOMPARE(setRatingCalled.at(i).at(0).toInt(), photoIds[i]);
        QCOMPARE(setRatingCalled.at(i).at(1).toInt(), rating);

        PhotoJob job = static_cast<const PhotoJob &>(jobs.at(i));
        QCOMPARE(job.photoId(), photoIds[i]);
        QCOMPARE(job.operations(), PhotoJob::WriteRating);
    }

    delete object;
    delete db;
}

void WriterTest::testSetLocation_data()
{
    QTest::addColumn<QString>("photoIdString");
    QTest::addColumn<GeoPoint>("location");
    QTest::addColumn<QList<int> >("photoIds");

    QTest::newRow("empty list") <<
        "[]" <<
        GeoPoint(10, 20) <<
        QList<int>();

    QTest::newRow("single item") <<
        "1" <<
        GeoPoint(23.23413, -40.1004) <<
        (QList<int>() << 1);

    QTest::newRow("list") <<
        "[1, 0]" <<
        GeoPoint(-12.21435, 60.124543) <<
        (QList<int>() << 1 << 0);
}

void WriterTest::testSetLocation()
{
    QFETCH(QString, photoIdString);
    QFETCH(GeoPoint, location);
    QFETCH(QList<int>, photoIds);

    // Create the images in the DB
    QDir dataDir(ITEMS_DIR);

    Database *db = Database::instance();
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db);

    Database::Photo photo;
    photo.setBaseUri("file://" + dataDir.absolutePath());
    photo.setFileName("image0.png");
    PhotoId id = db->addPhoto(photo, 1);
    QCOMPARE(id, 0);

    photo.setFileName("image1.jpg");
    id = db->addPhoto(photo, 1);
    QCOMPARE(id, 1);

    QSignalSpy setLocationCalled(dbMock,
                                 SIGNAL(setLocationCalled(int,GeoPoint)));

    QScopedPointer<JobExecutor> executor(JobExecutor::instance());
    JobExecutorPrivate *executorMock =
        JobExecutorPrivate::mocked(executor.data());
    QSignalSpy addJobsCalled(executorMock,
                             SIGNAL(addJobsCalled(QVector<Imaginario::Job>)));

    QQmlEngine engine;
    qmlRegisterType<Writer>("MyTest", 1, 0, "Writer");
    QQmlComponent component(&engine);
    QByteArray idsText = photoIdString.toUtf8();
    component.setData("import MyTest 1.0\n"
                      "Writer {\n"
                      "  property var newLoc\n"
                      "  function run() {\n"
                      "    setLocation(" + idsText + ", newLoc)\n"
                      "  }\n"
                      "}",
                      QUrl());
    QObject *object = component.create();
    QVERIFY(object != 0);

    object->setProperty("newLoc", QVariant::fromValue(location));
    bool ok = QMetaObject::invokeMethod(object, "run");
    QVERIFY(ok);

    QCOMPARE(setLocationCalled.count(), photoIds.count());
    QVector<Job> jobs = addedJobs(addJobsCalled);
    QCOMPARE(jobs.count(), photoIds.count());
    for (int i = 0; i < photoIds.count(); i++) {
        QCOMPARE(setLocationCalled.at(i).at(0).toInt(), photoIds[i]);
        QCOMPARE(setLocationCalled.at(i).at(1).value<GeoPoint>(), location);

        PhotoJob job = static_cast<const PhotoJob &>(jobs.at(i));
        QCOMPARE(job.photoId(), photoIds[i]);
        QCOMPARE(job.operations(), PhotoJob::WriteLocation);
    }

    delete object;
    delete db;
}

void WriterTest::testSetTitleAndDescription_data()
{
    QTest::addColumn<QList<int> >("photoIds");
    QTest::addColumn<QString>("title");
    QTest::addColumn<QString>("description");

    QTest::newRow("empty list") <<
        QList<int>() <<
        "a house" << "Not written";

    QTest::newRow("set title") <<
        (QList<int>() << 1) <<
        "a big house" << QString();

    QTest::newRow("set desc") <<
        (QList<int>() << 1 << 0) <<
        QString() << "A long\ndescription";
}

void WriterTest::testSetTitleAndDescription()
{
    QFETCH(QList<int>, photoIds);
    QFETCH(QString, title);
    QFETCH(QString, description);

    // Create the images in the DB
    QDir dataDir(ITEMS_DIR);

    Database *db = Database::instance();
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db);

    Database::Photo photo;
    photo.setBaseUri("file://" + dataDir.absolutePath());
    photo.setFileName("image0.png");
    PhotoId id = db->addPhoto(photo, 1);
    QCOMPARE(id, 0);

    photo.setFileName("image1.jpg");
    id = db->addPhoto(photo, 1);
    QCOMPARE(id, 1);

    QSignalSpy setDescriptionCalled(dbMock,
                                    SIGNAL(setDescriptionCalled(int,QString)));

    QScopedPointer<JobExecutor> executor(JobExecutor::instance());
    JobExecutorPrivate *executorMock =
        JobExecutorPrivate::mocked(executor.data());
    QSignalSpy addJobsCalled(executorMock,
                             SIGNAL(addJobsCalled(QVector<Imaginario::Job>)));

    Writer writer;
    writer.setTitle(QVariant::fromValue(photoIds), title);
    writer.setDescription(QVariant::fromValue(photoIds), description);

    QCOMPARE(setDescriptionCalled.count(), photoIds.count());
    QVector<Job> jobs = addedJobs(addJobsCalled);
    QCOMPARE(jobs.count(), photoIds.count() * 2);
    for (int i = 0; i < photoIds.count(); i++) {
        photo = db->photo(photoIds[i]);
        QCOMPARE(setDescriptionCalled.at(i).at(0).toInt(), photo.id());
        QCOMPARE(setDescriptionCalled.at(i).at(1).toString(), title);

        PhotoJob job = static_cast<const PhotoJob &>(jobs.at(i));
        QCOMPARE(job.photoId(), photoIds[i]);
        QCOMPARE(job.operations(), PhotoJob::WriteTitle);
        job = static_cast<const PhotoJob &>(jobs.at(photoIds.count() + i));
        QCOMPARE(job.photoId(), photoIds[i]);
        QCOMPARE(job.operations(), PhotoJob::WriteDescription);
        QCOMPARE(job.description(), description);
    }

    delete db;
}

void WriterTest::testMakeVersion()
{
    Database *db = Database::instance();
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db);
    QSignalSpy makeVersionCalled(dbMock, SIGNAL(makeVersionCalled(int,int)));

    Writer writer;
    writer.makeVersion(2, 54);

    QTRY_COMPARE(makeVersionCalled.count(), 1);
    QVariantList args = makeVersionCalled.at(0);
    QCOMPARE(args.at(0).toInt(), 2);
    QCOMPARE(args.at(1).toInt(), 54);

    delete db;
}

void WriterTest::testMakeDefaultVersion_data()
{
    QTest::addColumn<int>("photoId");
    QTest::addColumn<QList<int> >("versions");
    QTest::addColumn<QList<int> >("expectedChangedIds");

    QTest::newRow("no versions") <<
        1 <<
        (QList<int>() << 1) <<
        (QList<int>());

    QTest::newRow("one version") <<
        2 <<
        (QList<int>() << 1 << 2) <<
        (QList<int>() << 1);

    QTest::newRow("many versions") <<
        3 <<
        (QList<int>() << 1 << 3 << 4 << 7) <<
        (QList<int>() << 1 << 4 << 7);
}

void WriterTest::testMakeDefaultVersion()
{
    QFETCH(int, photoId);
    QFETCH(QList<int>, versions);
    QFETCH(QList<int>, expectedChangedIds);

    Database *db = Database::instance();
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db);

    QList<Database::Photo> allPhotos;
    Q_FOREACH(int id, versions) {
        Database::Photo photo;
        dbMock->setPhotoId(photo, id);
        allPhotos.append(photo);
    }
    dbMock->setPhotos(allPhotos);

    QSignalSpy makeVersionCalled(dbMock,
                                 SIGNAL(makeVersionCalled(int,int)));

    if (expectedChangedIds.isEmpty()) {
        QTest::ignoreMessage(QtWarningMsg, "makeDefaultVersion: needed at least 2 photos ");
    }

    Writer writer;
    writer.makeDefaultVersion(photoId);

    QCOMPARE(makeVersionCalled.count(), expectedChangedIds.count());
    QList<int> changedIds;
    QList<QVariantList> emissions = makeVersionCalled;
    Q_FOREACH(const QVariantList &args, emissions) {
        changedIds.append(args.at(0).toInt());
        QCOMPARE(args.at(1).toInt(), photoId);
    }
    QCOMPARE(changedIds.toSet(), expectedChangedIds.toSet());

    delete db;

}

void WriterTest::testRemoveVersion_data()
{
    QTest::addColumn<int>("photoId");
    QTest::addColumn<QList<int> >("versions");
    QTest::addColumn<int>("masterId");
    QTest::addColumn<QList<int> >("expectedChangedIds");
    QTest::addColumn<QList<int> >("expectedMasterIds");

    QTest::newRow("no versions") <<
        1 <<
        (QList<int>()) <<
        -1 <<
        (QList<int>()) <<
        (QList<int>());

    QTest::newRow("out of many") <<
        2 <<
        (QList<int>() << 1 << 2 << 3 << 4) <<
        3 <<
        (QList<int>() << 2) <<
        (QList<int>() << -1);

    QTest::newRow("out of two") <<
        2 <<
        (QList<int>() << 2 << 4) <<
        4 <<
        (QList<int>() << 2 << 4) <<
        (QList<int>() << -1 << -1);

    QTest::newRow("master out of many") <<
        3 <<
        (QList<int>() << 1 << 2 << 3 << 4) <<
        3 <<
        (QList<int>() << 1 << 2 << 3 << 4) <<
        (QList<int>() << -1 << 1 << -1 << 1);

    QTest::newRow("master out of two") <<
        4 <<
        (QList<int>() << 2 << 4) <<
        4 <<
        (QList<int>() << 2 << 4) <<
        (QList<int>() << -1 << -1);
}

void WriterTest::testRemoveVersion()
{
    QFETCH(int, photoId);
    QFETCH(QList<int>, versions);
    QFETCH(int, masterId);
    QFETCH(QList<int>, expectedChangedIds);
    QFETCH(QList<int>, expectedMasterIds);

    Database *db = Database::instance();
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db);

    QList<Database::Photo> allPhotos;
    Q_FOREACH(int id, versions) {
        Database::Photo photo;
        dbMock->setPhotoId(photo, id);
        dbMock->setPhotoVersionFlags(photo, true, id == masterId);
        allPhotos.append(photo);
    }
    dbMock->setPhotos(allPhotos);

    QSignalSpy makeVersionCalled(dbMock,
                                 SIGNAL(makeVersionCalled(int,int)));

    if (expectedChangedIds.isEmpty()) {
        QTest::ignoreMessage(QtWarningMsg, "removeVersion: no versions found ");
    }

    Writer writer;
    writer.removeVersion(photoId);

    QCOMPARE(makeVersionCalled.count(), expectedChangedIds.count());
    QList<int> changedIds;
    QList<QVariantList> emissions = makeVersionCalled;
    Q_FOREACH(const QVariantList &args, emissions) {
        PhotoId changedId = args.at(0).toInt();
        PhotoId masterId = args.at(1).toInt();
        /* Find the changed ID i the expectedChangedIds array, and verify that
         * the corresponding masters are the same. */
        int i = expectedChangedIds.indexOf(changedId);
        QVERIFY(i >= 0);
        QCOMPARE(masterId, expectedMasterIds[i]);
        changedIds.append(changedId);
    }
    QCOMPARE(changedIds.toSet(), expectedChangedIds.toSet());

    delete db;
}

void WriterTest::testDeletePhotos_data()
{
    QTest::addColumn<QString>("photoIdString");
    QTest::addColumn<QStringList>("expectedDeletedFiles");

    QTest::newRow("empty list") <<
        "[]" <<
        QStringList();

    QTest::newRow("one file, no sidecar") <<
        "0" <<
        (QStringList() << "image0.png");

    QTest::newRow("one file, with sidecar") <<
        "1" <<
        (QStringList() << "image1.jpg" << "image1.jpg.xmp");
}

void WriterTest::testDeletePhotos()
{
    QFETCH(QString, photoIdString);
    QFETCH(QStringList, expectedDeletedFiles);

    QTemporaryDir tmp;
    QDir tmpDir(tmp.path());


    Database *db = Database::instance();

    QStringList allFiles;
    allFiles << "image0.png" <<
        "image1.jpg" << "image1.jpg.xmp" <<
        "house.jpg";
    Q_FOREACH(const QString &fileName, allFiles) {
        QString filePath = tmpDir.filePath(fileName);
        QFile::copy(QString(ITEMS_DIR) + "/image0.png", filePath);

        if (!fileName.endsWith(".xmp")) {
            Database::Photo photo;
            photo.setBaseUri("file://" + tmpDir.absolutePath());
            photo.setFileName(fileName);
            db->addPhoto(photo, 1);
        }
    }

    QQmlEngine engine;
    qmlRegisterType<Writer>("MyTest", 1, 0, "Writer");
    QQmlComponent component(&engine);
    QByteArray idsText = photoIdString.toUtf8();
    component.setData("import MyTest 1.0\n"
                      "Writer {\n"
                      "  function run() {\n"
                      "    deletePhotos(" + idsText + ")\n"
                      "  }\n"
                      "}",
                      QUrl());
    QObject *object = component.create();
    QVERIFY(object != 0);

    bool ok = QMetaObject::invokeMethod(object, "run");
    QVERIFY(ok);

    QStringList remainingFiles = tmpDir.entryList();
    QStringList deletedFiles;
    Q_FOREACH(const QString &fileName, allFiles) {
        if (!remainingFiles.contains(fileName)) {
            deletedFiles.append(fileName);
        }
    }

    QCOMPARE(deletedFiles.toSet(), expectedDeletedFiles.toSet());

    delete object;
    delete db;
}

void WriterTest::testNoXmp()
{
    // Create the images in the DB
    QDir dataDir(ITEMS_DIR);

    QScopedPointer<Database> db(Database::instance());
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db.data());

    dbMock->setTags(QStringList{ "one", "two" });

    Database::Photo photo;
    photo.setBaseUri("file://" + dataDir.absolutePath());
    photo.setFileName("image0.png");
    PhotoId id = db->addPhoto(photo, 1);
    QCOMPARE(id, 0);

    photo.setFileName("image1.jpg");
    id = db->addPhoto(photo, 1);
    QCOMPARE(id, 1);

    QSignalSpy setDescriptionCalled(dbMock,
                                    SIGNAL(setDescriptionCalled(int,QString)));
    QSignalSpy addTagsCalled(dbMock, &DatabasePrivate::addTagsCalled);
    QSignalSpy setLocationCalled(dbMock,
                                 SIGNAL(setLocationCalled(int,GeoPoint)));
    QSignalSpy setRatingCalled(dbMock,
                               SIGNAL(setRatingCalled(int,int)));
    QSignalSpy tagChanged(db.data(), SIGNAL(tagChanged(Tag)));
    QSignalSpy tagDeleted(db.data(), SIGNAL(tagDeleted(Tag)));
    QSignalSpy photoTagAreasChanged(db.data(),
                                    SIGNAL(photoTagAreasChanged(PhotoId)));

    QScopedPointer<JobExecutor> executor(JobExecutor::instance());
    JobExecutorPrivate *executorMock =
        JobExecutorPrivate::mocked(executor.data());
    QSignalSpy addJobsCalled(executorMock,
                             SIGNAL(addJobsCalled(QVector<Imaginario::Job>)));

    Writer writer;
    writer.setWriteXmp(false);
    writer.setTitle(1, "A title");
    writer.setDescription(1, "The description");
    writer.changePhotoTags(1, QStringList{ "one", "two" }, QStringList{});
    writer.updateTag(db->tag("one"), QVariantMap{{"name", "three"}});
    writer.deleteTag(db->tag("two"));

    QJSEngine engine;
    QJSValue region = engine.newObject();
    region.setProperty("tag",
                       engine.toScriptValue(db->createTag(Tag(), "Tom", "")));
    region.setProperty("rect",
                       engine.toScriptValue(QRectF(0.1, 0.2, 0.3, 0.4)));
    QJSValue regions = engine.newArray(1);
    regions.setProperty(0, region);
    writer.setRegions(1, regions);

    writer.setRating(0, 4);
    writer.setLocation(1, GeoPoint(45, 20));


    QCOMPARE(setDescriptionCalled.count(), 1);
    QCOMPARE(addTagsCalled.count(), 1);
    QCOMPARE(setLocationCalled.count(), 1);
    QCOMPARE(setRatingCalled.count(), 1);
    QCOMPARE(tagChanged.count(), 1);
    QCOMPARE(tagDeleted.count(), 1);
    QCOMPARE(photoTagAreasChanged.count(), 1);

    QCOMPARE(addJobsCalled.count(), 0);
}

void WriterTest::testRebuildThumbnails_data()
{
    QTest::addColumn<QString>("photoIdString");
    QTest::addColumn<QStringList>("expectedFiles");
    QTest::addColumn<QList<int>>("expectedIds");

    QTest::newRow("empty list") <<
        "[]" <<
        QStringList() <<
        QList<int>{};

    QTest::newRow("one file") <<
        "0" <<
        QStringList { "image0.png" } <<
        QList<int>{ 0 };

    QTest::newRow("two files") <<
        "[0, 1]" <<
        QStringList { "image0.png", "image1.jpg" } <<
        QList<int>{ 0, 1 };
}

void WriterTest::testRebuildThumbnails()
{
    QFETCH(QString, photoIdString);
    QFETCH(QStringList, expectedFiles);
    QFETCH(QList<int>, expectedIds);

    QTemporaryDir tmp;
    QDir tmpDir(tmp.path());

    QScopedPointer<Database> db(Database::instance());

    QStringList allFiles {
        "image0.png",
        "image1.jpg",
        "house.jpg",
    };
    Q_FOREACH(const QString &fileName, allFiles) {
        QString filePath = tmpDir.filePath(fileName);
        QFile::copy(QString(ITEMS_DIR) + "/image0.png", filePath);

        Database::Photo photo;
        photo.setBaseUri("file://" + tmpDir.absolutePath());
        photo.setFileName(fileName);
        db->addPhoto(photo, 1);
    }

    QScopedPointer<JobExecutor> executor(JobExecutor::instance());
    JobExecutorPrivate *executorMock =
        JobExecutorPrivate::mocked(executor.data());
    QSignalSpy addJobsCalled(executorMock,
                             SIGNAL(addJobsCalled(QVector<Imaginario::Job>)));

    QQmlEngine engine;
    qmlRegisterType<Writer>("MyTest", 1, 0, "Writer");
    QQmlComponent component(&engine);
    QByteArray idsText = photoIdString.toUtf8();
    component.setData("import MyTest 1.0\n"
                      "Writer {\n"
                      "  function run() {\n"
                      "    rebuildThumbnails(" + idsText + ")\n"
                      "  }\n"
                      "}",
                      QUrl());
    QObject *object = component.create();
    QVERIFY(object != 0);

    QSignalSpy thumbnailUpdated(object, SIGNAL(thumbnailUpdated(int)));

    bool ok = QMetaObject::invokeMethod(object, "run");
    QVERIFY(ok);

    QVector<Job> jobs = addedJobs(addJobsCalled);
    QStringList fileNames;
    for (const Job &j: jobs) {
        QCOMPARE(j.type(), Job::Thumbnail);
        ThumbnailJob job = static_cast<const ThumbnailJob &>(j);
        QFileInfo fileInfo(job.uri().toLocalFile());
        QCOMPARE(fileInfo.absolutePath(), tmpDir.absolutePath());
        fileNames.append(fileInfo.fileName());

        executorMock->emitThumbnailUpdated(job.uri());
    }

    QCOMPARE(fileNames.toSet(), expectedFiles.toSet());

    QSet<int> thumbnailIds;
    for (const auto &signal: thumbnailUpdated) {
        thumbnailIds.insert(signal.at(0).toInt());
    }
    QCOMPARE(thumbnailIds, expectedIds.toSet());

    delete object;
}

void WriterTest::testCreateItem_data()
{
    QTest::addColumn<QString>("url");
    QTest::addColumn<bool>("expectedSuccess");
    QTest::addColumn<QString>("expectedBaseUri");
    QTest::addColumn<QString>("expectedFileName");
    QTest::addColumn<QString>("expectedDescription");
    QTest::addColumn<QDateTime>("expectedTime");
    QTest::addColumn<int>("expectedRating");
    QTest::addColumn<GeoPoint>("expectedLocation");

    QTest::newRow("empty file name") <<
        "" <<
        false <<
        "" << "" << "" << QDateTime() << 0 << GeoPoint();

    QTest::newRow("non existing file") <<
        "file://image0.png" <<
        false <<
        "" << "" << "" << QDateTime() << 0 << GeoPoint();

    QTest::newRow("existing file") <<
        "file://${TMPDIR}/image0.png" <<
        true <<
        "file://${TMPDIR}" <<
        "image0.png" <<
        "A house" <<
        QDateTime::fromString("2000-01-17T19:20:30", Qt::ISODate) <<
        3 <<
        GeoPoint(20.0, 40.0);
}

void WriterTest::testCreateItem()
{
    QFETCH(QString, url);
    QFETCH(bool, expectedSuccess);
    QFETCH(QString, expectedBaseUri);
    QFETCH(QString, expectedFileName);
    QFETCH(QString, expectedDescription);
    QFETCH(QDateTime, expectedTime);
    QFETCH(int, expectedRating);
    QFETCH(GeoPoint, expectedLocation);

    QTemporaryDir tmp;
    QDir tmpDir(tmp.path());

    QScopedPointer<Database> db(Database::instance());

    QStringList allFiles {
        "image0.png",
        "image1.jpg",
        "house.jpg",
    };
    Q_FOREACH(const QString &fileName, allFiles) {
        QString filePath = tmpDir.filePath(fileName);
        QFile::copy(QString(ITEMS_DIR) + "/image0.png", filePath);
    }
    url = url.replace("${TMPDIR}", tmpDir.path());
    expectedBaseUri = expectedBaseUri.replace("${TMPDIR}", tmpDir.path());

    MetadataPrivate *mdMocked = new MetadataPrivate();
    Metadata::ImportData data;
    data.title = expectedDescription;
    data.time = expectedTime;
    data.rating = expectedRating;
    data.location = expectedLocation.toGeoCoordinate();
    mdMocked->setImportData(QUrl(url).toLocalFile(), data);

    QQmlEngine engine;
    qmlRegisterType<Writer>("MyTest", 1, 0, "Writer");
    QQmlComponent component(&engine);
    QByteArray urlText = url.toUtf8();
    component.setData("import MyTest 1.0\n"
                      "Writer {\n"
                      "  function run() {\n"
                      "    return createItem(\"" + urlText + "\")\n"
                      "  }\n"
                      "}",
                      QUrl());
    QObject *object = component.create();
    QVERIFY(object != 0);

    QVariant ret;
    bool ok = QMetaObject::invokeMethod(object, "run",
                                        Q_RETURN_ARG(QVariant, ret));
    QVERIFY(ok);

    if (expectedSuccess) {
        QVERIFY(ret.toInt() >= 0);

        Database::Photo photo = db->photo(ret.toInt());
        QVERIFY(photo.isValid());
        QCOMPARE(photo.baseUri(), expectedBaseUri);
        QCOMPARE(photo.fileName(), expectedFileName);
        QCOMPARE(photo.description(), expectedDescription);
        QCOMPARE(photo.time(), expectedTime);
        QCOMPARE(photo.rating(), expectedRating);
        QCOMPARE(photo.location(), expectedLocation);
    } else {
        QCOMPARE(ret.toInt(), -1);
    }

    delete object;
}

QTEST_GUILESS_MAIN(WriterTest)

#include "tst_writer.moc"
