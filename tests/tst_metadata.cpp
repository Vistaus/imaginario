/*
 * Copyright (C) 2014-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "metadata.h"
#include "test_utils.h"

#include <QDateTime>
#include <QTemporaryDir>
#include <QTest>

using namespace Imaginario;

class MetadataTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testImport_data();
    void testImport();
    void testWriteTags_data();
    void testWriteTags();
    void testWriteRegions_data();
    void testWriteRegions();
    void testWriteRating_data();
    void testWriteRating();
    void testWriteLocation_data();
    void testWriteLocation();
    void testWriteTitle_data();
    void testWriteTitle();
    void testWriteDescription_data();
    void testWriteDescription();
    void testWriteFreeFields_data();
    void testWriteFreeFields();
    void testConcurrentChanges();
    void testDeletionWhenActive();
    void testLoadPreview_data();
    void testLoadPreview();
    void testTagDeletion();
};

void MetadataTest::testImport_data()
{
    QTest::addColumn<QString>("fileName");
    QTest::addColumn<QString>("title");
    QTest::addColumn<QString>("description");
    QTest::addColumn<QDateTime>("time");
    QTest::addColumn<QStringList>("tags");
    QTest::addColumn<Metadata::Regions>("regions");
    QTest::addColumn<int>("rating");
    QTest::addColumn<QGeoCoordinate>("location");

    Metadata::Regions regions;

    QFileInfo fileInfo(QString(ITEMS_DIR) + "/image0.png");
    QTest::newRow("no metadata") <<
        "image0.png" <<
        "" <<
        "" <<
        fileInfo.created() <<
        QStringList() <<
        regions <<
        int(-1) <<
        QGeoCoordinate();

    QTest::newRow("with xmp") <<
        "image1.jpg" <<
        "Small" <<
        "A tiny image" <<
        QDateTime::fromString("2015-01-06T10:57:57", Qt::ISODate) <<
        (QStringList() << "One image" << "cute") <<
        regions <<
        int(2) <<
        QGeoCoordinate(45.668546296, -12.2350138883);

    QTest::newRow("with exif") <<
        "image2.jpg" <<
        "" <<
        "A small image" <<
        QDateTime::fromString("2015-01-06T10:57:57", Qt::ISODate) <<
        QStringList() <<
        regions <<
        int(-1) <<
        QGeoCoordinate(45.668546296, 12.2350138883);

    QTest::newRow("with iptc") <<
        "image4.jpg" <<
        "The Earth" <<
        "Our lovely planet" <<
        QDateTime::fromString("2015-01-06T10:57:57", Qt::ISODate) <<
        QStringList() <<
        regions <<
        int(-1) <<
        QGeoCoordinate(45.668546296, 12.2350138883);

    regions.append(Metadata::RegionInfo(QRectF(0, 0.6, 0.6, 0.4),
                                        "Anna", QString()));
    regions.append(Metadata::RegionInfo(QRectF(0.4, 0, 0.2, 0.6),
                                        "Tom Dick", "My best friend Tom"));
    QTest::newRow("no metadata, xmp sidecar") <<
        "image3.png" <<
        "The other side" <<
        "A side image" <<
        QDateTime::fromString("2015-02-06T10:57:57", Qt::ISODate) <<
        (QStringList() << "info" << "by the side") <<
        regions <<
        int(4) <<
        QGeoCoordinate(45.668546296, 12.2350138883);
}

void MetadataTest::testImport()
{
    QFETCH(QString, fileName);
    QFETCH(QString, title);
    QFETCH(QString, description);
    QFETCH(QDateTime, time);
    QFETCH(QStringList, tags);
    QFETCH(Metadata::Regions, regions);
    QFETCH(int, rating);
    QFETCH(QGeoCoordinate, location);

    Metadata m;
    Metadata::ImportData data;
    bool ok = m.readImportData(QString(ITEMS_DIR) + "/" + fileName, data);
    QVERIFY(ok);

    QCOMPARE(data.title, title);
    QCOMPARE(data.description, description);
    QCOMPARE(data.time, time);
    QCOMPARE(data.tags.toSet(), tags.toSet());
    QCOMPARE(data.regions.toList().toSet(), regions.toList().toSet());
    QCOMPARE(data.rating, rating);
    QCOMPARE(data.location.toString(QGeoCoordinate::DegreesMinutes),
             location.toString(QGeoCoordinate::DegreesMinutes));
}

void MetadataTest::testWriteTags_data()
{
    QTest::addColumn<QString>("fileName");
    QTest::addColumn<bool>("embed");
    QTest::addColumn<QStringList>("tags");
    QTest::addColumn<QString>("xmpFileName");

    QTest::newRow("JPEG, embedding") <<
        "image1.jpg" <<
        true <<
        (QStringList() << "sun" << "a flower") <<
        "image1.jpg";

    QTest::newRow("JPEG, no embedding") <<
        "image1.jpg" <<
        false <<
        (QStringList() << "one" << "two" << "three") <<
        "image1.jpg.xmp";
}

void MetadataTest::testWriteTags()
{
    QFETCH(QString, fileName);
    QFETCH(bool, embed);
    QFETCH(QStringList, tags);
    QFETCH(QString, xmpFileName);

    QTemporaryDir tmpDir;
    QString filePath = tmpDir.path() + "/" + fileName;
    QFile::copy(QString(ITEMS_DIR) + "/" + fileName, filePath);

    Metadata m;
    m.setEmbed(embed);
    m.writeTags(filePath, tags);

    Metadata::ImportData data;
    bool ok = m.readImportData(tmpDir.path() + "/" + xmpFileName, data);
    QVERIFY(ok);

    QCOMPARE(data.tags.toSet(), tags.toSet());
}

void MetadataTest::testWriteRegions_data()
{
    QTest::addColumn<QString>("fileName");
    QTest::addColumn<bool>("embed");
    QTest::addColumn<Metadata::Regions>("regions");
    QTest::addColumn<QString>("xmpFileName");

    Metadata::Regions regions;

    regions.append(Metadata::RegionInfo(QRectF(0.4, 0.5, 0.6, 0.4),
                                        "John", QString()));
    regions.append(Metadata::RegionInfo(QRectF(0.2, 0.3, 0.2, 0.6),
                                        "James", "Funny guy"));
    QTest::newRow("JPEG, embedding") <<
        "image1.jpg" <<
        true <<
        regions <<
        "image1.jpg";
    regions.clear();

    regions.append(Metadata::RegionInfo(QRectF(0.2, 0.1, 0.6, 0.4),
                                        "Harry", QString()));
    QTest::newRow("JPEG, no embedding") <<
        "image1.jpg" <<
        false <<
        regions <<
        "image1.jpg.xmp";
}

void MetadataTest::testWriteRegions()
{
    QFETCH(QString, fileName);
    QFETCH(bool, embed);
    QFETCH(Metadata::Regions, regions);
    QFETCH(QString, xmpFileName);

    QTemporaryDir tmpDir;
    QString filePath = tmpDir.path() + "/" + fileName;
    QFile::copy(QString(ITEMS_DIR) + "/" + fileName, filePath);

    Metadata m;
    m.setEmbed(embed);
    /* Write some initial regions, to verify that clearing works */
    Metadata::Regions oldRegions;
    oldRegions.append(Metadata::RegionInfo(QRectF(0.1, 0.5, 0.6, 0.4),
                                           "Sam", QString()));
    oldRegions.append(Metadata::RegionInfo(QRectF(0.2, 0.1, 0.2, 0.6),
                                           "Bill", "Sad guy"));
    m.writeRegions(filePath, oldRegions);

    m.writeRegions(filePath, regions);

    Metadata::ImportData data;
    bool ok = m.readImportData(tmpDir.path() + "/" + xmpFileName, data);
    QVERIFY(ok);

    QCOMPARE(data.regions.toList().toSet(), regions.toList().toSet());
}

void MetadataTest::testWriteRating_data()
{
    QTest::addColumn<QString>("fileName");
    QTest::addColumn<bool>("embed");
    QTest::addColumn<int>("rating");
    QTest::addColumn<QString>("xmpFileName");

    QTest::newRow("JPEG, embedding") <<
        "image1.jpg" <<
        true <<
        int(3) <<
        "image1.jpg";

    QTest::newRow("JPEG, embedding, clearing") <<
        "image1.jpg" <<
        true <<
        int(-2) <<
        "image1.jpg";

    QTest::newRow("JPEG, no embedding") <<
        "image1.jpg" <<
        false <<
        int(1) <<
        "image1.jpg.xmp";

    QTest::newRow("JPEG, no embedding, clearing") <<
        "image1.jpg" <<
        false <<
        int(-1) <<
        "image1.jpg.xmp";
}

void MetadataTest::testWriteRating()
{
    QFETCH(QString, fileName);
    QFETCH(bool, embed);
    QFETCH(int, rating);
    QFETCH(QString, xmpFileName);

    QTemporaryDir tmpDir;
    QString filePath = tmpDir.path() + "/" + fileName;
    QFile::copy(QString(ITEMS_DIR) + "/" + fileName, filePath);

    Metadata m;
    m.setEmbed(embed);
    m.writeRating(filePath, rating);

    Metadata::ImportData data;
    bool ok = m.readImportData(tmpDir.path() + "/" + xmpFileName, data);
    QVERIFY(ok);

    /* A negative rating just means that rating must be removed from the
     * metadata, so when we read it back, we expect to find -1 there. */
    if (rating < 0) rating = -1;
    QCOMPARE(data.rating, rating);
}

void MetadataTest::testWriteLocation_data()
{
    QTest::addColumn<QString>("fileName");
    QTest::addColumn<bool>("embed");
    QTest::addColumn<QGeoCoordinate>("location");
    QTest::addColumn<QString>("xmpFileName");

    QTest::newRow("JPEG, embedding") <<
        "image1.jpg" <<
        true <<
        QGeoCoordinate(24.987654, 12.1234512) <<
        "image1.jpg";

    QTest::newRow("JPEG, embedding, clearing") <<
        "image1.jpg" <<
        true <<
        QGeoCoordinate() <<
        "image1.jpg";

    QTest::newRow("JPEG, no embedding") <<
        "image3.png" <<
        false <<
        QGeoCoordinate(-24.98165, -12.12145321) <<
        "image3.png.xmp";

    QTest::newRow("JPEG, no embedding, clearing") <<
        "image1.jpg" <<
        false <<
        QGeoCoordinate() <<
        "image1.jpg.xmp";
}

void MetadataTest::testWriteLocation()
{
    QFETCH(QString, fileName);
    QFETCH(bool, embed);
    QFETCH(QGeoCoordinate, location);
    QFETCH(QString, xmpFileName);

    QTemporaryDir tmpDir;
    QString filePath = tmpDir.path() + "/" + fileName;
    QFile::copy(QString(ITEMS_DIR) + "/" + fileName, filePath);

    Metadata m;
    m.setEmbed(embed);
    m.writeLocation(filePath, location);

    Metadata::ImportData data;
    bool ok = m.readImportData(tmpDir.path() + "/" + xmpFileName, data);
    QVERIFY(ok);

    /* Compare the string representation to take fuzziness into account */
    QCOMPARE(QTest::toString(data.location), QTest::toString(location));
}

void MetadataTest::testWriteTitle_data()
{
    QTest::addColumn<QString>("fileName");
    QTest::addColumn<bool>("embed");
    QTest::addColumn<QString>("title");
    QTest::addColumn<QString>("xmpFileName");

    QTest::newRow("JPEG, embedding") <<
        "image1.jpg" <<
        true <<
        "A house" <<
        "image1.jpg";

    QTest::newRow("JPEG, embedding, clearing") <<
        "image1.jpg" <<
        true <<
        QString() <<
        "image1.jpg";

    QTest::newRow("JPEG, no embedding") <<
        "image1.jpg" <<
        false <<
        "Small dog!" <<
        "image1.jpg.xmp";

    QTest::newRow("JPEG, no embedding, clearing") <<
        "image1.jpg" <<
        false <<
        QString() <<
        "image1.jpg.xmp";
}

void MetadataTest::testWriteTitle()
{
    QFETCH(QString, fileName);
    QFETCH(bool, embed);
    QFETCH(QString, title);
    QFETCH(QString, xmpFileName);

    QTemporaryDir tmpDir;
    QString filePath = tmpDir.path() + "/" + fileName;
    QFile::copy(QString(ITEMS_DIR) + "/" + fileName, filePath);

    Metadata m;
    m.setEmbed(embed);
    m.writeTitle(filePath, title);

    Metadata::ImportData data;
    bool ok = m.readImportData(tmpDir.path() + "/" + xmpFileName, data);
    QVERIFY(ok);

    QCOMPARE(data.title, title);
}

void MetadataTest::testWriteDescription_data()
{
    QTest::addColumn<QString>("fileName");
    QTest::addColumn<bool>("embed");
    QTest::addColumn<QString>("description");
    QTest::addColumn<QString>("xmpFileName");

    QTest::newRow("JPEG, embedding") <<
        "image1.jpg" <<
        true <<
        "Little big adventure" <<
        "image1.jpg";

    QTest::newRow("JPEG, embedding, clearing") <<
        "image1.jpg" <<
        true <<
        QString() <<
        "image1.jpg";

    QTest::newRow("JPEG, no embedding") <<
        "image1.jpg" <<
        false <<
        "# cool #" <<
        "image1.jpg.xmp";

    QTest::newRow("JPEG, no embedding, clearing") <<
        "image1.jpg" <<
        false <<
        QString() <<
        "image1.jpg.xmp";
}

void MetadataTest::testWriteDescription()
{
    QFETCH(QString, fileName);
    QFETCH(bool, embed);
    QFETCH(QString, description);
    QFETCH(QString, xmpFileName);

    QTemporaryDir tmpDir;
    QString filePath = tmpDir.path() + "/" + fileName;
    QFile::copy(QString(ITEMS_DIR) + "/" + fileName, filePath);

    Metadata m;
    m.setEmbed(embed);
    m.writeDescription(filePath, description);

    Metadata::ImportData data;
    bool ok = m.readImportData(tmpDir.path() + "/" + xmpFileName, data);
    QVERIFY(ok);

    QCOMPARE(data.description, description);
}

void MetadataTest::testWriteFreeFields_data()
{
    QTest::addColumn<QVariantMap>("fields");

    QTest::newRow("IPTC caption") <<
        QVariantMap {
            { "Iptc.Application2.Caption", QString("The title") },
        };

    QTest::newRow("IPTC keywords") <<
        QVariantMap {
            { "Iptc.Application2.Keywords",
                QStringList {
                    "Some tag, with comma",
                    "another tag",
                }
            },
        };

    QTest::newRow("Mixed keys") <<
        QVariantMap {
            { "Iptc.Application2.Subject",
                QStringList { "Tom", "Dick", "Harry" }
            },
            { "Iptc.Application2.ObjectName", "A nice family photo" },
            { "Iptc.Application2.ObjectName", "A nice family photo" },
            { "Exif.Image.Software", "Imaginario, of course" },
            { "Xmp.xmp.Nickname", "Just a nick" },
            { "Xmp.dc.subject", QStringList { "one", "two", "three" }},
        };
}

void MetadataTest::testWriteFreeFields()
{
    QFETCH(QVariantMap, fields);

    QTemporaryDir tmpDir;

    QString fileName("image1.jpg");
    QString filePath = tmpDir.path() + "/" + fileName;
    QFile::copy(QString(ITEMS_DIR) + "/" + fileName, filePath);

    Metadata::Changes changes;
    for (auto i = fields.constBegin(); i != fields.constEnd(); i++) {
        changes.setFreeField(i.key(), i.value());
    }

    Metadata m;
    m.setEmbed(true);
    m.writeChanges(filePath, changes);

    Metadata::ImportData data;

    for (auto i = fields.constBegin(); i != fields.constEnd(); i++) {
        QVariant valueFromFile =
            m.readFreeField(filePath, i.key(),
                            QMetaType::Type(i.value().type()));
        QCOMPARE(valueFromFile, i.value());
    }
}

void MetadataTest::testConcurrentChanges()
{
    QTemporaryDir tmpDir;
    QString filePath = tmpDir.path() + "/image.jpg";
    QFile::copy(QString(ITEMS_DIR) + "/image1.jpg", filePath);

    Metadata m;
    m.setEmbed(true);
    QStringList allTags;
    allTags << "one" << "two" << "three" << "four" << "five";
    QString lastTag;
    int lastRating;
    for (int i = 0; i < 20; i++) {
        lastTag = allTags[i % allTags.count()];
        m.writeTags(filePath, QStringList() << lastTag);
        lastRating = i % 8;
        m.writeRating(filePath, lastRating);
    }

    Metadata::ImportData data;
    bool ok = m.readImportData(filePath, data);
    QVERIFY(ok);

    QCOMPARE(data.tags, (QStringList() << lastTag));
    QCOMPARE(data.rating, lastRating);
}

void MetadataTest::testDeletionWhenActive()
{
    QTemporaryDir tmpDir;
    QString filePath = tmpDir.path() + "/image.jpg";
    QFile::copy(QString(ITEMS_DIR) + "/image1.jpg", filePath);

    Metadata *m = new Metadata;
    m->setEmbed(true);
    QStringList allTags;
    allTags << "one" << "two" << "three" << "four" << "five";
    QString lastTag;
    int lastRating;
    for (int i = 0; i < 20; i++) {
        lastTag = allTags[i % allTags.count()];
        m->writeTags(filePath, QStringList() << lastTag);
        lastRating = i % 8;
        m->writeRating(filePath, lastRating);
    }
    delete m;

    Metadata::ImportData data;
    m = new Metadata;
    bool ok = m->readImportData(filePath, data);
    delete m;

    QVERIFY(ok);

    QCOMPARE(data.tags, (QStringList() << lastTag));
    QCOMPARE(data.rating, lastRating);
}

void MetadataTest::testLoadPreview_data()
{
    QTest::addColumn<QString>("fileName");
    QTest::addColumn<QSize>("requestedSize");
    QTest::addColumn<bool>("readSize");
    QTest::addColumn<QSize>("expectedSize");
    QTest::addColumn<bool>("isValid");

    QTest::newRow("no file") <<
        "missing.jpg" <<
        QSize(10, 10) <<
        true << QSize() <<
        false;

    QTest::newRow("no previews") <<
        "image0.png" <<
        QSize(10, 10) <<
        false << QSize() <<
        false;

    QTest::newRow("no previews, read size") <<
        "image0.png" <<
        QSize(10, 10) <<
        true << QSize() <<
        false;

    QTest::newRow("with preview, read size") <<
        "image1.jpg" <<
        QSize(1, 1) <<
        true << QSize(1,1) <<
        true;

    QTest::newRow("with preview, too small") <<
        "image1.jpg" <<
        QSize(10, 10) <<
        true << QSize() <<
        false;
}

void MetadataTest::testLoadPreview()
{
    QFETCH(QString, fileName);
    QFETCH(QSize, requestedSize);
    QFETCH(bool, readSize);
    QFETCH(QSize, expectedSize);
    QFETCH(bool, isValid);

    Metadata m;
    QSize size;
    QString file = QString(ITEMS_DIR) + "/" + fileName;
    QImage preview = m.loadPreview(file, readSize ? &size : 0,
                                   requestedSize);
    QCOMPARE(preview.isNull(), !isValid);

    if (readSize) {
        QCOMPARE(size, expectedSize);
    }

    if (isValid) {
        QCOMPARE(preview.size(), requestedSize);
    }
}

void MetadataTest::testTagDeletion()
{
    QString fileName("image1.jpg");
    QString title("The seaside");
    QString description("Nice view, isn't it?");
    QStringList tags { "one", "two", "three" };
    QGeoCoordinate location(34.56, 78.90);

    QTemporaryDir tmpDir;
    QString filePath = tmpDir.path() + "/" + fileName;
    QFile::copy(QString(ITEMS_DIR) + "/" + fileName, filePath);

    Metadata m;
    m.setEmbed(true);

    /* First, write some tags */
    m.writeTitle(filePath, title);
    m.writeDescription(filePath, description);
    m.writeTags(filePath, tags);

    /* Writing the location causes the previous location keys to be deleted; we
     * want to test that this only deletes the location keys and not also some
     * of the other keys we just wrote */
    m.writeLocation(filePath, location);

    Metadata::ImportData data;
    bool ok = m.readImportData(tmpDir.path() + "/" + fileName, data);
    QVERIFY(ok);

    QCOMPARE(data.title, title);
    QCOMPARE(data.description, description);
    QCOMPARE(data.tags, tags);
    /* Compare the string representation to take fuzziness into account */
    QCOMPARE(QTest::toString(data.location), QTest::toString(location));
}

QTEST_GUILESS_MAIN(MetadataTest)

#include "tst_metadata.moc"
