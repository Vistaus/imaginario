PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE AlbumRoots
                            (id INTEGER PRIMARY KEY,
                            label TEXT,
                            status INTEGER NOT NULL,
                            type INTEGER NOT NULL,
                            identifier TEXT,
                            specificPath TEXT,
                            UNIQUE(identifier, specificPath));
INSERT INTO "AlbumRoots" VALUES(1,NULL,0,1,'volumeid:?uuid=c3f37c59-c3fb-4356-aade-222a19d0db92','/home/mardy/Pictures/Photos');
CREATE TABLE Albums
                            (id INTEGER PRIMARY KEY,
                            albumRoot INTEGER NOT NULL,
                            relativePath TEXT NOT NULL,
                            date DATE,
                            caption TEXT,
                            collection TEXT,
                            icon INTEGER,
                            UNIQUE(albumRoot, relativePath));
INSERT INTO "Albums" VALUES(1,1,'/','2019-01-03',NULL,NULL,NULL);
INSERT INTO "Albums" VALUES(3,1,'/2014','2016-04-25',NULL,NULL,NULL);
INSERT INTO "Albums" VALUES(4,1,'/2014/08','2016-04-25',NULL,NULL,NULL);
INSERT INTO "Albums" VALUES(5,1,'/2014/08/01','2016-04-25',NULL,NULL,NULL);
INSERT INTO "Albums" VALUES(6,1,'/2015','2016-06-21',NULL,NULL,NULL);
INSERT INTO "Albums" VALUES(7,1,'/2015/01','2016-04-25',NULL,NULL,NULL);
INSERT INTO "Albums" VALUES(8,1,'/2015/01/02','2016-04-25',NULL,NULL,32);
INSERT INTO "Albums" VALUES(9,1,'/2015/08','2016-04-25',NULL,NULL,NULL);
INSERT INTO "Albums" VALUES(10,1,'/2015/08/23','2016-04-25',NULL,NULL,NULL);
INSERT INTO "Albums" VALUES(11,1,'/2015/10','2016-04-25',NULL,NULL,NULL);
INSERT INTO "Albums" VALUES(12,1,'/2015/10/11','2016-04-25',NULL,NULL,NULL);
INSERT INTO "Albums" VALUES(13,1,'/2015/11','2016-06-21',NULL,NULL,NULL);
INSERT INTO "Albums" VALUES(14,1,'/2015/11/22','2016-06-21',NULL,NULL,NULL);
INSERT INTO "Albums" VALUES(15,1,'/2016','2016-04-25',NULL,NULL,NULL);
INSERT INTO "Albums" VALUES(16,1,'/2016/01','2016-04-25',NULL,NULL,NULL);
INSERT INTO "Albums" VALUES(17,1,'/2016/01/27','2016-04-25',NULL,NULL,NULL);
INSERT INTO "Albums" VALUES(18,1,'/2016/04','2016-04-25',NULL,NULL,NULL);
INSERT INTO "Albums" VALUES(19,1,'/2016/04/19','2016-04-25',NULL,NULL,NULL);
CREATE TABLE Images
                            (id INTEGER PRIMARY KEY,
                            album INTEGER,
                            name TEXT NOT NULL,
                            status INTEGER NOT NULL,
                            category INTEGER NOT NULL,
                            modificationDate DATETIME,
                            fileSize INTEGER,
                            uniqueHash TEXT,
                            UNIQUE (album, name));
INSERT INTO "Images" VALUES(1,NULL,'000002.JPG',3,1,'2018-10-01T23:34:38',1493864,'8ddeda0cdc59632186e3603bd1f58906');
INSERT INTO "Images" VALUES(2,NULL,'000007.JPG',3,1,'2018-10-01T23:34:46',1620141,'0cd82da8b73e217c4c6064fc1cd66868');
INSERT INTO "Images" VALUES(3,NULL,'000008.JPG',3,1,'2018-10-01T23:34:54',1281894,'19e1fea7c15cecb6ae9c83d1be780098');
INSERT INTO "Images" VALUES(4,NULL,'000009.JPG',3,1,'2018-10-01T23:35:02',1652219,'3a3e259b87e801a8203b86f15eda6d08');
INSERT INTO "Images" VALUES(5,NULL,'000010.JPG',3,1,'2018-10-01T23:35:08',1627219,'81f64cd944361181a8c5659b4f8e68e1');
INSERT INTO "Images" VALUES(6,NULL,'000011.JPG',3,1,'2018-10-01T23:35:16',1618049,'f953f584bc54360ec4a55e04f2d719e7');
INSERT INTO "Images" VALUES(7,NULL,'000012.JPG',3,1,'2018-10-01T23:35:24',1531457,'b115e3ca62151df0b22f90467a6739ed');
INSERT INTO "Images" VALUES(8,NULL,'000013.JPG',3,1,'2018-10-01T23:35:32',1706678,'1afd38c2f3b04c443321c68f04876867');
INSERT INTO "Images" VALUES(9,NULL,'000014.JPG',3,1,'2018-10-01T23:35:40',1630988,'3d74f4de7a2c0bffe92d31759014e4d3');
INSERT INTO "Images" VALUES(10,NULL,'000015.JPG',3,1,'2018-10-01T23:35:44',1535455,'47f0dd13c6c59abc9969ce0270fc0c42');
INSERT INTO "Images" VALUES(11,NULL,'000016.JPG',3,1,'2018-10-01T23:35:52',1600600,'00b2e1ca700ff0d68cef18b3b7cd953f');
INSERT INTO "Images" VALUES(12,NULL,'000017.JPG',3,1,'2018-10-01T23:36:00',1589660,'34247a7d4fd3bf3deb2c666acafe6b70');
INSERT INTO "Images" VALUES(13,NULL,'000018.JPG',3,1,'2018-10-01T23:36:08',1449047,'e92b60d5ff3ce9025b3fc3c496dcf846');
INSERT INTO "Images" VALUES(14,NULL,'000019.JPG',3,1,'2018-10-01T23:36:14',1579685,'56b679a650e0297670cc9673880d4d4f');
INSERT INTO "Images" VALUES(15,NULL,'000020.JPG',3,1,'2018-10-01T23:36:22',1507432,'f1e27e6d957869ec527243c39c663441');
INSERT INTO "Images" VALUES(16,NULL,'000021.JPG',3,1,'2018-10-01T23:36:30',1565420,'7562247e78aac9b892373cebc16524b7');
INSERT INTO "Images" VALUES(17,NULL,'000022.JPG',3,1,'2018-10-01T23:36:36',1546569,'7e68c6bd2f84aaebf8bd8fdc4775ca63');
INSERT INTO "Images" VALUES(18,NULL,'000023.JPG',3,1,'2018-10-01T23:36:44',1519449,'d4a3bb8de8da9ecfe21d44761ce89d6c');
INSERT INTO "Images" VALUES(19,NULL,'000024.JPG',3,1,'2018-10-01T23:36:52',1991307,'4d68b4a6b38f21816b78ffb7724d320d');
INSERT INTO "Images" VALUES(20,NULL,'000025.JPG',3,1,'2018-10-01T23:37:00',2048320,'73267f5733b34a1fe7cf944b482efe05');
INSERT INTO "Images" VALUES(21,NULL,'000026.JPG',3,1,'2018-10-01T23:37:04',1553162,'1798bd5b0a812135bbe61e1b9cb92430');
INSERT INTO "Images" VALUES(22,NULL,'000027.JPG',3,1,'2018-10-01T23:37:12',2202258,'d009e10ccd3ae26ac527760ef9a03cea');
INSERT INTO "Images" VALUES(23,NULL,'000028.JPG',3,1,'2018-10-01T23:37:20',1551941,'3980a481d31b86449deaaab7de2bc8ad');
INSERT INTO "Images" VALUES(24,NULL,'000029.JPG',3,1,'2018-10-01T23:37:28',1645303,'3fccc4ec431a4d2b5e5f2a6ffa5a26ce');
INSERT INTO "Images" VALUES(25,5,'DSC00448 (1) (Modified (2)).jpg',1,1,'2016-04-25T08:04:27',587370,'ce4674c1af8c7bb2075de04930a78573');
INSERT INTO "Images" VALUES(26,5,'DSC00448 (1) (Modified).jpg',1,1,'2016-04-25T08:03:20',1437039,'e20e228dec6dd984318f72f15056b288');
INSERT INTO "Images" VALUES(27,5,'DSC00448 (1).jpg',1,1,'2016-03-20T15:52:56',3168274,'5451a8a37d1d7886cdc7b3ebd7a18358');
INSERT INTO "Images" VALUES(28,5,'DSC00448.jpg',1,1,'2016-03-19T14:47:16',3168274,'5451a8a37d1d7886cdc7b3ebd7a18358');
INSERT INTO "Images" VALUES(29,5,'DSC00461 (1).jpg',1,1,'2016-03-20T15:52:56',1100339,'72ce565f35b68b461901c57401b35d47');
INSERT INTO "Images" VALUES(30,5,'DSC00461.jpg',1,1,'2016-03-19T14:47:16',1100339,'72ce565f35b68b461901c57401b35d47');
INSERT INTO "Images" VALUES(31,8,'DSC02582 (1).jpg',1,1,'2016-03-20T15:52:56',1845350,'2aa699536fd49b949db854efccd62f63');
INSERT INTO "Images" VALUES(32,8,'DSC02582.jpg',1,1,'2016-03-19T14:47:16',1845350,'2aa699536fd49b949db854efccd62f63');
INSERT INTO "Images" VALUES(33,10,'DSC04967 (1).jpg',1,1,'2016-03-20T15:52:56',608789,'e3bee7acb191543265e5ce7a7c72e3b1');
INSERT INTO "Images" VALUES(34,10,'DSC04967.jpg',1,1,'2016-03-19T14:47:16',608789,'e3bee7acb191543265e5ce7a7c72e3b1');
INSERT INTO "Images" VALUES(35,10,'DSC04981 (1).jpg',1,1,'2016-03-20T15:52:56',1619607,'d336bdfddf1a3fc53b086590ef4c8679');
INSERT INTO "Images" VALUES(36,10,'DSC04981 (2).jpg',1,1,'2016-03-23T20:43:47',1619607,'d336bdfddf1a3fc53b086590ef4c8679');
INSERT INTO "Images" VALUES(37,10,'DSC04981.jpg',1,1,'2016-03-19T14:47:16',1619607,'d336bdfddf1a3fc53b086590ef4c8679');
INSERT INTO "Images" VALUES(38,12,'DSC06903 (1).jpg',1,1,'2016-03-20T15:52:56',1764790,'718f6f28fd04d1968cd529738459c524');
INSERT INTO "Images" VALUES(39,12,'DSC06903 (2).jpg',1,1,'2016-03-23T20:43:47',1764790,'718f6f28fd04d1968cd529738459c524');
INSERT INTO "Images" VALUES(40,12,'DSC06903.jpg',1,1,'2016-03-19T14:47:16',1764790,'718f6f28fd04d1968cd529738459c524');
INSERT INTO "Images" VALUES(41,NULL,'Screenshot from 2012-12-28 14:14:48.png',3,1,'2012-12-28T17:14:51',48877,'2482587348a96003d852c9ed61afd655');
INSERT INTO "Images" VALUES(42,NULL,'Screenshot from 2014-02-07 18:22:26.png',3,1,'2014-02-07T20:22:38',220782,'9e0582e3ee6ddb65939de88a15e6dbd7');
INSERT INTO "Images" VALUES(43,17,'Screenshot from 2014-12-27 21:30:34.png',1,1,'2014-12-27T23:30:37',48713,'886c332b16ac9d15301fbb73997b36b5');
INSERT INTO "Images" VALUES(44,19,'image20160419_193456352.jpg',1,1,'2016-04-19T20:47:50',2475585,'5fbbae8ea6225ba4dfeac5ed12033910');
CREATE TABLE ImageHaarMatrix
                            (imageid INTEGER PRIMARY KEY,
                            modificationDate DATETIME,
                            uniqueHash TEXT,
                            matrix BLOB);
CREATE TABLE ImageInformation
                            (imageid INTEGER PRIMARY KEY,
                            rating INTEGER,
                            creationDate DATETIME,
                            digitizationDate DATETIME,
                            orientation INTEGER,
                            width INTEGER,
                            height INTEGER,
                            format TEXT,
                            colorDepth INTEGER,
                colorModel INTEGER);
INSERT INTO "ImageInformation" VALUES(1,-1,'2018-10-01T21:52:05','2018-10-01T21:52:05',1,2751,1830,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(2,-1,'2018-10-01T21:52:05','2018-10-01T21:52:05',1,2751,1830,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(3,-1,'2018-10-01T21:52:05','2018-10-01T21:52:05',1,2751,1830,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(4,-1,'2018-10-01T21:52:05','2018-10-01T21:52:05',1,2751,1830,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(5,-1,'2018-10-01T21:52:05','2018-10-01T21:52:05',1,2751,1830,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(6,-1,'2018-10-01T21:52:12','2018-10-01T21:52:12',1,2751,1830,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(7,-1,'2018-10-01T21:52:13','2018-10-01T21:52:13',1,2751,1830,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(8,-1,'2018-10-01T21:52:13','2018-10-01T21:52:13',1,2751,1830,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(9,-1,'2018-10-01T21:52:13','2018-10-01T21:52:13',1,2751,1830,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(10,-1,'2018-10-01T21:52:13','2018-10-01T21:52:13',1,2751,1830,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(11,-1,'2018-10-01T21:52:13','2018-10-01T21:52:13',1,2751,1830,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(12,-1,'2018-10-01T21:52:21','2018-10-01T21:52:21',1,2751,1830,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(13,-1,'2018-10-01T21:52:21','2018-10-01T21:52:21',1,2751,1830,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(14,-1,'2018-10-01T21:52:21','2018-10-01T21:52:21',1,2751,1830,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(15,-1,'2018-10-01T21:52:21','2018-10-01T21:52:21',1,2751,1830,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(16,-1,'2018-10-01T21:52:21','2018-10-01T21:52:21',1,2751,1830,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(17,-1,'2018-10-01T21:52:21','2018-10-01T21:52:21',1,2751,1830,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(18,-1,'2018-10-01T21:52:28','2018-10-01T21:52:28',1,2751,1830,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(19,-1,'2018-10-01T21:52:28','2018-10-01T21:52:28',1,2751,1830,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(20,-1,'2018-10-01T21:52:28','2018-10-01T21:52:28',1,2751,1830,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(21,-1,'2018-10-01T21:52:28','2018-10-01T21:52:28',1,2751,1830,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(22,-1,'2018-10-01T21:52:29','2018-10-01T21:52:29',1,2751,1830,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(23,-1,'2018-10-01T21:52:29','2018-10-01T21:52:29',1,2751,1830,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(24,-1,'2018-10-01T21:52:32','2018-10-01T21:52:32',1,2751,1830,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(25,4,'2014-08-01T20:41:53','2014-08-01T20:41:53',1,843,1046,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(26,-1,'2014-08-01T20:41:53','2014-08-01T20:41:53',1,1369,2048,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(27,-1,'2014-08-01T20:41:53','2014-08-01T20:41:53',0,1369,2048,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(28,-1,'2014-08-01T20:41:53','2014-08-01T20:41:53',0,1369,2048,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(29,3,'2014-08-01T21:15:55','2014-08-01T21:15:55',0,2048,1369,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(30,-1,'2014-08-01T21:15:55','2014-08-01T21:15:55',0,2048,1369,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(31,2,'2015-01-02T11:25:13','2015-01-02T11:25:13',0,2048,1536,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(32,-1,'2015-01-02T11:25:13','2015-01-02T11:25:13',0,2048,1536,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(33,0,'2015-08-23T18:59:05','2015-08-23T18:59:05',1,2048,1369,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(34,0,'2015-08-23T18:59:05','2015-08-23T18:59:05',1,2048,1369,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(35,0,'2015-08-23T19:02:19','2015-08-23T19:02:19',1,2048,1369,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(36,0,'2015-08-23T19:02:19','2015-08-23T19:02:19',1,2048,1369,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(37,0,'2015-08-23T19:02:19','2015-08-23T19:02:19',1,2048,1369,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(38,-1,'2015-10-11T16:28:15','2015-10-11T16:28:15',1,2048,1369,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(39,-1,'2015-10-11T16:28:15','2015-10-11T16:28:15',1,2048,1369,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(40,-1,'2015-10-11T16:28:15','2015-10-11T16:28:15',1,2048,1369,'JPG',8,5);
INSERT INTO "ImageInformation" VALUES(41,-1,'2012-12-28T17:14:51',NULL,0,213,262,'PNG',8,1);
INSERT INTO "ImageInformation" VALUES(42,-1,'2014-02-07T20:22:38',NULL,0,445,670,'PNG',8,1);
INSERT INTO "ImageInformation" VALUES(43,-1,'2014-12-27T23:30:37',NULL,0,482,292,'PNG',8,1);
INSERT INTO "ImageInformation" VALUES(44,-1,'2016-04-19T19:34:56','2016-04-19T19:34:56',1,2448,4352,'JPG',8,5);
CREATE TABLE ImageMetadata
                            (imageid INTEGER PRIMARY KEY,
                            make TEXT,
                            model TEXT,
                            lens TEXT,
                            aperture REAL,
                            focalLength REAL,
                            focalLength35 REAL,
                            exposureTime REAL,
                            exposureProgram INTEGER,
                            exposureMode INTEGER,
                            sensitivity INTEGER,
                            flash INTEGER,
                            whiteBalance INTEGER,
                            whiteBalanceColorTemperature INTEGER,
                            meteringMode INTEGER,
                            subjectDistance REAL,
                            subjectDistanceCategory INTEGER);
INSERT INTO "ImageMetadata" VALUES(1,'FUJI PHOTO FILM CO., LTD.','SP-3000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(2,'FUJI PHOTO FILM CO., LTD.','SP-3000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(3,'FUJI PHOTO FILM CO., LTD.','SP-3000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(4,'FUJI PHOTO FILM CO., LTD.','SP-3000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(5,'FUJI PHOTO FILM CO., LTD.','SP-3000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(6,'FUJI PHOTO FILM CO., LTD.','SP-3000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(7,'FUJI PHOTO FILM CO., LTD.','SP-3000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(8,'FUJI PHOTO FILM CO., LTD.','SP-3000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(9,'FUJI PHOTO FILM CO., LTD.','SP-3000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(10,'FUJI PHOTO FILM CO., LTD.','SP-3000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(11,'FUJI PHOTO FILM CO., LTD.','SP-3000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(12,'FUJI PHOTO FILM CO., LTD.','SP-3000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(13,'FUJI PHOTO FILM CO., LTD.','SP-3000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(14,'FUJI PHOTO FILM CO., LTD.','SP-3000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(15,'FUJI PHOTO FILM CO., LTD.','SP-3000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(16,'FUJI PHOTO FILM CO., LTD.','SP-3000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(17,'FUJI PHOTO FILM CO., LTD.','SP-3000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(18,'FUJI PHOTO FILM CO., LTD.','SP-3000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(19,'FUJI PHOTO FILM CO., LTD.','SP-3000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(20,'FUJI PHOTO FILM CO., LTD.','SP-3000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(21,'FUJI PHOTO FILM CO., LTD.','SP-3000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(22,'FUJI PHOTO FILM CO., LTD.','SP-3000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(23,'FUJI PHOTO FILM CO., LTD.','SP-3000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(24,'FUJI PHOTO FILM CO., LTD.','SP-3000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(25,'SONY','SLT-A99V',NULL,6.0,28.0,28.0,-42949672.0,NULL,NULL,800,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(26,'SONY','SLT-A99V',NULL,6.0,28.0,28.0,-42949672.0,NULL,NULL,800,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(27,'SONY','SLT-A99V','Minolta/Sony AF 28mm F2.8',6.3,28.0,28.0,0.01,1,1,800,16,0,NULL,2,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(28,'SONY','SLT-A99V','Minolta/Sony AF 28mm F2.8',6.3,28.0,28.0,0.01,1,1,800,16,0,NULL,2,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(29,'SONY','SLT-A99V','Minolta/Sony AF 28mm F2.8',8.0,28.0,28.0,0.001,1,1,400,16,0,NULL,2,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(30,'SONY','SLT-A99V','Minolta/Sony AF 28mm F2.8',8.0,28.0,28.0,0.001,1,1,400,16,0,NULL,2,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(31,'SONY','SLT-A99V','Sony AF DT 16-105mm F3.5-5.6 (SAL16105)',3.5,16.0,24.0,0.05,1,1,1600,16,0,NULL,2,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(32,'SONY','SLT-A99V','Sony AF DT 16-105mm F3.5-5.6 (SAL16105)',3.5,16.0,24.0,0.05,1,1,1600,16,0,NULL,2,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(33,'SONY','SLT-A99V','100mm F2.8 Macro',2.8,100.0,100.0,0.0005,1,1,100,16,0,NULL,2,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(34,'SONY','SLT-A99V','100mm F2.8 Macro',2.8,100.0,100.0,0.0005,1,1,100,16,0,NULL,2,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(35,'SONY','SLT-A99V','100mm F2.8 Macro',4.0,100.0,100.0,0.00025,3,0,800,16,0,NULL,2,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(36,'SONY','SLT-A99V','100mm F2.8 Macro',4.0,100.0,100.0,0.00025,3,0,800,16,0,NULL,2,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(37,'SONY','SLT-A99V','100mm F2.8 Macro',4.0,100.0,100.0,0.00025,3,0,800,16,0,NULL,2,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(38,'SONY','SLT-A99V','100mm F2.8 Macro',2.8,100.0,100.0,0.002,3,0,200,16,0,NULL,2,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(39,'SONY','SLT-A99V','100mm F2.8 Macro',2.8,100.0,100.0,0.002,3,0,200,16,0,NULL,2,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(40,'SONY','SLT-A99V','100mm F2.8 Macro',2.8,100.0,100.0,0.002,3,0,200,16,0,NULL,2,NULL,NULL);
INSERT INTO "ImageMetadata" VALUES(44,'bq','Aquaris E4.5',NULL,2.4,3.5,NULL,0.003668,0,0,118,0,0,NULL,2,NULL,NULL);
CREATE TABLE VideoMetadata
                            (imageid INTEGER PRIMARY KEY,
                            aspectRatio TEXT,
                            audioBitRate TEXT,
                            audioChannelType TEXT,
                            audioCompressor TEXT,
                            duration TEXT,
                            frameRate TEXT,
                            exposureProgram INTEGER,
                            videoCodec TEXT);
CREATE TABLE ImagePositions
                            (imageid INTEGER PRIMARY KEY,
                            latitude TEXT,
                            latitudeNumber REAL,
                            longitude TEXT,
                            longitudeNumber REAL,
                            altitude REAL,
                            orientation REAL,
                            tilt REAL,
                            roll REAL,
                            accuracy REAL,
                            description TEXT);
INSERT INTO "ImagePositions" VALUES(25,'57,20.57416667N',5.73429027777777804413e+01,'12,8.00805550E',1.21334675916666654413e+01,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImagePositions" VALUES(26,'57,20.57416667N',5.73429027777777804413e+01,'12,8.00805550E',1.21334675916666654413e+01,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImagePositions" VALUES(27,'57,20.57416667N',5.73429027777777804413e+01,'12,8.00805556E',1.21334675925925914439e+01,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImagePositions" VALUES(28,'57,20.57416667N',5.73429027777777804413e+01,'12,8.00805556E',1.21334675925925914439e+01,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImagePositions" VALUES(29,'57,20.77416667N',5.73462361111111107447e+01,'12,7.49388889E',1.21248981481481479252e+01,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImagePositions" VALUES(30,'57,20.77416667N',5.73462361111111107447e+01,'12,7.49388889E',1.21248981481481479252e+01,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImagePositions" VALUES(31,'45,29.06833333N',4.54844722222222230811e+01,'12,25.13138889E',1.2418856481481480358e+01,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImagePositions" VALUES(32,'45,29.06833333N',4.54844722222222230811e+01,'12,25.13138889E',1.2418856481481480358e+01,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImagePositions" VALUES(38,'60,18.02208333N',6.03003680555555519058e+01,'25,0.89775000E',25.0149625,5.499,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImagePositions" VALUES(39,'60,18.02208333N',6.03003680555555519058e+01,'25,0.89775000E',25.0149625,5.499,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "ImagePositions" VALUES(40,'60,18.02208333N',6.03003680555555519058e+01,'25,0.89775000E',25.0149625,5.499,NULL,NULL,NULL,NULL,NULL);
CREATE TABLE ImageComments
                            (id INTEGER PRIMARY KEY,
                            imageid INTEGER,
                            type INTEGER,
                            language TEXT,
                            author TEXT,
                            date DATETIME,
                            comment TEXT,
                            UNIQUE(imageid, type, language, author));
INSERT INTO "ImageComments" VALUES(1,25,1,'x-default',NULL,NULL,'');
INSERT INTO "ImageComments" VALUES(2,26,1,'x-default',NULL,NULL,'');
INSERT INTO "ImageComments" VALUES(3,33,1,'x-default',NULL,NULL,'');
INSERT INTO "ImageComments" VALUES(4,34,1,'x-default',NULL,NULL,'');
INSERT INTO "ImageComments" VALUES(5,35,1,'x-default',NULL,NULL,'');
INSERT INTO "ImageComments" VALUES(6,36,1,'x-default',NULL,NULL,'');
INSERT INTO "ImageComments" VALUES(7,37,1,'x-default',NULL,NULL,'');
INSERT INTO "ImageComments" VALUES(8,32,3,'x-default',NULL,NULL,'This is a church
');
INSERT INTO "ImageComments" VALUES(9,32,1,'x-default',NULL,'2019-01-03T14:55:09','And its caption');
CREATE TABLE ImageCopyright
                            (id INTEGER PRIMARY KEY,
                            imageid INTEGER,
                            property TEXT,
                            value TEXT,
                            extraValue TEXT,
                            UNIQUE(imageid, property, value, extraValue));
INSERT INTO "ImageCopyright" VALUES(1,25,'provider',NULL,NULL);
INSERT INTO "ImageCopyright" VALUES(2,25,'source',NULL,NULL);
INSERT INTO "ImageCopyright" VALUES(3,25,'creatorJobTitle',NULL,NULL);
INSERT INTO "ImageCopyright" VALUES(4,25,'instructions',NULL,NULL);
INSERT INTO "ImageCopyright" VALUES(5,25,'creatorContactInfo.city',NULL,NULL);
INSERT INTO "ImageCopyright" VALUES(6,25,'creatorContactInfo.country',NULL,NULL);
INSERT INTO "ImageCopyright" VALUES(7,25,'creatorContactInfo.address',NULL,NULL);
INSERT INTO "ImageCopyright" VALUES(8,25,'creatorContactInfo.postalCode',NULL,NULL);
INSERT INTO "ImageCopyright" VALUES(9,25,'creatorContactInfo.provinceState',NULL,NULL);
INSERT INTO "ImageCopyright" VALUES(10,25,'creatorContactInfo.email',NULL,NULL);
INSERT INTO "ImageCopyright" VALUES(11,25,'creatorContactInfo.phone',NULL,NULL);
INSERT INTO "ImageCopyright" VALUES(12,25,'creatorContactInfo.webUrl',NULL,NULL);
INSERT INTO "ImageCopyright" VALUES(13,26,'provider',NULL,NULL);
INSERT INTO "ImageCopyright" VALUES(14,26,'source',NULL,NULL);
INSERT INTO "ImageCopyright" VALUES(15,26,'creatorJobTitle',NULL,NULL);
INSERT INTO "ImageCopyright" VALUES(16,26,'instructions',NULL,NULL);
INSERT INTO "ImageCopyright" VALUES(17,26,'creatorContactInfo.city',NULL,NULL);
INSERT INTO "ImageCopyright" VALUES(18,26,'creatorContactInfo.country',NULL,NULL);
INSERT INTO "ImageCopyright" VALUES(19,26,'creatorContactInfo.address',NULL,NULL);
INSERT INTO "ImageCopyright" VALUES(20,26,'creatorContactInfo.postalCode',NULL,NULL);
INSERT INTO "ImageCopyright" VALUES(21,26,'creatorContactInfo.provinceState',NULL,NULL);
INSERT INTO "ImageCopyright" VALUES(22,26,'creatorContactInfo.email',NULL,NULL);
INSERT INTO "ImageCopyright" VALUES(23,26,'creatorContactInfo.phone',NULL,NULL);
INSERT INTO "ImageCopyright" VALUES(24,26,'creatorContactInfo.webUrl',NULL,NULL);
CREATE TABLE Tags
                            (id INTEGER PRIMARY KEY,
                            pid INTEGER,
                            name TEXT NOT NULL,
                            icon INTEGER,
                            iconkde TEXT,
                            UNIQUE (name, pid));
INSERT INTO "Tags" VALUES(1,0,'_Digikam_Internal_Tags_',0,NULL);
INSERT INTO "Tags" VALUES(2,1,'Original Version',0,NULL);
INSERT INTO "Tags" VALUES(3,1,'Intermediate Version',0,NULL);
INSERT INTO "Tags" VALUES(4,1,'Current Version',0,NULL);
INSERT INTO "Tags" VALUES(5,1,'Version Always Visible',0,NULL);
INSERT INTO "Tags" VALUES(6,0,'SAL-100M28',0,NULL);
INSERT INTO "Tags" VALUES(7,0,'2015-08-23 Macro',0,NULL);
INSERT INTO "Tags" VALUES(8,9,'tarassaco',0,NULL);
INSERT INTO "Tags" VALUES(9,0,'fiori',0,NULL);
INSERT INTO "Tags" VALUES(10,0,'macro',0,NULL);
INSERT INTO "Tags" VALUES(11,1,'Color Label None',0,NULL);
INSERT INTO "Tags" VALUES(12,1,'Color Label Red',0,NULL);
INSERT INTO "Tags" VALUES(13,1,'Color Label Orange',0,NULL);
INSERT INTO "Tags" VALUES(14,1,'Color Label Yellow',0,NULL);
INSERT INTO "Tags" VALUES(15,1,'Color Label Green',0,NULL);
INSERT INTO "Tags" VALUES(16,1,'Color Label Blue',0,NULL);
INSERT INTO "Tags" VALUES(17,1,'Color Label Magenta',0,NULL);
INSERT INTO "Tags" VALUES(18,1,'Color Label Gray',0,NULL);
INSERT INTO "Tags" VALUES(19,1,'Color Label Black',0,NULL);
INSERT INTO "Tags" VALUES(20,1,'Color Label White',0,NULL);
INSERT INTO "Tags" VALUES(21,1,'Pick Label None',0,NULL);
INSERT INTO "Tags" VALUES(22,1,'Pick Label Rejected',0,NULL);
INSERT INTO "Tags" VALUES(23,1,'Pick Label Pending',0,NULL);
INSERT INTO "Tags" VALUES(24,1,'Pick Label Accepted',0,NULL);
INSERT INTO "Tags" VALUES(25,0,'prato',0,NULL);
INSERT INTO "Tags" VALUES(26,0,'controluce',0,NULL);
INSERT INTO "Tags" VALUES(27,0,'tramonto',0,NULL);
INSERT INTO "Tags" VALUES(28,0,'basso profilo',0,NULL);
INSERT INTO "Tags" VALUES(29,1,'Need Resolving History',0,NULL);
INSERT INTO "Tags" VALUES(30,1,'Need Tagging History Graph',0,NULL);
INSERT INTO "Tags" VALUES(31,0,'forest',NULL,'document-close');
INSERT INTO "Tags" VALUES(32,0,'People',0,NULL);
INSERT INTO "Tags" VALUES(33,0,'sunset',NULL,'/home/mardy/Pictures/Photos/2016/01/27/Screenshot from 2014-12-27 21:30:34.png');
INSERT INTO "Tags" VALUES(34,0,'wood',0,NULL);
INSERT INTO "Tags" VALUES(35,0,'church',0,NULL);
CREATE TABLE TagsTree
                            (id INTEGER NOT NULL,
                            pid INTEGER NOT NULL,
                            UNIQUE (id, pid));
INSERT INTO "TagsTree" VALUES(1,0);
INSERT INTO "TagsTree" VALUES(2,0);
INSERT INTO "TagsTree" VALUES(2,1);
INSERT INTO "TagsTree" VALUES(3,0);
INSERT INTO "TagsTree" VALUES(3,1);
INSERT INTO "TagsTree" VALUES(4,0);
INSERT INTO "TagsTree" VALUES(4,1);
INSERT INTO "TagsTree" VALUES(5,0);
INSERT INTO "TagsTree" VALUES(5,1);
INSERT INTO "TagsTree" VALUES(6,0);
INSERT INTO "TagsTree" VALUES(7,0);
INSERT INTO "TagsTree" VALUES(9,0);
INSERT INTO "TagsTree" VALUES(10,0);
INSERT INTO "TagsTree" VALUES(11,0);
INSERT INTO "TagsTree" VALUES(11,1);
INSERT INTO "TagsTree" VALUES(12,0);
INSERT INTO "TagsTree" VALUES(12,1);
INSERT INTO "TagsTree" VALUES(13,0);
INSERT INTO "TagsTree" VALUES(13,1);
INSERT INTO "TagsTree" VALUES(14,0);
INSERT INTO "TagsTree" VALUES(14,1);
INSERT INTO "TagsTree" VALUES(15,0);
INSERT INTO "TagsTree" VALUES(15,1);
INSERT INTO "TagsTree" VALUES(16,0);
INSERT INTO "TagsTree" VALUES(16,1);
INSERT INTO "TagsTree" VALUES(17,0);
INSERT INTO "TagsTree" VALUES(17,1);
INSERT INTO "TagsTree" VALUES(18,0);
INSERT INTO "TagsTree" VALUES(18,1);
INSERT INTO "TagsTree" VALUES(19,0);
INSERT INTO "TagsTree" VALUES(19,1);
INSERT INTO "TagsTree" VALUES(20,0);
INSERT INTO "TagsTree" VALUES(20,1);
INSERT INTO "TagsTree" VALUES(21,0);
INSERT INTO "TagsTree" VALUES(21,1);
INSERT INTO "TagsTree" VALUES(22,0);
INSERT INTO "TagsTree" VALUES(22,1);
INSERT INTO "TagsTree" VALUES(23,0);
INSERT INTO "TagsTree" VALUES(23,1);
INSERT INTO "TagsTree" VALUES(24,0);
INSERT INTO "TagsTree" VALUES(24,1);
INSERT INTO "TagsTree" VALUES(25,0);
INSERT INTO "TagsTree" VALUES(26,0);
INSERT INTO "TagsTree" VALUES(27,0);
INSERT INTO "TagsTree" VALUES(28,0);
INSERT INTO "TagsTree" VALUES(29,0);
INSERT INTO "TagsTree" VALUES(29,1);
INSERT INTO "TagsTree" VALUES(30,0);
INSERT INTO "TagsTree" VALUES(30,1);
INSERT INTO "TagsTree" VALUES(31,0);
INSERT INTO "TagsTree" VALUES(32,0);
INSERT INTO "TagsTree" VALUES(33,0);
INSERT INTO "TagsTree" VALUES(34,0);
INSERT INTO "TagsTree" VALUES(35,0);
INSERT INTO "TagsTree" VALUES(8,0);
INSERT INTO "TagsTree" VALUES(8,9);
CREATE TABLE ImageTags
                            (imageid INTEGER NOT NULL,
                            tagid INTEGER NOT NULL,
                            UNIQUE (imageid, tagid));
INSERT INTO "ImageTags" VALUES(33,6);
INSERT INTO "ImageTags" VALUES(33,7);
INSERT INTO "ImageTags" VALUES(33,8);
INSERT INTO "ImageTags" VALUES(33,9);
INSERT INTO "ImageTags" VALUES(33,10);
INSERT INTO "ImageTags" VALUES(34,6);
INSERT INTO "ImageTags" VALUES(34,7);
INSERT INTO "ImageTags" VALUES(34,8);
INSERT INTO "ImageTags" VALUES(34,9);
INSERT INTO "ImageTags" VALUES(34,10);
INSERT INTO "ImageTags" VALUES(35,6);
INSERT INTO "ImageTags" VALUES(35,7);
INSERT INTO "ImageTags" VALUES(35,25);
INSERT INTO "ImageTags" VALUES(35,26);
INSERT INTO "ImageTags" VALUES(35,27);
INSERT INTO "ImageTags" VALUES(35,28);
INSERT INTO "ImageTags" VALUES(36,6);
INSERT INTO "ImageTags" VALUES(36,7);
INSERT INTO "ImageTags" VALUES(36,25);
INSERT INTO "ImageTags" VALUES(36,26);
INSERT INTO "ImageTags" VALUES(36,27);
INSERT INTO "ImageTags" VALUES(36,28);
INSERT INTO "ImageTags" VALUES(37,6);
INSERT INTO "ImageTags" VALUES(37,7);
INSERT INTO "ImageTags" VALUES(37,25);
INSERT INTO "ImageTags" VALUES(37,26);
INSERT INTO "ImageTags" VALUES(37,27);
INSERT INTO "ImageTags" VALUES(37,28);
INSERT INTO "ImageTags" VALUES(29,33);
INSERT INTO "ImageTags" VALUES(25,31);
INSERT INTO "ImageTags" VALUES(25,34);
INSERT INTO "ImageTags" VALUES(32,35);
CREATE TABLE ImageProperties
                            (imageid  INTEGER NOT NULL,
                            property TEXT    NOT NULL,
                            value    TEXT    NOT NULL,
                            UNIQUE (imageid, property));
CREATE TABLE Searches
                            (id INTEGER PRIMARY KEY,
                            type INTEGER,
                            name TEXT NOT NULL,
                            query TEXT NOT NULL);
CREATE TABLE DownloadHistory
                            (id  INTEGER PRIMARY KEY,
                            identifier TEXT,
                            filename TEXT,
                            filesize INTEGER,
                            filedate DATETIME,
                            UNIQUE(identifier, filename, filesize, filedate));
CREATE TABLE Settings
                            (keyword TEXT NOT NULL UNIQUE,
                            value TEXT);
INSERT INTO "Settings" VALUES('preAlpha010Update1','true');
INSERT INTO "Settings" VALUES('preAlpha010Update2','true');
INSERT INTO "Settings" VALUES('preAlpha010Update3','true');
INSERT INTO "Settings" VALUES('beta010Update1','true');
INSERT INTO "Settings" VALUES('beta010Update2','true');
INSERT INTO "Settings" VALUES('uniqueHashVersion','2');
INSERT INTO "Settings" VALUES('databaseImageFormats','jpg;jpeg;jpe;jp2;j2k;jpx;jpc;pgx;tif;tiff;png;xpm;ppm;pnm;pgf;gif;bmp;xcf;pcx;bay;bmq;cr2;crw;cs1;dc2;dcr;dng;erf;fff;hdr;k25;kdc;mdc;mos;mrw;nef;orf;pef;pxn;raf;raw;rdc;sr2;srf;x3f;arw;3fr;cine;ia;kc2;mef;nrw;qtk;rw2;sti;rwl;srw;');
INSERT INTO "Settings" VALUES('databaseVideoFormats','mpeg;mpg;mpo;mpe;avi;divx;wmv;wmf;asf;mp4;3gp;mov;3g2;m4v;m2v;mkv;webm');
INSERT INTO "Settings" VALUES('databaseAudioFormats','ogg;mp3;wma;wav');
INSERT INTO "Settings" VALUES('FilterSettingsVersion','4');
INSERT INTO "Settings" VALUES('DcrawFilterSettingsVersion','4');
INSERT INTO "Settings" VALUES('databaseUUID','{7db5ccce-b4e6-4db9-a88e-5b3eff22f5cc}');
INSERT INTO "Settings" VALUES('Locale','UTF-8');
INSERT INTO "Settings" VALUES('RemovedItemsTime','2019-01-03T14:53:40');
INSERT INTO "Settings" VALUES('DBVersion','7');
INSERT INTO "Settings" VALUES('DBVersionRequired','7');
INSERT INTO "Settings" VALUES('DeleteRemovedCompleteScanCount','3');
INSERT INTO "Settings" VALUES('Scanned','2019-01-03T15:43:56');
CREATE TABLE ImageHistory
                            (imageid INTEGER PRIMARY KEY,
                             uuid TEXT,
                             history TEXT);
CREATE TABLE ImageRelations
                            (subject INTEGER,
                             object INTEGER,
                             type INTEGER,
                             UNIQUE(subject, object, type));
CREATE TABLE TagProperties
                            (tagid INTEGER,
                             property TEXT,
                             value TEXT);
INSERT INTO "TagProperties" VALUES(1,'internalTag',NULL);
INSERT INTO "TagProperties" VALUES(2,'internalTag',NULL);
INSERT INTO "TagProperties" VALUES(3,'internalTag',NULL);
INSERT INTO "TagProperties" VALUES(4,'internalTag',NULL);
INSERT INTO "TagProperties" VALUES(5,'internalTag',NULL);
INSERT INTO "TagProperties" VALUES(11,'internalTag',NULL);
INSERT INTO "TagProperties" VALUES(12,'internalTag',NULL);
INSERT INTO "TagProperties" VALUES(13,'internalTag',NULL);
INSERT INTO "TagProperties" VALUES(14,'internalTag',NULL);
INSERT INTO "TagProperties" VALUES(15,'internalTag',NULL);
INSERT INTO "TagProperties" VALUES(16,'internalTag',NULL);
INSERT INTO "TagProperties" VALUES(17,'internalTag',NULL);
INSERT INTO "TagProperties" VALUES(18,'internalTag',NULL);
INSERT INTO "TagProperties" VALUES(19,'internalTag',NULL);
INSERT INTO "TagProperties" VALUES(20,'internalTag',NULL);
INSERT INTO "TagProperties" VALUES(21,'internalTag',NULL);
INSERT INTO "TagProperties" VALUES(22,'internalTag',NULL);
INSERT INTO "TagProperties" VALUES(23,'internalTag',NULL);
INSERT INTO "TagProperties" VALUES(24,'internalTag',NULL);
INSERT INTO "TagProperties" VALUES(29,'internalTag',NULL);
INSERT INTO "TagProperties" VALUES(30,'internalTag',NULL);
CREATE TABLE ImageTagProperties
                            (imageid INTEGER,
                             tagid INTEGER,
                             property TEXT,
                             value TEXT);
CREATE INDEX dir_index  ON Images (album);
CREATE INDEX hash_index ON Images (uniqueHash);
CREATE INDEX tag_index  ON ImageTags (tagid);
CREATE INDEX tag_id_index  ON ImageTags (imageid);
CREATE INDEX image_name_index ON Images (name);
CREATE INDEX creationdate_index ON ImageInformation (creationDate);
CREATE INDEX comments_imageid_index ON ImageComments (imageid);
CREATE INDEX copyright_imageid_index ON ImageCopyright (imageid);
CREATE INDEX uuid_index ON ImageHistory (uuid);
CREATE INDEX subject_relations_index ON ImageRelations (subject);
CREATE INDEX object_relations_index ON ImageRelations (object);
CREATE INDEX tagproperties_index ON TagProperties (tagid);
CREATE INDEX imagetagproperties_index ON ImageTagProperties (imageid, tagid);
CREATE INDEX imagetagproperties_imageid_index ON ImageTagProperties (imageid);
CREATE INDEX imagetagproperties_tagid_index ON ImageTagProperties (tagid);
CREATE TRIGGER delete_albumroot DELETE ON AlbumRoots
                BEGIN
                DELETE FROM Albums
                WHERE Albums.albumRoot = OLD.id;
                END;
CREATE TRIGGER delete_album DELETE ON Albums
                BEGIN
                DELETE FROM Images
                WHERE Images.album = OLD.id;
                END;
CREATE TRIGGER delete_image DELETE ON Images
                    BEGIN
                        DELETE FROM ImageTags          WHERE imageid=OLD.id;
                        DELETE From ImageHaarMatrix    WHERE imageid=OLD.id;
                        DELETE From ImageInformation   WHERE imageid=OLD.id;
                        DELETE From ImageMetadata      WHERE imageid=OLD.id;
                        DELETE From VideoMetadata      WHERE imageid=OLD.id;
                        DELETE From ImagePositions     WHERE imageid=OLD.id;
                        DELETE From ImageComments      WHERE imageid=OLD.id;
                        DELETE From ImageCopyright     WHERE imageid=OLD.id;
                        DELETE From ImageProperties    WHERE imageid=OLD.id;
                        DELETE From ImageHistory       WHERE imageid=OLD.id;
                        DELETE FROM ImageRelations     WHERE subject=OLD.id OR object=OLD.id;
                        DELETE FROM ImageTagProperties WHERE imageid=OLD.id;
                        UPDATE Albums SET icon=null    WHERE icon=OLD.id;
                        UPDATE Tags SET icon=null      WHERE icon=OLD.id;
                    END;
CREATE TRIGGER delete_tag DELETE ON Tags
                    BEGIN
                        DELETE FROM ImageTags WHERE tagid=OLD.id;
                        DELETE FROM TagProperties WHERE tagid=OLD.id;
                        DELETE FROM ImageTagProperties WHERE tagid=OLD.id;
                    END;
CREATE TRIGGER insert_tagstree AFTER INSERT ON Tags
                BEGIN
                INSERT INTO TagsTree
                    SELECT NEW.id, NEW.pid
                    UNION
                    SELECT NEW.id, pid FROM TagsTree WHERE id=NEW.pid;
                END;
CREATE TRIGGER delete_tagstree DELETE ON Tags
                BEGIN
                DELETE FROM Tags
                WHERE id  IN (SELECT id FROM TagsTree WHERE pid=OLD.id);
                DELETE FROM TagsTree
                WHERE id IN (SELECT id FROM TagsTree WHERE pid=OLD.id);
                DELETE FROM TagsTree
                    WHERE id=OLD.id;
                END;
CREATE TRIGGER move_tagstree UPDATE OF pid ON Tags
                BEGIN
                DELETE FROM TagsTree
                    WHERE
                    ((id = OLD.id)
                    OR
                    id IN (SELECT id FROM TagsTree WHERE pid=OLD.id))
                    AND
                    pid IN (SELECT pid FROM TagsTree WHERE id=OLD.id);
                INSERT INTO TagsTree
                    SELECT NEW.id, NEW.pid
                    UNION
                    SELECT NEW.id, pid FROM TagsTree WHERE id=NEW.pid
                    UNION
                    SELECT id, NEW.pid FROM TagsTree WHERE pid=NEW.id
                    UNION
                    SELECT A.id, B.pid FROM TagsTree A, TagsTree B
                    WHERE
                    A.pid = NEW.id AND B.id = NEW.pid;
                END;
COMMIT;
