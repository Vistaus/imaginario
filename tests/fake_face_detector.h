/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FAKE_FACE_DETECTOR_H
#define FAKE_FACE_DETECTOR_H

#include "face_detector.h"

#include <QHash>
#include <QObject>

namespace Imaginario {

class FaceDetectorPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(FaceDetector)

public:
    FaceDetectorPrivate();
    ~FaceDetectorPrivate();
    static FaceDetectorPrivate *mocked(FaceDetector *o) {
        return o->d_ptr;
    }
    void setValid(bool valid) { m_isValid = valid; }
    void setFaces(const QUrl &url, const QList<QRectF> &rects) {
        m_faces[url] = rects;
    }
    void setDetectionTime(int ms) { m_detectionTime = ms; }

private:
    FaceDetectorPrivate(FaceDetector *q):
        m_isValid(true),
        m_detectionTime(0),
        q_ptr(q)
    {}

private:
    bool m_isValid;
    int m_detectionTime;
    QHash<QUrl,QList<QRectF> > m_faces;
    FaceDetector *q_ptr;
};

} // namespace

#endif // FAKE_FACE_DETECTOR_H
