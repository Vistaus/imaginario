TARGET = tst_fspot_importer

include(tests.pri)

QT += \
    concurrent \
    sql

SOURCES += \
    $${SRC_DIR}/fspot_importer.cpp \
    tst_fspot_importer.cpp

HEADERS += \
    $${SRC_DIR}/abstract_database.h \
    $${SRC_DIR}/database.h \
    $${SRC_DIR}/fspot_importer.h \
    $${SRC_DIR}/metadata.h \
    test_utils.h

DATA_DIR = $${TOP_SRC_DIR}/tests/data
DEFINES += \
    DATA_DIR=\\\"$${DATA_DIR}\\\"
