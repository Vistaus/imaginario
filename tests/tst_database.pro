TARGET = tst_database

include(tests.pri)

QT += \
    sql

SOURCES += \
    $${SRC_DIR}/abstract_database.cpp \
    $${SRC_DIR}/database.cpp \
    tst_database.cpp

HEADERS += \
    $${SRC_DIR}/abstract_database.h $${SRC_DIR}/abstract_database_p.h \
    $${SRC_DIR}/database.h \
    $${SRC_DIR}/roll.h \
    test_utils.h

DATA_DIR = $${TOP_SRC_DIR}/tests/data
DEFINES += \
    DATA_DIR=\\\"$${DATA_DIR}\\\"
