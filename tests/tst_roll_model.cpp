/*
 * Copyright (C) 2016-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fake_database.h"
#include "roll_model.h"
#include "test_utils.h"

#include <QDateTime>
#include <QQmlComponent>
#include <QQmlEngine>
#include <QScopedPointer>
#include <QSignalSpy>
#include <QTest>

using namespace Imaginario;

class RollModelTest: public QObject
{
    Q_OBJECT

public:
    static QVariant get(const QAbstractListModel *model, int row,
                        QString roleName);

private Q_SLOTS:
    void testProperties();
    void testRoles();
    void testFilters_data();
    void testFilters();
    void testCountUpdates();
};

QVariant RollModelTest::get(const QAbstractListModel *model, int row,
                             QString roleName)
{
    QHash<int, QByteArray> roleNames = model->roleNames();

    int role = roleNames.key(roleName.toLatin1(), -1);
    return model->data(model->index(row), role);
}

void RollModelTest::testProperties()
{
    RollModel model;
    model.classBegin();
    model.componentComplete();

    QScopedPointer<Database> db(Database::instance());

    QCOMPARE(model.property("count").toInt(), 0);

    model.setProperty("roll0", int(2));
    QCOMPARE(model.property("roll0").toInt(), 2);
    model.setProperty("roll1", int(5));
    QCOMPARE(model.property("roll1").toInt(), 5);

    QCOMPARE(model.property("lastRollId").toInt(), -1);
}

void RollModelTest::testRoles()
{
    QScopedPointer<Database> db(Database::instance());
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db.data());

    QSignalSpy findPhotosCalled(dbMock,
        SIGNAL(findPhotosCalled(Imaginario::SearchFilters,
                                Qt::SortOrder)));

    QDateTime time = QDateTime::fromString("1997-07-16T19:20:30",
                                           Qt::ISODate);
    RollId rollId = db->createRoll(time);
    Database::Photo photo;
    for (int i = 0; i < 13; i++) {
        db->addPhoto(photo, rollId);
    }

    RollModel model;
    model.classBegin();
    model.componentComplete();

    QCOMPARE(model.rowCount(), 1);

    QCOMPARE(model.get(0, "rollId").toInt(), rollId);
    QCOMPARE(model.get(0, "time").toDateTime(), time);
    QCOMPARE(model.get(0, "roll").value<Roll>().id(), rollId);
    QCOMPARE(model.get(0, "photoCount").toInt(), 13);

    QCOMPARE(findPhotosCalled.count(), 1);
    SearchFilters filters =
        findPhotosCalled.at(0).at(0).value<SearchFilters>();
    QCOMPARE(filters.roll0, rollId);
    QCOMPARE(filters.roll1, rollId);
}

void RollModelTest::testFilters_data()
{
    QTest::addColumn<QString>("properties");
    QTest::addColumn<QList<int>>("expectedRolls");

    QTest::newRow("no properties") <<
        "" <<
        QList<int> { 1, 2, 3, 4 };

    QTest::newRow("both -1") <<
        "roll0: -1; roll1: -1" <<
        QList<int> { 1, 2, 3, 4 };

    QTest::newRow("full range") <<
        "roll0: 1; roll1: 4" <<
        QList<int> { 1, 2, 3, 4 };
}

void RollModelTest::testFilters()
{
    QFETCH(QString, properties);
    QFETCH(QList<int>, expectedRolls);

    QScopedPointer<Database> db(Database::instance());

    /* Add some rolls */
    QDateTime time = QDateTime::fromString("1997-07-16T19:20:30",
                                           Qt::ISODate);
    db->createRoll(time);
    db->createRoll(time.addMonths(1));
    db->createRoll(time.addMonths(2));
    db->createRoll(time.addMonths(3));

    QQmlEngine engine;
    qmlRegisterType<RollModel>("MyTest", 1, 0, "RollModel");
    QQmlComponent component(&engine);
    component.setData("import MyTest 1.0\n"
                      "RollModel { " + properties.toUtf8() + "}",
                      QUrl());
    QObject *object = component.create();
    QVERIFY(object != 0);
    QAbstractListModel *model = qobject_cast<QAbstractListModel*>(object);
    QVERIFY(model != 0);

    QList<int> rolls;
    for (int i = 0; i < model->rowCount(); i++) {
        rolls.append(get(model, i, "rollId").toInt());
    }

    QCOMPARE(rolls.toSet(), expectedRolls.toSet());
}

void RollModelTest::testCountUpdates()
{
    QScopedPointer<Database> db(Database::instance());
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db.data());

    QDateTime time = QDateTime::fromString("1997-07-16T19:20:30",
                                           Qt::ISODate);
    RollId rollId = db->createRoll(time);
    Database::Photo photo;
    for (int i = 0; i < 13; i++) {
        db->addPhoto(photo, rollId);
    }

    RollModel model;
    model.classBegin();
    model.componentComplete();

    QSignalSpy countChanged(&model, SIGNAL(countChanged()));
    QSignalSpy findPhotosCalled(dbMock,
        SIGNAL(findPhotosCalled(Imaginario::SearchFilters,
                                Qt::SortOrder)));

    QCOMPARE(model.rowCount(), 1);

    /* create one more roll */
    time = time.addMonths(2);
    rollId = db->createRoll(time);
    for (int i = 0; i < 6; i++) {
        db->addPhoto(photo, rollId);
    }

    /* Check that the model got updated */
    QTRY_COMPARE(countChanged.count(), 1);
    QCOMPARE(model.rowCount(), 2);
    countChanged.clear();

    /* Check that the correct query is executed to get the photo
     * count */
    model.get(1, "photoCount");
    QCOMPARE(findPhotosCalled.count(), 1);
    SearchFilters filters =
        findPhotosCalled.at(0).at(0).value<SearchFilters>();
    QCOMPARE(filters.roll0, rollId);
    QCOMPARE(filters.roll1, rollId);
}

QTEST_GUILESS_MAIN(RollModelTest)

#include "tst_roll_model.moc"
