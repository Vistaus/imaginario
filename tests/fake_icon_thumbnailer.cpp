/*
 * Copyright (C) 2016-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fake_icon_thumbnailer.h"

using namespace Imaginario;

static IconThumbnailerPrivate *m_privateInstance = 0;

IconThumbnailerPrivate::IconThumbnailerPrivate():
    QObject()
{
    m_privateInstance = this;
}

IconThumbnailerPrivate::~IconThumbnailerPrivate()
{
    m_privateInstance = 0;
}

IconThumbnailer::IconThumbnailer(int size)
{
    Q_UNUSED(size);
    if (m_privateInstance) {
        m_privateInstance->q_ptr = this;
        d_ptr = m_privateInstance;
    } else {
        d_ptr = new IconThumbnailerPrivate(this);
    }
}

IconThumbnailer::~IconThumbnailer()
{
    delete d_ptr;
}

QImage IconThumbnailer::load(const QUrl &uri) const
{
    Q_D(const IconThumbnailer);
    Q_EMIT const_cast<IconThumbnailerPrivate*>(d)->loadCalled(uri);
    return d->m_loadReply[uri];
}
