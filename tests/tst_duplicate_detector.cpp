/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "duplicate_detector.h"

#include "fake_database.h"
#include "fake_metadata.h"

#include <QDateTime>
#include <QSignalSpy>
#include <QTest>

using namespace Imaginario;

Q_DECLARE_METATYPE(Imaginario::DuplicateDetector::RawPairs)

namespace QTest {

template<>
char *toString(const DuplicateDetector::RawPairs &pairs)
{
    QByteArray ba = "QVector(";
    QStringList parts;
    Q_FOREACH(const DuplicateDetector::RawPair &pair, pairs) {
        parts.append(QString("{ %1 - %2 }").
                     arg(toString(pair.raw)).arg(toString(pair.edited)));
    }
    ba += parts.join(", ").toUtf8().constData();
    ba += ")";
    return qstrdup(ba.data());
}

} // namespace

class DuplicateDetectorTest: public QObject
{
    Q_OBJECT

public:
    DuplicateDetectorTest() {
        qRegisterMetaType<PhotoId>("PhotoId");
        m_time1 = QDateTime::fromString("2000-01-17T19:20:30", Qt::ISODate);
        m_time2 = QDateTime::fromString("2000-02-17T19:20:30", Qt::ISODate);
    }

private Q_SLOTS:
    void testProperties();
    void testEmpty();
    void testDetection_data();
    void testDetection();
    void testPairing_data();
    void testPairing();

private:
    QDateTime m_time1;
    QDateTime m_time2;
};

void DuplicateDetectorTest::testProperties()
{
    DuplicateDetector detector;

    QSignalSpy scoreChanged(&detector, SIGNAL(minimumScoreChanged()));

    detector.setProperty("minimumScore", double(0.2));
    QCOMPARE(scoreChanged.count(), 1);
    QCOMPARE(detector.property("minimumScore").toDouble(), double(0.2));
}

void DuplicateDetectorTest::testEmpty()
{
    DuplicateDetector detector;

    MaybeDuplicates dups =
        detector.duplicatesFor(QUrl("file:///tmp/myfile.jpg"));
    QVERIFY(dups.isEmpty());
}

void DuplicateDetectorTest::testDetection_data()
{
    QTest::addColumn<double>("minimumScore");
    QTest::addColumn<QUrl>("url");
    QTest::addColumn<QString>("title");
    QTest::addColumn<QDateTime>("time");
    QTest::addColumn<bool>("checkFileReturn");
    QTest::addColumn<QList<int> >("photoIds");

    QTest::newRow("empty url") <<
        double(0.1) <<
        QUrl() <<
        "" <<
        m_time1 <<
        false <<
        QList<int>();

    QTest::newRow("exact match, high score") <<
        double(0.9) <<
        QUrl("file:///tmp/exact/The-house.jpg") <<
        "The house on the hill" <<
        m_time1 <<
        true <<
        (QList<int>() << 1);

    QTest::newRow("exact match, impossible score") <<
        double(1.5) <<
        QUrl("file:///tmp/exact/The-house.jpg") <<
        "The house on the hill" <<
        m_time1 <<
        false <<
        QList<int>();

    QTest::newRow("exact match, lower score") <<
        double(0.7) <<
        QUrl("file:///tmp/exact/The-house.jpg") <<
        "The house on the hill" <<
        m_time1 <<
        true <<
        (QList<int>() << 1 << 2);

    QTest::newRow("exact match, even lower score") <<
        double(0.4) <<
        QUrl("file:///tmp/exact/The-house.jpg") <<
        "The house on the hill" <<
        m_time1 <<
        true <<
        (QList<int>() << 1 << 2 << 3);

    QTest::newRow("exact match, lowest score") <<
        double(0.0) <<
        QUrl("file:///tmp/exact/The-house.jpg") <<
        "The house on the hill" <<
        m_time1 <<
        true <<
        (QList<int>() << 1 << 2 << 3 << 4);

    QTest::newRow("similar filename") <<
        double(0.7) <<
        QUrl("file:///tmp/similar/Stay-a-cat.jpg") <<
        "" <<
        m_time1 <<
        true <<
        (QList<int>() << 3);
}

void DuplicateDetectorTest::testDetection()
{
    QFETCH(double, minimumScore);
    QFETCH(QUrl, url);
    QFETCH(QString, title);
    QFETCH(QDateTime, time);
    QFETCH(bool, checkFileReturn);
    QFETCH(QList<int>, photoIds);

    Database *db = Database::instance();
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db);
    QSignalSpy findPhotosCalled(dbMock,
        SIGNAL(findPhotosCalled(Imaginario::SearchFilters, Qt::SortOrder)));

    /* Populate the DB with some photos */

    QList<Database::Photo> allPhotos;
    Database::Photo photo;

    dbMock->setPhotoId(photo, 1);
    photo.setBaseUri("file:///tmp");
    photo.setFileName("The-house.jpg");
    photo.setDescription("The house on the hill");
    photo.setTime(m_time1);
    allPhotos.append(photo);

    dbMock->setPhotoId(photo, 2);
    photo.setBaseUri("file:///tmp/somepath");
    photo.setFileName("TheHome.png");
    photo.setDescription("");
    photo.setTime(m_time1);
    allPhotos.append(photo);

    dbMock->setPhotoId(photo, 3);
    photo.setBaseUri("file:///tmp");
    photo.setFileName("Straycat.jpg");
    photo.setDescription("Cute kitten");
    photo.setTime(m_time1);
    allPhotos.append(photo);

    dbMock->setPhotoId(photo, 4);
    photo.setBaseUri("file:///tmp");
    photo.setFileName("wrongtime.jpg");
    photo.setDescription("Taken at the wrong time");
    photo.setTime(m_time2);
    allPhotos.append(photo);

    dbMock->setPhotos(allPhotos);

    /* Prepare the fake metadata for the URLs we are going to test */
    MetadataPrivate *mdMocked = new MetadataPrivate();
    if (url.isValid()) {
        Metadata::ImportData data;
        data.title = title;
        data.time = time;
        mdMocked->setImportData(url.toLocalFile(), data);
    }

    DuplicateDetector detector;
    detector.setMinimumScore(minimumScore);

    bool ret = detector.checkFile(url);
    QCOMPARE(ret, checkFileReturn);

    QCOMPARE(findPhotosCalled.count(), url.isValid() ? 1 : 0);
    if (!findPhotosCalled.isEmpty()) {
        SearchFilters filters =
            findPhotosCalled.at(0).at(0).value<SearchFilters>();
        QCOMPARE(filters.time0, time);
        QCOMPARE(filters.time1, time);
    }

    MaybeDuplicates dups = detector.duplicatesFor(url);
    QList<int> dupIds;
    Q_FOREACH(const MaybeDuplicate &dup, dups) {
        dupIds.append(dup.id);
    }

    QCOMPARE(dupIds, photoIds);

    delete db;
}

void DuplicateDetectorTest::testPairing_data()
{
    QTest::addColumn<QList<QUrl>>("urls");
    QTest::addColumn<DuplicateDetector::RawPairs>("expectedPairs");

    QTest::newRow("empty list") <<
        QList<QUrl>() <<
        DuplicateDetector::RawPairs();

    QTest::newRow("1, edited") <<
        QList<QUrl> {
            QUrl("file:///tmp/path/image.jpg"),
        } <<
        DuplicateDetector::RawPairs {
            { QUrl(), QUrl("file:///tmp/path/image.jpg") },
        };

    QTest::newRow("1, raw") <<
        QList<QUrl> {
            QUrl("file:///tmp/path/image with spaces and .dots.RAW"),
        } <<
        DuplicateDetector::RawPairs {
            { QUrl("file:///tmp/path/image with spaces and .dots.RAW"), QUrl() },
        };

    QTest::newRow("all raw") <<
        QList<QUrl> {
            QUrl("file:///tmp/path/image with spaces and .dots.RAW"),
            QUrl("file:///tmp/path/flowers.RAW"),
            QUrl("file:///tmp/path/image.NEF"),
            QUrl("file:///tmp/path with spaces/photo.cr2"),
            QUrl("file:///tmp/path/image with spaces and .dots.nef"),
            QUrl("file:///tmp/path/panorama.CR2"),
        } <<
        DuplicateDetector::RawPairs {
            { QUrl("file:///tmp/path/image with spaces and .dots.RAW"), QUrl() },
            { QUrl("file:///tmp/path/flowers.RAW"), QUrl() },
            { QUrl("file:///tmp/path/image.NEF"), QUrl() },
            { QUrl("file:///tmp/path with spaces/photo.cr2"), QUrl() },
            { QUrl("file:///tmp/path/image with spaces and .dots.nef"), QUrl() },
            { QUrl("file:///tmp/path/panorama.CR2"), QUrl() },
        };

    QTest::newRow("all edited") <<
        QList<QUrl> {
            QUrl("file:///tmp/path/image with spaces and .dots.Jpeg"),
            QUrl("file:///tmp/path/flowers.png"),
            QUrl("file:///tmp/path/image.GIF"),
            QUrl("file:///tmp/path with spaces/photo.jpg"),
            QUrl("file:///tmp/path/image with spaces and .dots.JPG"),
            QUrl("file:///tmp/path/panorama.jpg"),
        } <<
        DuplicateDetector::RawPairs {
            { QUrl(), QUrl("file:///tmp/path/image with spaces and .dots.Jpeg") },
            { QUrl(), QUrl("file:///tmp/path/flowers.png") },
            { QUrl(), QUrl("file:///tmp/path/image.GIF") },
            { QUrl(), QUrl("file:///tmp/path with spaces/photo.jpg") },
            { QUrl(), QUrl("file:///tmp/path/image with spaces and .dots.JPG") },
            { QUrl(), QUrl("file:///tmp/path/panorama.jpg") },
        };

    QTest::newRow("some pairs") <<
        QList<QUrl> {
            QUrl("file:///tmp/path/panorama.cr2"),
            QUrl("file:///tmp/path/image with spaces and .dots.Jpeg"),
            QUrl("file:///tmp/path/flowers.png"),
            QUrl("file:///tmp/path/image.GIF"),
            QUrl("file:///tmp/path with spaces/photo.jpg"),
            QUrl("file:///tmp/path/flowers.ARW"),
            QUrl("file:///tmp/path/image with spaces and .dots.JPG"),
            QUrl("file:///tmp/path/panorama.jpg"),
        } <<
        DuplicateDetector::RawPairs {
            { QUrl("file:///tmp/path/panorama.cr2"), QUrl("file:///tmp/path/panorama.jpg") },
            { QUrl(), QUrl("file:///tmp/path/image with spaces and .dots.Jpeg") },
            { QUrl("file:///tmp/path/flowers.ARW"), QUrl("file:///tmp/path/flowers.png") },
            { QUrl(), QUrl("file:///tmp/path/image.GIF") },
            { QUrl(), QUrl("file:///tmp/path with spaces/photo.jpg") },
            { QUrl(), QUrl("file:///tmp/path/image with spaces and .dots.JPG") },
        };
}

void DuplicateDetectorTest::testPairing()
{
    QFETCH(QList<QUrl>, urls);
    QFETCH(DuplicateDetector::RawPairs, expectedPairs);

    DuplicateDetector detector;
    DuplicateDetector::RawPairs pairs = detector.makeRawPairs(urls);

    QCOMPARE(pairs, expectedPairs);
}

QTEST_GUILESS_MAIN(DuplicateDetectorTest)

#include "tst_duplicate_detector.moc"
