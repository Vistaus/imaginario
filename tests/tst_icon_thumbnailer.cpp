/*
 * Copyright (C) 2016-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "icon_thumbnailer.h"

#include <QCryptographicHash>
#include <QDir>
#include <QSize>
#include <QTemporaryDir>
#include <QTest>

using namespace Imaginario;

#define XPM_COLORS "a c #ff0000", \
    "b c #00ff00", \
    "c c #0000ff", \
    "d c #ffffff", \
    "e c #ff0080", \
    "f c #ffff00", \
    "g c #0080ff", \
    "h c #808080", \
    "i c #008000", \
    "j c #ff00ff", \
    "k c #80ff00", \
    "l c #000000", \
    "m c #008080", \
    "n c #00ffff", \
    "o c #800000", \
    "p c #ff0080"

typedef const char *const XpmData[];

class IconThumbnailerTest: public QObject
{
    Q_OBJECT

public:
    IconThumbnailerTest();

    void createImage(const QString &filePath, const QSize &size);
    void populateCache(int size);

private Q_SLOTS:
    void init();
    void cleanup();
    void testLoad_data();
    void testLoad();
    void testLoadWithArea_data();
    void testLoadWithArea();

private:
    QTemporaryDir *m_tmp;
    QDir m_dataDir;
};

IconThumbnailerTest::IconThumbnailerTest():
    m_dataDir(QString(ITEMS_DIR "/"))
{
}

void IconThumbnailerTest::createImage(const QString &filePath,
                                      const QSize &size)
{
    QImage baseImage((const char *const []) {
        /* <width/columns> <height/rows> <colors> <chars per pixel>*/
        "4 4 16 1",
        /* <Colors> */
        XPM_COLORS,
        /* <Pixels> */
        "abcd",
        "efgh",
        "ijkl",
        "mnop"
    });
    baseImage.scaled(size).save(filePath, nullptr, 100);
}

void IconThumbnailerTest::populateCache(int size)
{
    IconThumbnailer t(size);

    QStringList fileNames {
        "image0.png", "image1.jpg", "image2.jpg", "image3.png", "image4.jpg",
        "image5.jpg", "image6.jpg"
    };

    Q_FOREACH(const QString &fileName, fileNames) {
        t.load(QUrl::fromLocalFile(m_dataDir.filePath(fileName)));
    }
}

void IconThumbnailerTest::init()
{
    m_tmp = new QTemporaryDir();
    QVERIFY(m_tmp->isValid());
}

void IconThumbnailerTest::cleanup()
{
    if (QTest::currentTestFailed()) {
        m_tmp->setAutoRemove(false);
        qDebug() << "Temporary files have been left in" << m_tmp->path();
    }

    delete m_tmp;
    m_tmp = nullptr;
}

void IconThumbnailerTest::testLoad_data()
{
    QTest::addColumn<QUrl>("uri");
    QTest::addColumn<int>("requestedSize");
    QTest::addColumn<QSize>("expectedSize");

    QTest::newRow("invalid URI") <<
        QUrl("http?:don't think so") <<
        32 <<
        QSize(-1, -1);

    QTest::newRow("nonexisting file") <<
        QUrl("file:///etc/do you really have a file with this name?") <<
        32 <<
        QSize(-1, -1);

    QTest::newRow("landscape") <<
        QUrl::fromLocalFile(m_dataDir.filePath("image5.jpg")) <<
        32 <<
        QSize(32, 32);

    QTest::newRow("portrait") <<
        QUrl::fromLocalFile(m_dataDir.filePath("image6.jpg")) <<
        32 <<
        QSize(32, 32);

    QTest::newRow("size 48") <<
        QUrl::fromLocalFile(m_dataDir.filePath("image6.jpg")) <<
        48 <<
        QSize(48, 48);
}

void IconThumbnailerTest::testLoad()
{
    QFETCH(QUrl, uri);
    QFETCH(int, requestedSize);
    QFETCH(QSize, expectedSize);

    QTemporaryDir tmpDir;
    qputenv("XDG_CACHE_HOME", tmpDir.path().toUtf8());
    populateCache(requestedSize);

    IconThumbnailer thumbnailer(requestedSize);
    QImage t = thumbnailer.load(uri);

    if (expectedSize.isValid()) {
        QCOMPARE(t.size(), expectedSize);

        QByteArray ba = QCryptographicHash::hash(uri.toEncoded(),
                                                 QCryptographicHash::Md5);
        QString fileName = QString::fromLatin1(ba.toHex()) + ".png";
        QString subDir =
            QString("/tst_icon_thumbnailer/tag_icons_%1/").arg(requestedSize);
        QFileInfo fileInfo(tmpDir.path() + subDir + fileName);
        /*Check if the created file is proper */
        QImage cachedImage(fileInfo.filePath());
        QVERIFY(!cachedImage.isNull());
    } else {
        QVERIFY(t.isNull());
    }
}

void IconThumbnailerTest::testLoadWithArea_data()
{
    QTest::addColumn<QString>("fileName"); // The extension decides the format
    QTest::addColumn<int>("requestedSize");
    QTest::addColumn<QString>("query");
    QTest::addColumn<QImage>("expectedImage");

    QTest::newRow("top right") <<
        "original.png" <<
        2 <<
        "x=0.5&y=0&width=0.5&height=0.5" <<
        QImage((XpmData) {
            "2 2 16 1", XPM_COLORS,
            "cd",
            "gh"
        });

    QTest::newRow("bottom right") <<
        "original.png" <<
        3 <<
        "x=0.25&y=0.25&width=0.75&height=0.75" <<
        QImage((XpmData) {
            "3 3 16 1", XPM_COLORS,
            "fgh",
            "jkl",
            "nop"
        });

    QTest::newRow("bottom, crop") <<
        "original.png" <<
        2 <<
        "x=0&y=0.5&width=1&height=0.5" <<
        QImage((XpmData) {
            "2 2 16 1", XPM_COLORS,
            "jk",
            "no"
        });

    QTest::newRow("webp, top with crop") <<
        "original.webp" <<
        2 <<
        "x=0&y=0&width=1&height=0.5" <<
        QImage((XpmData) {
            "2 2 16 1", XPM_COLORS,
            "bc",
            "fg"
        });
}

void IconThumbnailerTest::testLoadWithArea()
{
    QFETCH(QString, fileName);
    QFETCH(int, requestedSize);
    QFETCH(QString, query);
    QFETCH(QImage, expectedImage);

    qputenv("XDG_CACHE_HOME", m_tmp->path().toUtf8());

    QString filePath = m_tmp->filePath(fileName);
    /* The image is
        "abcd",
        "efgh",
        "ijkl",
        "mnop"
     */
    createImage(filePath, QSize(256, 256));
    QUrl uri(QString("file://%1?%2").arg(filePath).arg(query));

    IconThumbnailer thumbnailer(requestedSize);
    QImage subImage = thumbnailer.load(uri);

    QImage convertedExpectedImage =
        expectedImage.convertToFormat(subImage.format());

    // Save only for debugging
    subImage.save(m_tmp->filePath("thumbnail.png"));
    convertedExpectedImage.save(m_tmp->filePath("expected.png"));
    QCOMPARE(subImage, convertedExpectedImage);
}

QTEST_GUILESS_MAIN(IconThumbnailerTest)

#include "tst_icon_thumbnailer.moc"
