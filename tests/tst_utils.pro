TARGET = tst_utils

include(tests.pri)

QT += \
    qml \
    sql

SOURCES += \
    $${SRC_DIR}/utils.cpp \
    tst_utils.cpp

HEADERS += \
    $${SRC_DIR}/utils.h

ITEMS_DIR = $${TOP_SRC_DIR}/tests/data
DEFINES += \
    ITEMS_DIR=\\\"$${ITEMS_DIR}\\\"
