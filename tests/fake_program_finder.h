/*
 * Copyright (C) 2018-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FAKE_PROGRAM_FINDER_H
#define FAKE_PROGRAM_FINDER_H

#include "program_finder.h"

#include <QHash>
#include <QObject>

namespace Imaginario {

typedef ProgramFinderPrivate ProgramFinderMocked;
class ProgramFinderPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(ProgramFinder)

public:
    typedef QHash<QString,QJsonObject> Responses;

    ProgramFinderPrivate(ProgramFinder *q = 0);
    ~ProgramFinderPrivate();
    static ProgramFinderPrivate *mocked(ProgramFinder *o) {
        return o->d_ptr.data();
    }

    void setResponses(const Responses &responses) {
        m_responses = responses;
    }

    void setCallableApplications(const QJsonArray &apps) {
        m_callableApplications = apps;
    }

Q_SIGNALS:
    void findCalled(const QString &programName);

private:
    bool m_preserveDPtr;
    Responses m_responses;
    QJsonArray m_callableApplications;
    mutable ProgramFinder *q_ptr;
};

} // namespace

#endif // FAKE_PROGRAM_FINDER_H
