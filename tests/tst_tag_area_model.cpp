/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fake_database.h"
#include "fake_face_detector.h"
#include "tag_area_model.h"

#include <QJsonObject>
#include <QJsonValue>
#include <QScopedPointer>
#include <QSet>
#include <QSignalSpy>
#include <QTest>
#include <QThreadPool>

using namespace Imaginario;

typedef QList<QRectF> Areas;

class TagAreaModelTest: public QObject
{
    Q_OBJECT

public:
    TagAreaModelTest() {
        qRegisterMetaType<PhotoId>("PhotoId");
    }

private Q_SLOTS:
    void testProperties();
    void testRoles_data();
    void testRoles();
    void testDatabaseChange();
    void testCancellation();
    void testSimilarity_data();
    void testSimilarity();
    void testSignalAggregation();
    void testTagDeletion();

private:
    QSet<TagArea> modelAreas(TagAreaModel *model);
};

QSet<TagArea> TagAreaModelTest::modelAreas(TagAreaModel *model)
{
    QSet<TagArea> areas;

    int count = model->rowCount();
    for (int i = 0; i < count; i++) {
        areas.insert(TagArea(model->get(i, "rect").toRectF(),
                             model->get(i, "tag").value<Tag>()));
    }

    return areas;
}

void TagAreaModelTest::testProperties()
{
    QScopedPointer<Database> db(Database::instance());
    Database::Photo photo;
    photo.setBaseUri("/tmp");
    photo.setFileName("foo.jpg");
    PhotoId photoId = db->addPhoto(photo, 1);
    QVERIFY(photoId != -1);

    FaceDetectorPrivate *fdMock = new FaceDetectorPrivate();
    Areas faces;
    faces << QRectF(0.1, 0.2, 0.3, 0.4);
    fdMock->setFaces(photo.url(), faces);
    fdMock->setValid(true);

    TagAreaModel model;
    model.classBegin();
    model.componentComplete();

    QSignalSpy countChanged(&model, SIGNAL(countChanged()));
    QSignalSpy photoIdChanged(&model, SIGNAL(photoIdChanged()));
    QSignalSpy detectingChanged(&model, SIGNAL(detectingChanged()));

    QCOMPARE(model.property("count").toInt(), 0);
    QCOMPARE(model.property("photoId").toInt(), -1);
    QCOMPARE(model.property("detecting").toBool(), false);
    QCOMPARE(model.property("canDetect").toBool(), true);

    model.setProperty("photoId", 12);
    QCOMPARE(model.property("photoId").toInt(), 12);

    QCOMPARE(photoIdChanged.count(), 1);
    photoIdChanged.clear();
    QTRY_COMPARE(countChanged.count(), 1);
    countChanged.clear();
    QCOMPARE(model.property("count").toInt(), 0);

    /* Set the photo Id to a valid photo */
    model.setProperty("photoId", photoId);
    QCOMPARE(model.property("photoId").toInt(), photoId);
    QCOMPARE(photoIdChanged.count(), 1);
    photoIdChanged.clear();
    QTRY_COMPARE(countChanged.count(), 1);
    countChanged.clear();
    QCOMPARE(model.property("count").toInt(), 0);

    /* Start detection */
    QCOMPARE(detectingChanged.count(), 0);
    model.detectFaces();

    countChanged.wait();
    QCOMPARE(countChanged.count(), 1);
    QCOMPARE(model.property("count").toInt(), 1);
    QCOMPARE(detectingChanged.count(), 2);
    QCOMPARE(model.property("detecting").toBool(), false);

    /* Set the detector as invalid */
    fdMock->setValid(false);
    QCOMPARE(model.property("canDetect").toBool(), false);
}

void TagAreaModelTest::testRoles_data()
{
    QTest::addColumn<QString>("tag");
    QTest::addColumn<QRectF>("area");

    QTest::newRow("empty") <<
        "" <<
        QRectF();

    QTest::newRow("no tag") <<
        "" <<
        QRectF(0.1, 0.2, 0.3, 0.4);

    QTest::newRow("all set") <<
        "Maria" <<
        QRectF(0.3, 0.2, 0.4, 0.6);
}

void TagAreaModelTest::testRoles()
{
    QFETCH(QString, tag);
    QFETCH(QRectF, area);

    QScopedPointer<Database> db(Database::instance());
    Database::Photo photo;
    photo.setBaseUri("/tmp");
    photo.setFileName("foo.jpg");
    PhotoId photoId = db->addPhoto(photo, 1);
    QVERIFY(photoId != -1);

    FaceDetectorPrivate *fdMock = new FaceDetectorPrivate();
    fdMock->setFaces(photo.url(), Areas() << area);

    TagAreaModel model;
    model.classBegin();
    model.setPhotoId(photoId);
    model.componentComplete();
    model.detectFaces();
    QTRY_COMPARE(model.rowCount(), 1);

    QCOMPARE(model.get(0, "rect").toRectF(), area);
    QCOMPARE(model.get(0, "editing").toBool(), false);
    QVERIFY(!model.get(0, "tag").value<Tag>().isValid());
}

void TagAreaModelTest::testDatabaseChange()
{
    QScopedPointer<Database> db(Database::instance());
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db.data());
    Database::Photo photo;
    photo.setBaseUri("/tmp");
    photo.setFileName("foo.jpg");
    PhotoId photoId = db->addPhoto(photo, 1);
    QVERIFY(photoId != -1);

    dbMock->setTags(QStringList() << "one" << "two" << "three");
    Tag tag1 = db->tag("one");
    Tag tag2 = db->tag("two");
    Tag tag3 = db->tag("three");
    QRectF rect1(0.1, 0.2, 0.3, 0.4);
    QRectF rect2(0.2, 0.1, 0.4, 0.3);
    bool ok = db->addTagWithArea(photoId, tag1, rect1);
    QVERIFY(ok);
    ok = db->addTagWithArea(photoId, tag2, rect2);
    QVERIFY(ok);

    QRectF faceRect(0.3, 0.4, 0.1, 0.2);
    FaceDetectorPrivate *fdMock = new FaceDetectorPrivate();
    fdMock->setFaces(photo.url(), Areas() << faceRect);

    TagAreaModel model;
    QSignalSpy countChanged(&model, SIGNAL(countChanged()));

    model.setPhotoId(photoId);
    QTRY_COMPARE(model.rowCount(), 2);
    countChanged.clear();

    QSet<TagArea> expectedAreas;
    expectedAreas.insert(TagArea(rect1, tag1));
    expectedAreas.insert(TagArea(rect2, tag2));
    QCOMPARE(modelAreas(&model), expectedAreas);

    model.detectFaces();
    QTRY_COMPARE(model.rowCount(), 3);
    expectedAreas.insert(TagArea(faceRect, Tag()));
    QCOMPARE(modelAreas(&model), expectedAreas);
    QCOMPARE(countChanged.count(), 1);
    countChanged.clear();

    /* Now add a new tag */
    QRectF rect3(0.2, 0.0, 0.1, 0.3);
    ok = db->addTagWithArea(photoId, tag3, rect3);
    QVERIFY(ok);
    QTRY_COMPARE(countChanged.count(), 1);
    expectedAreas.insert(TagArea(rect3, tag3));
    QCOMPARE(modelAreas(&model), expectedAreas);
}

void TagAreaModelTest::testCancellation()
{
    QScopedPointer<Database> db(Database::instance());
    Database::Photo photo;
    photo.setBaseUri("/tmp");
    photo.setFileName("foo.jpg");
    PhotoId photoId = db->addPhoto(photo, 1);
    QVERIFY(photoId != -1);

    FaceDetectorPrivate *fdMock = new FaceDetectorPrivate();
    Areas faces;
    faces << QRectF(0.1, 0.2, 0.3, 0.4);
    fdMock->setFaces(photo.url(), faces);
    fdMock->setValid(true);
    fdMock->setDetectionTime(200);

    TagAreaModel model;

    QSignalSpy countChanged(&model, SIGNAL(countChanged()));
    QSignalSpy detectingChanged(&model, SIGNAL(detectingChanged()));

    model.classBegin();
    model.setProperty("photoId", photoId);
    model.componentComplete();
    QCOMPARE(model.property("count").toInt(), 0);
    countChanged.clear();

    /* Start detection */
    QCOMPARE(detectingChanged.count(), 0);
    model.detectFaces();

    QTRY_COMPARE(detectingChanged.count(), 1);
    QTest::qWait(5);
    model.cancelDetection();

    detectingChanged.wait();
    QCOMPARE(countChanged.count(), 0);
    QCOMPARE(model.property("detecting").toBool(), false);

    QThreadPool::globalInstance()->waitForDone(5000);
}

void TagAreaModelTest::testSimilarity_data()
{
    QTest::addColumn<QRectF>("area");
    QTest::addColumn<QVector<QRectF>>("areas");
    QTest::addColumn<int>("expectedRowCount");

    QTest::newRow("empty") <<
        QRectF(0.1, 0.2, 0.3, 0.4) <<
        QVector<QRectF>() <<
        1;

    QTest::newRow("one new") <<
        QRectF(0.1, 0.2, 0.3, 0.4) <<
        (QVector<QRectF>() << QRectF(0.4, 0.6, 0.2, 0.3)) <<
        2;

    QTest::newRow("exact same") <<
        QRectF(0.1, 0.2, 0.3, 0.4) <<
        (QVector<QRectF>() << QRectF(0.1, 0.2, 0.3, 0.4)) <<
        1;

    QTest::newRow("one similar") <<
        QRectF(0.1, 0.2, 0.3, 0.4) <<
        (QVector<QRectF>() <<
         QRectF(0.11, 0.21, 0.31, 0.41) <<
         QRectF(0.4, 0.0, 0.2, 0.2)) <<
        2;
}

void TagAreaModelTest::testSimilarity()
{
    QFETCH(QRectF, area);
    QFETCH(QVector<QRectF>, areas);
    QFETCH(int, expectedRowCount);

    QScopedPointer<Database> db(Database::instance());
    Database::Photo photo;
    photo.setBaseUri("/tmp");
    photo.setFileName("foo.jpg");
    PhotoId photoId = db->addPhoto(photo, 1);
    QVERIFY(photoId != -1);

    int i = 0;
    Q_FOREACH(const QRectF &a, areas) {
        Tag tag = db->createTag(Tag(), QString("person-%1").arg(i++), "");
        bool ok = db->addTagWithArea(photoId, tag, a);
        QVERIFY(ok);
    }

    FaceDetectorPrivate *fdMock = new FaceDetectorPrivate();
    fdMock->setFaces(photo.url(), Areas() << area);

    TagAreaModel model;
    QSignalSpy detectingChanged(&model, SIGNAL(detectingChanged()));
    model.setPhotoId(photoId);
    model.detectFaces();
    QTRY_COMPARE(detectingChanged.count(), 2);
    QCOMPARE(model.rowCount(), expectedRowCount);
}

void TagAreaModelTest::testSignalAggregation()
{
    QScopedPointer<Database> db(Database::instance());
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db.data());
    Database::Photo photo;
    photo.setBaseUri("/tmp");
    photo.setFileName("foo.jpg");
    PhotoId photoId = db->addPhoto(photo, 1);
    QVERIFY(photoId != -1);

    dbMock->setTags(QStringList() << "one");
    Tag tag1 = db->tag("one");
    QRectF rect1(0.1, 0.2, 0.3, 0.4);
    bool ok = db->addTagWithArea(photoId, tag1, rect1);
    QVERIFY(ok);

    TagAreaModel model;

    QSignalSpy countChanged(&model, SIGNAL(countChanged()));
    QSignalSpy modelReset(&model, SIGNAL(modelReset()));

    model.classBegin();
    model.setProperty("photoId", photoId);
    model.setEditingAreas({
        QJsonObject {
            { "x", 0.2 },
            { "y", 0.1 },
            { "width", 0.3 },
            { "height", 0.4 },
        }
    });
    model.componentComplete();
    QCOMPARE(modelReset.count(), 1);
    QCOMPARE(countChanged.count(), 1);
    QCOMPARE(model.property("count").toInt(), 2);
    countChanged.clear();
    modelReset.clear();

    model.setEditingAreas({});
    QVERIFY(modelReset.wait());
    QCOMPARE(modelReset.count(), 1);
    QCOMPARE(model.property("count").toInt(), 1);
}

void TagAreaModelTest::testTagDeletion()
{
    QScopedPointer<Database> db(Database::instance());
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db.data());
    Database::Photo photo;
    photo.setBaseUri("/tmp");
    photo.setFileName("foo.jpg");
    PhotoId photoId = db->addPhoto(photo, 1);
    QVERIFY(photoId != -1);

    dbMock->setTags(QStringList { "one", "two" });

    Tag tag1 = db->tag("one");
    QRectF rect1(0.1, 0.2, 0.3, 0.4);
    bool ok = db->addTagWithArea(photoId, tag1, rect1);
    QVERIFY(ok);

    Tag tag2 = db->tag("two");
    QRectF rect2(0.3, 0.4, 0.5, 0.6);
    ok = db->addTagWithArea(photoId, tag2, rect2);
    QVERIFY(ok);

    TagAreaModel model;
    model.classBegin();
    model.setProperty("photoId", photoId);
    model.componentComplete();
    QCOMPARE(model.property("count").toInt(), 2);

    QSignalSpy countChanged(&model, SIGNAL(countChanged()));
    QSignalSpy modelReset(&model, SIGNAL(modelReset()));

    QVERIFY(db->deleteTag(tag1));

    QTRY_COMPARE(modelReset.count(), 1);
    QCOMPARE(model.property("count").toInt(), 1);
}

QTEST_GUILESS_MAIN(TagAreaModelTest)

#include "tst_tag_area_model.moc"
