/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "thumbnailer.h"

#include <QCryptographicHash>
#include <QDateTime>
#include <QDir>
#include <QSize>
#include <QTemporaryDir>
#include <QTest>

using namespace Imaginario;

class ThumbnailerTest: public QObject
{
    Q_OBJECT

public:
    ThumbnailerTest();

    void populateCache(bool size256Used = false, bool size512Used = false);

private Q_SLOTS:
    void testLoad_data();
    void testLoad();
    void testLoadSize_data();
    void testLoadSize();
    void testCreate_data();
    void testCreate();

private:
    QDir m_dataDir;
};

ThumbnailerTest::ThumbnailerTest():
    m_dataDir(QString(ITEMS_DIR "/"))
{
}

void ThumbnailerTest::populateCache(bool size256Used, bool size512Used)
{
    Thumbnailer t;

    Thumbnailer::size256Used() = size256Used;
    Thumbnailer::size512Used() = size512Used;

    QStringList fileNames {
        "image0.png", "image1.jpg", "image2.jpg", "image3.png", "image4.jpg",
        "image5.jpg", "image6.jpg"
    };

    Q_FOREACH(const QString &fileName, fileNames) {
        t.create(QUrl::fromLocalFile(m_dataDir.filePath(fileName)));
    }
}

void ThumbnailerTest::testLoad_data()
{
    QTest::addColumn<QUrl>("uri");
    QTest::addColumn<bool>("has256thumbnail");
    QTest::addColumn<bool>("has512thumbnail");
    QTest::addColumn<QSize>("requestedSize");
    QTest::addColumn<QSize>("expectedSize");

    QTest::newRow("invalid URI") <<
        QUrl("http?:don't think so") <<
        true << false <<
        QSize(40, 40) <<
        QSize(-1, -1);

    QTest::newRow("nonexisting file") <<
        QUrl("file:///etc/do you really have a file with this name?") <<
        true << false <<
        QSize(40, 40) <<
        QSize(-1, -1);

    QTest::newRow("landscape") <<
        QUrl::fromLocalFile(m_dataDir.filePath("image5.jpg")) <<
        true << false <<
        QSize(40, 40) <<
        QSize(40, 26);

    QTest::newRow("portrait") <<
        QUrl::fromLocalFile(m_dataDir.filePath("image6.jpg")) <<
        true << false <<
        QSize(40, 40) <<
        QSize(30, 40);

    QTest::newRow("request xlarge size, present") <<
        QUrl::fromLocalFile(m_dataDir.filePath("image6.jpg")) <<
        false << true <<
        QSize(400, 400) <<
        QSize(300, 400);

    QTest::newRow("request xlarge size, missing") <<
        QUrl::fromLocalFile(m_dataDir.filePath("image6.jpg")) <<
        true << false <<
        QSize(400, 400) <<
        QSize(-1, -1);

    QTest::newRow("request huge size") <<
        QUrl::fromLocalFile(m_dataDir.filePath("image6.jpg")) <<
        true << true <<
        QSize(40, 800) <<
        QSize(-1, -1);
}

void ThumbnailerTest::testLoad()
{
    QFETCH(QUrl, uri);
    QFETCH(bool, has256thumbnail);
    QFETCH(bool, has512thumbnail);
    QFETCH(QSize, requestedSize);
    QFETCH(QSize, expectedSize);

    QTemporaryDir tmpDir;
    qputenv("XDG_CACHE_HOME", tmpDir.path().toUtf8());
    populateCache(has256thumbnail, has512thumbnail);

    Thumbnailer thumbnailer;
    QImage t = thumbnailer.load(uri, requestedSize);

    if (expectedSize.isValid()) {
        QCOMPARE(t.size(), expectedSize);
    } else {
        QVERIFY(t.isNull());
    }
}

void ThumbnailerTest::testLoadSize_data()
{
    QTest::addColumn<QUrl>("uri");
    QTest::addColumn<QList<QSize>>("requestedSizes");
    QTest::addColumn<bool>("size256Used");
    QTest::addColumn<bool>("size512Used");

    QTest::newRow("invalid URI") <<
        QUrl("http?:don't think so") <<
        QList<QSize> { QSize(40, 40) } <<
        true << false;

    QTest::newRow("small sizes") <<
        QUrl::fromLocalFile(m_dataDir.filePath("image5.jpg")) <<
        QList<QSize> {
            QSize(40, 40),
            QSize(30, 40),
            QSize(40, 60),
        } <<
        true << false;

    QTest::newRow("request big sizes") <<
        QUrl::fromLocalFile(m_dataDir.filePath("image6.jpg")) <<
        QList<QSize> {
            QSize(300, 200),
            QSize(400, 300),
        } <<
        false << true;

    QTest::newRow("request all sizes") <<
        QUrl::fromLocalFile(m_dataDir.filePath("image6.jpg")) <<
        QList<QSize> {
            QSize(40, 60),
            QSize(300, 200),
            QSize(400, 300),
        } <<
        true << true;
}

void ThumbnailerTest::testLoadSize()
{
    QFETCH(QUrl, uri);
    QFETCH(QList<QSize>, requestedSizes);
    QFETCH(bool, size256Used);
    QFETCH(bool, size512Used);

    QTemporaryDir tmpDir;
    qputenv("XDG_CACHE_HOME", tmpDir.path().toUtf8());
    populateCache();

    Thumbnailer::size256Used() = false;
    Thumbnailer::size512Used() = false;

    Thumbnailer thumbnailer;
    for (const QSize &requestedSize: requestedSizes) {
        thumbnailer.load(uri, requestedSize);
    }

    QCOMPARE(thumbnailer.size256Used(), size256Used);
    QCOMPARE(thumbnailer.size512Used(), size512Used);
}

void ThumbnailerTest::testCreate_data()
{
    QTest::addColumn<QUrl>("uri");
    QTest::addColumn<bool>("shouldSucceed");
    QTest::addColumn<bool>("failFile");

    QTest::newRow("nonexisting file") <<
        QUrl("file:///etc/do you really have a file with this name?") <<
        false <<
        false;

    QTest::newRow("non image file") <<
        QUrl::fromLocalFile(QDir::homePath()) <<
        false <<
        true;

    QTest::newRow("tiny size") <<
        QUrl::fromLocalFile(m_dataDir.filePath("image1.jpg")) <<
        true << false;

    QTest::newRow("landscape") <<
        QUrl::fromLocalFile(m_dataDir.filePath("image5.jpg")) <<
        true << false;

    QTest::newRow("portrait") <<
        QUrl::fromLocalFile(m_dataDir.filePath("image6.jpg")) <<
        true << false;
}

void ThumbnailerTest::testCreate()
{
    QFETCH(QUrl, uri);
    QFETCH(bool, shouldSucceed);
    QFETCH(bool, failFile);

    QTemporaryDir tmpDir;
    qputenv("XDG_CACHE_HOME", tmpDir.path().toUtf8());
    QCoreApplication::setApplicationName("gallery");
    QCoreApplication::setApplicationVersion("0.34");

    Thumbnailer thumbnailer;
    bool ok = thumbnailer.create(uri);

    QCOMPARE(ok, shouldSucceed);

    QByteArray ba = QCryptographicHash::hash(uri.toEncoded(),
                                             QCryptographicHash::Md5);
    QString fileName = QString::fromLatin1(ba.toHex()) + ".png";
    QFileInfo thumbInfo(tmpDir.path() + "/thumbnails/large/" + fileName);
    QFileInfo failInfo(tmpDir.path() + "/thumbnails/fail/gallery/" + fileName);

    QCOMPARE(thumbInfo.exists(), shouldSucceed);
    QCOMPARE(failInfo.exists(), failFile);

    if (!shouldSucceed && !failFile) return;

    QFileInfo fileInfo(uri.toLocalFile());
    /*Check if the created file is proper */
    QImage t(shouldSucceed ? thumbInfo.filePath() : failInfo.filePath());
    QCOMPARE(t.text("Software"), QString("gallery 0.34"));
    QCOMPARE(t.text("Thumb::URI"), uri.toString());
    QCOMPARE(t.text("Thumb::MTime"),
             QString::number(fileInfo.lastModified().toTime_t()));
}

QTEST_GUILESS_MAIN(ThumbnailerTest)

#include "tst_thumbnailer.moc"
