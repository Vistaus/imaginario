/*
 * Copyright (C) 2016-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "location_model.h"
#include "test_utils.h"
#include "types.h"

#include <QSignalSpy>
#include <QTest>
#include <QVariantMap>
#include <QVector>

using namespace Imaginario;

class SourceModel: public QAbstractListModel
{
    Q_OBJECT

public:
    enum Roles {
        StringRole = Qt::UserRole + 1,
        IntRole,
        BoolRole,
        LocationRole,
    };

    void setData(const QVector<GeoPoint> &locations);
    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index,
                  int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

private:
    QVector<GeoPoint> m_locations;
};

void SourceModel::setData(const QVector<GeoPoint> &locations)
{
    beginResetModel();
    m_locations = locations;
    endResetModel();
}

int SourceModel::rowCount(const QModelIndex &) const
{
    return m_locations.count();
}

QVariant SourceModel::data(const QModelIndex &index, int role) const
{
    GeoPoint loc = m_locations[index.row()];
    switch (role) {
    case StringRole:
        return QString("%1,%2").
            arg(loc.lat, 0, 'f', 2).arg(loc.lon, 0, 'f', 2);
    case IntRole:
        return int(loc.lat + loc.lon);
    case BoolRole:
        return loc.lat > 0;
    case LocationRole:
        return QVariant::fromValue(loc);
    default:
        qWarning() << "Unknown role ID:" << role;
        return QVariant();
    }
}

QHash<int, QByteArray> SourceModel::roleNames() const
{
    return QHash<int, QByteArray> {
        { StringRole, "string" },
        { IntRole, "int" },
        { BoolRole, "bool" },
        { LocationRole, "location" },
    };
}

class LocationModelTest: public QObject
{
    Q_OBJECT

public:
    LocationModelTest() {
        qRegisterMetaType<GeoPoint>("GeoPoint");
    }

    static QVariant get(const QAbstractListModel *model, int row,
                        QString roleName);

private Q_SLOTS:
    void testProperties();
    void testCount_data();
    void testCount();
    void testNonGeotaggedCount_data();
    void testNonGeotaggedCount();
    void testSourceRoles_data();
    void testSourceRoles();
};

QVariant LocationModelTest::get(const QAbstractListModel *model, int row,
                             QString roleName)
{
    QHash<int, QByteArray> roleNames = model->roleNames();

    int role = roleNames.key(roleName.toLatin1(), -1);
    return model->data(model->index(row), role);
}

void LocationModelTest::testProperties()
{
    LocationModel model;
    SourceModel source;

    QSignalSpy modelChanged(&model, SIGNAL(modelChanged()));
    QSignalSpy usedRolesChanged(&model, SIGNAL(usedRolesChanged()));
    QSignalSpy zoomLevelChanged(&model, SIGNAL(zoomLevelChanged()));

    QVERIFY(!model.property("model").value<QAbstractItemModel*>());
    model.setProperty("model", QVariant::fromValue(&source));
    QCOMPARE(model.property("model").value<QAbstractItemModel*>(), &source);
    QCOMPARE(modelChanged.count(), 1);
    model.setProperty("model", QVariant::fromValue((QAbstractItemModel*)nullptr));
    QCOMPARE(modelChanged.count(), 2);

    QCOMPARE(model.property("usedRoles").toStringList(), QStringList());
    QStringList roles { "first", "second" };
    model.setProperty("usedRoles", roles);
    QCOMPARE(model.property("usedRoles").toStringList(), roles);
    QCOMPARE(usedRolesChanged.count(), 1);

    QVERIFY(model.property("zoomLevel").toDouble() < 0);
    model.setProperty("zoomLevel", qreal(8.4));
    QVERIFY(qFuzzyCompare(model.zoomLevel(), 8.4));
    QCOMPARE(zoomLevelChanged.count(), 1);

    QCOMPARE(model.property("count").toInt(), 0);

    QHash<int, QByteArray> expectedRoleNames {
        { LocationModel::LocationRole, "location" },
        { LocationModel::CountRole, "count" },
    };
    QCOMPARE(model.roleNames(), expectedRoleNames);
}

void LocationModelTest::testCount_data()
{
    QTest::addColumn<QVector<GeoPoint>>("points");
    QTest::addColumn<qreal>("zoomLevel");
    QTest::addColumn<int>("expectedCount");

    QTest::newRow("empty") <<
        QVector<GeoPoint>() <<
        4.0 <<
        0;

    QTest::newRow("one, z=4") <<
        QVector<GeoPoint>{{60.0, 24.0}} <<
        4.0 <<
        1;

    QTest::newRow("one, z=12") <<
        QVector<GeoPoint>{{60.0, 24.0}} <<
        12.0 <<
        1;

    QTest::newRow("three, z=12") <<
        QVector<GeoPoint>{
            {60.0, 24.0},
            {60.3, 24.4},
            {50.0, 24.0},
        } <<
        12.0 <<
        3;

    QTest::newRow("three, z=6") <<
        QVector<GeoPoint>{
            {60.0, 24.0},
            {60.3, 24.4},
            {50.0, 24.0},
        } <<
        6.0 <<
        2;

    QTest::newRow("three, z=1") <<
        QVector<GeoPoint>{
            {60.0, 24.0},
            {60.3, 24.4},
            {50.0, 24.0},
        } <<
        1.0 <<
        1;
}

void LocationModelTest::testCount()
{
    QFETCH(QVector<GeoPoint>, points);
    QFETCH(qreal, zoomLevel);
    QFETCH(int, expectedCount);

    LocationModel model;
    SourceModel source;

    QSignalSpy countChanged(&model, SIGNAL(countChanged()));

    model.classBegin();
    model.setModel(QVariant::fromValue(&source));
    model.setZoomLevel(zoomLevel);
    model.componentComplete();

    QCOMPARE(model.rowCount(), 0);
    countChanged.clear();

    source.setData(points);
    countChanged.wait();
    QCOMPARE(countChanged.count(), 1);

    QCOMPARE(model.rowCount(), expectedCount);

    /* Check the total number of items */
    int expectedNumItems = points.count();
    int numItems = 0;
    for (int i = 0; i < expectedCount; i++) {
        QModelIndex index = model.index(i, 0);
        numItems += index.data(LocationModel::CountRole).toInt();

        // all locations must be valid
        QVariant vGeo = model.get(i, "location");
        QVERIFY(vGeo.isValid());
        GeoPoint geo = vGeo.value<GeoPoint>();
        QVERIFY(geo.isValid());
    }

    QCOMPARE(numItems, expectedNumItems);
}

void LocationModelTest::testNonGeotaggedCount_data()
{
    QTest::addColumn<QVector<GeoPoint>>("points");
    QTest::addColumn<int>("expectedCount");

    QTest::newRow("empty") <<
        QVector<GeoPoint>() <<
        0;

    QTest::newRow("all geotagged") <<
        QVector<GeoPoint>{
            {60.0, 24.0},
            {60.3, 24.4},
            {50.0, 24.0},
        } <<
        0;

    QTest::newRow("one untagged") <<
        QVector<GeoPoint>{
            {60.0, 24.0},
            {60.3, 24.4},
            {},
            {50.0, 24.0},
        } <<
        1;

    QTest::newRow("all untagged") <<
        QVector<GeoPoint>{
            {},
            {},
            {},
            {},
        } <<
        4;
}

void LocationModelTest::testNonGeotaggedCount()
{
    QFETCH(QVector<GeoPoint>, points);
    QFETCH(int, expectedCount);

    LocationModel model;
    SourceModel source;

    QSignalSpy countChanged(&model, SIGNAL(countChanged()));

    model.classBegin();
    model.setModel(QVariant::fromValue(&source));
    model.setZoomLevel(8.0);
    model.componentComplete();

    QCOMPARE(model.nonGeoTaggedCount(), 0);
    countChanged.clear();

    source.setData(points);
    countChanged.wait();
    QCOMPARE(countChanged.count(), 1);

    QCOMPARE(model.nonGeoTaggedCount(), expectedCount);
}

void LocationModelTest::testSourceRoles_data()
{
    QTest::addColumn<QStringList>("usedRoles");
    QTest::addColumn<QVariantMap>("expectedItem");

    QTest::newRow("empty") <<
        QStringList() <<
        QVariantMap {
            { "bool", true },
            { "int", 84 },
            { "location", QVariant::fromValue(GeoPoint{60.0, 24.0}) },
            { "string", "60.00,24.00" },
        };

    QTest::newRow("one role") <<
        QStringList { "int" } <<
        QVariantMap {
            { "int", 84 },
        };
}

void LocationModelTest::testSourceRoles()
{
    QFETCH(QStringList, usedRoles);
    QFETCH(QVariantMap, expectedItem);

    LocationModel model;
    SourceModel source;

    source.setData(QVector<GeoPoint>{
        { 60.0, 24.0 },
    });

    model.classBegin();
    model.setModel(QVariant::fromValue(&source));
    model.setZoomLevel(8);
    model.setUsedRoles(usedRoles);
    model.componentComplete();

    QCOMPARE(model.rowCount(), 1);

    auto items = model.getItems(0);
    QVariantMap item = items[0].toMap();

    QCOMPARE(item, expectedItem);
}

QTEST_GUILESS_MAIN(LocationModelTest)

#include "tst_location_model.moc"
