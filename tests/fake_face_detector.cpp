/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fake_face_detector.h"

#include <QThread>

using namespace Imaginario;

static FaceDetectorPrivate *m_privateInstance = 0;

FaceDetectorPrivate::FaceDetectorPrivate():
    QObject(),
    m_isValid(true),
    m_detectionTime(0),
    q_ptr(0)
{
    m_privateInstance = this;
}

FaceDetectorPrivate::~FaceDetectorPrivate()
{
    m_privateInstance = 0;
}

FaceDetector::FaceDetector(const QString &)
{
    if (m_privateInstance) {
        m_privateInstance->q_ptr = this;
        d_ptr = m_privateInstance;
    } else {
        d_ptr = new FaceDetectorPrivate(this);
    }
}

FaceDetector::~FaceDetector()
{
    delete d_ptr;
}

bool FaceDetector::isValid() const
{
    Q_D(const FaceDetector);
    return d->m_isValid;
}

QList<QRectF> FaceDetector::checkFile(const QUrl &url)
{
    Q_D(FaceDetector);

    if (!d->m_isValid || !d->m_faces.contains(url)) {
       return QList<QRectF>();
    }
    QThread::msleep(d->m_detectionTime);
    return d->m_faces[url];
}
