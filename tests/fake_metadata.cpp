/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fake_metadata.h"

using namespace Imaginario;

static MetadataPrivate *m_privateInstance = 0;

MetadataPrivate::MetadataPrivate():
    m_embed(false)
{
    m_privateInstance = this;
}

MetadataPrivate::~MetadataPrivate()
{
    m_privateInstance = 0;
}

Metadata::Metadata(QObject *parent):
    QObject(parent)
{
    if (m_privateInstance) {
        m_privateInstance->q_ptr = this;
        d_ptr = m_privateInstance;
        Q_EMIT d_ptr->instanceCreated(this);
    } else {
        d_ptr = new MetadataPrivate(this);
    }
    qRegisterMetaType<ImportData>();
}

Metadata::~Metadata()
{
    delete d_ptr;
}

void Metadata::setEmbed(bool embed)
{
    Q_D(Metadata);
    if (embed == d->m_embed) return;
    d->m_embed = embed;
    Q_EMIT embedChanged();
}

bool Metadata::embed() const
{
    Q_D(const Metadata);
    return d->m_embed;
}

bool Metadata::writeChanges(const QString &file, const Changes &changes)
{
    Q_D(Metadata);
    Q_EMIT d->writeChangesCalled(file, changes);
    return true;
}

bool Metadata::readImportData(const QString &file, ImportData &data) const
{
    Q_D(const Metadata);
    if (d->m_importData.contains(file)) {
        data = d->m_importData[file];
        return true;
    } else {
        return false;
    }
}

QImage Metadata::loadPreview(const QString &file, QSize *size,
                             const QSize &requestedSize) const
{
    Q_D(const Metadata);
    Q_EMIT const_cast<MetadataPrivate*>(d)->loadPreviewCalled(file,
                                                              requestedSize);
    if (d->m_preview.contains(file)) {
        const MetadataPrivate::PreviewData &pd = d->m_preview[file];
        if (pd.size.isValid()) {
            *size = pd.size;
        }
        return pd.preview;
    } else {
        return QImage();
    }
}
