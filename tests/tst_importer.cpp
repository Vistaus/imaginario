/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fake_database.h"
#include "fake_duplicate_detector.h"
#include "fake_metadata.h"
#include "importer.h"
#include "test_utils.h"

#include <QMetaObject>
#include <QSignalSpy>
#include <QTemporaryDir>
#include <QTest>
#include <QThreadPool>

using namespace Imaginario;

typedef QList<QList<QVariant> > SignalData;
typedef QHash<QString,QSet<QString>> PhotoTags;
typedef QHash<QString,QSet<Metadata::RegionInfo>> PhotoRegions;
typedef QHash<int,QStringList> IndexTags;

namespace QTest {

template<>
char *toString(const QVector<QPair<int,int>> &pairs)
{
    QByteArray ba = "QVector(";
    QStringList parts;
    for (const QPair<int,int> &pair: pairs) {
        parts.append(QString("{ %1 - %2 }").
                     arg(pair.first).arg(pair.second));
    }
    ba += parts.join(", ").toUtf8().constData();
    ba += ")";
    return qstrdup(ba.data());
}

} // namespace

class ImporterTest: public QObject
{
    Q_OBJECT

public:
    ImporterTest() {
        qRegisterMetaType<PhotoId>("PhotoId");
        qRegisterMetaType<Tag>("Tag");
    }

private Q_SLOTS:
    void cleanup() {
        QThreadPool::globalInstance()->waitForDone(5000);
    }
    void testProperties();
    void testEmpty();
    void testRoles_data();
    void testRoles();
    void testCopy_data();
    void testCopy();
    void testCopyCollision_data();
    void testCopyCollision();
    void testTags_data();
    void testTags();
    void testRegions_data();
    void testRegions();
    void testDuplicates_data();
    void testDuplicates();
    void testAutoClear_data();
    void testAutoClear();
    void testItemAdding();
    void testItemTags_data();
    void testItemTags();
    void testPairing_data();
    void testPairing();
    void testFiltering_data();
    void testFiltering();
    void testRollCreation_data();
    void testRollCreation();
};

void ImporterTest::testProperties()
{
    Database *db = Database::instance();
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db);
    QStringList allTags;
    allTags << "a house" << "red";
    dbMock->setTags(allTags);

    Importer importer;

    QCOMPARE(importer.property("running").toBool(), false);

    QCOMPARE(importer.property("unsupportedCount").toInt(), 0);

    QCOMPARE(importer.property("embed").toBool(), false);
    importer.setProperty("embed", true);
    QCOMPARE(importer.property("embed").toBool(), true);

    QCOMPARE(importer.property("writeXmp").toBool(), true);
    importer.setProperty("writeXmp", false);
    QCOMPARE(importer.property("writeXmp").toBool(), false);

    QCOMPARE(importer.property("copyFiles").toBool(), false);
    importer.setProperty("copyFiles", true);
    QCOMPARE(importer.property("copyFiles").toBool(), true);

    importer.setProperty("destination", "/here");
    QCOMPARE(importer.property("destination").toString(), QString("/here"));

    Tag tag = db->tag("red");
    importer.setProperty("parentTag", QVariant::fromValue(tag));
    QCOMPARE(importer.property("parentTag").value<Tag>(), tag);

    QStringList tags = allTags;
    importer.setProperty("commonTags", tags);
    QCOMPARE(importer.property("commonTags").toStringList(), tags);

    Importer::ClearStatuses autoClear =
        Importer::ClearIgnored | Importer::ClearFailed;
    importer.setProperty("autoClear", QVariant::fromValue(autoClear));
    QCOMPARE(importer.property("autoClear").value<Importer::ClearStatuses>(),
             autoClear);

    QCOMPARE(importer.property("rawPolicy").value<Importer::RawPolicy>(),
             Importer::EditedIsMaster);
    QCOMPARE(importer.rawPolicy(), Importer::EditedIsMaster);
    importer.setProperty("rawPolicy",
                         QVariant::fromValue(Importer::RawIsMaster));
    QCOMPARE(importer.rawPolicy(), Importer::RawIsMaster);

    delete db;
}

void ImporterTest::testEmpty()
{
    Database *db = Database::instance();

    Importer importer;

    QCOMPARE(importer.importedCount(), 0);
    QCOMPARE(importer.failedCount(), 0);
    QCOMPARE(importer.rowCount(), 0);

    QSignalSpy finished(&importer, SIGNAL(finished(int)));

    importer.exec();

    finished.wait();
    QCOMPARE(finished.count(), 1);
    QCOMPARE(importer.importedCount(), 0);
    QCOMPARE(importer.failedCount(), 0);

    delete db;
}

void ImporterTest::testRoles_data()
{
    QTest::addColumn<QUrl>("url");

    QTest::newRow("local URL") <<
        QUrl("file:///tmp/foo.png");
}

void ImporterTest::testRoles()
{
    QFETCH(QUrl, url);

    Database *db = Database::instance();

    Importer importer;
    importer.addItems(QList<QUrl>() << url);

    QCOMPARE(importer.rowCount(), 1);

    QCOMPARE(importer.get(0, "url").toUrl(), url);
    QCOMPARE(importer.get(0, "status").toInt(), int(Importer::Waiting));
    QCOMPARE(importer.get(0, "duplicateAction").toInt(),
             int(Importer::DuplicateCheckNeeded));
    QCOMPARE(importer.get(0, "isPair").toBool(), false);

    delete db;
}

void ImporterTest::testCopy_data()
{
    QTest::addColumn<QStringList>("fileNames");
    QTest::addColumn<QStringList>("expectedFailures");

    QTest::newRow("one file") <<
        (QStringList() << "image0.png") <<
        QStringList();

    QTest::newRow("some files") <<
        (QStringList() << "image1.jpg" << "image0.png" << "image2.jpg") <<
        QStringList();

    QTest::newRow("with missing file") <<
        (QStringList() << "image1.jpg" << "imageNaN.png") <<
        (QStringList() << "imageNaN.png");
}

void ImporterTest::testCopy()
{
    QFETCH(QStringList, fileNames);
    QFETCH(QStringList, expectedFailures);

    Database *db = Database::instance();

    // Prepare the list of URLs
    QDir dataDir(ITEMS_DIR);
    QList<QUrl> items;
    QList<QUrl> expectedImportedItems;
    QList<QUrl> expectedFailedItems;
    Q_FOREACH(const QString &fileName, fileNames) {
        QUrl url;
        if (fileName.startsWith("http://")) {
            url = QUrl(fileName);
        } else {
            QString sourceFile = dataDir.absoluteFilePath(fileName);
            url = QUrl::fromLocalFile(sourceFile);
        }
        items.append(url);
        if (expectedFailures.contains(fileName)) {
            expectedFailedItems.append(url);
        } else {
            expectedImportedItems.append(url);
        }
    }

    QTemporaryDir tmpDir;

    Importer importer;
    QSignalSpy dataChanged(&importer,
                           SIGNAL(dataChanged(const QModelIndex&,
                                              const QModelIndex&)));
    QSignalSpy finished(&importer, SIGNAL(finished(int)));
    QSignalSpy runningChanged(&importer, SIGNAL(isRunningChanged()));
    importer.setCopyFiles(true);
    importer.setDestination(tmpDir.path());
    importer.addItems(items);
    importer.exec();

    finished.wait();
    QCOMPARE(finished.count(), 1);
    QVERIFY(finished.at(0).at(0).toInt() >= 0);

    QTRY_COMPARE(runningChanged.count(), 2);

    QTRY_COMPARE(importer.importedCount(), expectedImportedItems.count());
    QTRY_COMPARE(importer.failedCount(), expectedFailedItems.count());

    /* For each item, there should be a dataChanged signal to the
     * Processing State, and a second one for either Done or Failed */
    QCOMPARE(dataChanged.count(), items.count() * 2);

    /* Verify that the status is either Done or Failed */
    for (int i = 0; i < items.count(); i++) {
        Importer::ItemStatus expectedStatus =
            expectedImportedItems.contains(items.at(i)) ?
            Importer::Done : Importer::Failed;
        QCOMPARE(importer.get(i, "status").toInt(), int(expectedStatus));
    }
    delete db;
}

void ImporterTest::testCopyCollision_data()
{
    QTest::addColumn<QStringList>("fileNames");
    QTest::addColumn<QStringList>("expectedFileNames");

    QTest::newRow("one file, no collisions") <<
        (QStringList() << "image0.png") <<
        (QStringList() << "image0.png");

    QTest::newRow("some files, with collisions") <<
        (QStringList() << "image1.jpg" << "image2.jpg") <<
        (QStringList() << "image1 (1).jpg" << "image2 (3).jpg");
}

void ImporterTest::testCopyCollision()
{
    QFETCH(QStringList, fileNames);
    QFETCH(QStringList, expectedFileNames);

    Database *db = Database::instance();
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db);
    QSignalSpy addPhotoCalled(dbMock,
                  SIGNAL(addPhotoCalled(Imaginario::Database::Photo,int)));

    // Prepare the list of URLs
    QDir dataDir(ITEMS_DIR);
    QList<QUrl> items;
    Q_FOREACH(const QString &fileName, fileNames) {
        QString sourceFile = dataDir.absoluteFilePath(fileName);
        QUrl url = QUrl::fromLocalFile(sourceFile);
        items.append(url);
    }

    QTemporaryDir tmp;
    QDir tmpDir(tmp.path());
    // Create a few existing files
    QString sourceFile = dataDir.absoluteFilePath("image1.jpg");
    QFile::copy(sourceFile, tmpDir.filePath("image1.jpg"));
    sourceFile = dataDir.absoluteFilePath("image2.jpg");
    QFile::copy(sourceFile, tmpDir.filePath("image2.jpg"));
    QFile::copy(sourceFile, tmpDir.filePath("image2 (1).jpg"));
    QFile::copy(sourceFile, tmpDir.filePath("image2 (2).jpg"));

    Importer importer;
    QSignalSpy dataChanged(&importer,
                           SIGNAL(dataChanged(const QModelIndex&,
                                              const QModelIndex&)));
    QSignalSpy finished(&importer, SIGNAL(finished(int)));
    importer.setCopyFiles(true);
    importer.setDestination(tmp.path());
    importer.addItems(items);
    importer.exec();

    finished.wait();
    QCOMPARE(finished.count(), 1);
    QVERIFY(finished.at(0).at(0).toInt() >= 0);
    QCOMPARE(importer.importedCount(), items.count());
    QCOMPARE(importer.failedCount(), 0);

    QCOMPARE(addPhotoCalled.count(), items.count());
    QStringList createdFileNames;
    for (int i = 0; i < addPhotoCalled.count(); i++) {
        Database::Photo p =
            addPhotoCalled.at(i).at(0).value<Database::Photo>();
        createdFileNames.append(p.fileName());
    }
    QCOMPARE(createdFileNames, expectedFileNames);

    delete db;
}

void ImporterTest::testTags_data()
{
    QTest::addColumn<QStringList>("fileNames");
    QTest::addColumn<QStringList>("assignedTags");
    QTest::addColumn<QStringList>("commonTags");
    QTest::addColumn<IndexTags>("setTags");
    QTest::addColumn<PhotoTags>("importedTags");

    IndexTags setTags;
    QTest::newRow("no file tags, no common tags") <<
        (QStringList() << "image0.png") <<
        QStringList() <<
        QStringList() <<
        setTags <<
        PhotoTags {
            { "image0.png", {} },
        };

    QTest::newRow("no file tags, no common tags, assigned") <<
        (QStringList() << "image0.png") <<
        (QStringList() << "green") <<
        QStringList() <<
        setTags <<
        PhotoTags {
            { "image0.png", { "green" } },
        };

    QTest::newRow("no file tags, common tags") <<
        (QStringList() << "image0.png") <<
        QStringList() <<
        (QStringList() << "blue" << "cat") <<
        setTags <<
        PhotoTags {
            { "image0.png", { "blue", "cat" } },
        };

    QTest::newRow("file tags, common tags") <<
        (QStringList() << "image1.jpg") <<
        QStringList() <<
        (QStringList() << "blue" << "big") <<
        setTags <<
        PhotoTags {
            { "image1.jpg", { "blue", "big" } },
        };

    QTest::newRow("file tags, no common tags") <<
        (QStringList() << "image1.jpg") <<
        QStringList() <<
        QStringList() <<
        setTags <<
        PhotoTags {
            { "image1.jpg", {} },
        };

    setTags[0] = QStringList() << "dog";
    QTest::newRow("file tags, common tags, set tags") <<
        (QStringList() << "image1.jpg") <<
        QStringList() <<
        (QStringList() << "blue" << "big") <<
        setTags <<
        PhotoTags {
            { "image1.jpg", { "blue", "big", "dog" } },
        };
}

void ImporterTest::testTags()
{
    QFETCH(QStringList, fileNames);
    QFETCH(QStringList, assignedTags);
    QFETCH(QStringList, commonTags);
    QFETCH(IndexTags, setTags);
    QFETCH(PhotoTags, importedTags);

    /* Prepare some fake image metadata */
    QHash<QString,QStringList> photoTags;
    photoTags["image0.png"] = QStringList();
    photoTags["image1.jpg"] =
        QStringList() << "red" << "dog" << "big";

    MetadataPrivate *mdMocked = new MetadataPrivate();

    // Prepare the list of URLs, and setup the metadata
    QDir dataDir(ITEMS_DIR);
    QList<QUrl> items;
    Q_FOREACH(const QString &fileName, fileNames) {
        QString sourceFile = dataDir.absoluteFilePath(fileName);
        QUrl url = QUrl::fromLocalFile(sourceFile);
        items.append(url);

        Metadata::ImportData data;
        data.tags = photoTags[fileName];
        mdMocked->setImportData(sourceFile, data);
    }

    Database *db = Database::instance();
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db);
    QSignalSpy addPhotoCalled(dbMock,
                  SIGNAL(addPhotoCalled(Imaginario::Database::Photo,int)));
    QSignalSpy createTagCalled(dbMock,
                  SIGNAL(createTagCalled(Imaginario::Tag,QString,QString)));
    QSignalSpy createRollCalled(dbMock, SIGNAL(createRollCalled()));
    QSignalSpy photoAdded(db, SIGNAL(photoAdded(PhotoId)));

    QStringList allTags;
    allTags << "imported" << "red" << "green" << "blue";
    dbMock->setTags(allTags);

    Importer importer;
    QSignalSpy finished(&importer, SIGNAL(finished(int)));
    importer.setCopyFiles(false);
    importer.setParentTag(db->tag("imported"));
    importer.setCommonTags(commonTags);
    importer.addItems(items, assignedTags);
    for (IndexTags::const_iterator i = setTags.constBegin();
         i != setTags.constEnd();
         i++) {
        importer.setItemTags(i.key(), i.value());
    }
    importer.exec();

    finished.wait();
    QCOMPARE(finished.count(), 1);
    QVERIFY(finished.at(0).at(0).toInt() >= 0);
    QTRY_COMPARE(importer.importedCount(), items.count());
    QTRY_COMPARE(importer.failedCount(), 0);

    for (int i = 0; i < createTagCalled.count(); i++) {
        QVariantList args = createTagCalled.at(i);
        Tag parent = args.at(0).value<Tag>();
        QString tagName = args.at(1).toString();
        QCOMPARE(parent, db->tag("imported"));
    }

    QCOMPARE(createRollCalled.count(), 1);
    RollId rollId = dbMock->lastRollId();

    QCOMPARE(addPhotoCalled.count(), fileNames.count());
    for (int i = 0; i < addPhotoCalled.count(); i++) {
        QVariantList args = addPhotoCalled.at(i);
        Database::Photo photo = args.at(0).value<Database::Photo>();
        QCOMPARE(photo.fileName(), fileNames[i]);
        QCOMPARE(args.at(1).toInt(), int(rollId));
    }

    QCOMPARE(photoAdded.count(), fileNames.count());
    for (int i = 0; i < photoAdded.count(); i++) {
        PhotoId photoId = photoAdded.at(i).at(0).toInt();
        Database::Photo photo = db->photo(photoId);
        QSet<QString> expectedTags = importedTags[photo.fileName()];
        QCOMPARE(Tag::tagNames(db->tags(photoId)).toSet(),
                 expectedTags);
    }

    delete db;
}

void ImporterTest::testRegions_data()
{
    QTest::addColumn<QStringList>("fileNames");
    QTest::addColumn<bool>("importTags");
    QTest::addColumn<PhotoTags>("expectedTags");
    QTest::addColumn<PhotoRegions>("expectedRegions");

    QTest::newRow("no file tags, no region tags") <<
        QStringList { "image0.png" } <<
        true <<
        PhotoTags {
            { "image0.png", {} },
        } <<
        PhotoRegions {
            { "image0.png", {} },
        };

    QTest::newRow("region tag, importing") <<
        QStringList { "image1.jpg" } <<
        true <<
        PhotoTags {
            { "image1.jpg", { "red", "dog", "big" } },
        } <<
        PhotoRegions {
            {
                "image1.jpg", {
                    { { 0.1, 0.1, 0.2, 0.2 }, "dog" },
                }
            },
        };

    QTest::newRow("region tag, not importing") <<
        QStringList { "image1.jpg" } <<
        false <<
        PhotoTags {
            { "image1.jpg", {} },
        } <<
        PhotoRegions {
            { "image1.jpg", {} },
        };

    QTest::newRow("only region tags, importing") <<
        QStringList { "image2.jpg" } <<
        true <<
        PhotoTags {
            { "image2.jpg", { "a house", "big" } },
        } <<
        PhotoRegions {
            {
                "image2.jpg", {
                    { { 0.1, 0.1, 0.4, 0.3 }, "big" },
                    { { 0.3, 0.4, 0.8, 0.9 }, "a house" },
                }
            },
        };

    QTest::newRow("region tags, importing") <<
        QStringList { "image1.jpg", "image2.jpg" } <<
        true <<
        PhotoTags {
            { "image1.jpg", { "red", "dog", "big" } },
            { "image2.jpg", { "a house", "big" } },
        } <<
        PhotoRegions {
            {
                "image1.jpg", {
                    { { 0.1, 0.1, 0.2, 0.2 }, "dog" },
                }
            },
            {
                "image2.jpg", {
                    { { 0.1, 0.1, 0.4, 0.3 }, "big" },
                    { { 0.3, 0.4, 0.8, 0.9 }, "a house" },
                }
            },
        };
}

void ImporterTest::testRegions()
{
    QFETCH(QStringList, fileNames);
    QFETCH(bool, importTags);
    QFETCH(PhotoTags, expectedTags);
    QFETCH(PhotoRegions, expectedRegions);

    /* Prepare some fake image metadata */
    QHash<QString,QStringList> photoTags {
        { "image0.png", {} },
        { "image1.jpg", { "red", "dog", "big" } },
        { "image2.jpg", { "a house" } },
    };

    QHash<QString,Metadata::Regions> photoRegions {
        {
            "image1.jpg", {
                { { 0.1, 0.1, 0.2, 0.2 }, "dog" },
            },
        },
        {
            "image2.jpg", {
                { { 0.1, 0.1, 0.4, 0.3 }, "big" },
                { { 0.3, 0.4, 0.8, 0.9 }, "a house" },
            },
        },
    };
    MetadataPrivate *mdMocked = new MetadataPrivate();

    // Prepare the list of URLs, and setup the metadata
    QDir dataDir(ITEMS_DIR);
    QList<QUrl> items;
    Q_FOREACH(const QString &fileName, fileNames) {
        QString sourceFile = dataDir.absoluteFilePath(fileName);
        QUrl url = QUrl::fromLocalFile(sourceFile);
        items.append(url);

        Metadata::ImportData data;
        data.tags = photoTags[fileName];
        data.regions = photoRegions[fileName];
        mdMocked->setImportData(sourceFile, data);
    }

    QScopedPointer<Database> db(Database::instance());
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db.data());
    QSignalSpy createTagCalled(dbMock,
                  SIGNAL(createTagCalled(Imaginario::Tag,QString,QString)));
    QSignalSpy createRollCalled(dbMock, SIGNAL(createRollCalled()));
    QSignalSpy photoAdded(db.data(), SIGNAL(photoAdded(PhotoId)));

    QStringList allTags;
    allTags << "imported" << "red" << "green" << "big";
    dbMock->setTags(allTags);

    Importer importer;
    QSignalSpy finished(&importer, SIGNAL(finished(int)));
    importer.setCopyFiles(false);
    importer.setParentTag(db->tag("imported"));
    importer.addItems(items, QStringList(), importTags);
    importer.exec();

    finished.wait();
    QCOMPARE(finished.count(), 1);
    QVERIFY(finished.at(0).at(0).toInt() >= 0);
    QTRY_COMPARE(importer.importedCount(), items.count());
    QTRY_COMPARE(importer.failedCount(), 0);

    QCOMPARE(createRollCalled.count(), 1);
    RollId rollId = dbMock->lastRollId();

    PhotoTags tags;
    PhotoRegions regions;
    QCOMPARE(photoAdded.count(), fileNames.count());
    for (int i = 0; i < photoAdded.count(); i++) {
        PhotoId photoId = photoAdded.at(i).at(0).toInt();
        Database::Photo photo = db->photo(photoId);
        QCOMPARE(photo.fileName(), fileNames[i]);
        QCOMPARE(photo.rollId(), int(rollId));

        tags.insert(photo.fileName(),
                    Tag::tagNames(db->tags(photo.id())).toSet());
        QSet<Metadata::RegionInfo> photoRegions;
        const auto tagAreas = db->tagAreas(photo.id());
        for (const TagArea &a: tagAreas) {
            photoRegions.insert(Metadata::RegionInfo(a.rect(), a.tag().name()));
        }
        regions.insert(photo.fileName(), photoRegions);
    }

    QCOMPARE(tags, expectedTags);
    QCOMPARE(regions, expectedRegions);
}

void ImporterTest::testDuplicates_data()
{
    QTest::addColumn<QString>("action");
    QTest::addColumn<int>("actionParam");
    QTest::addColumn<int>("expectedStatus");
    QTest::addColumn<int>("expectedAction");
    QTest::addColumn<SignalData>("expectedMakeVersionCount");
    QTest::addColumn<SignalData>("expectedPhotoDeleted");

    QTest::newRow("ignore") <<
        "ignoreItem" <<
        0 <<
        int(Importer::Ignored) <<
        int(Importer::DuplicateCheckNeeded) <<
        SignalData() <<
        SignalData();

    QTest::newRow("import as not dup") <<
        "importAsNotDuplicate" <<
        0 <<
        int(Importer::Done) <<
        int(Importer::ImportAsNew) <<
        SignalData() <<
        SignalData();

    QTest::newRow("import as version") <<
        "importAsVersion" <<
        1 <<
        int(Importer::Done) <<
        int(Importer::ImportAsVersion) <<
        (SignalData() << (QVariantList() << 4 << 1)) <<
        SignalData();

    QTest::newRow("import replacing") <<
        "importReplacing" <<
        1 <<
        int(Importer::Done) <<
        int(Importer::ImportReplacing) <<
        SignalData() <<
        (SignalData() << (QVariantList() << 1));
}

void ImporterTest::testDuplicates()
{
    QFETCH(QString, action);
    QFETCH(int, actionParam);
    QFETCH(int, expectedStatus);
    QFETCH(int, expectedAction);
    QFETCH(SignalData, expectedMakeVersionCount);
    QFETCH(SignalData, expectedPhotoDeleted);

    // Prepare the list of URLs, and setup the metadata
    QDir dataDir(ITEMS_DIR);
    QList<QUrl> items;
    QString sourceFile = dataDir.absoluteFilePath("image0.png");
    items.append(QUrl::fromLocalFile(sourceFile));
    sourceFile = dataDir.absoluteFilePath("image1.jpg");
    items.append(QUrl::fromLocalFile(sourceFile));

    Database *db = Database::instance();
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db);
    QSignalSpy addPhotoCalled(dbMock,
                  SIGNAL(addPhotoCalled(Imaginario::Database::Photo,int)));
    QSignalSpy makeVersionCalled(dbMock, SIGNAL(makeVersionCalled(int,int)));
    QSignalSpy photoDeleted(db, SIGNAL(photoDeleted(PhotoId)));

    Database::Photo photo;
    photo.setBaseUri("file:///tmp");
    photo.setFileName("dup1.jpg");
    Database::Photo photo1 = db->photo(db->addPhoto(photo, 1));

    photo.setBaseUri("file:///tmp");
    photo.setFileName("nodup.jpg");
    Database::Photo photo2 = db->photo(db->addPhoto(photo, 1));

    photo.setBaseUri("file:///tmp");
    photo.setFileName("dup3.jpg");
    Database::Photo photo3 = db->photo(db->addPhoto(photo, 1));
    addPhotoCalled.clear();

    DuplicateDetectorPrivate *ddMocked = new DuplicateDetectorPrivate();
    MaybeDuplicates dups;
    dups.append(MaybeDuplicate(photo1.id(), 0.8));
    dups.append(MaybeDuplicate(photo3.id(), 0.5));
    ddMocked->setDuplicates(items[0], dups);

    Importer importer;
    QSignalSpy finished(&importer, SIGNAL(finished(int)));
    importer.setCopyFiles(false);
    importer.addItems(items);
    QCOMPARE(importer.rowCount(), 2);
    importer.exec();

    finished.wait();
    QCOMPARE(finished.count(), 1);
    QVERIFY(finished.at(0).at(0).toInt() >= 0);
    finished.clear();
    QTRY_COMPARE(importer.importedCount(), 1);
    QTRY_COMPARE(importer.failedCount(), 0);
    QTRY_COMPARE(importer.ignoredCount(), 0);
    QCOMPARE(addPhotoCalled.count(), 1);

    // Check the item's status
    QCOMPARE(importer.get(0, "status").toInt(), int(Importer::DuplicateCheck));
    QCOMPARE(importer.get(1, "status").toInt(), int(Importer::Done));

    // Compare the duplicate's list
    QList<int> duplicates =
        importer.get(0, "duplicateList").value<QList<int> >();
    QList<int> expectedDups;
    expectedDups << photo1.id() << photo3.id();
    QCOMPARE(duplicates, expectedDups);

    QGenericArgument arg = (action == "ignoreItem" ||
                            action == "importAsNotDuplicate") ?
        QGenericArgument() : Q_ARG(int, actionParam);
    bool ok = QMetaObject::invokeMethod(&importer, action.toUtf8().constData(),
                                        Q_ARG(int, 0), arg);
    QVERIFY(ok);
    QCOMPARE(importer.get(0, "duplicateAction").toInt(), expectedAction);

    importer.exec();

    finished.wait();
    QCOMPARE(finished.count(), 1);
    QTRY_COMPARE(importer.get(0, "status").toInt(), expectedStatus);
    if (expectedStatus == int(Importer::Ignored)) {
        QTRY_COMPARE(importer.importedCount(), 1);
        QTRY_COMPARE(importer.failedCount(), 0);
        QTRY_COMPARE(importer.ignoredCount(), 1);
    } else if (expectedStatus == int(Importer::Done)) {
        QTRY_COMPARE(importer.importedCount(), 2);
        QTRY_COMPARE(importer.failedCount(), 0);
        QTRY_COMPARE(importer.ignoredCount(), 0);
    }
    QCOMPARE(SignalData(makeVersionCalled), expectedMakeVersionCount);
    QCOMPARE(SignalData(photoDeleted), expectedPhotoDeleted);
    delete db;
}

void ImporterTest::testAutoClear_data()
{
    QTest::addColumn<QStringList>("fileNames");
    QTest::addColumn<QStringList>("ignoredItems");
    QTest::addColumn<Importer::ClearStatuses>("autoClear");
    QTest::addColumn<QStringList>("expectedItemsLeft");

    QTest::newRow("all done, clear done") <<
        (QStringList() << "image0.png" << "image1.jpg") <<
        QStringList() <<
        Importer::ClearStatuses(Importer::ClearDone) <<
        QStringList();

    QTest::newRow("all done, clear ignored") <<
        (QStringList() << "image0.png" << "image1.jpg") <<
        QStringList() <<
        Importer::ClearStatuses(Importer::ClearIgnored) <<
        (QStringList() << "image0.png" << "image1.jpg");

    QTest::newRow("1 done, 1 failed, clear failed") <<
        (QStringList() << "image0.png" << "missing.jpg") <<
        QStringList() <<
        Importer::ClearStatuses(Importer::ClearFailed) <<
        (QStringList() << "image0.png");

    QTest::newRow("one each, clear failed and ignored") <<
        (QStringList() << "image0.png" << "missing.jpg" << "image1.jpg") <<
        (QStringList() << "image1.jpg") <<
        Importer::ClearStatuses(Importer::ClearFailed |
                                Importer::ClearIgnored) <<
        (QStringList() << "image0.png");

    QTest::newRow("index mapping") <<
        QStringList { "m1.jpg", "m2.jpg", "m3.jpg", "m4.jpg", "m5.jpg",
            "m6.jpg"
        } <<
        QStringList { "m3.jpg", "m5.jpg" } <<
        Importer::ClearStatuses(Importer::ClearIgnored) <<
        QStringList{ "m1.jpg", "m2.jpg", "m4.jpg", "m6.jpg" };
}

void ImporterTest::testAutoClear()
{
    QFETCH(QStringList, fileNames);
    QFETCH(QStringList, ignoredItems);
    QFETCH(Importer::ClearStatuses, autoClear);
    QFETCH(QStringList, expectedItemsLeft);

    MetadataPrivate *mdMocked = new MetadataPrivate();

    // Prepare the list of URLs, and setup the metadata
    QDir dataDir(ITEMS_DIR);
    QList<QUrl> items;
    Q_FOREACH(const QString &fileName, fileNames) {
        QString sourceFile = dataDir.absoluteFilePath(fileName);
        QUrl url = QUrl::fromLocalFile(sourceFile);
        items.append(url);

        Metadata::ImportData data;
        mdMocked->setImportData(sourceFile, data);
    }

    Database *db = Database::instance();

    Importer importer;
    importer.setAutoClear(autoClear);
    QSignalSpy finished(&importer, SIGNAL(finished(int)));
    importer.setCopyFiles(false);
    importer.addItems(items);

    for (int i = 0; i < importer.rowCount(); i++) {
        QFileInfo fileInfo(importer.get(i, "url").toUrl().toLocalFile());
        if (ignoredItems.contains(fileInfo.fileName())) {
            importer.ignoreItem(i);
        }
    }

    importer.exec();

    QTRY_COMPARE(finished.count(), 1);
    QTRY_COMPARE(importer.rowCount(), expectedItemsLeft.count());

    QStringList itemsLeft;
    for (int i = 0; i < importer.rowCount(); i++) {
        QFileInfo fileInfo(importer.get(i, "url").toUrl().toLocalFile());
        itemsLeft.append(fileInfo.fileName());
    }

    QCOMPARE(itemsLeft, expectedItemsLeft);

    delete db;
}

void ImporterTest::testItemAdding()
{
    Importer importer;
    QSignalSpy rowsInserted(&importer,
                            SIGNAL(rowsInserted(const QModelIndex&,int,int)));

    QList<QUrl> items;
    items << QUrl("file:///a.jpg");
    importer.addItems(items, QStringList());

    QCOMPARE(rowsInserted.count(), 1);
    QCOMPARE(rowsInserted.at(0).at(1).toInt(), 0);
    QCOMPARE(rowsInserted.at(0).at(2).toInt(), 0);

    rowsInserted.clear();
    items.clear();
    items << QUrl("file:///b.jpg") << QUrl("file:///c.jpg");
    importer.addItems(items, QStringList());

    QCOMPARE(rowsInserted.count(), 1);
    QCOMPARE(rowsInserted.at(0).at(1).toInt(), 1);
    QCOMPARE(rowsInserted.at(0).at(2).toInt(), 2);
}

void ImporterTest::testItemTags_data()
{
    QTest::addColumn<QStringList>("ownTags");
    QTest::addColumn<QStringList>("addedTags");
    QTest::addColumn<bool>("importTags");
    QTest::addColumn<QStringList>("expectedCurrentTags");

    QTest::newRow("no tags, no import") <<
        QStringList() <<
        QStringList() <<
        false <<
        QStringList();

    QTest::newRow("no tags, import") <<
        QStringList() <<
        QStringList() <<
        false <<
        QStringList();

    QTest::newRow("added tags") <<
        QStringList() <<
        (QStringList() << "blue" << "green") <<
        false <<
        (QStringList() << "blue" << "green");

    QTest::newRow("own tags, no import") <<
        (QStringList() << "cat" << "big") <<
        QStringList() <<
        false <<
        QStringList();

    QTest::newRow("own tags, import") <<
        (QStringList() << "fox" << "big") <<
        QStringList() <<
        true <<
        (QStringList() << "fox" << "big");

    QTest::newRow("own tags, added, no import") <<
        (QStringList() << "cat") <<
        (QStringList() << "cute" << "lazy") <<
        false <<
        (QStringList() << "cute" << "lazy");

    QTest::newRow("own tags, added, import") <<
        (QStringList() << "cat") <<
        (QStringList() << "cute" << "lazy" << "kitty") <<
        true <<
        (QStringList() << "cute" << "lazy" << "kitty" << "cat");
}

void ImporterTest::testItemTags()
{
    QFETCH(QStringList, ownTags);
    QFETCH(QStringList, addedTags);
    QFETCH(bool, importTags);
    QFETCH(QStringList, expectedCurrentTags);

    MetadataPrivate *mdMocked = new MetadataPrivate();

    // Prepare the list of URLs, and setup the metadata
    QDir dataDir(ITEMS_DIR);
    QList<QUrl> items;
    QString sourceFile = dataDir.absoluteFilePath("image0.png");
    QUrl url = QUrl::fromLocalFile(sourceFile);
    items.append(url);

    Metadata::ImportData data;
    data.tags = ownTags;
    mdMocked->setImportData(sourceFile, data);

    Database *db = Database::instance();

    Importer importer;
    importer.setCopyFiles(false);
    importer.addItems(items, addedTags, importTags);

    QCOMPARE(importer.rowCount(), 1);

    QCOMPARE(importer.get(0, "ownTags").toStringList().toSet(),
             ownTags.toSet());
    QCOMPARE(importer.get(0, "addedTags").toStringList().toSet(),
             addedTags.toSet());
    QCOMPARE(importer.get(0, "currentTags").toStringList().toSet(),
             expectedCurrentTags.toSet());

    delete db;
}

void ImporterTest::testPairing_data()
{
    typedef QVector<QPair<int,int>> IdPairs;
    QTest::addColumn<QStringList>("fileNames");
    QTest::addColumn<int>("pairingMode");
    QTest::addColumn<int>("rawPolicy");
    QTest::addColumn<IdPairs>("expectedPairs");

    QTest::newRow("all edited, edited is master") <<
        QStringList {
            "image0.png", "image1.jpg", "image2.jpg", "image3.png",
            "image4.jpg",
        } <<
        int(DuplicateDetectorPrivate::AllEditedPairing) <<
        int(Importer::EditedIsMaster) <<
        IdPairs {
            { 0, -1},
            { 1, -1},
            { 2, -1},
            { 3, -1},
            { 4, -1},
        };

    QTest::newRow("all raw, edited is master") <<
        QStringList {
            "image0.png", "image1.jpg", "image2.jpg", "image3.png",
            "image4.jpg",
        } <<
        int(DuplicateDetectorPrivate::AllRawPairing) <<
        int(Importer::EditedIsMaster) <<
        IdPairs {
            { 0, -1},
            { 1, -1},
            { 2, -1},
            { 3, -1},
            { 4, -1},
        };

    QTest::newRow("all paired, edited is master") <<
        QStringList {
            "image0.png", "image1.jpg", "image2.jpg", "image3.png",
            "image4.jpg",
        } <<
        int(DuplicateDetectorPrivate::FullPairing) <<
        int(Importer::EditedIsMaster) <<
        IdPairs {
            { 0, 1},
            { 2, 3},
            { 4, -1},
        };

    QTest::newRow("all raw, raw is master") <<
        QStringList {
            "image0.png", "image1.jpg", "image2.jpg", "image3.png",
            "image4.jpg",
        } <<
        int(DuplicateDetectorPrivate::AllRawPairing) <<
        int(Importer::RawIsMaster) <<
        IdPairs {
            { 0, -1},
            { 1, -1},
            { 2, -1},
            { 3, -1},
            { 4, -1},
        };

    QTest::newRow("all paired, raw is master") <<
        QStringList {
            "image0.png", "image1.jpg", "image2.jpg", "image3.png",
            "image4.jpg",
        } <<
        int(DuplicateDetectorPrivate::FullPairing) <<
        int(Importer::RawIsMaster) <<
        IdPairs {
            { 1, 0},
            { 3, 2},
            { 4, -1},
        };
}

void ImporterTest::testPairing()
{
    typedef QVector<QPair<int,int>> IdPairs;
    QFETCH(QStringList, fileNames);
    QFETCH(int, pairingMode);
    QFETCH(int, rawPolicy);
    QFETCH(IdPairs, expectedPairs);

    // Prepare the list of URLs, and setup the metadata
    QDir dataDir(ITEMS_DIR);
    QList<QUrl> items;
    for (const QString &fileName: fileNames) {
        QString sourceFile = dataDir.absoluteFilePath(fileName);
        QUrl url = QUrl::fromLocalFile(sourceFile);
        items.append(url);
    }

    QScopedPointer<Database> db(Database::instance());
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db.data());
    QSignalSpy addPhotoCalled(dbMock,
                  SIGNAL(addPhotoCalled(Imaginario::Database::Photo,int)));
    QSignalSpy makeVersionCalled(dbMock, SIGNAL(makeVersionCalled(int,int)));

    DuplicateDetectorPrivate *ddMocked = new DuplicateDetectorPrivate();
    ddMocked->setPairingAlgorithm(
                    DuplicateDetectorPrivate::PairingAlgorithm(pairingMode));

    Importer importer;
    importer.setCopyFiles(false);
    importer.addItems(items);
    importer.setRawPolicy(Importer::RawPolicy(rawPolicy));

    QCOMPARE(importer.rowCount(), expectedPairs.count());

    QSignalSpy finished(&importer, SIGNAL(finished(int)));
    importer.exec();
    QTRY_COMPARE(finished.count(), 1);
    QCOMPARE(addPhotoCalled.count(), fileNames.count());

    // Map photoId to index in file list
    QHash<PhotoId, int> photoIndex;
    for (int i = 0; i < addPhotoCalled.count(); i++) {
        const auto &args = addPhotoCalled.at(i);
        const Database::Photo &p = args.at(0).value<Database::Photo>();
        int index = fileNames.indexOf(p.fileName());
        QVERIFY(index >= 0);
        photoIndex[i] = index;
    }

    IdPairs pairs;
    for (const auto &args: makeVersionCalled) {
        PhotoId version = args.at(0).toInt();
        PhotoId master = args.at(1).toInt();
        pairs.append(QPair<int,int>(photoIndex.take(master),
                                    photoIndex.take(version)));
    }

    for (int id: photoIndex) {
        pairs.append(QPair<int,int>(id, -1));
    }

    std::sort(pairs.begin(), pairs.end());
    QCOMPARE(pairs, expectedPairs);
}

void ImporterTest::testFiltering_data()
{
    QTest::addColumn<QStringList>("fileNames");
    QTest::addColumn<QStringList>("expectedNames");

    QTest::newRow("all valid") <<
        QStringList {
            "image0.png", "image1.jpg", "image2.jpg", "image3.png",
            "image4.jpg",
        } <<
        QStringList {
            "image0.png", "image1.jpg", "image2.jpg", "image3.png",
            "image4.jpg",
        };

    QTest::newRow("some unwanted file") <<
        QStringList {
            "image0.png", "image1.jpg", "image3.xmp", "image2.jpg",
            "image3.png", "index.html", "image4.jpg", "notes.txt",
        } <<
        QStringList {
            "image0.png", "image1.jpg", "image2.jpg", "image3.png",
            "image4.jpg",
        };

    QTest::newRow("mix, with raw files") <<
        QStringList {
            "image0.png", "image1.jpg", "image3.xmp", "image2.arw",
            "image3.nef", "doc.txt", "image4.cr2", "im3.dcr",
        } <<
        QStringList {
            "image0.png", "image1.jpg", "image2.arw", "image3.nef",
            "image4.cr2", "im3.dcr",
        };
}

void ImporterTest::testFiltering()
{
    QFETCH(QStringList, fileNames);
    QFETCH(QStringList, expectedNames);

    // Prepare the list of URLs, and setup the metadata
    QDir dataDir(ITEMS_DIR);
    QList<QUrl> items;
    for (const QString &fileName: fileNames) {
        QString sourceFile = dataDir.absoluteFilePath(fileName);
        QUrl url = QUrl::fromLocalFile(sourceFile);
        items.append(url);
    }

    Importer importer;
    importer.setCopyFiles(false);
    importer.addItems(items);

    QSet<QString> filteredNames;
    for (int i = 0; i < importer.rowCount(); i++) {
        QFileInfo fileInfo(importer.get(i, "url").toUrl().toLocalFile());
        filteredNames.insert(fileInfo.fileName());
    }
    QCOMPARE(filteredNames, expectedNames.toSet());

    int expectedUnsupportedCount = fileNames.count() - expectedNames.count();
    QCOMPARE(importer.unsupportedCount(), expectedUnsupportedCount);
}

void ImporterTest::testRollCreation_data()
{
    QTest::addColumn<QStringList>("fileNames");
    QTest::addColumn<bool>("rollExpected");

    QTest::newRow("no files") <<
        QStringList {} <<
        false;

    QTest::newRow("one file") <<
        QStringList { "image0.png" } <<
        true;

    QTest::newRow("some files") <<
        QStringList { "image1.jpg", "image0.png", "image2.jpg" } <<
        true;

    QTest::newRow("with missing files") <<
        QStringList { "not-there.jpg", "imageNaN.png" } <<
        false;
}

void ImporterTest::testRollCreation()
{
    QFETCH(QStringList, fileNames);
    QFETCH(bool, rollExpected);

    QScopedPointer<Database> db(Database::instance());

    // Prepare the list of URLs
    QDir dataDir(ITEMS_DIR);
    QList<QUrl> items;
    QList<QUrl> expectedImportedItems;
    QList<QUrl> expectedFailedItems;
    Q_FOREACH(const QString &fileName, fileNames) {
        QString sourceFile = dataDir.absoluteFilePath(fileName);
        items.append(QUrl::fromLocalFile(sourceFile));
    }

    Importer importer;
    QSignalSpy finished(&importer, SIGNAL(finished(int)));
    importer.addItems(items);
    importer.exec();

    finished.wait();
    QCOMPARE(finished.count(), 1);
    RollId rollId = finished.at(0).at(0).toInt();
    bool rollIdCreated = rollId > 0;

    QCOMPARE(rollIdCreated, rollExpected);
}

QTEST_GUILESS_MAIN(ImporterTest)

#include "tst_importer.moc"
