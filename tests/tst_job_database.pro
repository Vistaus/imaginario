TARGET = tst_job_database

include(tests.pri)

QT += \
    concurrent \
    sql

SOURCES += \
    $${SRC_DIR}/abstract_database.cpp \
    $${SRC_DIR}/job_database.cpp \
    tst_job_database.cpp

HEADERS += \
    $${SRC_DIR}/abstract_database.h $${SRC_DIR}/abstract_database_p.h \
    $${SRC_DIR}/job_database.h \
    test_utils.h

DATA_DIR = $${TOP_SRC_DIR}/tests/data
DEFINES += \
    DATA_DIR=\\\"$${DATA_DIR}\\\"
