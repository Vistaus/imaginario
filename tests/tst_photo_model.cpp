/*
 * Copyright (C) 2014-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fake_database.h"
#include "fake_metadata.h"
#include "photo_model.h"

#include <QDateTime>
#include <QQmlComponent>
#include <QQmlEngine>
#include <QSignalSpy>
#include <QTest>

using namespace Imaginario;

class PhotoModelTest: public QObject
{
    Q_OBJECT

public:
    PhotoModelTest() {
        qRegisterMetaType<GeoPoint>("GeoPoint");
        qRegisterMetaType<PhotoId>("PhotoId");
    }

    static QVariant get(const QAbstractListModel *model, int row,
                        QString roleName);

private Q_SLOTS:
    void testProperties();
    void testEmpty();
    void testRoles_data();
    void testRoles();
    void testMasterVersionRole();
    void testMimeTypeRole_data();
    void testMimeTypeRole();
    void testTagFilters_data();
    void testTagFilters();
    void testFilters_data();
    void testFilters();
    void testPhotoIds();
    void testPhotoVersions();
    void testDeletePhoto_data();
    void testDeletePhoto();
    void testPhotoChanges();
    void testCountUpdates();
    void testNewPhotoChanges();
    void testFirstIndexOlderThan_data();
    void testFirstIndexOlderThan();
    void testForceItemRefresh_data();
    void testForceItemRefresh();
};

QVariant PhotoModelTest::get(const QAbstractListModel *model, int row,
                             QString roleName)
{
    QHash<int, QByteArray> roleNames = model->roleNames();

    int role = roleNames.key(roleName.toLatin1(), -1);
    return model->data(model->index(row), role);
}

void PhotoModelTest::testProperties()
{
    PhotoModel model;

    Database *db = Database::instance();
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db);
    QStringList allTags;
    allTags << "one" << "big" << "cat" << "one" << "small" << "dog";
    dbMock->setTags(allTags);

    model.setProperty("roll0", int(2));
    QCOMPARE(model.property("roll0").toInt(), 2);
    model.setProperty("roll1", int(4));
    QCOMPARE(model.property("roll1").toInt(), 4);

    model.setProperty("rating0", int(3));
    QCOMPARE(model.property("rating0").toInt(), 3);
    model.setProperty("rating1", int(5));
    QCOMPARE(model.property("rating1").toInt(), 5);

    QDateTime time =
        QDateTime::fromString("2000-01-17T19:20:30", Qt::ISODate);
    model.setProperty("time0", time);
    QCOMPARE(model.property("time0").toDateTime(), time);
    model.setProperty("time1", time);
    QCOMPARE(model.property("time1").toDateTime(), time);

    GeoPoint location(10, 20);
    model.setProperty("location0", QVariant::fromValue(location));
    QCOMPARE(model.property("location0").value<GeoPoint>(), location);
    location = GeoPoint(30, 1);
    model.setProperty("location1", QVariant::fromValue(location));
    QCOMPARE(model.property("location1").value<GeoPoint>(), location);

    QCOMPARE(model.property("nonGeoTagged").toBool(), false);
    model.setProperty("nonGeoTagged", true);
    QCOMPARE(model.property("nonGeoTagged").toBool(), true);

    QStringList tags = QStringList() << "one" << "big" << "cat";
    model.setProperty("requiredTags", tags);
    QCOMPARE(model.property("requiredTags").toStringList(), tags);
    tags = QStringList() << "one" << "small" << "dog";
    model.setProperty("forbiddenTags", tags);
    QCOMPARE(model.property("forbiddenTags").toStringList(), tags);

    QCOMPARE(model.property("areaTagged").toBool(), false);
    model.setProperty("areaTagged", true);
    QCOMPARE(model.property("areaTagged").toBool(), true);

    QList<int> photos = (QList<int>() << 1 << 6);
    model.setProperty("excludedPhotos", QVariant::fromValue(photos));
    QCOMPARE(model.property("excludedPhotos").value<QList<PhotoId> >(), photos);

    QCOMPARE(model.property("skipNonDefaultVersions").toBool(), false);
    model.setProperty("skipNonDefaultVersions", false);
    model.setProperty("skipNonDefaultVersions", true);
    QCOMPARE(model.property("skipNonDefaultVersions").toBool(), true);

    Qt::SortOrder order = Qt::DescendingOrder;
    model.setProperty("sortOrder", order);
    QCOMPARE(model.property("sortOrder").value<Qt::SortOrder>(), order);

    QList<PhotoId> ids = (QList<PhotoId>() << 3 << 4);
    model.setProperty("photoIds", QVariant::fromValue(ids));
    QCOMPARE(model.property("photoIds").value<QList<PhotoId> >(), ids);

    model.setProperty("photoVersions", 42);
    QCOMPARE(model.property("photoVersions").toInt(), 42);

    delete db;
}

void PhotoModelTest::testEmpty()
{
    PhotoModel model;

    QCOMPARE(model.rowCount(), 0);

    Database *db = Database::instance();
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db);

    QSignalSpy findPhotosCalled(dbMock,
        SIGNAL(findPhotosCalled(Imaginario::SearchFilters,
                                Qt::SortOrder)));

    // trigger an update
    QDateTime time =
        QDateTime::fromString("2000-01-17T19:20:30", Qt::ISODate);
    model.setRoll0(2);
    model.setTime1(time);
    model.setLocation0(GeoPoint(2, 2));
    findPhotosCalled.wait();
    QCOMPARE(model.rowCount(), 0);
    SearchFilters filters = findPhotosCalled.at(0).at(0).value<SearchFilters>();
    QCOMPARE(filters.roll0, 2);
    QCOMPARE(filters.time1, time);
    QCOMPARE(filters.location0, GeoPoint(2, 2));

    delete db;
}

void PhotoModelTest::testRoles_data()
{
    QTest::addColumn<int>("photoId");
    QTest::addColumn<QString>("baseUri");
    QTest::addColumn<QString>("fileName");
    QTest::addColumn<QString>("title");
    QTest::addColumn<QString>("description");
    QTest::addColumn<QDateTime>("time");
    QTest::addColumn<int>("rollId");
    QTest::addColumn<int>("rating");
    QTest::addColumn<GeoPoint>("location");
    QTest::addColumn<QUrl>("expectedUrl");
    QTest::addColumn<bool>("hasVersions");
    QTest::addColumn<bool>("isDefaultVersion");
    QTest::addColumn<QSize>("size");

    QTest::newRow("simple") <<
        int(4) <<
        "file:///tmp" << "foo.png" <<
        "The foo" << "Description of the foo" <<
        QDateTime::fromString("2000-01-17T19:20:30", Qt::ISODate) <<
        int(6) <<
        int(3) <<
        GeoPoint(23.4, -10.5) <<
        QUrl("file:///tmp/foo.png") <<
        false << false <<
        QSize();

    QTest::newRow("with versions") <<
        int(4) <<
        "file:///tmp" << "foo.png" <<
        "The foo" << "Description of the foo" <<
        QDateTime::fromString("2000-01-17T19:20:30", Qt::ISODate) <<
        int(6) <<
        int(3) <<
        GeoPoint(23.4, -10.5) <<
        QUrl("file:///tmp/foo.png") <<
        true << false <<
        QSize();

    QTest::newRow("default version") <<
        int(4) <<
        "file:///tmp" << "foo.png" <<
        "The foo" << "Description of the foo" <<
        QDateTime::fromString("2000-01-17T19:20:30", Qt::ISODate) <<
        int(6) <<
        int(3) <<
        GeoPoint(23.4, -10.5) <<
        QUrl("file:///tmp/foo.png") <<
        true << true <<
        QSize();
}

void PhotoModelTest::testRoles()
{
    QFETCH(int, photoId);
    QFETCH(QString, baseUri);
    QFETCH(QString, fileName);
    QFETCH(QString, title);
    QFETCH(QString, description);
    QFETCH(QDateTime, time);
    QFETCH(int, rollId);
    QFETCH(int, rating);
    QFETCH(GeoPoint, location);
    QFETCH(QUrl, expectedUrl);
    QFETCH(bool, hasVersions);
    QFETCH(bool, isDefaultVersion);
    QFETCH(QSize, size);

    Database *db = Database::instance();
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db);

    Database::Photo photo;
    dbMock->setPhotoId(photo, photoId);
    photo.setBaseUri(baseUri);
    photo.setFileName(fileName);
    photo.setDescription(title);
    photo.setTime(time);
    dbMock->setPhotoRoll(photo, rollId);
    photo.setRating(rating);
    photo.setLocation(location);
    dbMock->setPhotoVersionFlags(photo, hasVersions, isDefaultVersion);

    QList<Database::Photo> allPhotos;
    allPhotos.append(photo);
    dbMock->setPhotos(allPhotos);

    MetadataPrivate *mdMocked = new MetadataPrivate();
    Metadata::ImportData data;
    data.description = description;
    mdMocked->setImportData(QUrl(baseUri + "/" + fileName).toLocalFile(),
                            data);

    PhotoModel model;
    model.classBegin();
    model.componentComplete();

    QCOMPARE(model.rowCount(), 1);

    QCOMPARE(model.get(0, "photoId").toInt(), photoId);
    QCOMPARE(model.get(0, "url").toUrl(), expectedUrl);
    QCOMPARE(model.get(0, "fileName").toString(), fileName);
    QCOMPARE(model.get(0, "title").toString(), title);
    QCOMPARE(model.get(0, "description").toString(), description);
    QCOMPARE(model.get(0, "time").toDateTime(), time);
    QCOMPARE(model.get(0, "rollId").toInt(), rollId);
    QCOMPARE(model.get(0, "rating").toInt(), rating);
    QCOMPARE(model.get(0, "location").value<GeoPoint>(), location);
    QCOMPARE(model.get(0, "hasVersions").toBool(), hasVersions);
    QCOMPARE(model.get(0, "isDefaultVersion").toBool(), isDefaultVersion);
    QCOMPARE(model.get(0, "size").toSize(), size);

    delete db;
}

void PhotoModelTest::testMasterVersionRole()
{
    QScopedPointer<Database> db(Database::instance());
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db.data());
    QSignalSpy masterVersionCalled(dbMock,
                                   SIGNAL(masterVersionCalled(int)));

    PhotoId photoId = 3;

    Database::Photo photo;
    dbMock->setPhotoId(photo, photoId);
    photo.setBaseUri("file:///tmp");
    photo.setFileName("foo.jpg");

    QList<Database::Photo> allPhotos;
    allPhotos.append(photo);
    dbMock->setPhotos(allPhotos);

    PhotoModel model;
    model.classBegin();
    model.componentComplete();

    QCOMPARE(model.rowCount(), 1);

    QCOMPARE(model.get(0, "masterVersion").toInt(), photoId);

    QCOMPARE(masterVersionCalled.count(), 1);
    QCOMPARE(masterVersionCalled.at(0).at(0).toInt(), photoId);
}

void PhotoModelTest::testMimeTypeRole_data()
{
    QTest::addColumn<QString>("fileName");
    QTest::addColumn<QString>("expectedMimeType");

    QTest::newRow("jpeg") << "image.jpeg" << "image/jpeg";
    QTest::newRow("jpg") << "image.jpg" << "image/jpeg";
    QTest::newRow("png") << "house.png" << "image/png";
}

void PhotoModelTest::testMimeTypeRole()
{
    QFETCH(QString, fileName);
    QFETCH(QString, expectedMimeType);

    QScopedPointer<Database> db(Database::instance());
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db.data());

    Database::Photo photo;
    photo.setBaseUri("file:///tmp");
    photo.setFileName(fileName);

    QList<Database::Photo> allPhotos;
    allPhotos.append(photo);
    dbMock->setPhotos(allPhotos);

    /* We instantiate the model and retrieve the role via QML, to verify that
     * the QMImeType metadata has been properly registered. */
    QQmlEngine engine;
    qmlRegisterType<PhotoModel>("MyTest", 1, 0, "PhotoModel");
    QQmlComponent component(&engine);
    component.setData("import MyTest 1.0\n"
                      "PhotoModel {\n"
                      "  function run() {\n"
                      "    return get(0, \"mimeType\")\n"
                      "  }\n"
                      "}",
                      QUrl());
    QScopedPointer<QObject> object(component.create());
    QVERIFY(object != 0);
    QAbstractListModel *model = qobject_cast<QAbstractListModel*>(object.data());
    QVERIFY(model != 0);
    QCOMPARE(model->rowCount(), 1);

    QVariant ret;
    bool ok = QMetaObject::invokeMethod(object.data(), "run",
                                        Q_RETURN_ARG(QVariant, ret));
    QVERIFY(ok);

    QCOMPARE(ret.typeName(), "QMimeType");

    QMimeType mimeType = ret.value<QMimeType>();
    QCOMPARE(mimeType.name(), expectedMimeType);
}

void PhotoModelTest::testTagFilters_data()
{
    QTest::addColumn<QStringList>("allTags");
    QTest::addColumn<QString>("requiredTagsString");
    QTest::addColumn<QString>("forbiddenTagsString");
    QTest::addColumn<QStringList>("expectedRequiredTags");
    QTest::addColumn<QStringList>("expectedForbiddenTags");

    QStringList allTags;
    allTags << "blue" << "red" << "small" << "big" << "fast" << "slow";

    QTest::newRow("undefined") <<
        allTags <<
        "undefined" << "undefined" <<
        QStringList() << QStringList();

    QTest::newRow("empty list") <<
        allTags <<
        "[]" << "[]" <<
        QStringList() << QStringList();

    QTest::newRow("one in stringlist") <<
        allTags <<
        "[\"blue\"]" << "[\"big\"]" <<
        (QStringList() << "blue") << (QStringList() << "big");

    QTest::newRow("many in stringlist") <<
        allTags <<
        "[\"red\",\"slow\"]" << "[\"big\",\"blue\"]" <<
        (QStringList() << "red" << "slow") <<
        (QStringList() << "big" << "blue");

    QTest::newRow("one in int list") <<
        allTags <<
        "[1]" << "[3]" <<
        (QStringList() << "blue") << (QStringList() << "small");

    QTest::newRow("many in int list") <<
        allTags <<
        "[1,5]" << "[3,2,4]" <<
        (QStringList() << "blue" << "fast") <<
        (QStringList() << "small" << "red" << "big");
}

void PhotoModelTest::testTagFilters()
{
    QFETCH(QStringList, allTags);
    QFETCH(QString, requiredTagsString);
    QFETCH(QString, forbiddenTagsString);
    QFETCH(QStringList, expectedRequiredTags);
    QFETCH(QStringList, expectedForbiddenTags);

    Database *db = Database::instance();
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db);
    QSignalSpy findPhotosCalled(dbMock,
        SIGNAL(findPhotosCalled(Imaginario::SearchFilters,
                                Qt::SortOrder)));

    dbMock->setTags(allTags);

    QQmlEngine engine;
    qmlRegisterType<PhotoModel>("MyTest", 1, 0, "PhotoModel");
    QQmlComponent component(&engine);
    component.setData("import MyTest 1.0\n"
                      "PhotoModel {\n"
                      "  requiredTags: " + requiredTagsString.toUtf8() + "\n"
                      "  forbiddenTags: " + forbiddenTagsString.toUtf8() + "\n"
                      "}",
                      QUrl());
    QObject *object = component.create();
    QVERIFY(object != 0);
    QAbstractListModel *model = qobject_cast<QAbstractListModel*>(object);
    QVERIFY(model != 0);

    QCOMPARE(model->rowCount(), 0);
    QCOMPARE(findPhotosCalled.count(), 1);
    SearchFilters filters = findPhotosCalled.at(0).at(0).value<SearchFilters>();
    if (expectedRequiredTags.isEmpty()) {
        QVERIFY(filters.requiredTags.isEmpty());
    } else {
        QCOMPARE(filters.requiredTags.count(), 1);
        QCOMPARE(Tag::tagNames(filters.requiredTags[0]), expectedRequiredTags);
    }

    QCOMPARE(Tag::tagNames(filters.forbiddenTags), expectedForbiddenTags);

    delete db;
}

void PhotoModelTest::testFilters_data()
{
    QTest::addColumn<QString>("properties");
    QTest::addColumn<SearchFilters>("expectedFilters");

    SearchFilters filters;
    QTest::newRow("no properties") <<
        "" <<
        filters;

    filters.excludedPhotos = QList<PhotoId>() << 3 << 12;
    QTest::newRow("exclude photos") <<
        "excludedPhotos: [3, 12]" <<
        filters;
    filters = SearchFilters();

    filters.rating0 = 1;
    filters.rating1 = 3;
    QTest::newRow("by rating") <<
        "rating0: 1; rating1: 3" <<
        filters;
    filters = SearchFilters();

    filters.skipNonDefaultVersions = true;
    QTest::newRow("skip versions") <<
        "skipNonDefaultVersions: true" <<
        filters;
    filters = SearchFilters();

    filters.nonGeoTagged = true;
    QTest::newRow("only non geotagged") <<
        "nonGeoTagged: true" <<
        filters;
    filters = SearchFilters();

    filters.areaTagged = true;
    QTest::newRow("only with area tags") <<
        "areaTagged: true" <<
        filters;
    filters = SearchFilters();
}

void PhotoModelTest::testFilters()
{
    QFETCH(QString, properties);
    QFETCH(SearchFilters, expectedFilters);

    Database *db = Database::instance();
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db);
    QSignalSpy findPhotosCalled(dbMock,
        SIGNAL(findPhotosCalled(Imaginario::SearchFilters,
                                Qt::SortOrder)));

    QQmlEngine engine;
    qmlRegisterType<PhotoModel>("MyTest", 1, 0, "PhotoModel");
    QQmlComponent component(&engine);
    component.setData("import MyTest 1.0\n"
                      "PhotoModel { " + properties.toUtf8() + "}",
                      QUrl());
    QObject *object = component.create();
    QVERIFY(object != 0);
    QAbstractListModel *model = qobject_cast<QAbstractListModel*>(object);
    QVERIFY(model != 0);

    QTRY_COMPARE(findPhotosCalled.count(), 1);
    SearchFilters filters = findPhotosCalled.at(0).at(0).value<SearchFilters>();

    QCOMPARE(filters, expectedFilters);

    delete db;
}

void PhotoModelTest::testPhotoIds()
{
    Database *db = Database::instance();
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db);
    QSignalSpy photosCalled(dbMock, SIGNAL(photosCalled(QList<int>)));

    QList<PhotoId> photoIds = (QList<PhotoId>() << 3 << 1);
    PhotoModel model;
    model.setPhotoIds(photoIds);

    QTRY_COMPARE(photosCalled.count(), 1);
    QCOMPARE(photosCalled.at(0).at(0).value<QList<int> >(), photoIds);

    delete db;
}

void PhotoModelTest::testPhotoVersions()
{
    Database *db = Database::instance();
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db);
    QSignalSpy photoVersionsCalled(dbMock, SIGNAL(photoVersionsCalled(int)));

    PhotoModel model;
    model.setPhotoVersions(4);

    QTRY_COMPARE(photoVersionsCalled.count(), 1);
    QCOMPARE(photoVersionsCalled.at(0).at(0).toInt(), 4);

    delete db;
}

void PhotoModelTest::testDeletePhoto_data()
{
    QTest::addColumn<QString>("indexesString");
    QTest::addColumn<int>("removedCount");
    QTest::addColumn<QList<int> >("remainingIds");

    QTest::newRow("first") <<
        "0" <<
        1 <<
        (QList<int>() << 1 << 2 << 3 << 4);

    QTest::newRow("second") <<
        "1" <<
        1 <<
        (QList<int>() << 0 << 2 << 3 << 4);

    QTest::newRow("second and fourth") <<
        "[1,3]" <<
        2 <<
        (QList<int>() << 0 << 2 << 4);
}

void PhotoModelTest::testDeletePhoto()
{
    QFETCH(QString, indexesString);
    QFETCH(int, removedCount);
    QFETCH(QList<int>, remainingIds);

    Database *db = Database::instance();
    QSignalSpy photoDeleted(db, SIGNAL(photoDeleted(PhotoId)));

    DatabasePrivate *dbMock = DatabasePrivate::mocked(db);

    /* Create a few photos */
    QList<Database::Photo> allPhotos;
    PhotoId firstPhoto = 1;
    for (int i = 0; i < 5; i++) {
        Database::Photo photo;
        dbMock->setPhotoId(photo, firstPhoto + i);
        photo.setBaseUri("/tmp");
        QString fileName = QString("foo%1.jpg").arg(i);
        photo.setFileName(fileName);
        allPhotos.append(photo);
    }
    dbMock->setPhotos(allPhotos);

    QQmlEngine engine;
    qmlRegisterType<PhotoModel>("MyTest", 1, 0, "PhotoModel");
    QQmlComponent component(&engine);
    QByteArray indexesText = indexesString.toUtf8();
    component.setData("import MyTest 1.0\n"
                      "PhotoModel {\n"
                      "  function run() {\n"
                      "    deletePhotos(" + indexesText + ")\n"
                      "  }\n"
                      "}",
                      QUrl());
    QObject *object = component.create();
    QVERIFY(object != 0);
    QAbstractListModel *model = qobject_cast<QAbstractListModel*>(object);
    QVERIFY(model != 0);
    QCOMPARE(model->rowCount(), 5);

    QSignalSpy countChanged(model, SIGNAL(countChanged()));

    bool ok = QMetaObject::invokeMethod(object, "run");
    QVERIFY(ok);

    QCOMPARE(photoDeleted.count(), removedCount);
    QTRY_COMPARE(countChanged.count(), 1);

    QList<int> remainingPhotos;
    for (int i = 0; i < model->rowCount(); i++) {
        remainingPhotos.append(get(model, i, "photoId").toInt() - firstPhoto);
    }
    QCOMPARE(remainingIds, remainingPhotos);

    delete db;
}

void PhotoModelTest::testPhotoChanges()
{
    Database *db = Database::instance();
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db);

    /* Create a few photos */
    QList<Database::Photo> allPhotos;
    PhotoId firstPhoto = 1;
    for (int i = 0; i < 5; i++) {
        Database::Photo photo;
        dbMock->setPhotoId(photo, firstPhoto + i);
        photo.setBaseUri("/tmp");
        QString fileName = QString("foo%1.jpg").arg(i);
        photo.setFileName(fileName);
        allPhotos.append(photo);
    }
    dbMock->setPhotos(allPhotos);

    PhotoModel model;
    model.classBegin();
    model.componentComplete();

    QSignalSpy dataChanged(&model,
                           SIGNAL(dataChanged(const QModelIndex&,const QModelIndex&)));

    QCOMPARE(model.rowCount(), 5);

    QCOMPARE(model.get(0, "rating").toInt(), -1);
    QCOMPARE(model.get(0, "location").value<GeoPoint>(), GeoPoint());

    db->setRating(firstPhoto + 2, 3);
    QCOMPARE(model.get(2, "rating").toInt(), 3);
    QCOMPARE(dataChanged.count(), 1);
    QCOMPARE(dataChanged.at(0).at(0).value<QModelIndex>().row(), 2);
    dataChanged.clear();

    GeoPoint newLoc(40, 20);
    db->setLocation(firstPhoto, newLoc);
    QCOMPARE(model.get(0, "location").value<GeoPoint>(), newLoc);
    QCOMPARE(dataChanged.count(), 1);
    QCOMPARE(dataChanged.at(0).at(0).value<QModelIndex>().row(), 0);
    dataChanged.clear();

    delete db;
}

void PhotoModelTest::testCountUpdates()
{
    Database *db = Database::instance();

    PhotoModel model;

    QSignalSpy countChanged(&model, SIGNAL(countChanged()));

    /* Create a few photos */
    PhotoId firstPhoto = -1;
    for (int i = 0; i < 5; i++) {
        Database::Photo photo;
        photo.setBaseUri("/tmp");
        QString fileName = QString("foo%1.jpg").arg(i);
        photo.setFileName(fileName);
        PhotoId id = db->addPhoto(photo, 1);
        if (firstPhoto < 0) { firstPhoto = id; }
    }

    QVERIFY(countChanged.wait());
    /* We know that updates are batched, so we'll get only one signal */
    QCOMPARE(countChanged.count(), 1);
    QCOMPARE(model.rowCount(), 5);
    countChanged.clear();

    /* Remove some photos */
    db->deletePhoto(firstPhoto);
    db->deletePhoto(firstPhoto + 2);

    QVERIFY(countChanged.wait());
    QCOMPARE(countChanged.count(), 1);
    QCOMPARE(model.rowCount(), 3);

    delete db;
}

void PhotoModelTest::testNewPhotoChanges()
{
    Database *db = Database::instance();
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db);

    /* Create a few photos */
    QList<Database::Photo> allPhotos;
    PhotoId firstPhoto = 1;
    for (int i = 0; i < 5; i++) {
        Database::Photo photo;
        dbMock->setPhotoId(photo, firstPhoto + i);
        photo.setBaseUri("/tmp");
        QString fileName = QString("foo%1.jpg").arg(i);
        photo.setFileName(fileName);
        allPhotos.append(photo);
    }
    dbMock->setPhotos(allPhotos);
    QSignalSpy findPhotosCalled(dbMock,
        SIGNAL(findPhotosCalled(Imaginario::SearchFilters,
                                Qt::SortOrder)));

    PhotoModel model;
    model.classBegin();
    model.componentComplete();

    QTRY_COMPARE(findPhotosCalled.count(), 1);
    findPhotosCalled.clear();

    QCOMPARE(model.rowCount(), 5);

    /* Pretend that a photo which was not in the model has changed. */
    dbMock->emitPhotoChanged(41);
    QTRY_COMPARE(findPhotosCalled.count(), 1);

    delete db;
}

void PhotoModelTest::testFirstIndexOlderThan_data()
{
    QTest::addColumn<QStringList>("photoTimes");
    QTest::addColumn<PhotoModel::TimeOrder>("order");
    QTest::addColumn<QString>("time");
    QTest::addColumn<int>("expectedIndex");

    QTest::newRow("empty model") <<
        QStringList {} <<
        PhotoModel::OlderThan <<
        "2015-07-01" <<
        -1;

    QTest::newRow("oldest") <<
        QStringList {
            "2015-07-29",
            "2015-07-20",
            "2015-07-05",
            "2015-07-03",
            "2015-07-02",
            "2015-07-02",
            "2015-07-01",
        } <<
        PhotoModel::OlderThan <<
        "2015-07-01" <<
        6;

    QTest::newRow("too old") <<
        QStringList {
            "2015-07-29",
            "2015-07-20",
            "2015-07-05",
            "2015-07-03",
            "2015-07-02",
            "2015-07-02",
            "2015-07-01",
        } <<
        PhotoModel::OlderThan <<
        "2015-06-01" <<
        -1;

    QTest::newRow("too old, newer than") <<
        QStringList {
            "2015-07-29",
            "2015-07-20",
            "2015-07-05",
            "2015-07-03",
            "2015-07-02",
            "2015-07-02",
            "2015-07-01",
        } <<
        PhotoModel::NewerThan <<
        "2015-06-01" <<
        6;

    QTest::newRow("newest") <<
        QStringList {
            "2015-07-29",
            "2015-07-20",
            "2015-07-05",
            "2015-07-03",
            "2015-07-02",
            "2015-07-02",
            "2015-07-01",
        } <<
        PhotoModel::OlderThan <<
        "2015-07-29" <<
        0;

    QTest::newRow("too new") <<
        QStringList {
            "2015-07-29",
            "2015-07-20",
            "2015-07-05",
            "2015-07-03",
            "2015-07-02",
            "2015-07-02",
            "2015-07-01",
        } <<
        PhotoModel::OlderThan <<
        "2015-08-02" <<
        0;

    QTest::newRow("too new, newer than") <<
        QStringList {
            "2015-07-29",
            "2015-07-20",
            "2015-07-05",
            "2015-07-03",
            "2015-07-02",
            "2015-07-02",
            "2015-07-01",
        } <<
        PhotoModel::NewerThan <<
        "2015-08-02" <<
        -1;

    QTest::newRow("middle") <<
        QStringList {
            "2015-07-29",
            "2015-07-20",
            "2015-07-05",
            "2015-07-03",
            "2015-07-02",
            "2015-07-02",
            "2015-07-01",
        } <<
        PhotoModel::OlderThan <<
        "2015-07-03" <<
        3;

    QTest::newRow("middle, newer than") <<
        QStringList {
            "2015-07-29",
            "2015-07-20",
            "2015-07-05",
            "2015-07-03",
            "2015-07-02",
            "2015-07-02",
            "2015-07-01",
        } <<
        PhotoModel::NewerThan <<
        "2015-07-03" <<
        3;

    QTest::newRow("between, newer than") <<
        QStringList {
            "2015-07-29",
            "2015-07-20",
            "2015-07-05",
            "2015-07-03",
            "2015-07-02",
            "2015-07-02",
            "2015-07-01",
        } <<
        PhotoModel::NewerThan <<
        "2015-07-04" <<
        2;

    QTest::newRow("between, older than") <<
        QStringList {
            "2015-07-29",
            "2015-07-20",
            "2015-07-05",
            "2015-07-03",
            "2015-07-02",
            "2015-07-02",
            "2015-07-01",
        } <<
        PhotoModel::OlderThan <<
        "2015-07-04" <<
        3;
}

void PhotoModelTest::testFirstIndexOlderThan()
{
    QFETCH(QStringList, photoTimes);
    QFETCH(PhotoModel::TimeOrder, order);
    QFETCH(QString, time);
    QFETCH(int, expectedIndex);

    QScopedPointer<Database> db(Database::instance());
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db.data());

    /* Create the photos */
    QList<Database::Photo> allPhotos;
    Database::Photo photo;
    photo.setBaseUri("/tmp");
    photo.setFileName("foo");
    for (const QString &photoTime: photoTimes) {
        photo.setTime(QDateTime::fromString(photoTime, Qt::ISODate));
        allPhotos.append(photo);
    }
    dbMock->setPhotos(allPhotos);

    PhotoModel photoModel;
    photoModel.classBegin();
    photoModel.componentComplete();

    QDateTime t = QDateTime::fromString(time, Qt::ISODate);
    int index = photoModel.firstIndex(order, t);

    QCOMPARE(index, expectedIndex);
}

void PhotoModelTest::testForceItemRefresh_data()
{
    QTest::addColumn<QList<int>>("rows");
    QTest::addColumn<QStringList>("expectedUrls");

    QTest::newRow("no calls") <<
        QList<int> {} <<
        QStringList {};

    QTest::newRow("one call") <<
        QList<int> { 2 } <<
        QStringList { "/tmp/foo2.jpg#1" };

    QTest::newRow("many calls") <<
        QList<int> { 2, 0, 3 } <<
        QStringList { "/tmp/foo2.jpg#1", "/tmp/foo0.jpg#2", "/tmp/foo3.jpg#3" };
}

void PhotoModelTest::testForceItemRefresh()
{
    QFETCH(QList<int>, rows);
    QFETCH(QStringList, expectedUrls);

    QScopedPointer<Database> db(Database::instance());
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db.data());

    /* Create a few photos */
    QList<Database::Photo> allPhotos;
    PhotoId firstPhoto = 1;
    for (int i = 0; i < 5; i++) {
        Database::Photo photo;
        dbMock->setPhotoId(photo, firstPhoto + i);
        photo.setBaseUri("/tmp");
        QString fileName = QString("foo%1.jpg").arg(i);
        photo.setFileName(fileName);
        allPhotos.append(photo);
    }
    dbMock->setPhotos(allPhotos);

    PhotoModel photoModel;
    photoModel.classBegin();
    photoModel.componentComplete();

    QList<QVariantList> dataChanged;
    QObject::connect(&photoModel, &PhotoModel::dataChanged,
                     [&](QModelIndex idx0, QModelIndex idx1) {
        QVariantList args = {
            idx0.row(),
            idx1.row(),
            photoModel.get(idx0.row(), "url"),
        };
        dataChanged.append(args);
    });

    for (int row: rows) {
        photoModel.forceItemRefresh(row);
    }

    /* Verify that the items urls are presently the same they were initially */
    for (int i = 0; i < 5; i++) {
        QCOMPARE(photoModel.get(i, "url").toString(),
                 QString("/tmp/foo%1.jpg").arg(i));
    }

    QStringList urls;
    for (const QVariantList &args: dataChanged) {
        QCOMPARE(args.at(0).toInt(), args.at(1).toInt());
        urls.append(args.at(2).toString());
    }

    QCOMPARE(urls, expectedUrls);
}

QTEST_GUILESS_MAIN(PhotoModelTest)

#include "tst_photo_model.moc"
