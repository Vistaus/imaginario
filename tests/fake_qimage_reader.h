/*
 * Copyright (C) 2020-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGINARIO_FAKE_QIMAGE_READER_H
#define IMAGINARIO_FAKE_QIMAGE_READER_H

#include <QImageReader>

class FakeQImageReader: public QObject {
    Q_OBJECT

public:
    FakeQImageReader();
    FakeQImageReader(const QString &fileNamePattern);
    ~FakeQImageReader() = default;

    static FakeQImageReader *mockForFile(const QString &fileName);

    void setSizeResult(const QSize &size) { m_sizeResult = size; }
    void setReadResult(const QImage &image) { m_readResult = image; }

Q_SIGNALS:
    void setAutoTransformCalled(bool enabled);
    void setScaledSizeCalled(const QSize size);
    void readCalled();

private:
    friend class QImageReader;
    bool m_isControlled;
    QSize m_sizeResult;
    QImage m_readResult;
};

#endif // IMAGINARIO_FAKE_QIMAGE_READER_H
