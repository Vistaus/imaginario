/*
 * Copyright (C) 2014-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "database.h"
#include "test_utils.h"

#include <QCoreApplication>
#include <QDateTime>
#include <QProcess>
#include <QScopedPointer>
#include <QSignalSpy>
#include <QStandardPaths>
#include <QTemporaryDir>
#include <QTest>

using namespace Imaginario;

struct SearchFiltersData: public SearchFilters {
    QList<QStringList> requiredTagNames;
    QStringList forbiddenTagNames;

    void clear() { *this = SearchFiltersData(); }
};

Q_DECLARE_METATYPE(SearchFiltersData)

class DatabaseTest: public QObject
{
    Q_OBJECT

public:
    DatabaseTest() {
        qRegisterMetaType<PhotoId>("PhotoId");
        qRegisterMetaType<Tag>("Tag");
    }

private Q_SLOTS:
    void testPhoto_data();
    void testPhoto();
    void testAddPhoto_data();
    void testAddPhoto();
    void testDeletePhoto();
    void testPhotoTags();
    void testFindPhotos_data();
    void testFindPhotos();
    void testPhotos_data();
    void testPhotos();
    void testCreateTag();
    void testUpdateTag_data();
    void testUpdateTag();
    void testUpdateIsCategory();
    void testDeleteTag();
    void testSetDescription();
    void testSetRating();
    void testSetLocation();
    void testTagArea();
    void testTagPopularity_data();
    void testTagPopularity();
    void testTagUsageCount_data();
    void testTagUsageCount();
    void testTagParentLoop();
    void testDefaultTags();
    void testUpdate_data();
    void testUpdate();
    void testPhotoVersion_data();
    void testPhotoVersion();
    void testRolls();
};

void DatabaseTest::testPhoto_data()
{
    QTest::addColumn<QString>("baseUri");
    QTest::addColumn<QString>("fileName");
    QTest::addColumn<QString>("description");
    QTest::addColumn<QDateTime>("time");
    QTest::addColumn<int>("rating");
    QTest::addColumn<GeoPoint>("location");
    QTest::addColumn<QUrl>("expectedUrl");

    QTest::newRow("empty") <<
        QString() << QString() << QString() <<
        QDateTime() << 0 << GeoPoint() <<
        QUrl("/");

    QTest::newRow("simple") <<
        "file:///tmp/foo" << "home.jpg" << "My home" <<
        QDateTime::fromString("Tuesday, 23 April 12 22:51:41") << 4 <<
        GeoPoint(40, -10) <<
        QUrl("file:///tmp/foo/home.jpg");
}

void DatabaseTest::testPhoto()
{
    QFETCH(QString, baseUri);
    QFETCH(QString, fileName);
    QFETCH(QString, description);
    QFETCH(QDateTime, time);
    QFETCH(int, rating);
    QFETCH(GeoPoint, location);
    QFETCH(QUrl, expectedUrl);

    Database::Photo photo;
    photo.setBaseUri(baseUri);
    photo.setFileName(fileName);
    photo.setDescription(description);
    photo.setTime(time);
    photo.setRating(rating);
    photo.setLocation(location);

    QCOMPARE(photo.baseUri(), baseUri);
    QCOMPARE(photo.fileName(), fileName);
    QCOMPARE(photo.description(), description);
    QCOMPARE(photo.time(), time);
    QCOMPARE(photo.rating(), rating);
    QCOMPARE(photo.baseUri(), baseUri);
    QCOMPARE(photo.location(), location);
    QCOMPARE(photo.url(), expectedUrl);
}

void DatabaseTest::testAddPhoto_data()
{
    QTest::addColumn<QString>("baseUri");
    QTest::addColumn<QString>("fileName");
    QTest::addColumn<QString>("description");
    QTest::addColumn<QDateTime>("time");
    QTest::addColumn<int>("rating");
    QTest::addColumn<int>("rollId");
    QTest::addColumn<GeoPoint>("location");

    QTest::newRow("empty") <<
        "" << "" << "" <<
        QDateTime() << 0 << 0 << GeoPoint();

    QTest::newRow("simple") <<
        "/tmp/foo" << "home.jpg" << "My home" <<
        QDateTime::fromString("Tuesday, 23 April 12 22:51:41") << 4 << 2 <<
        GeoPoint(45.2, -3.6);
}

void DatabaseTest::testAddPhoto()
{
    QTemporaryDir tmpDir;
    qputenv("XDG_CONFIG_HOME", tmpDir.path().toUtf8());

    QFETCH(QString, baseUri);
    QFETCH(QString, fileName);
    QFETCH(QString, description);
    QFETCH(QDateTime, time);
    QFETCH(int, rating);
    QFETCH(int, rollId);
    QFETCH(GeoPoint, location);

    Database::Photo photo;
    photo.setBaseUri(baseUri);
    photo.setFileName(fileName);
    photo.setDescription(description);
    photo.setTime(time);
    photo.setRating(rating);
    photo.setLocation(location);

    QScopedPointer<Database> db(Database::instance());
    QVERIFY(db);
    QSignalSpy photoAdded(db.data(), SIGNAL(photoAdded(PhotoId)));

    PhotoId id = db->addPhoto(photo, rollId);
    QVERIFY(id >= 0);

    QCOMPARE(photoAdded.count(), 1);
    QCOMPARE(photoAdded.at(0).at(0).toInt(), int(id));

    // Tries with and without the cache
    for (int i = 0; i < 2; i++, db->clearCache()) {
        Database::Photo p = db->photo(id);
        QVERIFY(p.isValid());

        QCOMPARE(p.baseUri(), baseUri);
        QCOMPARE(p.fileName(), fileName);
        QCOMPARE(p.description(), description);
        QCOMPARE(p.time(), time);
        QCOMPARE(p.rating(), rating);
        QCOMPARE(p.baseUri(), baseUri);
        QCOMPARE(p.rollId(), rollId);
        QCOMPARE(p.id(), id);
        QCOMPARE(p.location(), location);
    }
}

void DatabaseTest::testDeletePhoto()
{
    QTemporaryDir tmpDir;
    qputenv("XDG_CONFIG_HOME", tmpDir.path().toUtf8());

    QScopedPointer<Database> db(Database::instance());
    QVERIFY(db);

    db->transaction();

    PhotoId firstPhoto = -1;
    Database::Photo photo;
    photo.setBaseUri("/tmp");
    for (int i = 0; i < 5; i++) {
        QString fileName = QString("foo%1.jpg").arg(i);
        photo.setFileName(fileName);
        PhotoId id = db->addPhoto(photo, 1);
        QVERIFY(id >= 0);

        if (firstPhoto < 0) {
            firstPhoto = id;
        }
    }

    bool ok = db->commit();
    QVERIFY(ok);

    QSignalSpy photoDeleted(db.data(), SIGNAL(photoDeleted(PhotoId)));

    // Delete some photos
    db->transaction();
    ok = db->deletePhoto(firstPhoto + 2);
    QVERIFY(ok);
    ok = db->deletePhoto(firstPhoto);
    QVERIFY(ok);
    db->commit();

    QCOMPARE(photoDeleted.count(), 2);
    QCOMPARE(photoDeleted.at(0).at(0).toInt(), int(firstPhoto + 2));
    QCOMPARE(photoDeleted.at(1).at(0).toInt(), int(firstPhoto));

    // Try reading the photo, this should fail
    Database::Photo p = db->photo(firstPhoto);
    QVERIFY(!p.isValid());
    p = db->photo(firstPhoto + 1);
    QVERIFY(p.isValid());
    p = db->photo(firstPhoto + 2);
    QVERIFY(!p.isValid());
}

void DatabaseTest::testPhotoTags()
{
    QTemporaryDir tmpDir;
    qputenv("XDG_CONFIG_HOME", tmpDir.path().toUtf8());

    QScopedPointer<Database> db(Database::instance());
    QVERIFY(db);

    db->transaction();

    // Create a few tags
    QStringList allTags;
    allTags << "red" << "green" << "blue" <<
        "tiny" << "so so" << "big" <<
        "a house" << "a dog" << "a cat";
    Q_FOREACH(const QString &tagName, allTags) {
        Tag tag = db->createTag(Tag(), tagName, QString());
        QVERIFY(tag.isValid());
    }

    bool ok;

    Database::Photo photo;
    photo.setBaseUri("/tmp");
    photo.setFileName("foo.jpg");
    photo.setDescription("");
    photo.setTime(QDateTime::fromString("1997-07-16T19:20:30",
                                        Qt::ISODate));
    PhotoId id = db->addPhoto(photo, 1);
    QVERIFY(id >= 0);

    /* Create another photo, which we'll never touch -- just to make sure that
     * tags operations affect the right photo */
    photo.setFileName("foo2.jpg");
    PhotoId id2 = db->addPhoto(photo, 1);
    QVERIFY(id2 >= 0);
    QList<Tag> tags2;
    tags2 << db->tag("so so") << db->tag("red");
    ok = db->setTags(id2, tags2);
    QVERIFY(ok);
    QCOMPARE(db->tags(id2).toSet(), tags2.toSet());

    // Set initial tags
    QStringList initialTags;
    initialTags << "green" << "so so" << "a dog";
    ok = db->setTags(id, initialTags);
    QVERIFY(ok);

    ok = db->commit();
    QVERIFY(ok);

    // Read the tags back
    QStringList tags = Tag::tagNames(db->tags(id));
    tags.sort();
    QStringList expectedTags = initialTags;
    expectedTags.sort();
    QCOMPARE(tags, expectedTags);
    QCOMPARE(db->tags(id2).toSet(), tags2.toSet());

    QSignalSpy photoTagsChanged(db.data(), SIGNAL(photoTagsChanged(PhotoId)));

    // Add more tags
    QStringList addedTags;
    addedTags << "red" << "tiny";
    ok = db->addTags(id, db->tags(addedTags));
    QVERIFY(ok);

    QCOMPARE(photoTagsChanged.count(), 1);
    QCOMPARE(photoTagsChanged.at(0).at(0).toInt(), int(id));
    photoTagsChanged.clear();

    // Read the tags back
    tags = Tag::tagNames(db->tags(id));
    tags.sort();
    expectedTags = initialTags + addedTags;
    expectedTags.sort();
    QCOMPARE(tags, expectedTags);
    QCOMPARE(db->tags(id2).toSet(), tags2.toSet());

    // Delete some tags;
    ok = db->removeTags(id, db->tags(initialTags));
    QVERIFY(ok);

    QCOMPARE(photoTagsChanged.count(), 1);
    QCOMPARE(photoTagsChanged.at(0).at(0).toInt(), int(id));
    photoTagsChanged.clear();

    // Read the tags back
    tags = Tag::tagNames(db->tags(id));
    tags.sort();
    expectedTags = addedTags;
    expectedTags.sort();
    QCOMPARE(tags, expectedTags);
    QCOMPARE(db->tags(id2).toSet(), tags2.toSet());
}

void DatabaseTest::testFindPhotos_data()
{
    QTest::addColumn<SearchFiltersData>("filters");
    QTest::addColumn<bool>("sortDesc");
    QTest::addColumn<QList<int> >("expectedPhotoIds");

    SearchFiltersData filters;
    QTest::newRow("empty filters, asc") <<
        filters <<
        false <<
        (QList<int>() << 4 << 1 << 2 << 3 << 6 << 5);

    QTest::newRow("empty filters, desc") <<
        filters <<
        true <<
        (QList<int>() << 5 << 6 << 3 << 2 << 1 << 4);

    filters.time0 = QDateTime::fromString("1997-07-17T19:20:30", Qt::ISODate);
    filters.time1 = QDateTime::fromString("2000-01-17T19:20:30", Qt::ISODate),
    QTest::newRow("by date, asc") <<
        filters <<
        false <<
        (QList<int>() << 2 << 3 << 6);

    filters.clear();
    filters.roll0 = 2;
    filters.roll1 = 2;
    QTest::newRow("by roll, desc") <<
        filters <<
        true <<
        (QList<int>() << 6 << 3);

    filters.clear();
    filters.rating0 = 3;
    filters.rating1 = 4;
    QTest::newRow("by rating, desc") <<
        filters <<
        true <<
        QList<int>{ 5, 3, 4 };

    filters.clear();
    filters.location0 = GeoPoint(60, -10);
    filters.location1 = GeoPoint(40, 10);
    QTest::newRow("by location, asc") <<
        filters <<
        false <<
        (QList<int>() << 2 << 3);

    filters.clear();
    filters.nonGeoTagged = true;
    QTest::newRow("non geotagged, asc") <<
        filters <<
        false <<
        (QList<int>() << 6);

    filters.clear();
    filters.requiredTagNames = {QStringList{"a dog"}};
    QTest::newRow("by required tags (1), asc") <<
        filters <<
        false <<
        (QList<int>() << 1 << 3 << 5);

    filters.clear();
    filters.requiredTagNames = {QStringList{"blue","big"}};
    QTest::newRow("by required tags (2), asc") <<
        filters <<
        false <<
        (QList<int>() << 4 << 5);

    filters.clear();
    filters.requiredTagNames = {QStringList{"blue","big"},QStringList{"a dog"}};
    QTest::newRow("by required tags (3), asc") <<
        filters <<
        false <<
        (QList<int>() << 4 << 1 << 3 << 5);

    filters.clear();
    filters.forbiddenTagNames = QStringList{"a house"};
    QTest::newRow("by forbidden tags (1), asc") <<
        filters <<
        false <<
        (QList<int>() << 4 << 1 << 3 << 5);

    filters.clear();
    filters.forbiddenTagNames = QStringList{"tiny", "a cat"};
    QTest::newRow("by forbidden tags (2), asc") <<
        filters <<
        false <<
        (QList<int>() << 1 << 3 << 5);

    filters.clear();
    filters.forbiddenTagNames = QStringList{"blue"};
    filters.requiredTagNames = {QStringList{"blue", "big"}};
    QTest::newRow("both forbidden and required, asc") <<
        filters <<
        false <<
        (QList<int>() << 4 << 5);

    filters.clear();
    filters.skipNonDefaultVersions = true;
    QTest::newRow("skipping versions, asc") <<
        filters <<
        false <<
        (QList<int>() << 1 << 2 << 3 << 6 << 5);

    filters.clear();
    filters.excludedPhotos = { 4, 6 };
    QTest::newRow("excluded photos, asc") <<
        filters <<
        false <<
        (QList<int>() << 1 << 2 << 3 << 5);

    filters.clear();
    filters.areaTagged = true;
    QTest::newRow("with area tags, asc") <<
        filters <<
        false <<
        QList<int>{ 3, 6 };
}

void DatabaseTest::testFindPhotos()
{
    QTemporaryDir tmpDir;
    qputenv("XDG_CONFIG_HOME", tmpDir.path().toUtf8());

    QFETCH(SearchFiltersData, filters);
    QFETCH(bool, sortDesc);
    QFETCH(QList<int>, expectedPhotoIds);

    /* Populate the DB with some photos */
    Database::Photo photo;
    photo.setBaseUri("/tmp");
    photo.setFileName("foo.jpg");
    photo.setDescription("");

    QScopedPointer<Database> db(Database::instance());
    QVERIFY(db);

    db->transaction();

    // Create a few tags
    QStringList allTags;
    allTags << "red" << "green" << "blue" <<
        "tiny" << "so so" << "big" <<
        "a house" << "a dog" << "a cat";
    Q_FOREACH(const QString &tagName, allTags) {
        Tag tag = db->createTag(Tag(), tagName, QString());
        QVERIFY(tag.isValid());
    }

    int progressiveId = 1;
    bool ok;

    // Photo #1
    photo.setRating(1);
    photo.setTime(QDateTime::fromString("1997-07-16T19:20:30",
                                        Qt::ISODate));
    photo.setLocation(GeoPoint(50, 20));
    PhotoId id = db->addPhoto(photo, 1);
    QCOMPARE(id, progressiveId++);
    ok = db->setTags(id, QStringList() << "green" << "so so" << "a dog");
    QVERIFY(ok);

    // Photo #2
    photo.setRating(2);
    photo.setTime(QDateTime::fromString("1997-07-17T19:20:30",
                                        Qt::ISODate));
    photo.setLocation(GeoPoint(51, 2));
    id = db->addPhoto(photo, 1);
    QCOMPARE(id, progressiveId++);
    ok = db->setTags(id, QStringList() << "red" << "tiny" << "a house");
    QVERIFY(ok);

    // Photo #3
    photo.setRating(3);
    photo.setTime(QDateTime::fromString("1998-07-17T19:20:30",
                                        Qt::ISODate));
    photo.setLocation(GeoPoint(41, -2));
    id = db->addPhoto(photo, 2);
    QCOMPARE(id, progressiveId++);
    ok = db->setTags(id, QStringList() << "green" << "so so" << "a dog");
    QVERIFY(ok);
    ok = db->addTagWithArea(id, db->tag("a dog"), QRectF(0.1, 0.1, 0.5, 0.4));
    QVERIFY(ok);

    // Photo #4
    photo.setRating(4);
    photo.setTime(QDateTime::fromString("1995-07-17T19:20:30",
                                        Qt::ISODate));
    photo.setLocation(GeoPoint(61, 4));
    id = db->addPhoto(photo, 1);
    QCOMPARE(id, progressiveId++);
    ok = db->setTags(id, QStringList() << "blue" << "big" << "a cat");
    QVERIFY(ok);

    // Photo #5
    photo.setRating(3);
    photo.setTime(QDateTime::fromString("2005-07-17T19:20:30",
                                        Qt::ISODate));
    photo.setLocation(GeoPoint(11, 4));
    id = db->addPhoto(photo, 3);
    QCOMPARE(id, progressiveId++);
    ok = db->setTags(id, QStringList() << "blue" << "big" << "a dog");
    QVERIFY(ok);

    // Photo #6
    photo.setRating(5);
    photo.setTime(QDateTime::fromString("1999-07-17T19:20:30",
                                        Qt::ISODate));
    photo.setLocation(GeoPoint());
    id = db->addPhoto(photo, 2);
    QCOMPARE(id, progressiveId++);
    ok = db->setTags(id, QStringList() << "blue" << "tiny" << "a house");
    QVERIFY(ok);
    ok = db->addTagWithArea(id, db->tag("blue"), QRectF(0.2, 0.3, 0.5, 0.4));
    QVERIFY(ok);
    ok = db->addTagWithArea(id, db->tag("a house"), QRectF(0.1, 0.1, 0.5, 0.4));
    QVERIFY(ok);

    // Make a version
    db->makeVersion(4, 2);

    ok = db->commit();
    QVERIFY(ok);

    Q_FOREACH(const QStringList &requiredTagsList, filters.requiredTagNames) {
        filters.requiredTags.append(db->tags(requiredTagsList));
    }
    filters.forbiddenTags = db->tags(filters.forbiddenTagNames);
    Qt::SortOrder order = sortDesc ?  Qt::DescendingOrder : Qt::AscendingOrder;
    QList<Database::Photo> photos = db->findPhotos(filters, order);
    QCOMPARE(photos.count(), expectedPhotoIds.count());

    int i = 0;
    Q_FOREACH(const Database::Photo &p, photos) {
        QCOMPARE(p.id(), expectedPhotoIds[i++]);
    }
}

void DatabaseTest::testPhotos_data()
{
    QTest::addColumn<QList<int> >("photoIds");
    QTest::addColumn<QList<int> >("expectedPhotoIds");

    QTest::newRow("empty list") <<
        QList<int>() <<
        QList<int>();

    QTest::newRow("one item") <<
        (QList<int>() << 4) <<
        (QList<int>() << 4);

    QTest::newRow("missing items") <<
        (QList<int>() << 4 << 1 << 2 << 3 << 6 << 5) <<
        (QList<int>() << 4 << 1 << 2 << 3 << 5);
}

void DatabaseTest::testPhotos()
{
    QTemporaryDir tmpDir;
    qputenv("XDG_CONFIG_HOME", tmpDir.path().toUtf8());

    QFETCH(QList<int>, photoIds);
    QFETCH(QList<int>, expectedPhotoIds);

    /* Populate the DB with some photos */
    Database::Photo photo;
    photo.setBaseUri("/tmp");
    photo.setFileName("foo.jpg");
    photo.setDescription("");

    QScopedPointer<Database> db(Database::instance());
    QVERIFY(db);

    db->transaction();

    int progressiveId = 1;
    bool ok;

    // Photo #1
    photo.setTime(QDateTime::fromString("1999-07-17T19:20:30",
                                        Qt::ISODate));
    PhotoId id = db->addPhoto(photo, 1);
    QCOMPARE(id, progressiveId++);
    // Photo #2
    photo.setTime(QDateTime::fromString("1999-08-17T19:20:30",
                                        Qt::ISODate));
    id = db->addPhoto(photo, 1);
    QCOMPARE(id, progressiveId++);
    // Photo #3
    photo.setTime(QDateTime::fromString("1999-09-17T19:20:30",
                                        Qt::ISODate));
    id = db->addPhoto(photo, 2);
    QCOMPARE(id, progressiveId++);
    // Photo #4
    photo.setTime(QDateTime::fromString("1999-06-17T19:20:30",
                                        Qt::ISODate));
    id = db->addPhoto(photo, 1);
    QCOMPARE(id, progressiveId++);
    // Photo #5
    photo.setTime(QDateTime::fromString("1999-10-17T19:20:30",
                                        Qt::ISODate));
    id = db->addPhoto(photo, 3);
    QCOMPARE(id, progressiveId++);

    ok = db->commit();
    QVERIFY(ok);

    QList<Database::Photo> photos = db->photos(photoIds);
    QCOMPARE(photos.count(), expectedPhotoIds.count());

    int i = 0;
    Q_FOREACH(const Database::Photo &p, photos) {
        QCOMPARE(p.id(), expectedPhotoIds[i++]);
    }
}

void DatabaseTest::testCreateTag()
{
    QTemporaryDir tmpDir;
    qputenv("XDG_CONFIG_HOME", tmpDir.path().toUtf8());

    QScopedPointer<Database> db(Database::instance());
    QVERIFY(db);

    QSignalSpy tagCreated(db.data(), SIGNAL(tagCreated(Tag)));

    // Create a few tags
    Tag whiteTag = db->createTag(Tag(), "white", "icon-white");
    Tag blueTag = db->createTag(Tag(), "blue", "icon-blue");
    Tag redTag = db->createTag(Tag(), "red", "icon-red");
    Tag greenTag = db->createTag(Tag(), "green", "icon-green");
    QCOMPARE(tagCreated.count(), 4);
    QCOMPARE(tagCreated.at(0).at(0).value<Tag>(), whiteTag);
    QCOMPARE(tagCreated.at(3).at(0).value<Tag>(), greenTag);

    // verify that they are there
    QList<Tag> tags = db->tags(Tag());
    QCOMPARE(tags.count(), 9);
    QVERIFY(tags.contains(whiteTag));
    QVERIFY(tags.contains(blueTag));
    QVERIFY(tags.contains(redTag));
    QVERIFY(tags.contains(greenTag));

    // and that their content matches
    QCOMPARE(whiteTag.name(), QString("white"));
    QCOMPARE(blueTag.name(), QString("blue"));
    QCOMPARE(redTag.name(), QString("red"));
    QCOMPARE(greenTag.name(), QString("green"));

    /* some more checks */
    QCOMPARE(redTag.icon(), QString("icon-red"));
    QCOMPARE(blueTag.parent(), Tag());
    QVERIFY(greenTag.isValid());
    QCOMPARE(db->tag("white"), whiteTag);

    /* We know that tags are cached; let's now try to reopen that DB, and
     * verify that they are still there */

    db.reset();
    db.reset(Database::instance());
    QVERIFY(db);

    // verify that they are there
    QCOMPARE(db->tag("blue"), blueTag);
    tags = db->tags(Tag());
    QCOMPARE(tags.count(), 9);
    QVERIFY(tags.contains(whiteTag));
    QVERIFY(tags.contains(blueTag));
    QVERIFY(tags.contains(redTag));
    QVERIFY(tags.contains(greenTag));

    // and that their content matches
    QCOMPARE(whiteTag.name(), QString("white"));
    QCOMPARE(blueTag.name(), QString("blue"));
    QCOMPARE(redTag.name(), QString("red"));
    QCOMPARE(greenTag.name(), QString("green"));

    // Test also the allTags() method
    tags = db->tags(Tag());
    QCOMPARE(tags.count(), 9);
    QVERIFY(tags.contains(whiteTag));
    QVERIFY(tags.contains(blueTag));
    QVERIFY(tags.contains(redTag));
    QVERIFY(tags.contains(greenTag));
}

void DatabaseTest::testUpdateTag_data()
{
    QTest::addColumn<QString>("name");
    QTest::addColumn<QString>("icon");
    QTest::addColumn<QString>("parentTag");
    QTest::addColumn<int>("expectedUpdates");

    QTest::newRow("no changes") <<
        QString() << QString() << "empty" << 0;

    QTest::newRow("invalid parent") <<
        QString() << QString() << "unexisting" << 2;

    QTest::newRow("all changes") <<
        "green" << "green-icon" << "colors" << 3;
}

void DatabaseTest::testUpdateTag()
{
    QFETCH(QString, name);
    QFETCH(QString, icon);
    QFETCH(QString, parentTag);
    QFETCH(int, expectedUpdates);

    QTemporaryDir tmpDir;
    qputenv("XDG_CONFIG_HOME", tmpDir.path().toUtf8());

    QScopedPointer<Database> db(Database::instance());
    QVERIFY(db);

    db->transaction();

    // Create a few tags
    db->createTag(db->uncategorizedTag(), "colors", "icon-colors");
    Tag whiteTag = db->createTag(db->uncategorizedTag(),
                                 "white", "icon-white");
    Tag blueTag = db->createTag(db->placesTag(), "blue", "icon-blue");

    // Set initial tags
    bool ok = db->commit();

    QSignalSpy tagChanged(db.data(), SIGNAL(tagChanged(Tag)));

    Database::TagUpdate data;
    data.name = name;
    data.icon = icon;
    Tag newParent = db->tag(parentTag);
    if (parentTag != "empty") {
        data.parent = QVariant::fromValue(newParent);
    }

    db->transaction();
    ok = db->updateTag(blueTag, data);
    QVERIFY(ok);
    ok = db->commit();
    QVERIFY(ok);

    QCOMPARE(tagChanged.count(), expectedUpdates);
    if (!expectedUpdates) {
        return;
    }

    QCOMPARE(tagChanged.at(0).at(0).value<Tag>(), blueTag);

    // verify that the others tags are unchanged
    QCOMPARE(whiteTag.name(), QString("white"));

    QString newName = name.isEmpty() ? "blue" : name;
    QString newIcon = icon.isEmpty() ? "icon-blue" : icon;
    QCOMPARE(blueTag.name(), newName);
    QCOMPARE(blueTag.icon(), newIcon);
    QCOMPARE(blueTag.parent(), newParent);


    /* We know that tags are cached; let's now try to reopen that DB, and
     * verify that the updated tag has changed */

    db.reset();
    db.reset(Database::instance());
    QVERIFY(db);

    blueTag = db->tag(newName);
    QCOMPARE(blueTag.name(), newName);
    QCOMPARE(blueTag.icon(), newIcon);
    QCOMPARE(blueTag.parent(), newParent);
}

void DatabaseTest::testUpdateIsCategory()
{
    QTemporaryDir tmpDir;
    qputenv("XDG_CONFIG_HOME", tmpDir.path().toUtf8());

    QScopedPointer<Database> db(Database::instance());
    QVERIFY(db);

    QSignalSpy tagChanged(db.data(), SIGNAL(tagChanged(Tag)));

    QCOMPARE(db->uncategorizedTag().isCategory(), false);

    /* Create a child tag, and verify that the parent isCategory
     * becomes true, and that the notification is emitted */
    db->transaction();
    Tag colorsTag =
        db->createTag(db->uncategorizedTag(), "colors", "icon-colors");
    QVERIFY(db->commit());
    QCOMPARE(tagChanged.count(), 1);
    QCOMPARE(tagChanged.at(0).at(0).value<Tag>(),
             db->uncategorizedTag());
    QCOMPARE(db->uncategorizedTag().isCategory(), true);
    tagChanged.clear();

    /* Create another child, this time no notification */
    Tag shapesTag =
        db->createTag(db->uncategorizedTag(), "shapes", "icon-shapes");
    QCOMPARE(tagChanged.count(), 0);
    QCOMPARE(db->uncategorizedTag().isCategory(), true);

    /* Create a tag inside colors, expect notification */
    Tag blueTag = db->createTag(colorsTag, "blue", "icon-blue");
    QCOMPARE(tagChanged.count(), 1);
    QCOMPARE(tagChanged.at(0).at(0).value<Tag>(), colorsTag);
    QCOMPARE(blueTag.isCategory(), false);
    QCOMPARE(colorsTag.isCategory(), true);
    QCOMPARE(db->uncategorizedTag().isCategory(), true);
    tagChanged.clear();

    /* Move "blue" from "colors" to "uncategorized" */
    Database::TagUpdate data;
    data.parent = QVariant::fromValue(db->uncategorizedTag());

    db->transaction();
    QVERIFY(db->updateTag(blueTag, data));
    QVERIFY(db->commit());

    QCOMPARE(tagChanged.count(), 2);
    QCOMPARE(tagChanged.at(0).at(0).value<Tag>(), blueTag);
    QCOMPARE(tagChanged.at(1).at(0).value<Tag>(), colorsTag);
    QCOMPARE(blueTag.isCategory(), false);
    QCOMPARE(colorsTag.isCategory(), false);
    QCOMPARE(db->uncategorizedTag().isCategory(), true);
    tagChanged.clear();

    /* and move it back */
    data.parent = QVariant::fromValue(colorsTag);
    db->transaction();
    QVERIFY(db->updateTag(blueTag, data));
    QVERIFY(db->commit());

    QCOMPARE(tagChanged.count(), 2);
    QCOMPARE(tagChanged.at(0).at(0).value<Tag>(), blueTag);
    QCOMPARE(tagChanged.at(1).at(0).value<Tag>(), colorsTag);
    QCOMPARE(blueTag.isCategory(), false);
    QCOMPARE(colorsTag.isCategory(), true);
    QCOMPARE(db->uncategorizedTag().isCategory(), true);
    tagChanged.clear();

    /* We know that tags are cached; let's now try to reopen that DB, and
     * verify that things are how we expect them to be. */
    db.reset();
    db.reset(Database::instance());
    QVERIFY(db);
    QSignalSpy tagChanged2(db.data(), SIGNAL(tagChanged(Tag)));
    blueTag = db->tag("blue");
    QCOMPARE(blueTag.isCategory(), false);
    colorsTag = db->tag("colors");
    QCOMPARE(colorsTag.isCategory(), true);
    QCOMPARE(db->uncategorizedTag().isCategory(), true);

    /* delete all the tags one by one */
    db->transaction();
    QVERIFY(db->deleteTag(shapesTag));
    QVERIFY(db->commit());

    QCOMPARE(tagChanged2.count(), 0);
    QCOMPARE(db->uncategorizedTag().isCategory(), true);

    db->transaction();
    QVERIFY(db->deleteTag(blueTag));
    QVERIFY(db->commit());

    QCOMPARE(tagChanged2.count(), 1);
    QCOMPARE(tagChanged2.at(0).at(0).value<Tag>(), colorsTag);
    QCOMPARE(colorsTag.isCategory(), false);
    QCOMPARE(db->uncategorizedTag().isCategory(), true);
    tagChanged2.clear();

    db->transaction();
    QVERIFY(db->deleteTag(colorsTag));
    QVERIFY(db->commit());

    QCOMPARE(tagChanged2.count(), 1);
    QCOMPARE(tagChanged2.at(0).at(0).value<Tag>(),
             db->uncategorizedTag());
    QCOMPARE(db->uncategorizedTag().isCategory(), false);
}

void DatabaseTest::testDeleteTag()
{
    QTemporaryDir tmpDir;
    qputenv("XDG_CONFIG_HOME", tmpDir.path().toUtf8());

    QScopedPointer<Database> db(Database::instance());
    QVERIFY(db);

    QSignalSpy tagDeleted(db.data(), SIGNAL(tagDeleted(Tag)));

    int defaultTagCount = db->allTags().count();

    db->transaction();

    // Create a few tags
    Tag whiteTag = db->createTag(Tag(), "white", "icon-white");
    Tag blueTag = db->createTag(Tag(), "blue", "icon-blue");
    Tag redTag = db->createTag(Tag(), "red", "icon-red");
    Tag greenTag = db->createTag(Tag(), "green", "icon-green");

    /* Set them on a photo */
    Database::Photo photo;
    photo.setBaseUri("/tmp");
    photo.setFileName("foo.jpg");
    photo.setDescription("");
    photo.setTime(QDateTime::fromString("1997-07-16T19:20:30",
                                        Qt::ISODate));
    PhotoId id = db->addPhoto(photo, 1);
    QVERIFY(id >= 0);

    // Set initial tags
    bool ok = db->setTags(id, QStringList() << "blue" << "red" << "green");
    QVERIFY(ok);

    ok = db->commit();

    // delete one tag
    db->transaction();
    ok = db->deleteTag(redTag);
    QVERIFY(ok);
    ok = db->commit();
    QVERIFY(ok);

    QCOMPARE(tagDeleted.count(), 1);
    QCOMPARE(tagDeleted.at(0).at(0).value<Tag>(), redTag);

    // verify that the others are still there
    QList<Tag> tags = db->allTags();
    QCOMPARE(tags.count(), defaultTagCount + 3);
    QVERIFY(tags.contains(whiteTag));
    QVERIFY(tags.contains(blueTag));
    QVERIFY(tags.contains(greenTag));

    // verify that the picture has lost that tag
    photo = db->photo(id);
    tags = db->tags(id);
    QCOMPARE(tags.count(), 2);
    QVERIFY(tags.contains(blueTag));
    QVERIFY(tags.contains(greenTag));

    /* We know that tags are cached; let's now try to reopen that DB, and
     * verify that the deleted tag is no longer there */

    db.reset();
    db.reset(Database::instance());
    QVERIFY(db);

    // verify that they are there
    tags = db->allTags();
    QCOMPARE(tags.count(), defaultTagCount + 3);
    QVERIFY(tags.contains(whiteTag));
    QVERIFY(tags.contains(blueTag));
    QVERIFY(!tags.contains(redTag));
    QVERIFY(tags.contains(greenTag));
}

void DatabaseTest::testSetDescription()
{
    QTemporaryDir tmpDir;
    qputenv("XDG_CONFIG_HOME", tmpDir.path().toUtf8());

    Database::Photo photo;
    photo.setBaseUri("file:///tmp");
    photo.setDescription("A foo");
    photo.setFileName("foo.jpg");

    QScopedPointer<Database> db(Database::instance());
    QVERIFY(db);

    PhotoId id = db->addPhoto(photo, 1);
    QVERIFY(id >= 0);

    Database::Photo p = db->photo(id);
    QCOMPARE(p.description(), QString("A foo"));

    // Change the description
    db->setDescription(id, "The real foo");

    // Tries with and without the cache
    for (int i = 0; i < 2; i++, db->clearCache()) {
        Database::Photo p = db->photo(id);
        QVERIFY(p.isValid());

        QCOMPARE(p.description(), QString("The real foo"));
    }
}

void DatabaseTest::testSetRating()
{
    QTemporaryDir tmpDir;
    qputenv("XDG_CONFIG_HOME", tmpDir.path().toUtf8());

    Database::Photo photo;
    photo.setBaseUri("file:///tmp");
    photo.setDescription("A foo");
    photo.setFileName("foo.jpg");
    photo.setRating(1);

    QScopedPointer<Database> db(Database::instance());
    QVERIFY(db);

    PhotoId id = db->addPhoto(photo, 1);
    QVERIFY(id >= 0);

    Database::Photo p = db->photo(id);
    QCOMPARE(p.rating(), 1);

    // Change the rating
    db->setRating(id, 4);

    // Tries with and without the cache
    for (int i = 0; i < 2; i++, db->clearCache()) {
        Database::Photo p = db->photo(id);
        QVERIFY(p.isValid());

        QCOMPARE(p.rating(), 4);
    }
}

void DatabaseTest::testSetLocation()
{
    QTemporaryDir tmpDir;
    qputenv("XDG_CONFIG_HOME", tmpDir.path().toUtf8());

    Database::Photo photo;
    photo.setBaseUri("file:///tmp");
    photo.setDescription("A foo");
    photo.setFileName("foo.jpg");
    GeoPoint loc(60.54321, -20.12345);
    photo.setLocation(loc);

    QScopedPointer<Database> db(Database::instance());
    QVERIFY(db);

    PhotoId id = db->addPhoto(photo, 1);
    QVERIFY(id >= 0);

    Database::Photo p = db->photo(id);
    /* Compare the string representation to take fuzziness into account */
    QCOMPARE(QTest::toString(p.location()), QTest::toString(loc));

    // Change the location
    GeoPoint newLoc(-10.98765, 40.76543);
    db->setLocation(id, newLoc);

    // Tries with and without the cache
    for (int i = 0; i < 2; i++, db->clearCache()) {
        Database::Photo p = db->photo(id);
        QVERIFY(p.isValid());

        QCOMPARE(QTest::toString(p.location()), QTest::toString(newLoc));
    }

    // Clear the location
    db->setLocation(id, GeoPoint());
    p = db->photo(id);
    QVERIFY(p.isValid());
    QCOMPARE(p.location(), GeoPoint());
}

void DatabaseTest::testTagArea()
{
    QTemporaryDir tmpDir;
    qputenv("XDG_CONFIG_HOME", tmpDir.path().toUtf8());

    /* Populate the DB with some photos */
    Database::Photo photo;
    photo.setBaseUri("/tmp");
    photo.setDescription("");

    QScopedPointer<Database> db(Database::instance());
    QVERIFY(db);

    QSignalSpy photoTagAreasChanged(db.data(), SIGNAL(photoTagAreasChanged(PhotoId)));

    db->transaction();

    // Create a few tags
    for (int i = 1; i <= 10; i++) {
        QString tagName = QString("t%1").arg(i);
        Tag tag = db->createTag(Tag(), tagName, QString());
        QVERIFY(tag.isValid());
    }

    PhotoId firstId = -1;
    for (int i = 0; i < 5; i++) {
        photo.setFileName(QString("foo%1.jpg").arg(i));
        PhotoId id = db->addPhoto(photo, 1);
        if (i == 0) firstId = id;
    }

    db->commit();

    /* Add some tags to the images */
    db->transaction();
    Tag t1 = db->tag("t1");
    Tag t2 = db->tag("t2");
    bool ok = db->addTags(firstId + 2, QList<Tag>() << t1);
    QVERIFY(ok);
    ok = db->addTags(firstId + 4, QList<Tag>() << t2 << t1);
    QVERIFY(ok);
    db->commit();

    /* Check that there are no areas defined */
    for (int i = 0; i < 5; i++) {
        QVERIFY(db->tagAreas(firstId + i).isEmpty());
    }
    QCOMPARE(photoTagAreasChanged.count(), 0);

    /* Set the area on a photo */
    QRectF r1 = QRectF(0.2, 0.3, 0.5, 0.4);
    ok = db->addTagWithArea(firstId + 3, t2, r1);
    QVERIFY(ok);
    QCOMPARE(photoTagAreasChanged.count(), 1);
    QRectF r2 = QRectF(0.2, 0.1, 0.3, 0.43);
    ok = db->addTagWithArea(firstId + 4, t1, r2);
    QVERIFY(ok);
    QCOMPARE(photoTagAreasChanged.count(), 2);
    photoTagAreasChanged.clear();

    /* Check that areas are as expected */
    QVERIFY(db->tagAreas(firstId + 2).isEmpty());
    QCOMPARE(db->tagAreas(firstId + 3),
             QVector<TagArea>() << TagArea(r1, t2.id()));
    QCOMPARE(db->tagAreas(firstId + 4),
             QVector<TagArea>() << TagArea(r2, t1.id()));

    /* Alter some tag areas */
    ok = db->addTagWithArea(firstId + 4, t2, r1);
    QVERIFY(ok);
    ok = db->addTagWithArea(firstId + 3, t2, QRectF());
    QVERIFY(ok);
    QCOMPARE(photoTagAreasChanged.count(), 2);

    /* Check that areas are as expected */
    QVERIFY(db->tagAreas(firstId + 3).isEmpty());
    QCOMPARE(db->tagAreas(firstId + 4),
             QVector<TagArea>() << TagArea(r2, t1.id()) << TagArea(r1, t2.id()));
}

void DatabaseTest::testTagPopularity_data()
{
    QTest::addColumn<QStringList>("tags");
    QTest::addColumn<QList<int> >("addCount");
    QTest::addColumn<QList<float> >("expectedPopularity");

    QTest::newRow("all tags once") <<
        (QStringList() << "t1" << "t2" << "t3" << "t4" <<
         "t5" << "t6" << "t7" << "t8" <<
         "t9" << "t10" << "t11" << "t12" <<
         "t13" << "t14" << "t15" << "t16" <<
         "t17" << "t18" << "t19" << "t20") <<
        (QList<int>() << 1 << 1 << 1 << 1 <<
         1 << 1 << 1 << 1 <<
         1 << 1 << 1 << 1 <<
         1 << 1 << 1 << 1 <<
         1 << 1 << 1 << 1) <<
        (QList<float>() << 1 << 1 << 1 << 1 <<
         1 << 1 << 1 << 1 <<
         1 << 1 << 1 << 1 <<
         1 << 1 << 1 << 1 <<
         1 << 1 << 1 << 1);

    QTest::newRow("only one tag used") <<
        (QStringList() << "t1" << "t2" << "t3" << "t4") <<
        (QList<int>() << 40 << 0 << 0 << 0) <<
        (QList<float>() << 1 << 0 << 0 << 0);

    QTest::newRow("various usages") <<
        (QStringList() << "t1" << "t2" << "t3" << "t4" <<
         "t5" << "t6" << "t7" << "t8" <<
         "t9" << "t10" << "t11" << "t12") <<
        (QList<int>() << 40 << 50 << 10 << 20 <<
         18 << 11 << 0 << 1 <<
         4 << 3 << 0 << 49) <<
        (QList<float>() << 0.94449 << 1 << 0.60987 << 0.77433 <<
         0.74887 << 0.632 << 0 << 0.17629 <<
         0.40934 << 0.35258 << 0 << 0.99496);
}

void DatabaseTest::testTagPopularity()
{
    QTemporaryDir tmpDir;
    qputenv("XDG_CONFIG_HOME", tmpDir.path().toUtf8());

    QFETCH(QStringList, tags);
    QFETCH(QList<int>, addCount);
    QFETCH(QList<float>, expectedPopularity);

    /* Populate the DB with some photos */
    Database::Photo photo;
    photo.setBaseUri("/tmp");
    photo.setDescription("");

    QScopedPointer<Database> db(Database::instance());
    QVERIFY(db);

    db->transaction();

    // Create a few tags
    for (int i = 1; i <= 20; i++) {
        QString tagName = QString("t%1").arg(i);
        Tag tag = db->createTag(Tag(), tagName, QString());
        QVERIFY(tag.isValid());
    }

    PhotoId firstId = -1;
    for (int i = 0; i < 50; i++) {
        photo.setFileName(QString("foo%1.jpg").arg(i));
        PhotoId id = db->addPhoto(photo, 1);
        if (i == 0) firstId = id;
    }

    db->commit();

    /* Add the tags to the images */
    db->transaction();
    for (int i = 0; i < tags.count(); i++) {
        Tag tag = db->tag(tags[i]);
        QList<Tag> tagAsList = QList<Tag>() << tag;
        for (int j = 0; j < addCount[i]; j++) {
            bool ok = db->addTags(firstId + j, tagAsList);
            QVERIFY(ok);
        }
    }
    db->commit();

    /* Iterate the main loop so that tag popularity gets updated */
    QTest::qWait(5);

    for (int i = 0; i < tags.count(); i++) {
        Tag tag = db->tag(tags[i]);
        QCOMPARE(tag.popularity() + 1, expectedPopularity[i] + 1);
    }

    /* Now destroy the DB, reinstantiate it, and check that the popularity is
     * still correct */
    db.reset();
    db.reset(Database::instance());
    for (int i = 0; i < tags.count(); i++) {
        Tag tag = db->tag(tags[i]);
        QCOMPARE(tag.popularity() + 1, expectedPopularity[i] + 1);
    }
}

void DatabaseTest::testTagUsageCount_data()
{
    QTest::addColumn<QString>("countedTag");
    QTest::addColumn<QList<int> >("photoIds");
    QTest::addColumn<int>("expectedUsageCount");

    QTest::newRow("unused tag, no photos") <<
        "t0" <<
        QList<int>() <<
        0;

    QTest::newRow("unused tag, some photos") <<
        "t0" <<
        (QList<int>() << 0 << 4 << 12) <<
        0;

    QTest::newRow("tag used once, no photos") <<
        "t1" <<
        QList<int>() <<
        0;

    QTest::newRow("tag used once, other photos") <<
        "t1" <<
        (QList<int>() << 2 << 6 << 17) <<
        0;

    QTest::newRow("tag used once, its photo") <<
        "t1" <<
        (QList<int>() << 18) <<
        1;

    QTest::newRow("tag used once, its photo and others") <<
        "t1" <<
        (QList<int>() << 0 << 3 << 18) <<
        1;

    QTest::newRow("tag used always, no photos") <<
        "t19" <<
        QList<int>() <<
        0;

    QTest::newRow("tag used always, some photos") <<
        "t19" <<
        (QList<int>() << 0 << 3 << 18) <<
        3;
}

void DatabaseTest::testTagUsageCount()
{
    QTemporaryDir tmpDir;
    qputenv("XDG_CONFIG_HOME", tmpDir.path().toUtf8());

    QFETCH(QString, countedTag);
    QFETCH(QList<int>, photoIds);
    QFETCH(int, expectedUsageCount);

    /* Populate the DB with some photos */
    Database::Photo photo;
    photo.setBaseUri("/tmp");
    photo.setDescription("");

    QScopedPointer<Database> db(Database::instance());
    QVERIFY(db);

    db->transaction();

    // Create a few tags
    for (int i = 0; i < 20; i++) {
        QString tagName = QString("t%1").arg(i);
        Tag tag = db->createTag(Tag(), tagName, QString());
        QVERIFY(tag.isValid());
    }

    PhotoId firstId = -1;
    QList<Tag> tags;
    for (int i = 0; i < 19; i++) {
        photo.setFileName(QString("foo%1.jpg").arg(i));
        PhotoId id = db->addPhoto(photo, 1);
        if (i == 0) firstId = id;

        /* To each photo, add the last i tags, so that tag t0 will be used 0
         * times, and t19 will be used 19 times. */
        QString tagName = QString("t%1").arg(19 - i);
        tags.append(db->tag(tagName));
        bool ok = db->addTags(id, tags);
        QVERIFY(ok);
    }

    db->commit();

    Tag tag = db->tag(countedTag);
    QList<PhotoId> ids;
    Q_FOREACH(int id, photoIds) {
        ids.append(id + firstId);
    }

    QCOMPARE(db->computeUsageCount(tag, ids), expectedUsageCount);
}

void DatabaseTest::testTagParentLoop()
{
    QTemporaryDir tmpDir;
    qputenv("XDG_CONFIG_HOME", tmpDir.path().toUtf8());

    QScopedPointer<Database> db(Database::instance());
    QVERIFY(db);

    // Create a few tags
    db->transaction();
    QStringList allTags { "one", "two", "three" };
    for (const QString &tagName: allTags) {
        Tag tag = db->createTag(Tag(), tagName, QString());
        QVERIFY(tag.isValid());
    }
    bool ok = db->commit();
    QVERIFY(ok);

    Tag one = db->tag("one");
    Tag two = db->tag("two");
    Tag three = db->tag("three");

    db->transaction();

    Database::TagUpdate data;

    data.parent = QVariant::fromValue(one);
    ok = db->updateTag(two, data);
    QVERIFY(ok);

    data.parent = QVariant::fromValue(two);
    ok = db->updateTag(three, data);
    QVERIFY(ok);

    /* Now try setting "three" as parent for "one", therefore
     * creating a loop; this should fail. */
    data.parent = QVariant::fromValue(three);
    ok = db->updateTag(one, data);
    QVERIFY(!ok);

    db->rollback();
}

void DatabaseTest::testDefaultTags()
{
    QTemporaryDir tmpDir;
    qputenv("XDG_CONFIG_HOME", tmpDir.path().toUtf8());

    QScopedPointer<Database> db(Database::instance());
    QVERIFY(db);

    // Check that default tags are there
    Tag tag = db->uncategorizedTag();
    QCOMPARE(tag.name(), QString("Uncategorized"));
    QVERIFY(!tag.isCategory());
    tag = db->peopleTag();
    QCOMPARE(tag.name(), QString("People"));
    QVERIFY(!tag.isCategory());
    tag = db->placesTag();
    QCOMPARE(tag.name(), QString("Places"));
    QVERIFY(!tag.isCategory());
    tag = db->eventsTag();
    QCOMPARE(tag.name(), QString("Events"));
    QVERIFY(!tag.isCategory());
    tag = db->importedTagsTag();
    QCOMPARE(tag.name(), QString("Imported tags"));
    QVERIFY(!tag.isCategory());

    /* Now that tags have been written, test loading them */
    db.reset();
    db.reset(Database::instance());
    tag = db->uncategorizedTag();
    QCOMPARE(tag.name(), QString("Uncategorized"));
    QVERIFY(!tag.isCategory());
}

void DatabaseTest::testUpdate_data()
{
    QTest::addColumn<QString>("dumpFile");

    QTest::newRow("no DB") <<
        QString();

    QTest::newRow("Version 1") <<
        "version1.sql";
}

void DatabaseTest::testUpdate()
{
    QTemporaryDir tmpDir;
    qputenv("XDG_CONFIG_HOME", tmpDir.path().toUtf8());

    QFETCH(QString, dumpFile);

    if (!dumpFile.isEmpty()) {
        /* Directly open/create the SQL DB file */
        QString dirName =
            QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) +
            "/" + QCoreApplication::applicationName();
        QDir dir(dirName);
        dir.mkpath(".");

        QStringList args;
        args << dir.absoluteFilePath("photos.db");
        QProcess sqlite3;
        sqlite3.setStandardInputFile(QString(DATA_DIR) + "/" + dumpFile);
        sqlite3.start("sqlite3", args);
        QVERIFY(sqlite3.waitForStarted());
        QVERIFY(sqlite3.waitForFinished());
    }

    QScopedPointer<Database> db(Database::instance());
    QVERIFY(db);

    /* Just do a couple of writes and reads to make sure that the DB is not
     * totally broken. */
    QString baseUri("file:///my/path");
    QString fileName("OneImage.jpg");
    QString description("One nice image");
    QDateTime time = QDateTime::fromString("Tuesday, 23 April 12 22:51:41");
    int rating = 4;
    GeoPoint location(60, 25);

    Database::Photo photo;
    photo.setBaseUri(baseUri);
    photo.setFileName(fileName);
    photo.setDescription(description);
    photo.setTime(time);
    photo.setRating(rating);
    photo.setLocation(location);

    RollId rollId = db->createRoll();
    QVERIFY(rollId > 0);
    PhotoId id = db->addPhoto(photo, rollId);
    QVERIFY(id >= 0);

    Tag tag = db->createTag(db->peopleTag(), "John", "John.jpg");
    QVERIFY(tag.isValid());

    QList<Tag> tags;
    tags << tag;
    QVERIFY(db->addTags(id, tags));

    db->clearCache();

    /* Read the data back and compare */
    photo = db->photo(id);
    QVERIFY(photo.isValid());
    QCOMPARE(photo.baseUri(), baseUri);
    QCOMPARE(photo.fileName(), fileName);
    QCOMPARE(photo.description(), description);
    QCOMPARE(photo.time(), time);
    QCOMPARE(photo.rating(), rating);
    QCOMPARE(photo.baseUri(), baseUri);
    QCOMPARE(photo.rollId(), rollId);
    QCOMPARE(photo.id(), id);
    QCOMPARE(photo.location(), location);

    QCOMPARE(db->tags(id), tags);
}

void DatabaseTest::testPhotoVersion_data()
{
    QTest::addColumn<bool>("clearCache");

    QTest::newRow("without clearing cache") <<
        false;

    QTest::newRow("Clearing cache") <<
        true;
}

void DatabaseTest::testPhotoVersion()
{
    QTemporaryDir tmpDir;
    qputenv("XDG_CONFIG_HOME", tmpDir.path().toUtf8());

    QFETCH(bool, clearCache);

    QScopedPointer<Database> db(Database::instance());
    QVERIFY(db);

    db->transaction();

    Database::Photo photo;
    photo.setBaseUri("file:///tmp");
    for (int i = 0; i < 6; i++) {
        QString fileName = QString("foo%1.jpg").arg(i);
        photo.setFileName(fileName);
        PhotoId id = db->addPhoto(photo, 1);
        QVERIFY(id >= 0);
    }

    QVERIFY(db->commit());

    if (clearCache) db->clearCache();

    for (int i = 0; i < 6; i++) {
        photo = db->photo(1 + i);
        QVERIFY(photo.isValid());
        QCOMPARE(photo.isDefaultVersion(), false);
        QCOMPARE(photo.isNonDefaultVersion(), false);
        QCOMPARE(db->masterVersion(1 + i), 1 + i);
    }

    if (clearCache) db->clearCache();

    /* Create a couple of versions */
    db->transaction();
    QVERIFY(db->makeVersion(2, 1));
    QVERIFY(db->makeVersion(6, 1));
    QVERIFY(db->makeVersion(3, 5));
    QVERIFY(db->commit());

    if (clearCache) db->clearCache();

    QCOMPARE(db->photo(1).isDefaultVersion(), true);
    QCOMPARE(db->photo(1).isNonDefaultVersion(), false);
    QCOMPARE(db->masterVersion(1), 1);
    QCOMPARE(db->photo(2).isDefaultVersion(), false);
    QCOMPARE(db->photo(2).isNonDefaultVersion(), true);
    QCOMPARE(db->masterVersion(2), 1);
    QCOMPARE(db->photo(3).isDefaultVersion(), false);
    QCOMPARE(db->photo(3).isNonDefaultVersion(), true);
    QCOMPARE(db->masterVersion(3), 5);
    QCOMPARE(db->photo(4).isDefaultVersion(), false);
    QCOMPARE(db->photo(4).isNonDefaultVersion(), false);
    QCOMPARE(db->masterVersion(4), 4);
    QCOMPARE(db->photo(5).isDefaultVersion(), true);
    QCOMPARE(db->photo(5).isNonDefaultVersion(), false);
    QCOMPARE(db->masterVersion(5), 5);
    QCOMPARE(db->photo(6).isDefaultVersion(), false);
    QCOMPARE(db->photo(6).isNonDefaultVersion(), true);
    QCOMPARE(db->masterVersion(6), 1);

    /* Check the photoVersions() method */
    QCOMPARE(db->photoVersions(4).count(), 0);
    QList<Database::Photo> photos = db->photoVersions(3);
    QCOMPARE(photos.count(), 2);
    QCOMPARE(photos.at(0).id(), 5);
    QCOMPARE(photos.at(1).id(), 3);
    photos = db->photoVersions(1);
    QCOMPARE(photos.count(), 3);
    QCOMPARE(photos.at(0).id(), 1);
    QList<int> ids;
    ids << photos.at(1).id() << photos.at(2).id();
    QCOMPARE(ids.toSet(), QSet<int>() << 2 << 6);


    /* Test version removal */
    if (clearCache) db->clearCache();
    QVERIFY(db->makeVersion(2, 0));

    if (clearCache) db->clearCache();

    QCOMPARE(db->photo(1).isDefaultVersion(), true);
    QCOMPARE(db->photo(1).isNonDefaultVersion(), false);
    QCOMPARE(db->photo(2).isDefaultVersion(), false);
    QCOMPARE(db->photo(2).isNonDefaultVersion(), false);
    QCOMPARE(db->photo(3).isDefaultVersion(), false);
    QCOMPARE(db->photo(3).isNonDefaultVersion(), true);
    QCOMPARE(db->photo(4).isDefaultVersion(), false);
    QCOMPARE(db->photo(4).isNonDefaultVersion(), false);
    QCOMPARE(db->photo(5).isDefaultVersion(), true);
    QCOMPARE(db->photo(5).isNonDefaultVersion(), false);
    QCOMPARE(db->photo(6).isDefaultVersion(), false);
    QCOMPARE(db->photo(6).isNonDefaultVersion(), true);

    if (clearCache) db->clearCache();
    QVERIFY(db->makeVersion(6, 0));

    if (clearCache) db->clearCache();

    QCOMPARE(db->photo(1).isDefaultVersion(), false);
    QCOMPARE(db->photo(1).isNonDefaultVersion(), false);
    QCOMPARE(db->photo(2).isDefaultVersion(), false);
    QCOMPARE(db->photo(2).isNonDefaultVersion(), false);
    QCOMPARE(db->photo(3).isDefaultVersion(), false);
    QCOMPARE(db->photo(3).isNonDefaultVersion(), true);
    QCOMPARE(db->photo(4).isDefaultVersion(), false);
    QCOMPARE(db->photo(4).isNonDefaultVersion(), false);
    QCOMPARE(db->photo(5).isDefaultVersion(), true);
    QCOMPARE(db->photo(5).isNonDefaultVersion(), false);
    QCOMPARE(db->photo(6).isDefaultVersion(), false);
    QCOMPARE(db->photo(6).isNonDefaultVersion(), false);
}

void DatabaseTest::testRolls()
{
    QTemporaryDir tmpDir;
    qputenv("XDG_CONFIG_HOME", tmpDir.path().toUtf8());

    QScopedPointer<Database> db(Database::instance());
    QVERIFY(db);

    // Non existing roll
    Roll roll(23);
    QVERIFY(roll.isValid());
    QCOMPARE(roll.time(), QDateTime());
    QVERIFY(db->rolls(12).isEmpty());

    // Create a roll
    QSignalSpy rollAdded(db.data(), SIGNAL(rollAdded(Roll)));
    QDateTime time = QDateTime::fromString("1997-07-16T19:20:30",
                                           Qt::ISODate);
    RollId rollId = db->createRoll(time);
    roll = Roll(rollId);
    QVERIFY(roll.isValid());
    QCOMPARE(roll.time(), time);
    QCOMPARE(rollAdded.count(), 1);
    QCOMPARE(rollAdded.at(0).at(0).value<Roll>().id(), rollId);

    QCOMPARE(db->rolls(4), QList<Roll>{roll});

    // Create a few more rolls
    QList<QDateTime> expectedTimes;
    db->transaction();
    for (int i = 0; i < 10; i++) {
        time = time.addMonths(1);
        db->createRoll(time);
        expectedTimes.prepend(time);
    }
    db->commit();
    QList<Roll> rolls = db->rolls(10);
    QList<QDateTime> times;
    Q_FOREACH(const Roll &roll, rolls) {
        times.append(roll.time());
    }

    QCOMPARE(times, expectedTimes);

    // Reopen the DB, to test rolls loading
    db.reset();
    db.reset(Database::instance());
    QVERIFY(db);

    times.clear();
    rolls = db->rolls(10);
    Q_FOREACH(const Roll &roll, rolls) {
        times.append(roll.time());
    }

    QCOMPARE(times, expectedTimes);
}

QTEST_GUILESS_MAIN(DatabaseTest)

#include "tst_database.moc"
