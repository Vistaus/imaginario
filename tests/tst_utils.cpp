/*
 * Copyright (C) 2016-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "utils.h"

#include <QJsonObject>
#include <QQmlComponent>
#include <QQmlEngine>
#include <QScopedPointer>
#include <QSignalSpy>
#include <QTemporaryDir>
#include <QTest>

using namespace Imaginario;

class UtilsTest: public QObject
{
    Q_OBJECT

public:
    UtilsTest();

private Q_SLOTS:
    void testFindFiles_data();
    void testFindFiles();
    void testFindFilesAsync();
    void testParseListTag_data();
    void testParseListTag();
    void testUrlForArea_data();
    void testUrlForArea();
    void testAreaFromUrl_data();
    void testAreaFromUrl();
    void testScaleArea_data();
    void testScaleArea();

private:
    QDir m_dataDir;
};

UtilsTest::UtilsTest():
    m_dataDir(QString(ITEMS_DIR "/"))
{
}

void UtilsTest::testFindFiles_data()
{
    QTest::addColumn<bool>("recursive");
    QTest::addColumn<QStringList>("expectedFiles");

    QTest::newRow("non recursive") <<
        false <<
        QStringList { "a.jpg", "b.jpg", "c.jpg", "z.jpg" };

    QTest::newRow("recursive") <<
        true <<
        QStringList { "a.jpg", "b.jpg", "c.jpg",
            "subdir/1.jpg",
            "subdir/2.jpg",
            "subdir/3.jpg",
            "z.jpg"
        };
}

void UtilsTest::testFindFiles()
{
    QFETCH(bool, recursive);
    QFETCH(QStringList, expectedFiles);

    QTemporaryDir tmpDir;
    QDir dest(tmpDir.path());

    QString fileName = m_dataDir.filePath("image0.png");
    QFile::copy(fileName, dest.filePath("a.jpg"));
    QFile::copy(fileName, dest.filePath("b.jpg"));
    QFile::copy(fileName, dest.filePath("c.jpg"));
    QFile::copy(fileName, dest.filePath("z.jpg"));
    dest.mkdir("subdir");
    QFile::copy(fileName, dest.filePath("subdir/1.jpg"));
    QFile::copy(fileName, dest.filePath("subdir/2.jpg"));
    QFile::copy(fileName, dest.filePath("subdir/3.jpg"));

    Utils utils;
    auto result =
        utils.findFiles(QUrl::fromLocalFile(tmpDir.path()), recursive);

    QStringList files;
    for (const QUrl &url: result) {
        files.append(dest.relativeFilePath(url.toLocalFile()));
    }

    QCOMPARE(files, expectedFiles);
}

void UtilsTest::testFindFilesAsync()
{
    QTemporaryDir tmpDir;
    QDir dest(tmpDir.path());

    QString fileName = m_dataDir.filePath("image0.png");
    QFile::copy(fileName, dest.filePath("a.jpg"));
    QFile::copy(fileName, dest.filePath("b.jpg"));
    QFile::copy(fileName, dest.filePath("c.jpg"));
    QFile::copy(fileName, dest.filePath("z.jpg"));
    dest.mkdir("subdir");
    QFile::copy(fileName, dest.filePath("subdir/1.jpg"));
    QFile::copy(fileName, dest.filePath("subdir/2.jpg"));
    QFile::copy(fileName, dest.filePath("subdir/3.jpg"));

    QStringList expectedFiles {
        "a.jpg", "b.jpg", "c.jpg",
        "subdir/1.jpg",
        "subdir/2.jpg",
        "subdir/3.jpg",
        "z.jpg",
    };

    QQmlEngine engine;
    qmlRegisterType<Utils>("MyTest", 1, 0, "Utils");
    QQmlComponent component(&engine);
    component.setData(R"(
        import MyTest 1.0
        Utils {
          id: root
          signal done(var files)
          function run(folder) {
            findFiles(folder, true, function(files) {
              root.done(files)
            })
          }
        })",
        QUrl());
    QScopedPointer<QObject> object(component.create());
    QVERIFY(object != 0);

    QSignalSpy done(object.data(), SIGNAL(done(QVariant)));

    QVariant folder(QUrl::fromLocalFile(tmpDir.path()));
    bool ok = QMetaObject::invokeMethod(object.data(), "run",
                                        Q_ARG(QVariant, folder));
    QVERIFY(ok);

    QTRY_COMPARE(done.count(), 1);
    QStringList files;
    QList<QUrl> urlList = done.at(0).at(0).value<QList<QUrl>>();
    for (const QUrl &url: urlList) {
        files.append(dest.relativeFilePath(url.toLocalFile()));
    }
    QCOMPARE(files, expectedFiles);

    QTest::qWait(10); // Wait for QFuture destruction
}

void UtilsTest::testParseListTag_data()
{
    typedef QList<int> TagList;
    typedef QList<QList<int>> TagList2;
    QTest::addColumn<QVariant>("input");
    QTest::addColumn<TagList2>("expectedTags");

    QTest::newRow("empty") <<
        QVariant() <<
        TagList2();

    QTest::newRow("single tag") <<
        QVariant(4) <<
        TagList2 { TagList { 4 }};

    QTest::newRow("tag list") <<
        QVariant::fromValue(TagList { 4, 2, 7, 12 }) <<
        TagList2 { TagList { 4, 2, 7, 12 }};

    QTest::newRow("list of lists") <<
        QVariant::fromValue(TagList2{
                            TagList { 4, 2, 7, 12 },
                            TagList { 6 },
                            TagList { 1, 5 }
                            }) <<
        TagList2 {
            TagList { 4, 2, 7, 12 },
            TagList { 6 },
            TagList { 1, 5 }
        };
}

void UtilsTest::testParseListTag()
{
    typedef QList<int> TagList;
    typedef QList<QList<int>> TagList2;

    QFETCH(QVariant, input);
    QFETCH(TagList2, expectedTags);

    bool ok = true;
    QList<QList<Tag>> result = parseList<QList<Tag>>(input, &ok);
    QVERIFY(ok);

    TagList2 tags;
    Q_FOREACH(const QList<Tag> &tl, result) {
        TagList intList;
        Q_FOREACH(Tag t, tl) {
            intList.append(t.id());
        }
        tags.append(intList);
    }
    QCOMPARE(tags, expectedTags);
}

void UtilsTest::testUrlForArea_data()
{
    QTest::addColumn<QUrl>("inputUrl");
    QTest::addColumn<QJsonObject>("area");
    QTest::addColumn<QUrl>("expectedUrl");

    QTest::newRow("invalid area") <<
        QUrl("file:///my-file.png") <<
        QJsonObject {} <<
        QUrl("file:///my-file.png");

    QTest::newRow("incomplete area") <<
        QUrl("file:///my-file2.png") <<
        QJsonObject {
            { "width", 0.4 },
            { "height", 0.5 },
        } <<
        QUrl("file:///my-file2.png");

    QTest::newRow("full area") <<
        QUrl("file:///my-file3.png") <<
        QJsonObject {
            { "x", 0.1 },
            { "y", 0.2 },
            { "width", 0.4 },
            { "height", 0.5 },
        } <<
        QUrl("file:///my-file3.png?x=0.1&y=0.2&width=0.4&height=0.5");
}

void UtilsTest::testUrlForArea()
{
    QFETCH(QUrl, inputUrl);
    QFETCH(QJsonObject, area);
    QFETCH(QUrl, expectedUrl);

    Utils utils;
    QCOMPARE(utils.urlForArea(inputUrl, area), expectedUrl);
}

void UtilsTest::testAreaFromUrl_data()
{
    QTest::addColumn<QUrl>("url");
    QTest::addColumn<QRectF>("expectedArea");

    QTest::newRow("missing query") <<
        QUrl("file:///my-file.png") <<
        QRectF();

    QTest::newRow("incomplete query") <<
        QUrl("file:///my-file2.png?x=0.1&z=0.2&width=0.4&height=0.5") <<
        QRectF();

    QTest::newRow("null width") <<
        QUrl("file:///my-file2.png?x=0.1&y=0.2&width=0&height=0.5") <<
        QRectF();

    QTest::newRow("null height") <<
        QUrl("file:///my-file2.png?x=0.1&y=0.2&width=0.3&height=0") <<
        QRectF();

    QTest::newRow("regular") <<
        QUrl("file:///my-file2.png?x=0.1&y=0.2&width=0.3&height=0.4") <<
        QRectF(0.1, 0.2, 0.3, 0.4);
}

void UtilsTest::testAreaFromUrl()
{
    QFETCH(QUrl, url);
    QFETCH(QRectF, expectedArea);

    QCOMPARE(Utils::areaFromUrl(url), expectedArea);
}

void UtilsTest::testScaleArea_data()
{
    QTest::addColumn<QRectF>("area");
    QTest::addColumn<QSize>("size");
    QTest::addColumn<QRect>("expectedRect");

    QTest::newRow("null size") <<
        QRectF(0.1, 0.2, 0.3, 0.4) <<
        QSize(200, 0) <<
        QRect(20, 0, 60, 0);

    QTest::newRow("regular") <<
        QRectF(0.1, 0.2, 0.3, 0.4) <<
        QSize(100, 200) <<
        QRect(10, 40, 30, 80);
}

void UtilsTest::testScaleArea()
{
    QFETCH(QRectF, area);
    QFETCH(QSize, size);
    QFETCH(QRect, expectedRect);

    QCOMPARE(Utils::scaleArea(area, size), expectedRect);
}

QTEST_GUILESS_MAIN(UtilsTest)

#include "tst_utils.moc"
