TARGET = tst_thumbnailer

include(tests.pri)

QT += \
    gui

SOURCES += \
    $${SRC_DIR}/thumbnailer.cpp \
    tst_thumbnailer.cpp

HEADERS += \
    $${SRC_DIR}/thumbnailer.h

ITEMS_DIR = $${TOP_SRC_DIR}/tests/data
DEFINES += \
    ITEMS_DIR=\\\"$${ITEMS_DIR}\\\"
