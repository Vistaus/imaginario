TARGET = tst_metadata

include(tests.pri)

QT += \
    concurrent

CONFIG += link_pkgconfig

PKGCONFIG += exiv2

SOURCES += \
    $${SRC_DIR}/metadata.cpp \
    tst_metadata.cpp

HEADERS += \
    $${SRC_DIR}/metadata.h \
    $${SRC_DIR}/types.h \
    test_utils.h

ITEMS_DIR = $${TOP_SRC_DIR}/tests/data
DEFINES += \
    ITEMS_DIR=\\\"$${ITEMS_DIR}\\\"
