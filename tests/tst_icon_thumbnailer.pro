TARGET = tst_icon_thumbnailer

include(tests.pri)

QT += \
    gui \
    qml

SOURCES += \
    $${SRC_DIR}/icon_thumbnailer.cpp \
    $${SRC_DIR}/utils.cpp \
    tst_icon_thumbnailer.cpp

HEADERS += \
    $${SRC_DIR}/icon_thumbnailer.h \
    $${SRC_DIR}/utils.h

ITEMS_DIR = $${TOP_SRC_DIR}/tests/data
DEFINES += \
    ITEMS_DIR=\\\"$${ITEMS_DIR}\\\"
