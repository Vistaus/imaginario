/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fake_job_executor.h"

#include <QDebug>

using namespace Imaginario;

static JobExecutor *m_instance = 0;

JobExecutor::JobExecutor(QObject *parent):
    QObject(parent),
    d_ptr(new JobExecutorPrivate(this))
{
    qRegisterMetaType<Job>();
    qRegisterMetaType<QVector<Job>>();
}

JobExecutor::~JobExecutor()
{
    delete d_ptr;
    m_instance = 0;
}

JobExecutor *JobExecutor::instance()
{
    if (!m_instance) {
        m_instance = new JobExecutor;
    }
    return m_instance;
}

void JobExecutor::setEmbedMetadata(bool embed)
{
    Q_D(JobExecutor);
    d->m_embed = embed;
}

bool JobExecutor::embedMetadata() const
{
    Q_D(const JobExecutor);
    return d->m_embed;
}

void JobExecutor::addJobs(const QVector<Job> &jobs) {
    Q_D(JobExecutor);
    Q_EMIT d->addJobsCalled(jobs);
}

void JobExecutor::start()
{
    Q_D(JobExecutor);
    Q_EMIT d->startCalled();
}

void JobExecutor::stop()
{
    Q_D(JobExecutor);
    Q_EMIT d->stopCalled();
}
