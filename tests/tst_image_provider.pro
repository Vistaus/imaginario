TARGET = tst_image_provider

include(tests.pri)

QT += \
    gui \
    quick

SOURCES += \
    $${SRC_DIR}/image_provider.cpp \
    $${SRC_DIR}/utils.cpp \
    fake_qimage_reader.cpp \
    tst_image_provider.cpp

HEADERS += \
    $${SRC_DIR}/image_provider.h \
    $${SRC_DIR}/job_executor.h \
    $${SRC_DIR}/metadata.h \
    $${SRC_DIR}/thumbnailer.h \
    $${SRC_DIR}/utils.h \
    fake_qimage_reader.h

ITEMS_DIR = $${TOP_SRC_DIR}/tests/data
DEFINES += \
    ITEMS_DIR=\\\"$${ITEMS_DIR}\\\"
