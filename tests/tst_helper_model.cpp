/*
 * Copyright (C) 2017-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "helper_model.h"

#include "fake_program_finder.h"
#include "test_utils.h"

#include <QJsonObject>
#include <QMetaObject>
#include <QMimeDatabase>
#include <QQmlComponent>
#include <QQmlEngine>
#include <QSignalSpy>
#include <QTemporaryDir>
#include <QTest>
#include <QVariantMap>

using namespace Imaginario;

class HelperModelTest: public QObject
{
    Q_OBJECT

public:
    static QVariant get(const QAbstractListModel *model, int row,
                        QString roleName);

private Q_SLOTS:
    void testProperties();
    void testRoles();
    void testMimeFilter_data();
    void testMimeFilter();
    void testExecution_data();
    void testExecution();
    void testCallback_data();
    void testCallback();
    void testFindPrograms_data();
    void testFindPrograms();
    void testCallableApplications();
};

QVariant HelperModelTest::get(const QAbstractListModel *model, int row,
                             QString roleName)
{
    QHash<int, QByteArray> roleNames = model->roleNames();

    int role = roleNames.key(roleName.toLatin1(), -1);
    return model->data(model->index(row), role);
}

void HelperModelTest::testProperties()
{
    HelperModel model;

    QCOMPARE(model.property("count").toInt(), 0);

    QJsonArray data {
        QJsonObject {
            { "actionName", "Edit with Gimp" },
            { "command", "gimp" },
            { "makeVersion", true },
        },
        QJsonObject {
            { "actionName", "Rotate CW" },
            { "command", "rotate" },
            { "makeVersion", false },
        },
    };
    model.setHelpersData(data);
    QCOMPARE(model.helpersData(), data);

    QVERIFY(!model.defaultHelpersData().isEmpty());

    model.classBegin();
    model.componentComplete();
    QCOMPARE(model.property("count").toInt(), 2);

    QVERIFY(!model.photoDir().isEmpty());
    model.setPhotoDir("/tmp/photos");
    QCOMPARE(model.photoDir(), QString("/tmp/photos"));
}

void HelperModelTest::testRoles()
{
    HelperModel model;

    QJsonArray data {
        QJsonObject {
            { "actionName", "Edit with Gimp" },
            { "command", "gimp" },
            { "makeVersion", true },
        },
        QJsonObject {
            { "actionName", "Rotate CW" },
            { "command", "rotate" },
            { "makeVersion", false },
        },
    };
    model.setHelpersData(data);

    model.classBegin();
    model.componentComplete();
    QCOMPARE(model.property("count").toInt(), 2);

    QCOMPARE(model.get(0, "command").toString(), QString("gimp"));
    QCOMPARE(model.get(1, "makeVersion").toBool(), false);
    QCOMPARE(get(&model, 0, "actionName").toString(), QString("Edit with Gimp"));
    QCOMPARE(get(&model, 1, "command").toString(), QString("rotate"));
    QModelIndex index = model.index(0, 0);
    QCOMPARE(index.data(HelperModel::MakeVersionRole).toBool(), true);
}

void HelperModelTest::testMimeFilter_data()
{
    QTest::addColumn<QJsonArray>("helpersData");
    QTest::addColumn<QString>("mimeType");
    QTest::addColumn<QStringList>("expectedActions");

    QTest::newRow("no mime") <<
        QJsonArray {
            QJsonObject {
                { "actionName", "Rotate 90" },
                { "command", "touch" },
                { "makeVersion", false },
            },
        } <<
        "" <<
        QStringList{ "Rotate 90" };

    QTest::newRow("no mime 2") <<
        QJsonArray {
            QJsonObject {
                { "actionName", "Rotate 90" },
                { "command", "touch" },
                { "makeVersion", false },
                { "mimeTypes", QJsonArray { "image/jpeg", "image/png" } },
            },
        } <<
        "" <<
        QStringList{ "Rotate 90" };

    QTest::newRow("no mime in command") <<
        QJsonArray {
            QJsonObject {
                { "actionName", "Rotate 90" },
                { "command", "touch" },
                { "makeVersion", false },
            },
        } <<
        "image/jpeg" <<
        QStringList{ "Rotate 90" };

    QTest::newRow("filter 1") <<
        QJsonArray {
            QJsonObject {
                { "actionName", "Rotate 90" },
                { "command", "touch" },
                { "makeVersion", true },
                { "mimeTypes", QJsonArray { "image/jpeg", "image/png" } },
            },
            QJsonObject {
                { "actionName", "Rotate 270" },
                { "command", "touch2" },
                { "makeVersion", true },
                { "mimeTypes", QJsonArray { "image/jpeg" } },
            },
            QJsonObject {
                { "actionName", "Do stuff" },
                { "command", "touch3" },
                { "makeVersion", true },
                { "mimeTypes", QJsonArray { "image/png", "text/plain" } },
            },
        } <<
        "image/png" <<
        QStringList{ "Rotate 90", "Do stuff" };
}

void HelperModelTest::testMimeFilter()
{
    QFETCH(QJsonArray, helpersData);
    QFETCH(QString, mimeType);
    QFETCH(QStringList, expectedActions);

    QMimeDatabase mimeDb;
    QMimeType type = mimeDb.mimeTypeForName(mimeType);

    HelperModel model;
    model.setHelpersData(helpersData);
    model.setMimeType(type);

    model.classBegin();
    model.componentComplete();

    QStringList actions;

    for (int i = 0; i < model.rowCount(); i++) {
        actions.append(model.get(i, "actionName").toString());
    }

    QCOMPARE(actions, expectedActions);
}

void HelperModelTest::testExecution_data()
{
    QTest::addColumn<QJsonArray>("helpersData");
    QTest::addColumn<int>("row");
    QTest::addColumn<QString>("fileName");
    QTest::addColumn<QString>("expectedFileName");
    QTest::addColumn<QStringList>("expectedWarnings");

    QTest::newRow("empty") <<
        QJsonArray{} <<
        0 <<
        "http://mardy.it/archivos/text.file" <<
        QString() <<
        QStringList{};

    QTest::newRow("no version, remote file") <<
        QJsonArray {
            QJsonObject {
                { "actionName", "Change timestamp" },
                { "command", "touch" },
                { "makeVersion", false },
            },
        } <<
        0 <<
        "http://mardy.it/archivos/text.file" <<
        "http://mardy.it/archivos/text.file" <<
        QStringList{};

    QTest::newRow("failed, remote file") <<
        QJsonArray {
            QJsonObject {
                { "actionName", "Change timestamp" },
                { "command", "touch" },
                { "makeVersion", true },
            },
        } <<
        0 <<
        "http://mardy.it/archivos/text.file" <<
        QString() <<
        QStringList{ "Versioning of non local files is currently unsupported" };

    QTest::newRow("failed, file copy") <<
        QJsonArray {
            QJsonObject {
                { "actionName", "Change timestamp" },
                { "command", "touch" },
                { "makeVersion", true },
            },
        } <<
        0 <<
        "file:///home/user/text.file" <<
        QString() <<
        QStringList{ "Could not copy file into new version: \"/home/user/text.file\"" };

    QTest::newRow("failed, remote with %f") <<
        QJsonArray {
            QJsonObject {
                { "actionName", "Change timestamp" },
                { "command", "touch %f" },
                { "makeVersion", false },
            },
        } <<
        0 <<
        "http://mardy.it/archivos/text.file" <<
        QString() <<
        QStringList{ "Passing of non local files is currently unsupported" };

    QTest::newRow("ok, local with %U") <<
        QJsonArray {
            QJsonObject {
                { "actionName", "Change timestamp" },
                { "command", "touch %U" },
                { "makeVersion", false },
            },
        } <<
        0 <<
        "file:///home/user/text.file" <<
        "file:///home/user/text.file" <<
        QStringList{};

    QTest::newRow("ok, versioning") <<
        QJsonArray {
            QJsonObject {
                { "actionName", "Change timestamp" },
                { "command", "touch" },
                { "makeVersion", true },
            },
        } <<
        0 <<
        "file://${TMPDIR}/image0.png" <<
        "${TMPDIR}/image0 (1).png" <<
        QStringList{};
}

void HelperModelTest::testExecution()
{
    QFETCH(QJsonArray, helpersData);
    QFETCH(int, row);
    QFETCH(QString, fileName);
    QFETCH(QString, expectedFileName);
    QFETCH(QStringList, expectedWarnings);

    // Prepare any needed files
    QTemporaryDir tmpDir;
    QFile::copy(QString(ITEMS_DIR) + "/image0.png",
                tmpDir.path() + "/image0.png");
    fileName = fileName.replace("${TMPDIR}", tmpDir.path());
    expectedFileName = expectedFileName.replace("${TMPDIR}", tmpDir.path());

    for (const QString &warning: expectedWarnings) {
        QTest::ignoreMessage(QtWarningMsg, warning.toUtf8().constData());
    }

    QQmlEngine engine;
    qmlRegisterType<HelperModel>("MyTest", 1, 0, "HelperModel");
    QQmlComponent component(&engine);
    component.setData("import MyTest 1.0\n"
                      "HelperModel {\n"
                      "  id: root\n"
                      "  signal done(var fileName, var extra)\n"
                      "  function run(row, file, userdata) {\n"
                      "    return runHelper(row, file, function(name) {\n"
                      "      root.done(name, userdata)\n"
                      "    })\n"
                      "  }\n"
                      "}",
                      QUrl());
    QObject *object = component.create();
    QVERIFY(object != 0);
    QAbstractListModel *model = qobject_cast<QAbstractListModel*>(object);
    QVERIFY(model != 0);

    model->setProperty("helpersData", helpersData);
    QTest::qWait(5);

    QSignalSpy done(model, SIGNAL(done(QVariant, QVariant)));

    QVariant arg(QString("a simple string"));
    QVariant rowVariant = row;
    QVariant fileNameVariant = QUrl(fileName);
    QVariant ret;// = false;
    bool ok = QMetaObject::invokeMethod(object, "run",
                                        Q_RETURN_ARG(QVariant, ret),
                                        Q_ARG(QVariant, rowVariant),
                                        Q_ARG(QVariant, fileNameVariant),
                                        Q_ARG(QVariant, arg));
    QVERIFY(ok);

    bool expectedSuccess = !expectedFileName.isEmpty();
    QCOMPARE(ret.toBool(), expectedSuccess);

    if (expectedSuccess) {
        done.wait();
        QCOMPARE(done.count(), 1);
        QCOMPARE(done.at(0).at(1), arg);
        QCOMPARE(done.at(0).at(0).toString(), expectedFileName);
    }
}

void HelperModelTest::testCallback_data()
{
    QTest::addColumn<QJsonArray>("helpersData");
    QTest::addColumn<QString>("fileName");
    QTest::addColumn<QString>("expectedFileName");
    QTest::addColumn<bool>("expectedMakeVersion");

    QTest::newRow("no version, remote file") <<
        QJsonArray {
            QJsonObject {
                { "actionName", "Change timestamp" },
                { "command", "touch" },
                { "makeVersion", false },
            },
        } <<
        "http://mardy.it/archivos/text.file" <<
        "http://mardy.it/archivos/text.file" <<
        false;

    QTest::newRow("ok, versioning") <<
        QJsonArray {
            QJsonObject {
                { "actionName", "Change timestamp" },
                { "command", "touch" },
                { "makeVersion", true },
            },
        } <<
        "file://${TMPDIR}/image0.png" <<
        "${TMPDIR}/image0 (1).png" <<
        true;
}

void HelperModelTest::testCallback()
{
    QFETCH(QJsonArray, helpersData);
    QFETCH(QString, fileName);
    QFETCH(QString, expectedFileName);
    QFETCH(bool, expectedMakeVersion);

    // Prepare any needed files
    QTemporaryDir tmpDir;
    QFile::copy(QString(ITEMS_DIR) + "/image0.png",
                tmpDir.path() + "/image0.png");
    fileName = fileName.replace("${TMPDIR}", tmpDir.path());
    expectedFileName = expectedFileName.replace("${TMPDIR}", tmpDir.path());

    QQmlEngine engine;
    qmlRegisterType<HelperModel>("MyTest", 1, 0, "HelperModel");
    QQmlComponent component(&engine);
    component.setData("import MyTest 1.0\n"
                      "HelperModel {\n"
                      "  id: root\n"
                      "  signal done(var args)\n"
                      "  function run(file) {\n"
                      "    return runHelper(0, file, function(name, makeVersion) {\n"
                      "      root.done(arguments)\n"
                      "    })\n"
                      "  }\n"
                      "}",
                      QUrl());
    QObject *object = component.create();
    QVERIFY(object != 0);
    QAbstractListModel *model = qobject_cast<QAbstractListModel*>(object);
    QVERIFY(model != 0);

    model->setProperty("helpersData", helpersData);
    QTest::qWait(5);

    QSignalSpy done(model, SIGNAL(done(QVariant)));

    QVariant fileNameVariant = QUrl(fileName);
    QVariant ret;// = false;
    bool ok = QMetaObject::invokeMethod(object, "run",
                                        Q_RETURN_ARG(QVariant, ret),
                                        Q_ARG(QVariant, fileNameVariant));
    QVERIFY(ok);

    QVERIFY(ret.toBool());

    done.wait();
    QCOMPARE(done.count(), 1);
    QJSValue arguments = done.at(0).at(0).value<QJSValue>();
    QCOMPARE(arguments.property(0).toString(), expectedFileName);
    QCOMPARE(arguments.property(1).toBool(), expectedMakeVersion);
}

void HelperModelTest::testFindPrograms_data()
{
    QTest::addColumn<ProgramFinderMocked::Responses>("finderResponses");
    QTest::addColumn<QString>("actionNameLookup");
    QTest::addColumn<QVariantMap>("expectedObject");

    QTest::newRow("phototeleport not found") <<
        ProgramFinderMocked::Responses {} <<
        "Share online with PhotoTeleport" <<
        QVariantMap {};

    QTest::newRow("phototeleport found") <<
        ProgramFinderMocked::Responses {
            { "PhotoTeleport", QJsonObject {
                    { "actionName", "Something" },
                    { "command", "/usr/bin/photokinesis" },
                }
            },
        } <<
        "Share online with PhotoTeleport" <<
        QVariantMap {
            { "actionName", "Share online with PhotoTeleport" },
            { "command", "/usr/bin/photokinesis" },
            { "readOnly", true },
            { "makeVersion", false },
            { "canBatch", true },
        };
}

void HelperModelTest::testFindPrograms()
{
    QFETCH(ProgramFinderMocked::Responses, finderResponses);
    QFETCH(QString, actionNameLookup);
    QFETCH(QVariantMap, expectedObject);

    ProgramFinderMocked finder;
    finder.setResponses(finderResponses);

    // Prepare any needed files
    HelperModel model;
    model.setHelpersData(QJsonArray());

    model.classBegin();
    model.componentComplete();

    int row = -1;
    int count = model.rowCount();
    for (int i = 0; i < count; i++) {
        QModelIndex index = model.index(i, 0);
        QString actionName =
            index.data(HelperModel::ActionNameRole).toString();
        if (actionName == actionNameLookup) {
            row = i;
            break;
        }
    }

    if (expectedObject.isEmpty()) {
        QCOMPARE(row, -1);
    } else {
        QVERIFY(row >= 0);
        QModelIndex index = model.index(row, 0);
        const auto roleNames = model.roleNames();
        for (auto i = expectedObject.constBegin();
             i != expectedObject.constEnd();
             i++) {
            int role = roleNames.key(i.key().toUtf8());
            QVariant value = model.data(index, role);
            QCOMPARE(value, i.value());
        }
    }
}

void HelperModelTest::testCallableApplications()
{
    /* Test that results are sorted by the application name */
    QJsonArray apps {
        QJsonObject {
            { "actionName", "Zoomer" },
            { "command", "zoomer" },
            { "makeVersion", true },
        },
        QJsonObject {
            { "actionName", "Application" },
            { "command", "bump" },
            { "makeVersion", false },
        },
        QJsonObject {
            { "actionName", "Daring app" },
            { "command", "touch" },
            { "makeVersion", false },
        },
    };

    QJsonArray expectedApps {
        QJsonObject {
            { "actionName", "Application" },
            { "command", "bump" },
            { "makeVersion", false },
        },
        QJsonObject {
            { "actionName", "Daring app" },
            { "command", "touch" },
            { "makeVersion", false },
        },
        QJsonObject {
            { "actionName", "Zoomer" },
            { "command", "zoomer" },
            { "makeVersion", true },
        },
    };

    ProgramFinderMocked finder;
    finder.setCallableApplications(apps);

    HelperModel model;
    model.classBegin();
    model.componentComplete();

    QCOMPARE(model.callableApplications(), expectedApps);
}

QTEST_GUILESS_MAIN(HelperModelTest)

#include "tst_helper_model.moc"
