TEMPLATE = subdirs
SUBDIRS = \
    tst_database.pro \
    tst_digikam_importer.pro \
    tst_duplicate_detector.pro \
    tst_fspot_importer.pro \
    tst_helper_model.pro \
    tst_job_database.pro \
    tst_job_executor.pro \
    tst_icon_thumbnailer.pro \
    tst_image_provider.pro \
    tst_importer.pro \
    tst_location_model.pro \
    tst_metadata.pro \
    tst_photo_model.pro \
    tst_program_finder.pro \
    tst_roll_model.pro \
    tst_settings.pro \
    tst_shotwell_importer.pro \
    tst_tag_area_model.pro \
    tst_tag_model.pro \
    tst_thumbnailer.pro \
    tst_time_model.pro \
    tst_updater.pro \
    tst_utils.pro \
    tst_writer.pro

equals(QT_MAJOR_VERSION, 5):greaterThan(QT_MINOR_VERSION, 4) {
    SUBDIRS += tst_folder_model.pro
}

exists("/usr/include/opencv2/objdetect/objdetect.hpp") {
    SUBDIRS += tst_face_detector.pro
}
