TARGET = tst_settings

include(tests.pri)

QT += \
    qml

SOURCES += \
    $${SRC_DIR}/settings.cpp \
    tst_settings.cpp

HEADERS += \
    $${SRC_DIR}/settings.h
