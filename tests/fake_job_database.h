/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FAKE_JOB_DATABASE_H
#define FAKE_JOB_DATABASE_H

#include "fake_abstract_database.h"
#include "job.h"
#include "job_database.h"

#include <QMap>

namespace Imaginario {

class JobDatabasePrivate: public AbstractDatabasePrivate
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(JobDatabase)

public:
    static JobDatabasePrivate *mocked(JobDatabase *o) { return o->d_func(); }

private:
    JobDatabasePrivate(JobDatabase *q):
        AbstractDatabasePrivate(q),
        m_lastJobId(1),
        m_currentId(-1)
    {}

Q_SIGNALS:
    void addJobsCalled(QVector<Job> jobs);
    void markJobInProgressCalled(int jobId);
    void removeJobCalled(int jobId);

private:
    int m_lastJobId;
    QMap<int,Job> m_jobs;
    int m_currentId;
};

} // namespace

#endif // FAKE_JOB_DATABASE_H
