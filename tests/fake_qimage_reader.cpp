/*
 * Copyright (C) 2020-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fake_qimage_reader.h"

#include <QDebug>
#include <QHash>
#include <QRegularExpression>

static QHash<QRegularExpression,FakeQImageReader *> m_mocks;

class QImageReaderPrivate: public FakeQImageReader {};

FakeQImageReader::FakeQImageReader():
    m_isControlled(false)
{
}

FakeQImageReader::FakeQImageReader(const QString &fileNamePattern):
    m_isControlled(true)
{
    m_mocks.insert(QRegularExpression(fileNamePattern), this);
}

QImageReader::QImageReader(const QString &filename,
                           const QByteArray &):
    d(nullptr)
{
    for (auto i = m_mocks.begin(); i != m_mocks.end(); i++) {
        if (i.key().match(filename).hasMatch()) {
            d = static_cast<QImageReaderPrivate*>(i.value());
            break;
        }
    }

    if (!d) {
        d = new QImageReaderPrivate;
    }
}

QImageReader::~QImageReader()
{
    if (!d->m_isControlled) {
        delete d;
    }
}

void QImageReader::setAutoTransform(bool enabled)
{
    Q_EMIT d->setAutoTransformCalled(enabled);
}

QSize QImageReader::size() const
{
    return d->m_sizeResult;
}

void QImageReader::setScaledSize(const QSize &size)
{
    Q_EMIT d->setScaledSizeCalled(size);
}

QImage QImageReader::read()
{
    return d->m_readResult;
}
