/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fake_job_database.h"

#include <QDebug>

using namespace Imaginario;

JobDatabase::JobDatabase(QObject *parent):
    AbstractDatabase(new JobDatabasePrivate(this), parent)
{
}

JobDatabase::~JobDatabase()
{
}

bool JobDatabase::addJobs(const QVector<Job> &jobs)
{
    Q_D(JobDatabase);
    Q_FOREACH(const Job &j, jobs) {
        d->m_jobs.insert(d->m_lastJobId, j);
        d->m_jobs[d->m_lastJobId].setId(d->m_lastJobId);
        d->m_lastJobId++;
    }
    Q_EMIT d->addJobsCalled(jobs);
    return true;
}

bool JobDatabase::markJobInProgress(int jobId)
{
    Q_D(JobDatabase);
    if (!d->m_jobs.contains(jobId)) return false;
    d->m_jobs[jobId].setInProgress(true);
    Q_EMIT d->markJobInProgressCalled(jobId);
    return true;
}

bool JobDatabase::removeJob(int jobId)
{
    Q_D(JobDatabase);
    if (!d->m_jobs.contains(jobId)) return false;
    d->m_jobs.remove(jobId);
    Q_EMIT d->removeJobCalled(jobId);
    return true;
}

bool JobDatabase::next(Job &job)
{
    Q_D(JobDatabase);
    auto i = d->m_jobs.lowerBound(d->m_currentId + 1);
    if (i == d->m_jobs.end()) {
        d->m_currentId = -1;
        i = d->m_jobs.begin();
    }
    if (i == d->m_jobs.end()) {
        job = Job();
        return false;
    }
    job = i.value();
    d->m_currentId = i.key();
    return true;
}
