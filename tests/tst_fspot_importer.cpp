/*
 * Copyright (C) 2016-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "database.h"
#include "fspot_importer.h"
#include "test_utils.h"

#include <QCoreApplication>
#include <QDateTime>
#include <QProcess>
#include <QScopedPointer>
#include <QSignalSpy>
#include <QStandardPaths>
#include <QTemporaryDir>
#include <QTest>

using namespace Imaginario;

class FspotImporterTest: public QObject
{
    Q_OBJECT

public:
    FspotImporterTest() {
        qRegisterMetaType<PhotoId>("PhotoId");
        qRegisterMetaType<Tag>("Tag");
    }

    void setupSource(const QString &fileName);
    QSize iconSize(const QString &tmpDir, const QString &icon);

private Q_SLOTS:
    void testEmpty();
    void testComplete();
};

void FspotImporterTest::setupSource(const QString &fileName)
{
    QString dirName =
        QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) +
        "/f-spot";
    QDir dir(dirName);
    dir.mkpath(".");

    QStringList args;
    args << dir.absoluteFilePath("photos.db");
    QProcess sqlite3;
    sqlite3.setStandardInputFile(QString(DATA_DIR) + "/fspot_import/" +
                                 fileName);
    sqlite3.start("sqlite3", args);
    QVERIFY(sqlite3.waitForStarted());
    QVERIFY(sqlite3.waitForFinished());
}

QSize FspotImporterTest::iconSize(const QString &tmpDir, const QString &icon)
{
    QImage image;
    if (icon.startsWith("tag_icon:")) {
        image = QImage(tmpDir + "/tst_fspot_importer/tag_icons/" + icon.mid(9));
    }
    return image.isNull() ? QSize() : image.size();
}

void FspotImporterTest::testEmpty()
{
    QTemporaryDir tmpDir;
    qputenv("XDG_CACHE_HOME", tmpDir.path().toUtf8());
    qputenv("XDG_CONFIG_HOME", tmpDir.path().toUtf8());
    qputenv("XDG_DATA_HOME", tmpDir.path().toUtf8());

    setupSource("empty.sql");

    QScopedPointer<Database> db(Database::instance());
    QVERIFY(db);

    FspotImporter importer;
    QSignalSpy statusChanged(&importer,
                             SIGNAL(statusChanged(FspotImporter::Status)));

    QTRY_COMPARE(importer.status(), FspotImporter::Ready);
    QCOMPARE(importer.dbVersion(), QString("18"));
    QCOMPARE(importer.version(), QString("0.8.2"));
    QCOMPARE(importer.count(), 0);

    statusChanged.clear();
    importer.exec();
    QTRY_VERIFY(statusChanged.count() > 0);
    QCOMPARE(statusChanged.at(0).at(0).toInt(),
             int(FspotImporter::ImportingTags));

    QTRY_VERIFY(statusChanged.count() > 1);
    QCOMPARE(statusChanged.at(1).at(0).toInt(),
             int(FspotImporter::ImportingRolls));

    QTRY_VERIFY(statusChanged.count() > 2);
    QCOMPARE(statusChanged.at(2).at(0).toInt(),
             int(FspotImporter::ImportingPhotos));

    QTRY_VERIFY(statusChanged.count() > 3);
    QCOMPARE(statusChanged.at(3).at(0).toInt(),
             int(FspotImporter::Done));

    QMap<QString,QString> tags {
        { "People", "stock_icon:emblem-people" },
        { "Events", "stock_icon:emblem-event" },
        { "Places", "stock_icon:emblem-places" },
        { "Favorites", "stock_icon:emblem-favorite" },
        { "Hidden", "stock_icon:emblem-readonly" },
    };
    for (auto i = tags.constBegin(); i != tags.constEnd(); i++) {
        Tag tag = db->tag(i.key());
        QVERIFY(tag.isValid());
        QCOMPARE(tag.icon(), i.value());
        QVERIFY(!tag.parent().isValid());
        QVERIFY(!tag.isCategory());
    }
}

void FspotImporterTest::testComplete()
{
    QTemporaryDir tmpDir;
    qputenv("XDG_CACHE_HOME", tmpDir.path().toUtf8());
    qputenv("XDG_CONFIG_HOME", tmpDir.path().toUtf8());
    qputenv("XDG_DATA_HOME", tmpDir.path().toUtf8());

    setupSource("1.sql");

    QScopedPointer<Database> db(Database::instance());
    QVERIFY(db);

    FspotImporter importer;
    QSignalSpy statusChanged(&importer,
                             SIGNAL(statusChanged(FspotImporter::Status)));
    QSignalSpy progressChanged(&importer, SIGNAL(progressChanged()));

    QTRY_COMPARE(importer.status(), FspotImporter::Ready);
    QCOMPARE(importer.dbVersion(), QString("18"));
    QCOMPARE(importer.version(), QString("0.8.2"));
    QCOMPARE(importer.count(), 10);

    statusChanged.clear();
    importer.exec();
    QTRY_VERIFY(statusChanged.count() > 0);
    QCOMPARE(statusChanged.at(0).at(0).toInt(),
             int(FspotImporter::ImportingTags));

    QTRY_VERIFY(statusChanged.count() > 1);
    QCOMPARE(statusChanged.at(1).at(0).toInt(),
             int(FspotImporter::ImportingRolls));

    QTRY_VERIFY(statusChanged.count() > 2);
    QCOMPARE(statusChanged.at(2).at(0).toInt(),
             int(FspotImporter::ImportingPhotos));
    QTRY_VERIFY(progressChanged.count() >= 1);

    QTRY_VERIFY(statusChanged.count() > 3);
    QCOMPARE(statusChanged.at(3).at(0).toInt(),
             int(FspotImporter::Done));

    /* Check tags */

    struct TagData {
        QString name;
        QSize iconSize;
        QString parent;
        bool isCategory;
    };
    QVector<TagData> tags {
        { "Imported Tags", QSize(), "", true },
        { "oldies", QSize(), "Imported Tags", false },
        { "rose", QSize(48, 48), "flowers", false },
        { "flowers", QSize(48, 48), "Objects", true },
        { "macro", QSize(), "Imported Tags", false },
        { "Objects", QSize(48, 48), "", true },
        { "Europe", QSize(48, 48), "Places", false },
        { "screenshots", QSize(48, 48), "Objects", false },
    };
    for (const TagData &td: tags) {
        Tag tag = db->tag(td.name);
        QVERIFY(tag.isValid());
        QString parentName =
            tag.parent().isValid() ? tag.parent().name() : "";
        QCOMPARE(parentName, td.parent);
        QCOMPARE(iconSize(tmpDir.path(), tag.icon()), td.iconSize);
        QCOMPARE(tag.isCategory(), td.isCategory);
    }

    /* Check rolls */

    QList<Roll> rolls = db->rolls(10);
    QCOMPARE(rolls.count(), 1);
    Roll roll = rolls[0];
    QVERIFY(roll.isValid());
    QCOMPARE(roll.time(), QDateTime::fromTime_t(1461549513));

    /* Check photos */

    SearchFilters filters;
    QList<Database::Photo> photos = db->findPhotos(filters);
    QCOMPARE(photos.count(), 12);

    QHash<QString,QSet<QString>> expectedTags {
        { "DSC00461%20(1).jpg", { "Europe","oldies" }},
        { "DSC00448 (1) (Modified).jpg", { "Europe","oldies" }},
        { "DSC00448 (1) (Modified%20(2)).jpg", { "Europe","oldies" }},
        { "DSC00448%20(1).jpg", { "Europe","oldies" }},
        { "DSC04967.jpg", { "Europe","oldies","macro","rose","flowers" }},
        { "DSC04981.jpg", { "Europe","oldies" }},
        { "DSC06903.jpg", { "Europe","oldies" }},
        { "DSC02582.jpg", { "Europe","oldies" }},
        { "Screenshot%20from%202012-12-28%2014:14:48.png", { "oldies" }},
        { "Screenshot%20from%202014-12-27%2021:30:34.png", { "screenshots","rose","oldies" }},
        { "Screenshot%20from%202014-02-07%2018:22:26.png", { "screenshots","rose","oldies" }},
        { "image20160419_193456352.jpg", { "oldies" }},
    };
    QHash<QString,QSet<QString>> photoTags;
    Q_FOREACH(const auto &p, photos) {
        photoTags.insert(p.fileName(), Tag::tagNames(db->tags(p.id())).toSet());
    }
    QCOMPARE(photoTags, expectedTags);
}

QTEST_GUILESS_MAIN(FspotImporterTest)

#include "tst_fspot_importer.moc"
