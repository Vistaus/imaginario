/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fake_database.h"
#include "fake_job_database.h"
#include "fake_metadata.h"
#include "fake_thumbnailer.h"
#include "job_executor.h"
#include "test_utils.h"

#include <QMetaObject>
#include <QMutex>
#include <QMutexLocker>
#include <QScopedPointer>
#include <QSignalSpy>
#include <QTemporaryDir>
#include <QTest>
#include <QThreadPool>

using namespace Imaginario;

Q_DECLARE_METATYPE(Imaginario::Metadata::Changes)

struct TestJob: public Job {
    TestJob(int jobId): Job(new JobData(TestJob::type())) {
        d->m_id = jobId;
    }

    static Job::Type type() { return Job::Type(19); }
};

class JobExecutorTest: public QObject
{
    Q_OBJECT

public:
    enum TagOperation {
        Add,
        Remove,
        Set,
    };

    JobExecutorTest() {
        qRegisterMetaType<GeoPoint>("GeoPoint");
        qRegisterMetaType<PhotoId>("PhotoId");
        qRegisterMetaType<Metadata::Changes>("Metadata::Changes");
        qRegisterMetaType<Tag>("Tag");
    }

private Q_SLOTS:
    void testProperties();
    void testPhotoJob_data();
    void testPhotoJob();
    void testThumbnailJob_data();
    void testThumbnailJob();
    void testStartStop();
};

void JobExecutorTest::testProperties()
{
    QScopedPointer<Database> db(Database::instance());

    MetadataPrivate *mdMocked = new MetadataPrivate();
    QSignalSpy metadataCreated(mdMocked, SIGNAL(instanceCreated(Metadata*)));

    QScopedPointer<JobExecutor> executor(JobExecutor::instance());
    QCOMPARE(metadataCreated.count(), 1);

    Metadata *md = metadataCreated.at(0).at(0).value<Metadata*>();
    QSignalSpy embedChanged(md, SIGNAL(embedChanged()));

    bool embed = md->embed();
    QCOMPARE(executor->embedMetadata(), embed);

    executor->setEmbedMetadata(!embed);
    QCOMPARE(executor->embedMetadata(), !embed);
    QCOMPARE(embedChanged.count(), 1);
}

void JobExecutorTest::testPhotoJob_data()
{
    QTest::addColumn<int>("photoId");
    QTest::addColumn<int>("ops");
    QTest::addColumn<QString>("description");
    QTest::addColumn<QString>("expectedFileName");
    QTest::addColumn<Metadata::Changes>("expectedChanges");

    Metadata::Changes changes;
    QTest::newRow("empty") <<
        0 << 0 << QString() <<
        ITEMS_DIR "/image0.png" << changes;

    changes.setLocation(QGeoCoordinate(20, 30));
    QTest::newRow("location") <<
        1 <<
        int(PhotoJob::WriteLocation) <<
        QString() <<
        ITEMS_DIR "/image1.jpg" << changes;

    changes.clear();
    changes.setTags(QStringList());
    QTest::newRow("tags-empty") <<
        0 <<
        int(PhotoJob::WriteTags) <<
        QString() <<
        ITEMS_DIR "/image0.png" << changes;

    changes.clear();
    changes.setTags(QStringList() << "red" << "Jim");
    QTest::newRow("tags") <<
        2 <<
        int(PhotoJob::WriteTags) <<
        QString() <<
        ITEMS_DIR "/image2.jpg" << changes;

    changes.clear();
    changes.setRegions(Metadata::Regions() <<
        Metadata::RegionInfo(QRectF(0.1, 0.2, 0.5, 0.4), "Dick") <<
        Metadata::RegionInfo(QRectF(0.3, 0.2, 0.6, 0.8), "Tom"));
    QTest::newRow("regions") <<
        1 <<
        int(PhotoJob::WriteRegions) <<
        QString() <<
        ITEMS_DIR "/image1.jpg" << changes;

    changes.clear();
    changes.setRating(4);
    QTest::newRow("rating") <<
        1 <<
        int(PhotoJob::WriteRating) <<
        QString() <<
        ITEMS_DIR "/image1.jpg" << changes;

    changes.clear();
    changes.setTitle("Green cat");
    QTest::newRow("title") <<
        1 <<
        int(PhotoJob::WriteTitle) <<
        QString() <<
        ITEMS_DIR "/image1.jpg" << changes;

    changes.clear();
    changes.setDescription("A nice photo");
    QTest::newRow("description") <<
        0 <<
        0 <<
        "A nice photo" <<
        ITEMS_DIR "/image0.png" << changes;
}

void JobExecutorTest::testPhotoJob()
{
    QFETCH(int, photoId);
    QFETCH(int, ops);
    QFETCH(QString, description);
    QFETCH(QString, expectedFileName);
    QFETCH(Metadata::Changes, expectedChanges);

    MetadataPrivate *mdMocked = new MetadataPrivate();

    // Create the images in the DB
    QDir dataDir(ITEMS_DIR);

    QScopedPointer<Database> db(Database::instance());
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db.data());

    QStringList allTags;
    allTags << "Tom" << "red" << "green" << "Jim" <<
        "Sam" << "Dick" << "Harry";
    dbMock->setTags(allTags);

    Database::Photo photo;
    photo.setBaseUri("file://" + dataDir.absolutePath());
    photo.setFileName("image0.png");
    photo.setLocation(GeoPoint(20, 30));
    PhotoId id = db->addPhoto(photo, 1);
    QVERIFY(id >= 0);
    int firstId = id;

    photo.setFileName("image1.jpg");
    photo.setRating(4);
    photo.setDescription("Green cat");
    id = db->addPhoto(photo, 1);
    QVERIFY(id > 0);
    QRectF area1_0(0.1, 0.2, 0.5, 0.4);
    db->addTagWithArea(id, db->tag("Dick"), area1_0);
    QRectF area1_1(0.3, 0.2, 0.6, 0.8);
    db->addTagWithArea(id, db->tag("Tom"), area1_1);

    photo.setFileName("image2.jpg");
    id = db->addPhoto(photo, 1);
    QVERIFY(id > 0);
    db->setTags(id, QStringList() << "Jim" << "red");

    QSignalSpy photoTagAreasChanged(db.data(),
                                    SIGNAL(photoTagAreasChanged(PhotoId)));
    QSignalSpy writeChangesCalled(mdMocked,
        SIGNAL(writeChangesCalled(QString,Metadata::Changes)));

    QScopedPointer<JobExecutor> executor(JobExecutor::instance());
    PhotoJob job(photoId + firstId, PhotoJob::Operations(ops));
    if (!description.isEmpty()) {
        job.setDescription(description);
    }
    executor->addJob(job);

    QThreadPool::globalInstance()->waitForDone(5000);

    QCOMPARE(writeChangesCalled.count(), 1);
    QCOMPARE(writeChangesCalled.at(0).at(0).toString(), expectedFileName);

    Metadata::Changes changes =
        writeChangesCalled.at(0).at(1).value<Metadata::Changes>();
    QCOMPARE(changes.fields, expectedChanges.fields);
    QCOMPARE(changes.tags.toSet(), expectedChanges.tags.toSet());
    QCOMPARE(changes.regions.toList().toSet(),
             expectedChanges.regions.toList().toSet());
    QCOMPARE(changes.rating, expectedChanges.rating);
    QCOMPARE(changes.location, expectedChanges.location);
    QCOMPARE(changes.title, expectedChanges.title);
    QCOMPARE(changes.description, expectedChanges.description);
}

void JobExecutorTest::testThumbnailJob_data()
{
    QTest::addColumn<QUrl>("imageUri");

    QTest::newRow("local file") <<
        QUrl::fromLocalFile("/tmp/image.jpg");
}

void JobExecutorTest::testThumbnailJob()
{
    QFETCH(QUrl, imageUri);

    ThumbnailerPrivate *tnMocked = new ThumbnailerPrivate();
    QSignalSpy createCalled(tnMocked, SIGNAL(createCalled(QUrl)));

    QScopedPointer<JobExecutor> executor(JobExecutor::instance());
    QSignalSpy thumbnailUpdated(executor.data(),
                                SIGNAL(thumbnailUpdated(QUrl)));
    ThumbnailJob job(imageUri);
    executor->addJob(job);

    QThreadPool::globalInstance()->waitForDone(5000);

    QCOMPARE(createCalled.count(), 1);
    QCOMPARE(createCalled.at(0).at(0).toUrl(), imageUri);

    QCOMPARE(thumbnailUpdated.count(), 1);
    QCOMPARE(thumbnailUpdated.at(0).at(0).toUrl(), imageUri);
}

void JobExecutorTest::testStartStop()
{
    QScopedPointer<JobExecutor> executor(JobExecutor::instance());

    QThreadPool::globalInstance()->setMaxThreadCount(1);

    QMutex mutex;
    QSet<int> completedJobs;
    executor->registerExecutor(TestJob::type(),
                               [&completedJobs,&mutex](const Job &j) {
        QThread::msleep(50);
        QMutexLocker locker(&mutex);
        completedJobs.insert(j.id());
    });

    QVector<Job> jobs;
    for (int i = 1; i <= 10; i++) {
        jobs.append(TestJob(i));
    }
    executor->addJobs(jobs);

    QTest::qWait(125);
    executor->stop();
    QThreadPool::globalInstance()->waitForDone(5000);
    QVERIFY(completedJobs.count() >= 2);
    QVERIFY(completedJobs.count() <= 4);

    executor->start();
    QThreadPool::globalInstance()->waitForDone(5000);
    QCOMPARE(completedJobs.count(), 10);
}

QTEST_GUILESS_MAIN(JobExecutorTest)

#include "tst_job_executor.moc"
