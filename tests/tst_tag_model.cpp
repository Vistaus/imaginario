/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fake_database.h"
#include "tag_model.h"

#include <QLocale>
#include <QQmlComponent>
#include <QQmlEngine>
#include <QScopedPointer>
#include <QSignalSpy>
#include <QTest>

using namespace Imaginario;

class TagModelTest: public QObject
{
    Q_OBJECT

public:
    TagModelTest() {
        qRegisterMetaType<Tag>("Tag");
        qRegisterMetaType<PhotoId>("PhotoId");
        QLocale::setDefault(QLocale("it_IT"));
    }

    static QVariant get(const QAbstractItemModel *model, int row,
                        QString roleName);

private Q_SLOTS:
    void testProperties();
    void testQml();
    void testRoles_data();
    void testRoles();
    void testFilters_data();
    void testFilters();
    void testTagCreation_data();
    void testTagCreation();
    void testTagDeletion();
    void testUsageCount();
    void testTagNames();
    void testSorting();
    void testMatches_data();
    void testMatches();
    void testTreeUpdates();
};

QVariant TagModelTest::get(const QAbstractItemModel *model, int row,
                           QString roleName)
{
    QHash<int, QByteArray> roleNames = model->roleNames();

    int role = roleNames.key(roleName.toLatin1(), -1);
    return model->data(model->index(row, 0), role);
}

void TagModelTest::testProperties()
{
    Database *db = Database::instance();
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db);
    QStringList allTags;
    allTags << "red" << "green" << "blue";
    // add default tags
    allTags << "uncategorized" << "people" << "places" <<
        "events" << "importedTags";
    dbMock->setTags(allTags);

    TagModel model;
    model.classBegin();
    model.componentComplete();

    QCOMPARE(model.columnCount(QModelIndex()), 1);

    Tag tag = db->tag("red");
    model.setProperty("parentTag", QVariant::fromValue(tag));
    QCOMPARE(model.property("parentTag").value<Tag>(), tag);

    model.setProperty("photoId", int(2));
    QCOMPARE(model.property("photoId").toInt(), 2);

    model.setProperty("allTags", true);
    QCOMPARE(model.property("allTags").toBool(), true);

    QCOMPARE(model.property("uncategorizedTag").value<Tag>().name(),
             QString("uncategorized"));
    QCOMPARE(model.property("peopleTag").value<Tag>().name(),
             QString("people"));
    QCOMPARE(model.property("placesTag").value<Tag>().name(),
             QString("places"));
    QCOMPARE(model.property("eventsTag").value<Tag>().name(),
             QString("events"));
    QCOMPARE(model.property("importedTagsTag").value<Tag>().name(),
             QString("importedTags"));

    QList<int> photoIds = QList<int>() << 2 << 4 << 5;
    model.setProperty("photosForUsageCount", QVariant::fromValue(photoIds));
    QCOMPARE(model.property("photosForUsageCount").value<QList<int> >(), photoIds);

    QCOMPARE(model.property("hideUnused").toBool(), false);
    model.setProperty("hideUnused", true);
    QCOMPARE(model.property("hideUnused").toBool(), true);

    delete db;
}

void TagModelTest::testQml()
{
    Database *db = Database::instance();

    QQmlEngine engine;
    qmlRegisterType<TagModel>("MyTest", 1, 0, "TagModel");
    QQmlComponent component(&engine);
    component.setData("import MyTest 1.0\n"
                      "TagModel {\n"
                      "  photoId: 2\n"
                      "}",
                      QUrl());
    QObject *object = component.create();
    QVERIFY(object != 0);
    QAbstractItemModel *model = qobject_cast<QAbstractItemModel*>(object);
    QVERIFY(model != 0);

    QCOMPARE(model->rowCount(), 0);
    QCOMPARE(model->property("photoId").toInt(), 2);

    delete db;
}

void TagModelTest::testRoles_data()
{
    QTest::addColumn<QString>("name");
    QTest::addColumn<QString>("icon");
    QTest::addColumn<float>("popularity");

    QTest::newRow("simple") <<
        "red" <<
        "/tmp/icon.png" <<
        float(0.2);

    QTest::newRow("with spaces") <<
        "a dog" <<
        "theme-icon" <<
        float(1.0);
}

void TagModelTest::testRoles()
{
    QFETCH(QString, name);
    QFETCH(QString, icon);
    QFETCH(float, popularity);

    Database *db = Database::instance();
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db);
    Tag parent = db->createTag(Tag(), "parent", "parent.ico");

    Tag child = db->createTag(parent, name, icon);
    dbMock->setTagPopularity(child, popularity);

    TagModel model;
    model.classBegin();
    model.setParentTag(parent);
    model.componentComplete();

    QCOMPARE(model.rowCount(), 1);

    QCOMPARE(model.get(0, "name").toString(), name);
    QCOMPARE(model.get(0, "icon").toString(), icon);
    QCOMPARE(model.get(0, "tagId").toInt(), int(child.id()));
    QCOMPARE(model.get(0, "tag").value<Tag>(), child);
    QCOMPARE(model.get(0, "popularity").toReal(), qreal(popularity));
    QCOMPARE(model.get(0, "usageCount").toInt(), 0);
    QCOMPARE(model.get(0, "parentTagId").toInt(), int(parent.id()));

    delete db;
}

void TagModelTest::testFilters_data()
{
    QTest::addColumn<QString>("filters");
    QTest::addColumn<QStringList>("expectedTags");

    QTest::newRow("empty") <<
        "" <<
        (QStringList() << "colors" << "sizes" << "animals" << "nochildren");

    QTest::newRow("allTags") <<
        "allTags: true" <<
        (QStringList() << "colors" << "red" << "green" << "blue" <<
         "sizes" << "big" << "medium" << "small" <<
         "animals" << "cat" << "dog" <<
         "nochildren");

    QTest::newRow("hide unused") <<
        "allTags: true; hideUnused: true" <<
        (QStringList() << "colors" << "sizes" << "nochildren");
}

void TagModelTest::testFilters()
{
    QFETCH(QString, filters);
    QFETCH(QStringList, expectedTags);

    Database *db = Database::instance();
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db);

    Tag tag = db->createTag(Tag(), "colors", "");
    dbMock->setTagUsageCount(tag, 2);
    db->createTag(tag, "red", "");
    db->createTag(tag, "green", "");
    db->createTag(tag, "blue", "");

    tag = db->createTag(Tag(), "sizes", "");
    dbMock->setTagUsageCount(tag, 3);
    db->createTag(tag, "big", "");
    db->createTag(tag, "medium", "");
    db->createTag(tag, "small", "");

    tag = db->createTag(Tag(), "animals", "");
    db->createTag(tag, "cat", "");
    db->createTag(tag, "dog", "");

    tag = db->createTag(Tag(), "nochildren", "");
    dbMock->setTagUsageCount(tag, 7);

    QQmlEngine engine;
    qmlRegisterType<TagModel>("MyTest", 1, 0, "TagModel");
    QQmlComponent component(&engine);
    component.setData("import MyTest 1.0\n"
                      "TagModel {\n"
                      "  " + filters.toUtf8() + "\n"
                      "}",
                      QUrl());
    QObject *object = component.create();
    QVERIFY(object != 0);
    QAbstractItemModel *model = qobject_cast<QAbstractItemModel*>(object);
    QVERIFY(model != 0);

    QCOMPARE(model->rowCount(), expectedTags.count());

    QStringList tags;
    for (int i = 0; i < model->rowCount(); i++) {
        tags.append(get(model, i, "name").toString());
    }

    QCOMPARE(tags.toSet(), expectedTags.toSet());

    delete db;
}

void TagModelTest::testTagCreation_data()
{
    QTest::addColumn<QString>("filters");
    QTest::addColumn<QStringList>("expectedTags");

    QTest::newRow("empty") <<
        "" <<
        (QStringList() << "colors" << "topLevel");

    QTest::newRow("colors") <<
        "parentTag: tag(\"colors\")" <<
        (QStringList() << "blue" << "green" << "pink" << "red");

    QTest::newRow("allTags") <<
        "allTags: true" <<
        (QStringList() << "blue" << "colors" << "green" <<
         "pink" << "red" << "topLevel");
}

void TagModelTest::testTagCreation()
{
    QFETCH(QString, filters);
    QFETCH(QStringList, expectedTags);

    Database *db = Database::instance();

    Tag colorsTag = db->createTag(Tag(), "colors", "");
    db->createTag(colorsTag, "red", "");
    db->createTag(colorsTag, "green", "");
    db->createTag(colorsTag, "blue", "");

    QQmlEngine engine;
    qmlRegisterType<TagModel>("MyTest", 1, 0, "TagModel");
    QQmlComponent component(&engine);
    component.setData("import MyTest 1.0\n"
                      "TagModel {\n"
                      "  " + filters.toUtf8() + "\n"
                      "}",
                      QUrl());
    QObject *object = component.create();
    QVERIFY(object != 0);
    QAbstractItemModel *model = qobject_cast<QAbstractItemModel*>(object);
    QVERIFY(model != 0);

    QSignalSpy countChanged(model, SIGNAL(countChanged()));

    db->createTag(colorsTag, "pink", "");
    db->createTag(Tag(), "topLevel", "");

    if (countChanged.count() == 0) {
        QVERIFY(countChanged.wait());
    }
    QCOMPARE(model->rowCount(), expectedTags.count());

    QStringList tags;
    for (int i = 0; i < model->rowCount(); i++) {
        tags.append(get(model, i, "name").toString());
    }

    QCOMPARE(tags, expectedTags);

    delete db;
}

void TagModelTest::testTagDeletion()
{
    QScopedPointer<Database> db(Database::instance());
    db->createTag(Tag(), "one", "");
    Tag two = db->createTag(Tag(), "two", "");
    db->createTag(Tag(), "three", "");

    TagModel model;
    model.classBegin();
    model.componentComplete();

    QCOMPARE(model.rowCount(), 3);

    QSignalSpy countChanged(&model, SIGNAL(countChanged()));
    db->deleteTag(two);

    QVERIFY(countChanged.wait());
    QCOMPARE(model.rowCount(), 2);

    QStringList expectedTags;
    expectedTags << "one" << "three";

    QStringList tags;
    for (int i = 0; i < model.rowCount(); i++) {
        tags.append(get(&model, i, "name").toString());
    }

    QCOMPARE(tags.toSet(), expectedTags.toSet());
}

Q_DECLARE_METATYPE(QList<int>)

void TagModelTest::testUsageCount()
{
    Database *db = Database::instance();
    Tag parent = db->createTag(Tag(), "parent", "parent.ico");

    Tag tag = db->createTag(parent, "countMe", "");

    /* Create a few photos */
    PhotoId firstPhoto = -1;
    for (int i = 0; i < 5; i++) {
        Database::Photo photo;
        photo.setBaseUri("/tmp");
        QString fileName = QString("foo%1.jpg").arg(i);
        photo.setFileName(fileName);
        PhotoId id = db->addPhoto(photo, 1);
        if (firstPhoto < 0) { firstPhoto = id; }
        db->addTags(id, QList<Tag>() << tag);
    }

    TagModel model;
    model.classBegin();
    model.setParentTag(parent);
    model.componentComplete();

    QCOMPARE(model.rowCount(), 1);

    /* With no photos to count, this will be 0 */
    QCOMPARE(model.get(0, "usageCount").toInt(), 0);

    QList<int> photoIds = QList<int>() << firstPhoto << (firstPhoto + 2);
    model.setProperty("photosForUsageCount", QVariant::fromValue(photoIds));

    /* Iterate the main loop so that tag usage count gets updated */
    QTest::qWait(5);
    QCOMPARE(model.get(0, "usageCount").toInt(), 2);

    delete db;
}

void TagModelTest::testTagNames()
{
    Database *db = Database::instance();
    Tag one = db->createTag(Tag(), "one", "");
    Tag two = db->createTag(Tag(), "two", "");

    TagModel model;
    model.classBegin();
    model.componentComplete();

    QList<Tag> tags;
    tags << one << two;

    QStringList expectedNames;
    expectedNames << "one" << "two";
    QCOMPARE(model.tagNames(QVariant::fromValue(tags)), expectedNames);

    delete db;
}

void TagModelTest::testSorting()
{
    QScopedPointer<Database> db(Database::instance());

    Tag colorsTag = db->createTag(Tag(), "colors", "");
    db->createTag(colorsTag, "red", "");
    db->createTag(colorsTag, "green", "");
    db->createTag(colorsTag, "blue", "");

    db->createTag(db->peopleTag(), "Rod", "");
    db->createTag(db->peopleTag(), "Jim", "");
    db->createTag(db->peopleTag(), "Bob", "");

    TagModel model;
    model.classBegin();
    model.setAllTags(true);
    model.componentComplete();

    QStringList tags;
    for (int i = 0; i < model.rowCount(); i++) {
        tags.append(get(&model, i, "name").toString());
    }

    QStringList expectedTags;
    expectedTags << "blue" << "Bob" << "colors" << "green" << "Jim" <<
        "red" << "Rod";
    QCOMPARE(tags, expectedTags);
}

void TagModelTest::testMatches_data()
{
    QTest::addColumn<QString>("text");
    QTest::addColumn<QStringList>("expectedMatches");

    QTest::newRow("empty") <<
        "" <<
        (QStringList() << "blue" << "Bob" << "colors" << "green" << "Jim" <<
        "red" << "Rod");

    QTest::newRow("no match") <<
        "seed" <<
        QStringList();

    QTest::newRow("one match") <<
        "gr" <<
        (QStringList() << "green");

    QTest::newRow("leading spaces") <<
        " Bl" <<
        (QStringList() << "blue");

    QTest::newRow("two matches") <<
        "r" <<
        (QStringList() << "red" << "Rod");
}

void TagModelTest::testMatches()
{
    QFETCH(QString, text);
    QFETCH(QStringList, expectedMatches);

    QScopedPointer<Database> db(Database::instance());

    Tag colorsTag = db->createTag(Tag(), "colors", "");
    db->createTag(colorsTag, "red", "");
    db->createTag(colorsTag, "green", "");
    db->createTag(colorsTag, "blue", "");

    db->createTag(db->peopleTag(), "Rod", "");
    db->createTag(db->peopleTag(), "Jim", "");
    db->createTag(db->peopleTag(), "Bob", "");

    TagModel model;
    model.classBegin();
    model.setAllTags(true);
    model.componentComplete();

    QStringList tags = model.findMatches(text);

    QCOMPARE(tags, expectedMatches);
}

void TagModelTest::testTreeUpdates()
{
    QScopedPointer<Database> db(Database::instance());

    Tag namesTag = db->createTag(Tag(), "names", "");
    db->createTag(namesTag, "Rod", "");
    db->createTag(namesTag, "Jim", "");
    db->createTag(namesTag, "Bob", "");

    Tag colorsTag = db->createTag(Tag(), "colors", "");
    db->createTag(colorsTag, "red", "");
    db->createTag(colorsTag, "green", "");
    db->createTag(colorsTag, "black", "");
    db->createTag(colorsTag, "blue", "");

    TagModel model;
    model.classBegin();
    model.componentComplete();

    QCOMPARE(model.rowCount(QModelIndex()), 2);

    QSignalSpy modelReset(&model, SIGNAL(modelReset()));

    /* check their values; remember that the tree is sorted */
    QModelIndex namesIdx = model.index(1, 0);
    QCOMPARE(model.data(namesIdx).toString(), QString("names"));
    QModelIndex colorsIdx = model.index(0, 0);
    QCOMPARE(model.data(colorsIdx).toString(), QString("colors"));

    /* check their children */
    QCOMPARE(model.hasChildren(namesIdx), true);
    QCOMPARE(model.rowCount(namesIdx), 3);
    QCOMPARE(model.hasChildren(colorsIdx), true);
    QCOMPARE(model.rowCount(colorsIdx), 4);

    /* are they sorted? */
    QModelIndex childIdx = colorsIdx.child(0, 0);
    QCOMPARE(childIdx.data().toString(), QString("black"));
    childIdx = childIdx.sibling(1, 0);
    QCOMPARE(childIdx.data().toString(), QString("blue"));
    childIdx = childIdx.sibling(2, 0);
    QCOMPARE(childIdx.data().toString(), QString("green"));
    childIdx = childIdx.sibling(3, 0);
    QCOMPARE(childIdx.data().toString(), QString("red"));

    /* add a node */
    db->createTag(colorsTag, "pink", "");
    modelReset.wait();

    QCOMPARE(model.rowCount(colorsIdx), 5);
    childIdx = colorsIdx.child(3, 0);
    QCOMPARE(childIdx.data().toString(), QString("pink"));
}

QTEST_GUILESS_MAIN(TagModelTest)

#include "tst_tag_model.moc"
