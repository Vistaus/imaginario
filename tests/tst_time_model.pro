TARGET = tst_time_model

include(tests.pri)

QT += \
    qml

SOURCES += \
    $${SRC_DIR}/photo_model.cpp \
    $${SRC_DIR}/time_model.cpp \
    $${SRC_DIR}/utils.cpp \
    tst_time_model.cpp

HEADERS += \
    $${SRC_DIR}/photo_model.h \
    $${SRC_DIR}/time_model.h \
    $${SRC_DIR}/utils.h \
    test_utils.h
