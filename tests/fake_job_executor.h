/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FAKE_JOB_EXECUTOR_H
#define FAKE_JOB_EXECUTOR_H

#include "job_executor.h"

#include <QObject>

namespace Imaginario {

class JobExecutorPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(JobExecutor)

public:
    static JobExecutorPrivate *mocked(JobExecutor *o) { return o->d_ptr; }

    void emitThumbnailUpdated(const QUrl &url) {
        Q_Q(JobExecutor);
        Q_EMIT q->thumbnailUpdated(url);
    }

private:
    JobExecutorPrivate(JobExecutor *q):
        QObject(q),
        m_embed(false),
        q_ptr(q)
    {}

Q_SIGNALS:
    void addJobsCalled(QVector<Imaginario::Job> jobs);
    void startCalled();
    void stopCalled();

private:
    friend class JobExecutor;
    bool m_embed;
    JobExecutor *q_ptr;
};

} // namespace

Q_DECLARE_METATYPE(Imaginario::Job)

#endif // FAKE_JOB_EXECUTOR_H
