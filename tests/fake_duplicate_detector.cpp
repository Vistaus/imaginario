/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fake_duplicate_detector.h"

using namespace Imaginario;

static DuplicateDetectorPrivate *m_privateInstance = 0;

DuplicateDetectorPrivate::DuplicateDetectorPrivate():
    QObject(),
    m_minimumScore(0.5),
    m_pairingAlgorithm(AllEditedPairing),
    q_ptr(0)
{
    m_privateInstance = this;
}

DuplicateDetectorPrivate::~DuplicateDetectorPrivate()
{
    m_privateInstance = 0;
}

DuplicateDetector::RawPairs
DuplicateDetectorPrivate::makeRawPairs(const QList<QUrl> &urls) const
{
    DuplicateDetector::RawPairs pairs;
    for (int i = 0; i < urls.count(); i++) {
        DuplicateDetector::RawPair p;
        if (m_pairingAlgorithm == AllEditedPairing) {
            p.edited = urls[i];
        } else if (m_pairingAlgorithm == AllRawPairing) {
            p.raw = urls[i];
        } else if (m_pairingAlgorithm == FullPairing) {
            p.edited = urls[i++];
            if (i < urls.count()) {
                p.raw = urls[i];
            }
        }
        pairs.append(p);
    }
    return pairs;
}

DuplicateDetector::DuplicateDetector(QObject *parent):
    QObject(parent)
{
    if (m_privateInstance) {
        m_privateInstance->q_ptr = this;
        d_ptr = m_privateInstance;
    } else {
        d_ptr = new DuplicateDetectorPrivate(this);
    }
}

DuplicateDetector::~DuplicateDetector()
{
    delete d_ptr;
}

void DuplicateDetector::setMinimumScore(double score)
{
    Q_D(DuplicateDetector);
    d->m_minimumScore = score;
    Q_EMIT minimumScoreChanged();
}

double DuplicateDetector::minimumScore() const
{
    Q_D(const DuplicateDetector);
    return d->m_minimumScore;
}

bool DuplicateDetector::checkFile(const QUrl &url)
{
    Q_D(DuplicateDetector);
    return d->m_duplicates.contains(url) && !d->m_duplicates[url].isEmpty();
}

MaybeDuplicates DuplicateDetector::duplicatesFor(const QUrl &url) const
{
    Q_D(const DuplicateDetector);
    return d->m_duplicates[url];
}

DuplicateDetector::RawPairs
DuplicateDetector::makeRawPairs(const QList<QUrl> &urls) const
{
    Q_D(const DuplicateDetector);
    return d->makeRawPairs(urls);
}
