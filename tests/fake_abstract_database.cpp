/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fake_abstract_database.h"

using namespace Imaginario;

AbstractDatabase::AbstractDatabase(AbstractDatabasePrivate *priv,
                                   QObject *parent):
    QObject(parent),
    d_ptr(priv)
{
}

AbstractDatabase::~AbstractDatabase()
{
    delete d_ptr;
}

bool AbstractDatabase::transaction()
{
    Q_D(AbstractDatabase);
    Q_EMIT d->transactionCalled();
    return true;
}

bool AbstractDatabase::commit()
{
    Q_D(AbstractDatabase);
    Q_EMIT d->commitCalled();
    return d->m_commitResult;
}

bool AbstractDatabase::rollback()
{
    Q_D(AbstractDatabase);
    Q_EMIT d->rollbackCalled();
    return true;
}

QMutex &AbstractDatabase::mutex()
{
    Q_D(AbstractDatabase);
    return d->m_mutex;
}
