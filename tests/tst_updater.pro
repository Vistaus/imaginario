TARGET = tst_updater

include(tests.pri)

QT += \
    network \
    qml

SOURCES += \
    $${SRC_DIR}/updater.cpp \
    tst_updater.cpp

HEADERS += \
    $${SRC_DIR}/updater.h \
    fake_network.h

