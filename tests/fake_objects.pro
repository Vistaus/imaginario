TARGET = fake_objects
TEMPLATE = lib

include(../common-config.pri)

CONFIG += \
    c++11 \
    static

QT += \
    positioning \
    qml \
    sql \
    testlib

SRC_DIR = $${TOP_SRC_DIR}/src

INCLUDEPATH += $${SRC_DIR}

SOURCES += \
    fake_abstract_database.cpp \
    fake_application.cpp \
    fake_database.cpp \
    fake_duplicate_detector.cpp \
    fake_face_detector.cpp \
    fake_icon_thumbnailer.cpp \
    fake_job_database.cpp \
    fake_job_executor.cpp \
    fake_metadata.cpp \
    fake_program_finder.cpp \
    fake_thumbnailer.cpp

HEADERS += \
    $${SRC_DIR}/abstract_database.h \
    $${SRC_DIR}/application.h \
    $${SRC_DIR}/database.h \
    $${SRC_DIR}/duplicate_detector.h \
    $${SRC_DIR}/face_detector.h \
    $${SRC_DIR}/icon_thumbnailer.h \
    $${SRC_DIR}/job_database.h \
    $${SRC_DIR}/job_executor.h \
    $${SRC_DIR}/metadata.h \
    $${SRC_DIR}/program_finder.h \
    $${SRC_DIR}/thumbnailer.h \
    $${SRC_DIR}/types.h \
    $${SRC_DIR}/utils.h \
    fake_abstract_database.h \
    fake_application.h \
    fake_database.h \
    fake_duplicate_detector.h \
    fake_face_detector.h \
    fake_icon_thumbnailer.h \
    fake_job_database.h \
    fake_job_executor.h \
    fake_metadata.h \
    fake_program_finder.h \
    fake_thumbnailer.h
