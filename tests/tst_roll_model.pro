TARGET = tst_roll_model

include(tests.pri)

QT += \
    qml

SOURCES += \
    $${SRC_DIR}/roll_model.cpp \
    tst_roll_model.cpp

HEADERS += \
    $${SRC_DIR}/abstract_database.h \
    $${SRC_DIR}/database.h \
    $${SRC_DIR}/roll.h \
    $${SRC_DIR}/roll_model.h
