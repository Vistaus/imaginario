/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "job_database.h"
#include "test_utils.h"

#include <QList>
#include <QProcess>
#include <QScopedPointer>
#include <QSet>
#include <QSharedPointer>
#include <QSignalSpy>
#include <QStandardPaths>
#include <QTemporaryDir>
#include <QTest>
#include <QtConcurrent>

using namespace Imaginario;

class JobDatabaseTest: public QObject
{
    Q_OBJECT

public:
    JobDatabaseTest() {
        qRegisterMetaType<PhotoId>("PhotoId");
    }

private Q_SLOTS:
    void testPhotoJob_data();
    void testPhotoJob();
    void testAddPhoto_data();
    void testAddPhoto();
    void testMarkInProgress_data();
    void testMarkInProgress();
    void testRemoveJob_data();
    void testRemoveJob();
    void testUpdate_data();
    void testUpdate();
    void testMultithread();
};

void JobDatabaseTest::testPhotoJob_data()
{
    QTest::addColumn<int>("photoId");
    QTest::addColumn<int>("ops");
    QTest::addColumn<QString>("description");
    QTest::addColumn<int>("expectedOps");

    QTest::newRow("empty") <<
        0 << 0 << QString() << 0;

    QTest::newRow("simple") <<
        3 <<
        int(PhotoJob::WriteLocation) <<
        QString() <<
        int(PhotoJob::WriteLocation);

    QTest::newRow("description") <<
        3 <<
        int(PhotoJob::WriteTags) <<
        "Nice photo" <<
        int(PhotoJob::WriteTags | PhotoJob::WriteDescription);
}

void JobDatabaseTest::testPhotoJob()
{
    QFETCH(int, photoId);
    QFETCH(int, ops);
    QFETCH(QString, description);
    QFETCH(int, expectedOps);

    PhotoJob job(photoId, PhotoJob::Operations(ops));
    QVERIFY(!job.isValid());

    if (!description.isEmpty()) {
        job.setDescription(description);
    }
    QCOMPARE(job.photoId(), PhotoId(photoId));
    QCOMPARE(job.operations(), PhotoJob::Operations(expectedOps));
    QCOMPARE(job.description(), description);
}

void JobDatabaseTest::testAddPhoto_data()
{
    QTest::addColumn<PhotoId>("photoId");
    QTest::addColumn<int>("ops");
    QTest::addColumn<QString>("description");
    QTest::addColumn<int>("expectedOps");

    QTest::newRow("empty") <<
        0 << 0 << QString() << 0;

    QTest::newRow("simple") <<
        4 <<
        int(PhotoJob::WriteLocation) <<
        QString() <<
        int(PhotoJob::WriteLocation);

    QTest::newRow("description") <<
        5 <<
        int(PhotoJob::WriteTags) <<
        "Nice photo" <<
        int(PhotoJob::WriteTags | PhotoJob::WriteDescription);

    QTest::newRow("adding description") <<
        2 <<
        int(PhotoJob::None) <<
        "Hello crazy world" <<
        int(PhotoJob::WriteDescription | PhotoJob::WriteTags | PhotoJob::WriteLocation);
}

void JobDatabaseTest::testAddPhoto()
{
    QTemporaryDir tmpDir;
    qputenv("XDG_CONFIG_HOME", tmpDir.path().toUtf8());

    QFETCH(PhotoId, photoId);
    QFETCH(int, ops);
    QFETCH(QString, description);
    QFETCH(int, expectedOps);

    PhotoJob job(photoId, PhotoJob::Operations(ops));
    if (!description.isEmpty()) {
        job.setDescription(description);
    }

    QScopedPointer<JobDatabase> db(new JobDatabase);
    QVERIFY(db);

    // Populate the DB with some jobs
    bool ok = db->addJob(PhotoJob(2, PhotoJob::WriteTags | PhotoJob::WriteLocation));
    QVERIFY(ok);

    PhotoJob pj0(1, PhotoJob::None);
    pj0.setDescription("Hello World");
    ok = db->addJob(pj0);
    QVERIFY(ok);

    ok = db->addJob(PhotoJob(3, PhotoJob::WriteRating));
    QVERIFY(ok);

    // Now add our job
    ok = db->addJob(job);
    QVERIFY(ok);

    // Find the job
    Job j;
    while (db->next(j)) {
        if (j.inProgress() || j.type() != Job::Photo) continue;
        if (static_cast<const PhotoJob&>(j).photoId() == photoId) {
            break;
        }
    }
    const PhotoJob &pj = static_cast<const PhotoJob&>(j);
    QVERIFY(pj.isValid());
    QCOMPARE(pj.photoId(), photoId);
    QCOMPARE(pj.operations(), expectedOps);
    QCOMPARE(pj.description(), description);
}

void JobDatabaseTest::testMarkInProgress_data()
{
    QTest::addColumn<QSet<int> >("jobIds");

    QTest::newRow("one") <<
        (QSet<int>() << 1);

    QTest::newRow("many") <<
        (QSet<int>() << 1 << 3);
}

void JobDatabaseTest::testMarkInProgress()
{
    QFETCH(QSet<int>, jobIds);

    QTemporaryDir tmpDir;
    qputenv("XDG_CONFIG_HOME", tmpDir.path().toUtf8());

    QScopedPointer<JobDatabase> db(new JobDatabase);
    QVERIFY(db);

    // Populate the DB with some jobs
    bool ok = db->addJob(PhotoJob(2, PhotoJob::WriteTags | PhotoJob::WriteLocation));
    QVERIFY(ok);

    PhotoJob pj0(1, PhotoJob::None);
    pj0.setDescription("Hello World");
    ok = db->addJob(pj0);
    QVERIFY(ok);

    ok = db->addJob(PhotoJob(3, PhotoJob::WriteRating));
    QVERIFY(ok);

    // Now mark the jobs as in progress
    Q_FOREACH(int jobId, jobIds) {
        ok = db->markJobInProgress(jobId);
        QVERIFY(ok);
    }

    // Find the remaining jobs
    Job j;
    QSet<int> jobsInProgress;
    for (int jobId = 0; db->next(j) && j.id() > jobId; jobId = j.id()) {
        if (j.inProgress()) {
            jobsInProgress.insert(j.id());
        }
    }

    QCOMPARE(jobsInProgress, jobIds);
}

void JobDatabaseTest::testRemoveJob_data()
{
    QTest::addColumn<QSet<int> >("jobIds");
    QTest::addColumn<QSet<int> >("expectedRemainingJobs");

    QTest::newRow("one") <<
        (QSet<int>() << 1) <<
        (QSet<int>() << 2 << 3);

    QTest::newRow("many") <<
        (QSet<int>() << 1 << 3) <<
        (QSet<int>() << 2);
}

void JobDatabaseTest::testRemoveJob()
{
    QFETCH(QSet<int>, jobIds);
    QFETCH(QSet<int>, expectedRemainingJobs);

    QTemporaryDir tmpDir;
    qputenv("XDG_CONFIG_HOME", tmpDir.path().toUtf8());

    QScopedPointer<JobDatabase> db(new JobDatabase);
    QVERIFY(db);

    // Populate the DB with some jobs
    bool ok = db->addJob(PhotoJob(2, PhotoJob::WriteTags | PhotoJob::WriteLocation));
    QVERIFY(ok);

    PhotoJob pj0(1, PhotoJob::None);
    pj0.setDescription("Hello World");
    ok = db->addJob(pj0);
    QVERIFY(ok);

    ok = db->addJob(PhotoJob(3, PhotoJob::WriteRating));
    QVERIFY(ok);

    // Now delete our jobs
    Q_FOREACH(int jobId, jobIds) {
        ok = db->removeJob(jobId);
        QVERIFY(ok);
    }

    // Find the remaining jobs
    Job j;
    QSet<int> remainingJobs;
    for (int jobId = 0; db->next(j) && j.id() > jobId; jobId = j.id()) {
        remainingJobs.insert(j.id());
    }

    QCOMPARE(remainingJobs, expectedRemainingJobs);
}

void JobDatabaseTest::testUpdate_data()
{
    QTest::addColumn<QString>("dumpFile");

    QTest::newRow("no DB") <<
        QString();

    QTest::newRow("Version 1") <<
        "job_version1.sql";
}

void JobDatabaseTest::testUpdate()
{
    QTemporaryDir tmpDir;
    qputenv("XDG_CONFIG_HOME", tmpDir.path().toUtf8());

    QFETCH(QString, dumpFile);

    if (!dumpFile.isEmpty()) {
        /* Directly open/create the SQL DB file */
        QString dirName =
            QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) +
            "/" + QCoreApplication::applicationName();
        QDir dir(dirName);
        dir.mkpath(".");

        QStringList args;
        args << dir.absoluteFilePath("jobs.db");
        QProcess sqlite3;
        sqlite3.setStandardInputFile(QString(DATA_DIR) + "/" + dumpFile);
        sqlite3.start("sqlite3", args);
        QVERIFY(sqlite3.waitForStarted());
        QVERIFY(sqlite3.waitForFinished());
    }

    QScopedPointer<JobDatabase> db(new JobDatabase);
    QVERIFY(db);

    /* Just do a couple of writes and reads to make sure that the DB is not
     * totally broken. */
    bool ok = db->addJob(PhotoJob(2, PhotoJob::WriteTags | PhotoJob::WriteLocation));
    QVERIFY(ok);

    PhotoJob job(3, PhotoJob::Operations(PhotoJob::WriteRating));
    job.setDescription("Hello world");
    ok = db->addJob(job);
    QVERIFY(ok);

    // Find the job
    Job j;
    while (db->next(j)) {
        if (j.inProgress() || j.type() != Job::Photo) continue;
        if (static_cast<const PhotoJob&>(j).photoId() == 3) {
            break;
        }
    }
    const PhotoJob &pj = static_cast<const PhotoJob&>(j);
    QVERIFY(pj.isValid());
    QCOMPARE(pj.photoId(), 3);
    QCOMPARE(pj.operations(),
             PhotoJob::WriteRating | PhotoJob::WriteDescription);
    QCOMPARE(pj.description(), QString("Hello world"));
}

void JobDatabaseTest::testMultithread()
{
    QTemporaryDir tmpDir;
    qputenv("XDG_CONFIG_HOME", tmpDir.path().toUtf8());

    QSharedPointer<JobDatabase> db(new JobDatabase);
    QVERIFY(db);

    /* Setting to a lower number otherwise the test takes ages; but
     * it's actually with higher numbers (500) that problems can be
     * detected. */
    const int numJobs = 20;

    QtConcurrent::run([db, numJobs]() {
        int jobsDone = 0;
        Job job;
        while (jobsDone < numJobs) {
            while (!db->next(job)) {
                QTest::qWait(5);
            }
            db->markJobInProgress(job.id());
            QCOMPARE(job.type(), Job::Photo);
            db->removeJob(job.id());

            jobsDone++;
        }
    });

    for (int i = 0; i < numJobs; i++) {
        bool ok = db->addJob(PhotoJob(i + 1, PhotoJob::WriteTags));
        QVERIFY(ok);
    }
    QThreadPool::globalInstance()->waitForDone();

    Job nonexistentJob;
    QVERIFY(!db->next(nonexistentJob));
}

QTEST_GUILESS_MAIN(JobDatabaseTest)

#include "tst_job_database.moc"
