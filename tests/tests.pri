include(../common-config.pri)

CONFIG += \
    c++11 \
    no_keywords

QT += \
    concurrent \
    positioning \
    testlib

LIBS += -L. -lfake_objects
PRE_TARGETDEPS += libfake_objects.a

OBJECTS_DIR = $${TARGET}.obj
SRC_DIR = $${TOP_SRC_DIR}/src
DATA_DIR = $${TOP_SRC_DIR}/data
TEST_DATA_DIR = $${TOP_SRC_DIR}/tests/data

INCLUDEPATH += $${SRC_DIR}

check.commands = ./$${TARGET}
check.depends = $${TARGET}
QMAKE_EXTRA_TARGETS += check
