TARGET = tst_photo_model

include(tests.pri)

QT += \
    qml \
    sql

SOURCES += \
    $${SRC_DIR}/photo_model.cpp \
    $${SRC_DIR}/utils.cpp \
    tst_photo_model.cpp

HEADERS += \
    $${SRC_DIR}/abstract_database.h \
    $${SRC_DIR}/database.h \
    $${SRC_DIR}/metadata.h \
    $${SRC_DIR}/photo_model.h \
    $${SRC_DIR}/utils.h
