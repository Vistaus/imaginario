/*
 * Copyright (C) 2016-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "database.h"
#include "shotwell_importer.h"
#include "test_utils.h"

#include <QCoreApplication>
#include <QDateTime>
#include <QProcess>
#include <QScopedPointer>
#include <QSignalSpy>
#include <QStandardPaths>
#include <QTemporaryDir>
#include <QTest>

using namespace Imaginario;

class ShotwellImporterTest: public QObject
{
    Q_OBJECT

public:
    ShotwellImporterTest() {
        qRegisterMetaType<PhotoId>("PhotoId");
        qRegisterMetaType<Tag>("Tag");
    }

    void setupSource(const QString &fileName);

private Q_SLOTS:
    void testEmpty();
    void testComplete();
};

void ShotwellImporterTest::setupSource(const QString &fileName)
{
    QString dirName =
        QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation) +
        "/shotwell/data";
    QDir dir(dirName);
    dir.mkpath(".");

    QStringList args;
    args << dir.absoluteFilePath("photo.db");
    QProcess sqlite3;
    sqlite3.setStandardInputFile(QString(DATA_DIR) + "/shotwell_import/" +
                                 fileName);
    sqlite3.start("sqlite3", args);
    QVERIFY(sqlite3.waitForStarted());
    QVERIFY(sqlite3.waitForFinished());
}

void ShotwellImporterTest::testEmpty()
{
    QTemporaryDir tmpDir;
    qputenv("XDG_CACHE_HOME", tmpDir.path().toUtf8());
    qputenv("XDG_CONFIG_HOME", tmpDir.path().toUtf8());
    qputenv("XDG_DATA_HOME", tmpDir.path().toUtf8());

    setupSource("empty.sql");

    QScopedPointer<Database> db(Database::instance());
    QVERIFY(db);

    ShotwellImporter importer;
    QSignalSpy statusChanged(&importer,
                             SIGNAL(statusChanged(ShotwellImporter::Status)));

    QTRY_COMPARE(importer.status(), ShotwellImporter::Ready);
    QCOMPARE(importer.dbVersion(), 20);
    QCOMPARE(importer.version(), QString("0.18.0"));
    QCOMPARE(importer.count(), 0);

    statusChanged.clear();
    importer.exec();
    QTRY_VERIFY(statusChanged.count() > 0);
    QCOMPARE(statusChanged.at(0).at(0).toInt(),
             int(ShotwellImporter::ImportingTags));

    QTRY_VERIFY(statusChanged.count() > 1);
    QCOMPARE(statusChanged.at(1).at(0).toInt(),
             int(ShotwellImporter::ImportingPhotos));

    QTRY_VERIFY(statusChanged.count() > 2);
    QCOMPARE(statusChanged.at(2).at(0).toInt(),
             int(ShotwellImporter::Done));
}

void ShotwellImporterTest::testComplete()
{
    QTemporaryDir tmpDir;
    qputenv("XDG_CACHE_HOME", tmpDir.path().toUtf8());
    qputenv("XDG_CONFIG_HOME", tmpDir.path().toUtf8());
    qputenv("XDG_DATA_HOME", tmpDir.path().toUtf8());

    setupSource("1.sql");

    QScopedPointer<Database> db(Database::instance());
    QVERIFY(db);

    ShotwellImporter importer;
    QSignalSpy statusChanged(&importer,
                             SIGNAL(statusChanged(ShotwellImporter::Status)));
    QSignalSpy progressChanged(&importer, SIGNAL(progressChanged()));

    QTRY_COMPARE(importer.status(), ShotwellImporter::Ready);
    QCOMPARE(importer.dbVersion(), 20);
    QCOMPARE(importer.version(), QString("0.18.0"));
    QCOMPARE(importer.count(), 12);

    statusChanged.clear();
    importer.exec();
    QTRY_VERIFY(statusChanged.count() > 0);
    QCOMPARE(statusChanged.at(0).at(0).toInt(),
             int(ShotwellImporter::ImportingTags));

    QTRY_VERIFY(statusChanged.count() > 1);
    QCOMPARE(statusChanged.at(1).at(0).toInt(),
             int(ShotwellImporter::ImportingPhotos));
    QTRY_VERIFY(progressChanged.count() >= 1);

    QTRY_VERIFY(statusChanged.count() > 2);
    QCOMPARE(statusChanged.at(2).at(0).toInt(),
             int(ShotwellImporter::Done));

    /* Check tags */

    struct TagData {
        QString name;
        QString parent;
        bool isCategory;
    };
    QVector<TagData> tags {
        { "macro", "", false },
        { "2015-08-23 Macro", "", false },
        { "nature", "", true },
        { "field", "nature", false },
        { "flowers", "nature", false },
    };
    for (const TagData &td: tags) {
        Tag tag = db->tag(td.name);
        QVERIFY(tag.isValid());
        QString parentName =
            tag.parent().isValid() ? tag.parent().name() : "";
        QCOMPARE(parentName, td.parent);
        QCOMPARE(tag.isCategory(), td.isCategory);
    }

    /* Check rolls */

    QList<Roll> rolls = db->rolls(10);
    QCOMPARE(rolls.count(), 1);
    Roll roll = rolls[0];
    QVERIFY(roll.isValid());
    QCOMPARE(roll.time(), QDateTime::fromTime_t(1461878187));

    /* Check photos */

    SearchFilters filters;
    QList<Database::Photo> photos = db->findPhotos(filters);
    QCOMPARE(photos.count(), 12);

    QHash<QString,QSet<QString>> expectedTags {
        { "DSC00448 (1).jpg", {}},
        { "DSC00448 (1) (Modified).jpg", {}},
        { "DSC00448 (1) (Modified (2)).jpg", {}},
        { "DSC00461 (1).jpg", {}},
        { "DSC04967.jpg", { "2015-08-23 Macro","macro","nature","flowers" }},
        { "DSC04981.jpg", { "2015-08-23 Macro","sunset","nature","field" }},
        { "DSC06903 (1).jpg", {}},
        { "DSC02582.jpg", {}},
        { "Screenshot from 2012-12-28 14:14:48.png", {}},
        { "Screenshot from 2014-12-27 21:30:34.png", {}},
        { "Screenshot from 2014-02-07 18:22:26.png", {}},
        { "image20160419_193456352.jpg", {}},
    };
    QHash<QString,QSet<QString>> photoTags;
    Q_FOREACH(const auto &p, photos) {
        photoTags.insert(p.fileName(), Tag::tagNames(db->tags(p.id())).toSet());
    }
    QCOMPARE(photoTags, expectedTags);
}

QTEST_GUILESS_MAIN(ShotwellImporterTest)

#include "tst_shotwell_importer.moc"
