/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "face_detector.h"
#include "test_utils.h"

#include <QTest>

using namespace Imaginario;

static uint qHash(const QRect & r)
{
    return qHash(r.left() + r.top());
}

class FaceDetectorTest: public QObject
{
    Q_OBJECT

public:
    FaceDetectorTest() {}

private Q_SLOTS:
    void testValid();
    void testDetection_data();
    void testDetection();
};

void FaceDetectorTest::testValid()
{
    FaceDetector detector(DATA_DIR "/");

    QVERIFY(detector.isValid());
}

void FaceDetectorTest::testDetection_data()
{
    QTest::addColumn<QString>("fileName");
    QTest::addColumn<QList<QRect> >("expectedFaces");

    QTest::newRow("missing file") <<
        "" <<
        QList<QRect>();

    QTest::newRow("many faces") <<
        "faces.jpg" <<
        (QList<QRect>() <<
         QRect(58, 60, 14, 19) <<
         QRect(71, 29, 14, 19) <<
         QRect(21, 9, 13, 18) <<
         QRect(0, 16, 15, 21) <<
         QRect(26, 63, 20, 27) <<
         QRect(34, 26, 19, 25)
         );

    QTest::newRow("one face, big file") <<
        "face.jpg" <<
        (QList<QRect>() <<
         QRect(17, 21, 36, 58)
         );
}

void FaceDetectorTest::testDetection()
{
    QFETCH(QString, fileName);
    QFETCH(QList<QRect>, expectedFaces);

    FaceDetector detector(DATA_DIR "/");
    QVERIFY(detector.isValid());

    QUrl url = QUrl::fromLocalFile(QString(ITEMS_DIR) + "/" + fileName);
    QList<QRectF> faces = detector.checkFile(url);
    /* Convert the coordinates into integers in the 0..100 range */
    QList<QRect> rounded;
    Q_FOREACH(const QRectF &r, faces) {
        rounded.append(QRect(r.x() * 100, r.y() * 100,
                             r.width() * 100, r.height() * 100));
    }
    QCOMPARE(rounded.toSet(), expectedFaces.toSet());
}

QTEST_GUILESS_MAIN(FaceDetectorTest)

#include "tst_face_detector.moc"
