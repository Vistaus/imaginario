#! /bin/bash

BUILDDIR="$(pwd)/opencv-build"

mkdir "$BUILDDIR"

VERSION="2.4.13.6"
OPENCV_ARCHIVE=opencv.tar.gz
SRCDIR="$(pwd)/opencv-$VERSION"

if [ ! -d $SRCDIR ]; then
    wget -c -O ${OPENCV_ARCHIVE} "https://github.com/opencv/opencv/archive/${VERSION}.tar.gz"
    tar xzf ${OPENCV_ARCHIVE}
fi

cd "$BUILDDIR"
cmake \
    -DCMAKE_INSTALL_PREFIX=install \
    -DBUILD_DOCS=OFF \
    -DBUILD_JPEG=OFF \
    -DBUILD_PNG=OFF \
    -DBUILD_PERF_TESTS=OFF \
    -DBUILD_TESTS=OFF \
    -DWITH_1394=OFF \
    -DWITH_CUDA=OFF \
    -DWITH_CUFFT=OFF \
    -DWITH_FFMPEG=OFF \
    -DWITH_GIGEAPI=OFF \
    -DWITH_GSTREAMER=OFF \
    -DWITH_GTK=OFF \
    -DWITH_JASPER=OFF \
    -DWITH_LIBV4L=OFF \
    -DWITH_OPENCL=OFF \
    -DWITH_OPENCLAMDBLAS=OFF \
    -DWITH_OPENCLAMDFFT=OFF \
    -DWITH_OPENEXR=OFF \
    -DWITH_PVAPI=OFF \
    -DWITH_V4L=OFF \
    $SRCDIR
make -j8
make install
